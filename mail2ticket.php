<?php
    include 'bvdg-gate/BVDGGate.php';
    $datei = fopen("logs\\protokoll_".date("Y-m-d-H-i-s").".txt","w");
    
	// credentials
	$server = "sslin.df.eu";
	$port = 993;
	$user = "bdvg@ecom-projects.de";
	$pass = "hwY4j.mfEwy1";
	
	// open mail server connection
	$mailbox = imap_open("{".$server.":".$port."/imap/ssl}INBOX", $user, $pass);
	if(!$mailbox) die("IMAP connection error: ".imap_last_error());
	
	// get all unread e-mails and create an array containing all relevant information
	$unreadMails = imap_search($mailbox, "UNSEEN");
	
	if(!is_array($unreadMails)){
		die("No e-mails found.");
	} else {
		$ticketItems = array();	
	
		foreach($unreadMails as $unreadMail) {
			$mailHeaderObj = imap_headerinfo($mailbox, $unreadMail);
			$structure = imap_fetchstructure($mailbox, $unreadMail);
			$part = $structure->parts[1];
			
			$body = imap_fetchbody($mailbox, $unreadMail, 1.2);
			if(!strlen($body) > 0){
		    	$body = imap_fetchbody($mailbox, $unreadMail, 1);
		    }
		    
		    switch($part->encoding){
			    # 7BIT
			    case 0:
			        $mailBody = $body;
			        break;
			    # 8BIT
			    case 1:
			        $mailBody = quoted_printable_decode(imap_8bit($body));
			        break;
			    # BINARY
			    case 2:
			        $mailBody = imap_binary($body);
			        break;
			    # BASE64
			    case 3:
			        $mailBody = imap_base64($body);
			        break;
			    # QUOTED-PRINTABLE
			    case 4:
			        $mailBody = mb_convert_encoding(quoted_printable_decode($body), 'UTF-8', 'ISO-8859-1');
			        break;
			    # OTHER
			    case 5:
			        $mailBody = $body;
			        break;
			    # UNKNOWN
			    default:
			        $mailBody = $body;
			        break;
			}
		    
			$ticketItems[] = array(
				"id" => $mailHeaderObj->message_id,
				"sender_name" => $mailHeaderObj->from[0]->personal,
				"sender_email" => strtolower($mailHeaderObj->from[0]->mailbox)."@".$mailHeaderObj->from[0]->host,
				"timestamp" => $mailHeaderObj->udate,
				"subject" => $mailHeaderObj->subject,
				"content" => $mailBody
			);
		}
		
		// debug
		echo "<pre>";
		print_r($ticketItems);
		echo "</pre>";
		
		
		//@todo:
		foreach($ticketItems as $ticket){
		    
 
            //Vorgang
		    $param = array('GWSTYPE' => array('value' => 'Ticket', 'fieldType' => 'STRING'),
		    		       'GWSTICKETORIGIN' => array('value' => 'E-Mail', 'fieldType' => 'STRING'),
		                   'KEYWORD' => array('value' => $ticket['subject'], 'fieldType' => 'STRING'),
// 		                   'NOTES2' => array('value' => $ticket['content'], 'fieldType' => 'STRING'),
// 		     		       'GWSTICKETTYPE' => array('value' => 'Fehler' /*$_POST['Art']*/, 'fieldType' => 'STRING'),
// 		     		       'GWSPRIORITY' => array('value' => $_POST['Prioritaet'], 'fieldType' => 'STRING'),
		    		       'ITDPROBLEM' => array('value' => $ticket['content'], 'fieldType' => 'STRING'),
		    		       'GWSSTATUS' => array('value' => 'Offen', 'fieldType' => 'STRING'),
		                   'GWTICKETCOMMUNICATION' => array('value' =>  $ticket['sender_email'], 'fieldType' => 'STRING'),
// 		     		       'TicketGroupname' => array('value' => 'IT'/*$_POST['Team']*/, 'fieldType' => 'STRING'),
// 		     		       'Ticketusername' => array('value' => $_POST['Mitarbeiter'], 'fieldType' => 'STRING'),
// 		                   'Level1Groupname' => array('value' => $_POST['UebergabeTeam'], 'fieldType' => 'STRING'),
// 		     			   'Level1User' => array('value' => $_POST['UebergabeMitarbeiter'], 'fieldType' => 'STRING')
		    );
		    $newticket = HochwarthIT_BVDGGate::getTaskDao()->createTask($param);
		    echo "Vorgang angelegt ";
		    fwrite($datei,  $ticket['subject']." von ".$ticket['sender_email']." [".$ticket['sender_name']."] angelegt ");
		    
		    
		    $option['conditions'] = array(		        
		        'OR' => array(		            
		            'MAILFIELDSTR1' => $ticket['sender_email'],
		            'MAILFIELDSTR2' => $ticket['sender_email'],
		            'MAILFIELDSTR3' => $ticket['sender_email'],
		            'MAILFIELDSTR4' => $ticket['sender_email'],
		            'MAILFIELDSTR5' => $ticket['sender_email']
    	        )
		    );		    
		    $address = HochwarthIT_BVDGGate::getAdressDao()->find('first', $option);
		    if (!empty($address)) {
		          HochwarthIT_BVDGGate::getTaskDao()->createLinkBetweenTaskAddress($newticket->getValue('GGUID'), $address['GGUID']['value'], 'L2UTicketMelder');
		          HochwarthIT_BVDGGate::getTaskDao()->createLinkBetweenTaskAddress($newticket->getValue('GGUID'), $address['GGUID']['value'], 'L2UTicketPart');
		          HochwarthIT_BVDGGate::getTaskDao()->createLinkBetweenTaskAddress($newticket->getValue('GGUID'), $address['GGUID']['value'], 'ITDTSKADR');
		          echo "...verknüpft!<br>";
		          fwrite($datei,  "und verknüpft!/r/n");
		    }  
		}
	}
	
	// close mail server connection
	imap_close($mailbox);
	fwrite($datei,  "Erfolgreich beendet!\r\n");
	fclose($datei);