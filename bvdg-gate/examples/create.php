<?php

#Including bvdg-gate
require __DIR__ . "/../BVDGGate.php";

$appointment = HochwarthIT_BVDGGate::getEIMInterface()->createObject('APPOINTMENT');
$appointment->setValue("Test Appointment", "KEYWORD", "STRING");
$appointment->setValue(FALSE, "ISPARTOFEVENT", "BOOLEAN");
$appointment = HochwarthIT_BVDGGate::getEIMInterface()->saveAndReturnObject($appointment);