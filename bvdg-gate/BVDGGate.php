<?php
require_once dirname(__FILE__)."/SecurityTokenProvider.php";
require_once dirname(__FILE__)."/Dao/EventDao.php";
require_once dirname(__FILE__)."/Dao/AddressDao.php";
require_once dirname(__FILE__)."/Dao/AppointmentDao.php";
require_once dirname(__FILE__)."/Dao/RegistrationDao.php";
require_once dirname(__FILE__)."/Dao/BillDao.php";
require_once dirname(__FILE__)."/Dao/StartrechtDao.php";
require_once dirname(__FILE__)."/Dao/StatistikDao.php";
require_once dirname(__FILE__)."/Dao/DocumentDao.php";
require_once dirname(__FILE__)."/Dao/TodoDao.php";
require_once dirname(__FILE__)."/Dao/TaskDao.php";
require_once dirname(__FILE__)."/Dao/WettkampfDao.php";
require_once dirname(__FILE__)."/Dao/TeilnehmerDao.php";
require_once dirname(__FILE__)."/Dao/MeisterschaftDao.php";
require_once dirname(__FILE__) . "/Dao/TeilnehmerMSDao.php";
require_once dirname(__FILE__)."/BVDGGate/InputAssistance/Address/Type.php";

class HochwarthIT_BVDGGate{

    static function getEIMInterface(){
        return SecurityTokenProvider::getInstance()->getEIMInterface();
    }

    static function getAvailableObjects(){
        return self::getEIMInterface()->getAvailableObjectNames();
    }

    static function getAdressDao(){
        return AddressDao::getInstance();
    }

    static function getEventDao(){
        return EventDao::getInstance();
    }

    static function getAppointmentDao(){
        return AppointmentDao::getInstance();
    }

    static function getRegistrationDao(){
        return RegistrationDao::getInstance();
    }

    static function getBillDao(){
        return BillDao::getInstance();
    }
    
    static function getStartrechtDao(){
    	return StartrechtDao::getInstance();
    }
    static function getStatistikDao(){
    	return StatistikDao::getInstance();
    }
    static function getDocumentDao(){
    	return DocumentDao::getInstance();
    }
    static function getTodoDao(){
    	return TodoDao::getInstance();
    }
    static function getTaskDao(){
        return TaskDao::getInstance();
    }
    static function getWettkampfDao(){
    	return WettkampfDao::getInstance();
    }
    static function getTeilnehmerDao(){
    	return TeilnehmerDao::getInstance();
    }
    static function getMeisterschaftDao(){
    	return MeisterschaftDao::getInstance();
    }
    static function getTeilnehmerMSDao(){
    	return TeilnehmerMSDao::getInstance();
    }
}