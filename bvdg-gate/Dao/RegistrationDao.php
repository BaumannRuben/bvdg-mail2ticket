<?php
require_once "AbstractDao.php";


class RegistrationDao extends AbstractDao
{
    protected $objectType = "REGISTRATION";
    static private $instance = null;

    /* @return AddressDao */
    static function getInstance()
    {
        if (null === self::$instance) {
            $class = get_called_class();
            self::$instance = new $class;
        }
        return self::$instance;
    }
}