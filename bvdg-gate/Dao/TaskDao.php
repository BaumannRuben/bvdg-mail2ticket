<?php
require_once "AbstractDao.php";

use \de\cas\open\server\api\business\GetLinksRequest;
use \de\cas\open\server\api\business\SaveLinkRequest;
use \de\cas\open\server\api\business\DeleteLinkRequest;
use \de\cas\open\server\api\types\LinkObject;

//Upload Kontaktbild
use de\cas\open\server\addresses\types\SaveImageForContactRequest;
use de\cas\open\server\api\business\GetJournalRequest;
use de\cas\open\server\addresses\types\GetImageForContactRequest;

class TaskDao extends AbstractDao
{

    protected $objectType = "TASK";
    static private $instance = null;

    /* @return TaskDao */
    static function getInstance()
    {
        if (null === self::$instance) {
            $class = get_called_class();
            self::$instance = new $class;
        }
        return self::$instance;
    }

    /*
     *  Erstellt einen neuen Vorgang
     */
    /* @return \de\cas\open\server\api\types\DataObject */
    public function createTask($taskdata)
    {
    	$taskObj = $this->createObject();
        foreach ($taskdata as $field => $row) {
            $taskObj->setValue($row['value'], $field, $row['fieldType']);
        }
        $taskObj->setPermissionsDirty(1);

        return $this->save($taskObj);
    }
    
    public function createLinkBetweenTaskAddress($gguid1, $gguid2, $attribute)
    {
    	$link = new LinkObject();
    	$link->objectType1 = "TASK";
    	$link->GGUID1 = $gguid1;
    	$link->objectType2 = "ADDRESS";
    	$link->GGUID2 = $gguid2;
    	$link->attribute = new StdClass();
    	$link->attribute->key = $attribute;
    	$link->isHierarchy = true;
    
    	$linkRequest = new SaveLinkRequest();
    	$linkRequest->links = array($link);
    
    	$linkResponse = HochwarthIT_BVDGGate::getEIMInterface()->execute($linkRequest);
    
    	return $linkResponse;
    }
}
