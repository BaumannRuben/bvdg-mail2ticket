<?php

require_once "AbstractDao.php";

use \de\cas\open\server\events\types\AutoCalculateAccountingPricesRequest;
use \de\cas\open\server\events\types\AutoCalculateAccountingPricesResponse;
use \de\cas\open\server\events\types\AddRegistrationRequest;
use \de\cas\open\server\events\types\AddRegistrationResponse;


class EventDao extends AbstractDao
{

    protected $objectType = "EVENT";
    static private $instance = null;

    /* @return EventDao */
    static function getInstance()
    {
        if (null === self::$instance) {
            $class = get_called_class();
            self::$instance = new $class;
        }
        return self::$instance;
    }

    /**
     * Get all Registrations for a specific Event
     *
     * @param $ev_gguid String Event GGUID
     * @return array
     */
    public function getRegistrations($ev_gguid){
        $sql = "SELECT GGUID, REG_EVENTGUID, REG_KEYWORD, REG_ADDGUID, REG_ADDSHORTINFO, REG_REGSHORTINFO, REG_COMPANION, REG_PAYSHORTINFO, REG_TYPE"
            . " FROM REGISTRATION AS R"
            . " WHERE R.IsLinkedToWhere(EVENT: WHERE EVENT.GGUID = 0x$ev_gguid)";
        $sql = $this->addTeamfilterToSql($sql, $this->objectType);
        $result = SecurityTokenProvider::getInstance()->getEIMInterface()->query($sql);
        return $this->convertResultObject($result);

    }

    public function registerAddress($ev_gguid, $add_gguid, $reg_gguid = null, $num_companions = 0, $reg_state = null, $reg_code = null, $app_gguids = array()){

        if(empty($app_gguids)){
            $app_gguids = array($ev_gguid);
        }

        if(empty($reg_gguid)){
            $reg_gguid = $add_gguid;
        }

        $addRegRequest = new AddRegistrationRequest();
        $addRegRequest->eventGUID = $ev_gguid;
        $addRegRequest->participantGUID = $add_gguid;
        $addRegRequest->appointmentGUIDs = $app_gguids;
        $addRegRequest->numCompanions = $num_companions;
        $addRegRequest->registererGUID = $reg_gguid;

        if(!$reg_state){
            $reg_state = 'offen';
        }
        $addRegRequest->registrationstate = $reg_state;


        $additional = HochwarthIT_BVDGGate::getEIMInterface()->createObject('REGISTRATION');
        $additional->setValue(TRUE, 'REG_ISPUBLIC', 'BOOLEAN');
        $writeCodeToAppointment = false;
        if($reg_code){
            if($app_gguids[0] == $ev_gguid){
                //Code in die Veranstaltungsanmeldung schreiben
                $additional->setValue($reg_code, "REG_BESTAETIGUNGSNUMMER", "STRING");
            }else{
                $writeCodeToAppointment = true;
            }
        }

        $addRegRequest->AdditionalFieldsAndValues = $additional->toDataObjectTransferable();
        $addRegResponse = HochwarthIT_BVDGGate::getEIMInterface()->execute($addRegRequest);

        if($writeCodeToAppointment){
            //Code in die Terminanmeldung schreiben
            foreach($app_gguids as $appGguid){
                $terminReg = HochwarthIT_BVDGGate::getAppointmentDao()->getRegistrations($appGguid, $add_gguid);
                if(isset($terminReg[0])){
                    $terminReg = $terminReg[0];
                    $terminReg = HochwarthIT_BVDGGate::getRegistrationDao()->load($terminReg['GGUID']['value']);
                    $terminReg->setValue($reg_code, "REG_BESTAETIGUNGSNUMMER", "STRING");
                    HochwarthIT_BVDGGate::getRegistrationDao()->save($terminReg);
                }
            }
        }

        /* @var $addRegResponse AddRegistrationResponse */
        $registrationGuid = $addRegResponse->result;
        return $registrationGuid;
    }

    /**
     * Get Accounting of Event
     *
     * @param $ev_gguid String
     * @param $pc_gguid String (optional)
     * @return mixed
     */
    public function getAccounting($ev_gguid, $pc_gguid = "")
    {
        $autoCalcRequest = new AutoCalculateAccountingPricesRequest();
        $autoCalcRequest->eventGuid = $ev_gguid;
        if (!empty($pc_gguid)) {
            $autoCalcRequest->participantGuid = $pc_gguid;
        } else {
            $autoCalcRequest->participantGuid = NULL;
        }
        $autoCalcRequest->registrationDate = date('c');

        /* @var $autoCalcResponse AutoCalculateAccountingPricesResponse */
        $autoCalcResponse = SecurityTokenProvider::getInstance()->getEIMInterface()->execute($autoCalcRequest);
        $autoSelect = $autoCalcResponse->autoSelectedSuccess;
        $accounting = $autoCalcResponse->accounting;

        return $accounting;
    }

    /* @var $accountingPos \de\cas\open\server\events\types\AccountingPosition */
    public function getAccountingPos($accountingPos, $acc_gguid){

        if($accountingPos->gguid == $acc_gguid){
            return $accountingPos;
        }

        $found = false;

//        Mage::log($accountingPos->gguid, null, "observer.log");
        if($accountingPos->children){
            /* @var $accountingChild \de\cas\open\server\events\types\AccountingPosition */
            foreach($accountingPos->children as $accountingChild){
                $found = $this->getAccountingPos($accountingChild, $acc_gguid);
                if($found) {
                    return $found;
                }
            }
        }




        return false;

    }

    /* @var $accountingPos \de\cas\open\server\events\types\AccountingPosition */
    public function getAccountingPosPrice($accountingPos, $includeDiscount = true){
        $price = $accountingPos->price;

        /* @var $accountingChild \de\cas\open\server\events\types\AccountingPosition */
        //if($accountingPos->isGroup){
            if($accountingPos->children){
                foreach($accountingPos->children as $accountingChild){
                    switch($accountingChild->type) {
                        case "Preis":
                            $price += $accountingChild->price;
                            break;
                        case "Rabatt":
                            if($includeDiscount){
                                $price += $accountingChild->price;
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
        //}
        return $price;
    }

    /* @var $accountingPos \de\cas\open\server\events\types\AccountingPosition */
    public function getAccountingWithLowestPrice($accountingPos){

        $prices = array();
        $accountingArray = array();

        /* @var $accountingChild \de\cas\open\server\events\types\AccountingPosition */
        if($accountingPos->children){
            foreach($accountingPos->children as $accountingChild){
                $accountingArray[$accountingChild->gguid] = $accountingChild;
                if($accountingChild->type == "Preis") {
                    if($accountingChild->price === ""){
                        $accountingChild->price = 0;
                    }
                    $prices[$accountingChild->gguid] = $accountingChild->price;
                }
            }
        }

        if($accountingPos->price > 0){
            $prices[$accountingPos->gguid] = $accountingPos->price;
        }

        if(empty($prices)){
            $prices[$accountingPos->gguid] = 0;
            $accountingPos->price = 0;
        }

        //den parent auch noch dem gesamt Array hinzufügen
        $accountingArray[$accountingPos->gguid] = $accountingPos;

        $accountingArrayKeys = array_keys($prices, min($prices));

        $lowestGguid = reset($accountingArrayKeys);

        return $accountingArray[$lowestGguid];
    }

    /* @var $accountingPos \de\cas\open\server\events\types\AccountingPosition */
    public function accountingToHtml(\de\cas\open\server\events\types\AccountingPosition $accountingPos, $outputTypes=array("Preis", "Rabatt", "Storno", "Zusatzleistung"), de\cas\open\server\events\types\AccountingPosition $preSelect = null){
        if(!$accountingPos->children){
            return "";
        }
        $html = "<ul>";
        $parent = $accountingPos;

        /* @var $accountingChild \de\cas\open\server\events\types\AccountingPosition */
        foreach($accountingPos->children as $accountingChild){

            if(!in_array($accountingChild->type, $outputTypes)){
                continue; //Wenn Type nicht ausgegeben werden soll überspringen
            }


            $accountingChild->price = number_format($accountingChild->price, 2, ".", "");
            $accountingChild->priceLabel = number_format($accountingChild->price, 2, ",", "")." €";

            switch($accountingChild->type){
                case "Preis":
                    $priceWithDiscounts = $this->getAccountingPosPrice($accountingChild);
                    $accountingChild->priceLabel = number_format($accountingChild->price, 2, ",", "")." €";
                    $checked = ($preSelect !== null && $preSelect->gguid == $accountingChild->gguid) ? 'checked' : '';
                    $html .= "<li data-type='$accountingChild->type' data-price='$priceWithDiscounts'>";
                        $html .= "<label for='$accountingChild->gguid'>$accountingChild->keyword ({$accountingChild->priceLabel})</label>";
                        $html .= "<input id='$accountingChild->gguid' type='radio' name='event[accounting][price]' class='validate-one-required-by-name' value='$accountingChild->gguid' $checked>";
                        $html .= $this->accountingToHtml($accountingChild);
                    $html .= "</li>";
                    break;
                case "Rabatt":
                    //nur wenn Rabatt aktiv
                    if($accountingChild->price < 0){
                        $html .= "<li data-type='$accountingChild->type' data-price='$accountingChild->price'>";
                        $html .= "<label>{$accountingChild->keyword}: $accountingChild->priceLabel".($accountingChild->percent > 0 ? " (".$accountingChild->percent."%)" : '')."</label>";
                        $html .= "</li>";
                    }
                    break;
                case "Storno":
                    $html .= "<li data-type='$accountingChild->type' data-price='$accountingChild->price'>";
                    $html .= "<label>Storno: $accountingChild->keyword ($accountingChild->priceLabel)</label>";
                    $html .= $this->accountingToHtml($accountingChild);
                    $html .= "</li>";
                    break;
                case "Zusatzleistung":
                    $type = $accountingChild->type;
                    if($accountingPos->isGroup){
                        $type .= "-child";
                    }

                    $html .= "<li data-type='$type' data-price='$accountingChild->price'>";
                    if(!$accountingPos->isGroup){
                        $html .= "<input id='$accountingChild->gguid' type='number' name='event[accounting][additional][$accountingChild->gguid]' value='0' />";
                    }
                    $html .= "<label for='$accountingChild->gguid'>$accountingChild->keyword".($accountingChild->price > 0 ? " (".$accountingChild->priceLabel.")" : '')."</label>";
                    $html .= $this->accountingToHtml($accountingChild);
                    $html .= "</li>";
                    break;
            }

        }

        $html .= "</ul>";
        return $html;
    }

    public function accountingPricesToHtml(\de\cas\open\server\events\types\AccountingPosition $accountingPos){
        $html = "";
        $data = $this->accountingToHtml($accountingPos, array("Preis", "Rabatt", "Storno"), $this->getAccountingWithLowestPrice($accountingPos));
        if(strip_tags($data)){
            $html .= "<label class='accounting_prices_label'>Bitte Tarif auswählen:</label>";
            $html .= $data;
        }
        return $html;
    }

    public function accountingAdditionalServicesToHtml(\de\cas\open\server\events\types\AccountingPosition $accountingPos){
        $html = "";
        $data = $this->accountingToHtml($accountingPos, array("Zusatzleistung"));
        if(strip_tags($data)){
            $html .= "<label class='accounting_prices_label'>Bitte Zusatzleistungen auswählen:</label>";
            $html .= $this->accountingToHtml($accountingPos, array("Zusatzleistung"));
        }
        return $html;
    }

}


