﻿<?php

abstract class AbstractDao{

    protected $objectType = "";

    public function getObjectDescription()
    {
        return HochwarthIT_BVDGGate::getEIMInterface()->getObjectDescription($this->objectType)->getFieldMap();

    }

    public function setData($field, $value, $dataObject){
        $fieldType = $dataObject->getFieldType($field);
        if($fieldType === NULL){
            switch(gettype($value)){
                case "boolean":
                    $fieldType = "BOOLEAN";
                    break;
                case "integer":
                    $fieldType = "INT";
                    break;
                case "string":
                    $fieldType = "STRING";
                    break;
                case "double":
                    $fieldType = "DOUBLE";
                    break;
                default:
                    $fieldType = "STRING";
                    break;
            }
        }
        $dataObject->setValue($value, $field, $fieldType);
        return $dataObject;
    }

    public function save($dataObject){
        return SecurityTokenProvider::getInstance()->getEIMInterface()->saveAndReturnObject($dataObject);
    }

    public function createObject(){
        return SecurityTokenProvider::getInstance()->getEIMInterface()->createObject($this->objectType);
    }

    /* @return \de\cas\open\server\api\types\DataObject */
    public function load($gguid){

        /* @var $event \de\cas\open\server\api\types\DataObject */
        $object = "";
        if($gguid){
            try{
                $object = SecurityTokenProvider::getInstance()->getEIMInterface()->getObject($this->objectType, $gguid);
            }catch(\de\cas\open\server\api\exceptions\DataLayerException $e){
                //return empty
            }
        }
        return $object;
    }

    public function find($findType, $options = array()){
        $objectType = $this->objectType;
        $fields = "*";
        if(isset($options['fields'])){
            $fields = "";
            $numItems = count($options['fields']);
            $i = 0;
            foreach($options['fields'] as $field){
                $i++;
                if($i == $numItems){
                    $fields .= $field;
                }else{
                    $fields .= "$field, ";
                }
            }
        }
        $sql = "SELECT $fields FROM $objectType";
		
        $sql = self::addLinkJoinToSql($sql, $options);
        $sql = self::addWhereToSql($sql, $options);
        $sql = self::addTeamfilterToSql($sql, $objectType);
        $sql = self::addOrderByToSql($sql, $options);
        $sql = self::addLimitToSql($sql, $options);
        
        $result = SecurityTokenProvider::getInstance()->getEIMInterface()->query($sql);
        $displayField = "";
        if($findType == "list" && isset($options['displayField'])){
            $displayField = $options['displayField'];
        }
        return self::convertResultObject($result, $findType, $displayField);
    }



    public function convertResultObject($result, $type='', $displayField='', $guidAsKey=false){
        /** @var $result \de\cas\open\server\api\types\MassQueryResult */
        $rows = array();
        switch($type){
            case "list":
                if($result->rows){
                    foreach ($result->rows as $rowObj) {
                        $row = array();
                        $gguid = "";
                        foreach ($result->columnNames as $i => $fieldName) {

                            if($fieldName == "GGUID"){
                                $gguid = $rowObj->values[$i];
                            }

                        }

                        if(!$displayField){
                            $displayField = "GGUID";
                        }

                        foreach ($result->columnNames as $i => $fieldName) {

                            $value = $rowObj->values[$i];
                            if ($result->types[$i] === 'DATETIME') {
                                $datetime = new \DateTime($value);
                                $value = $datetime->format('Y-m-d H:i:s');
                            }

                            if ($displayField && $fieldName == $displayField) {
                                $rows[$gguid] = $value;
                                break;
                            }

                        }
                    }
                }

                break;
            default:
                /*
                echo "<pre>";
                print_r($result->rows);
                */
                if($result->rows){
                    foreach ($result->rows as $rowObj) {
                        $row = array();

                        foreach ($result->columnNames as $i => $fieldName) {
                            $value = $rowObj->values[$i];
                            if ($result->types[$i] === 'DATETIME' && $value) { //Anpassung  && $value
                                $datetime = new \DateTime($value);
                                $value = $datetime->format('Y-m-d H:i:s\Z');    //Anpassung \Z
                            }

                            $row[$fieldName]['value'] = $value;
                            $row[$fieldName]['displayName'] = $result->displayNames[$i];


                        }

                        $rows[$row['GGUID']['value']] = $row;


                    }
                    if(!$guidAsKey){
                        $rows = array_values($rows);
                    }
                }
                break;
        }

        if($type == "first" && isset($rows[0])){
            $rows = $rows[0];
        }

        return $rows;

    }

    public function isComparisonOperator($string){
        $operators = array(
            ">",
            "<",
            ">=",
            "<=",
            "=",
            "!=",
            "LIKE",
            "IN"
        );

        return in_array($string,$operators);

    }

    /* Einen String Wert für MS-SQL escapen, solange keine Prepared-Statementsverfügbar sind das einzige was uns übrig bleibt*/
    public function ms_escape_string($data) {
        if ( !isset($data) or empty($data) ) return '';
        if ( is_numeric($data) ) return $data;

        $non_displayables = array(
            '/%0[0-8bcef]/',            // url encoded 00-08, 11, 12, 14, 15
            '/%1[0-9a-f]/',             // url encoded 16-31
            '/[\x00-\x08]/',            // 00-08
            '/\x0b/',                   // 11
            '/\x0c/',                   // 12
            '/[\x0e-\x1f]/'             // 14-31
        );
        foreach ( $non_displayables as $regex )
            $data = preg_replace( $regex, '', $data );
        $data = str_replace("'", "''", $data );
        return $data;
    }

    /* Ich hätte die funktion gerne mehr kommentiert aber ich krieg selber nicht zusammen wie es funktioniert */
    function conditionKeysToString($array, $glue = null) {
        $ret = '';
        $count = 0;

        foreach ($array as $key => $item) {
            $count++;

            if($count != 1){
                $ret .= "$glue ";
            }

            /* Wenn ein weiteres Array, dann rekursiv weiter*/
            if (is_array($item)) {
                $ret .= "(";
                $ret .= self::conditionKeysToString($item, $key);
                $ret .= ") ";
            } else {
                /* Wenn im Blatt-Knoten angekommen den String zusammenbauen */
                $operator = explode(" ", $key);
                if(count($operator) > 1){
                    $key = $operator[0];
                    if(self::isComparisonOperator($operator[1])){
                        $operator = $operator[1];
                    }else{
                        $operator = "=";
                    }
                }else{
                    $operator = "=";
                }

                /* Wurde ein HEX Wert übergeben z.b. eine GGUID ? */
                if(!(substr($item, 0, 2) == "0x" && ctype_xdigit(substr($item, 2)))){
                    /* ansonsten als String escapen ... */
                    $item = "'".self::ms_escape_string($item)."'";
                }

                $ret .= self::ms_escape_string($key)." $operator $item ";
            }
        }

        return $ret;
    }

    function addLinkJoinToSql($sql, $options){
        if(isset($options['link'])){
            $type = $options['link'];
            $sql .= " LINK_JOIN() $type";
        }
        return $sql;
    }

    function addWhereToSql($sql, $options){
        if(isset($options['conditions']) && count($options['conditions']) > 0){
            $sql .= " WHERE 1=1 AND ";
            $sql .= self::conditionKeysToString($options['conditions']);
        }
		return $sql;
    }

    function addOrderByToSql($sql, $options){
        if(isset($options['order']) && is_array($options['order']) && count($options['order']) > 0){
            $sql .= " ORDER BY ";
            $numItems = count($options['order']);
            $i = 0;
            foreach($options['order'] as $orderBy){
                $i++;
                if($i == $numItems){
                    $sql .= $orderBy;
                }else{
                    $sql .= "$orderBy, ";
                }
            }
        }

        return $sql;
    }

    function addLimitToSql($sql, $options){
        if(isset($options['limit']) && count($options['limit']) > 0){
            $sql .= " LIMIT ".$options['limit'];
        }

        return $sql;
    }

    function addTeamfilterToSql($sql, $type){
        $sql .= " TEAMFILTER($type;CASLoggedInUser,CASPublicRecords,CASExternalAccess)";
//        $sql .= " TEAMFILTER($type;CASLoggedInUser,CASPublicRecords)";
        return $sql;
    }
}