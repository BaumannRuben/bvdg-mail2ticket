<?php
require_once "AbstractDao.php";

use \de\cas\open\server\api\business\GetLinksRequest;
use \de\cas\open\server\api\business\SaveLinkRequest;
use \de\cas\open\server\api\business\DeleteLinkRequest;
use \de\cas\open\server\api\types\LinkObject;

class StatistikDao extends AbstractDao
{
    protected $objectType = "STATISTIK";
    static private $instance = null;

    /* @return StatistikDao */
    static function getInstance()
    {
        if (null === self::$instance) {
            $class = get_called_class();
            self::$instance = new $class;
        }
        return self::$instance;
    }

    /*
     *  Erstellt eine neue Statistik
     */
    /* @return \de\cas\open\server\api\types\DataObject */
    public function createStatistik($statistikdata)
    {
    	$statistikObj = $this->createObject();
        foreach ($statistikdata as $field => $row) {
            $statistikObj->setValue($row['value'], $field, $row['fieldType']);
        }
        return $this->save($statistikObj);
    }
    
    public function createLinkBetweenAdrStat($gguid1, $gguid2, $attribute)
    {
    	$link = new LinkObject();
    	$link->objectType1 = "ADDRESS";
    	$link->GGUID1 = $gguid1;
    	$link->objectType2 = "STATISTIK";
    	$link->GGUID2 = $gguid2;
    	$link->attribute = new StdClass();
    	$link->attribute->key = $attribute;
    	$link->isHierarchy = true;
    
    	$linkRequest = new SaveLinkRequest();
    	$linkRequest->links = array($link);
    
    	$linkResponse = HochwarthIT_BVDGGate::getEIMInterface()->execute($linkRequest);
    
    	return $linkResponse;
    }
}