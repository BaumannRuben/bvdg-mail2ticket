<?php
require_once "AbstractDao.php";

use \de\cas\open\server\api\business\GetLinksRequest;
use \de\cas\open\server\api\business\SaveLinkRequest;
use \de\cas\open\server\api\business\DeleteLinkRequest;
use \de\cas\open\server\api\types\LinkObject;

//Upload Kontaktbild
use de\cas\open\server\addresses\types\SaveImageForContactRequest;
use de\cas\open\server\api\business\GetJournalRequest;
use de\cas\open\server\addresses\types\GetImageForContactRequest;

class AddressDao extends AbstractDao
{

    protected $objectType = "ADDRESS";
    static private $instance = null;

    /* @return AddressDao */
    static function getInstance()
    {
        if (null === self::$instance) {
            $class = get_called_class();
            self::$instance = new $class;
        }
        return self::$instance;
    }

    /*
     *  Erstellt eine neue Adresse / einen neuen Kontakt
     */
    /* @return \de\cas\open\server\api\types\DataObject */
    public function createAddress($addressdata)
    {
    	$addressObj = $this->createObject();
        foreach ($addressdata as $field => $row) {
            $addressObj->setValue($row['value'], $field, $row['fieldType']);
        }
        return $this->save($addressObj);
    }

    /*
     *  Adressen, welcher mit einer bestimmter Adresse verknüpft sind
     */
    public function getLinkedAddresses($adr_gguid, $fields = '*')
    {
        $sql = "SELECT ".$fields
            . " FROM ADDRESS AS A"
            . " WHERE A.IsLinkedToWhere(ADDRESS: WHERE ADDRESS.GGUID = 0x$adr_gguid)";
        $sql = $this->addTeamfilterToSql($sql, $this->objectType);
        $result = HochwarthIT_BVDGGate::getEIMInterface()->query($sql);

        return $this->convertResultObject($result);
    }

    /*
     *  Listet alle Adressverknüpfungen
     */
    public function getAddressLinks()
    {
        $linkObj = new LinkObject();
        $linkObj->objectType1 = "ADDRESS";
        $linkObj->objectType2 = "ADDRESS";
        $linksReq = new GetLinksRequest();
        $linksReq->LinkObject = $linkObj;
        $linkResponse = HochwarthIT_BVDGGate::getEIMInterface()->execute($linksReq);

        return $linkResponse;
    }

    public function getLinkedGguids($gguid)
    {

        $linkedAddresses = null;
        $headquarterGguids = $this->getAddressGguidsForLinkDirection($gguid, "LEFT_TO_RIGHT");

        if($headquarterGguids){

            foreach ($headquarterGguids as $headquarterGguid) {
                $hqGguid = $headquarterGguid->GGUID2;

                $agencyGguids = $this->getAddressGguidsForLinkDirection($hqGguid, "RIGHT_TO_LEFT");

                foreach ($agencyGguids as $agencyGguid) {
                    $agGguid = $agencyGguid->GGUID2;
                    $linkedAddresses[$hqGguid][] = $agGguid;
                }
            }
        }
        else{

            $agencyGguids = $this->getAddressGguidsForLinkDirection($gguid, "RIGHT_TO_LEFT");

            if($agencyGguids){
                foreach ($agencyGguids as $agencyGguid) {
                    $agGguid = $agencyGguid->GGUID2;
                    $linkedAddresses[$gguid][] = $agGguid;
                }
            }
        }

        return $linkedAddresses;

    }

    public function getAddressGguidsForLinkDirection($gguid, $direction = "RIGHT_TO_LEFT")
    {
        $linkObj = new LinkObject();
        $linkObj->objectType1 = "ADDRESS";
        $linkObj->objectType2 = "ADDRESS";
        $linkObj->GGUID1 = $gguid;
        $linkObj->linkDirection = $direction;
        $linksReq = new GetLinksRequest();
        $linksReq->LinkObject = $linkObj;
        $linkResponse = HochwarthIT_BVDGGate::getEIMInterface()->execute($linksReq);

        return $linkResponse->links;

    }

    /**
     *  Erzeugt eine Verknüpfung zwischen zwei Adressen
     *
     *  @param $gguid1 String Filiale / Tochter
     *  @param $gguid2 String Zentrale / Mutter
     *  @param $attribute String L2UADRHIERARCHY --> Mutter-Tochter-Hierarchie
     */
    public function createLinkBetweenAddresses($gguid1, $gguid2, $attribute = null, $dir = "RIGHT_TO_LEFT")
    {
        $link = new LinkObject();
        $link->objectType1 = "ADDRESS";
        $link->GGUID1 = $gguid1;
        $link->objectType2 = "ADDRESS";
        $link->GGUID2 = $gguid2;
        $link->attribute = new StdClass();
        $link->attribute->key = $attribute;
        $link->isHierarchy = true;
        $link->linkDirection = $dir; //Right = Tochter, Left = Mutter

        $linkRequest = new SaveLinkRequest();
        $linkRequest->links = array($link);

        $linkResponse = HochwarthIT_BVDGGate::getEIMInterface()->execute($linkRequest);

        return $linkResponse;
    }

    /*
     *  Löscht eine Adressverknüpfung
     */
    public function deleteLinkBetweenAddresses($gguid)
    {
        $linkRequest = new DeleteLinkRequest();
        $linkRequest->linksToBeDeleted = array($gguid);
        $linkResponse = HochwarthIT_BVDGGate::getEIMInterface()->execute($linkRequest);

        return $linkResponse;
    }

    public function getGguidIfUniqueEmail($email){
        $options = array(
            "conditions" => array(
                "OR" => array(
                    "MAILFIELDSTR1" => $email,
                    "MAILFIELDSTR2" => $email,
                    "MAILFIELDSTR3" => $email,
                    "MAILFIELDSTR4" => $email,
                    "MAILFIELDSTR5" => $email,
                )
            )
        );
        $cas_customers = $this->find("list", $options);
        //wenn unique dann die GGUID zurückgeben
        if(count($cas_customers) === 1){
            reset($cas_customers);
            return key($cas_customers);
        }
        return false;
    }

    /* @return \de\cas\open\server\api\types\DataObject */
    public function checkDuplicate($firstname, $lastname, $postcode, $dataObject){
        $duplicate = $this->find("first",
            array(
                "conditions" => array(
                    "AND" => array(
                        "GGUID !=" => "0x{$dataObject->getValue('GGUID')}",
                        "CHRISTIANNAME" => $firstname,
                        "NAME" => $lastname,
                        "OR" => array(
                            "ZIP1" => $postcode,
                            "ZIP2" => $postcode,
                            "ZIP3" => $postcode
                        )
                    )
                )
            )
        );
        if($duplicate){
            $dataObject = $this->setData("DUPLETTENVERDACHT", true, $dataObject);
        }
        else{
            $dataObject = $this->setData("DUPLETTENVERDACHT", false, $dataObject);
        }
        return $dataObject;
    }
    
    //Upload Kontaktbild
    public function uploadPicture($path, $adr_gguid) {
    	$file = fopen($path, "rb");
       	$imageBytes = fread($file, filesize($path));
    	fclose($file);
    	$imageRequest = new SaveImageForContactRequest();
    	$imageRequest->AdressGGUID = "0x".$adr_gguid;
    	$imageRequest->ImageBytes = $imageBytes;
    	$imageRequest->ImageFileExtension = 'jpg';
    	$imageResponse = HochwarthIT_BVDGGate::getEIMInterface()->execute($imageRequest);
    }
    
    //Download Kontaktbild
    public function downloadPicture($adr_gguid) {
    	$imageRequest = new GetImageForContactRequest();
    	$imageRequest->AdressGGUID = "0x".$adr_gguid;
    	$imageResponse = HochwarthIT_BVDGGate::getEIMInterface()->execute($imageRequest);
    	
    	return $imageResponse;
    }
    
    
    
    public function deleteAddress($adr_gguid)
    {
    	HochwarthIT_BVDGGate::getEIMInterface()->deleteObject("ADDRESS", "0x".$adr_gguid);
    }
    
    //Verknüpfte Startrechte, sortiert nach $order
    public function getLinkedStartrecht($adr_gguid, $fields = '*', $order = null, $condition = null)
    {
    	$sql = "SELECT ".$fields
    		. " FROM STARTRECHT AS S"
    		. " WHERE S.IsLinkedToWhere(ADDRESS: WHERE ADDRESS.GGUID = 0x$adr_gguid)";
        if ($condition) {
            $sql .= " AND $condition";
        }
        $sql = $this->addTeamfilterToSql($sql, $this->objectType);
    	if ($order) {
            $sql = $sql . " ORDER BY S." . $order;
        }
        $result = HochwarthIT_BVDGGate::getEIMInterface()->query($sql);
    	return $this->convertResultObject($result);
    }
    
    //Verknüpfte Statistiken, sortiert nach $order
    public function getLinkedStatistik($adr_gguid, $fields = '*', $order)
    {
    	$sql = "SELECT ".$fields
    	. " FROM STATISTIK AS S"
    			. " WHERE S.IsLinkedToWhere(ADDRESS: WHERE ADDRESS.GGUID = 0x$adr_gguid)";
    			$sql = $this->addTeamfilterToSql($sql, $this->objectType);
    			$sql = $sql." ORDER BY S.".$order;
    			$result = HochwarthIT_BVDGGate::getEIMInterface()->query($sql);
    			return $this->convertResultObject($result);
    }
    
    public function getJournal($dataId)
    {	
    	$request = new GetJournalRequest(); 
    	$request->GGUID = "0x".$dataId;
    	$request->objectType = "ADDRESS";
    	
    	$response = HochwarthIT_BVDGGate::getEIMInterface()->execute($request);
    	
    	print_r($response);
    	
    	return $response;
    } 
    /*
     *  Adressen mit Typ = Verein, welche mit einer bestimmter Adresse verknüpft sind
     */
    public function getLinkedVereine($adr_gguid, $fields = '*')
    {
    	$sql = "SELECT ".$fields
    	. " FROM ADDRESS AS A"
    			. " WHERE A.IsLinkedToWhere(ADDRESS: WHERE ADDRESS.GGUID = 0x$adr_gguid) AND GWSTYPE = 'Verein'";
    			$sql = $this->addTeamfilterToSql($sql, $this->objectType);
    			$sql = $sql." ORDER BY COMPNAME";
    			$result = HochwarthIT_BVDGGate::getEIMInterface()->query($sql);
    
    			return $this->convertResultObject($result);
    }
    
    public function testIt($adr_gguid)
    {
    	$sql = "SELECT WITH_FULL_TEXT_FIELD_CONTENT(A.NOTES2) "
    		 . "FROM ADDRESS AS A "
    		 . "WHERE A.GGUID = 0x$adr_gguid";
    	$sql = $this->addTeamfilterToSql($sql, $this->objectType);
    	$result = HochwarthIT_BVDGGate::getEIMInterface()->query($sql);
    	return $this->convertResultObject($result);
    }
    
    public function getLinkedDocuments($adr_gguid, $fields = '*', $type)
    {
    	$sql = "SELECT ".$fields
    		. " FROM DOCUMENT AS D"
    		. " WHERE D.IsLinkedToWhere(ADDRESS: WHERE ADDRESS.GGUID = 0x$adr_gguid) AND KEYWORD = '$type'";
    	$sql = $this->addTeamfilterToSql($sql, $this->objectType);
    	$result = HochwarthIT_BVDGGate::getEIMInterface()->query($sql);
    	return $this->convertResultObject($result);
    }
    public function getLinkedVerbaende($adr_gguid, $fields = '*')
    {
    	$sql = "SELECT ".$fields
    	. " FROM ADDRESS AS A"
    			. " WHERE A.IsLinkedToWhere(ADDRESS: WHERE ADDRESS.GGUID = 0x$adr_gguid) AND GWSTYPE = 'Verband'";
    			$sql = $this->addTeamfilterToSql($sql, $this->objectType);
    			$result = HochwarthIT_BVDGGate::getEIMInterface()->query($sql);
    
    			return $this->convertResultObject($result);
    }
    
    //Verknüpfte Startrechte, sortiert nach $order
    public function getLinkedStartrecht2($ath_gguid, $ver_gguid, $fields = '*', $order)
    {
    	$sql = "SELECT ".$fields
    	. " FROM STARTRECHT AS S"
    			. " WHERE S.IsLinkedToWhere(ADDRESS: WHERE ADDRESS.GGUID = 0x$ath_gguid) AND S.IsLinkedToWhere(ADDRESS: WHERE ADDRESS.GGUID = 0x$ver_gguid)";
    			$sql = $this->addTeamfilterToSql($sql, $this->objectType);
    			$sql = $sql." ORDER BY S.".$order;
//     			echo $sql;
    			$result = HochwarthIT_BVDGGate::getEIMInterface()->query($sql);
    			return $this->convertResultObject($result);
    }

    public function getLinkedTeilnehmerMS($adr_gguid, $fields = '*', $option = null)
    {
        $sql = "SELECT ".$fields
            . " FROM TEILNAHMEMS AS TNMS"
            . " WHERE TNMS.IsLinkedToWhere(ADDRESS: WHERE ADDRESS.GGUID = 0x$adr_gguid)";
        if ($option) {
            $sql .= " AND $option";
        }
        $sql = $this->addTeamfilterToSql($sql, $this->objectType);
        $result = HochwarthIT_BVDGGate::getEIMInterface()->query($sql);

        return $this->convertResultObject($result);
    }
}
