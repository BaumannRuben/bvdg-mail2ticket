<?php
require_once "AbstractDao.php";

use \de\cas\open\server\api\business\GetLinksRequest;
use \de\cas\open\server\api\business\SaveLinkRequest;
use \de\cas\open\server\api\business\DeleteLinkRequest;
use \de\cas\open\server\api\types\LinkObject;

class TeilnehmerDao extends AbstractDao
{
    protected $objectType = "TEILNAHME";
    static private $instance = null;

    /* @return TeilnehmerDao */
    static function getInstance()
    {
        if (null === self::$instance) {
            $class = get_called_class();
            self::$instance = new $class;
        }
        return self::$instance;
    }

    /*
     *  Erstellt einen neuen Teilnehmer
     */
    /* @return \de\cas\open\server\api\types\DataObject */
    public function createTeilnehmer($teilnehmerdata)
    {
    	$teilnehmerObj = $this->createObject();
        foreach ($teilnehmerdata as $field => $row) {
            $teilnehmerObj->setValue($row['value'], $field, $row['fieldType']);
        }
        return $this->save($teilnehmerObj);
    }

    public function deleteTeilnahme($teilnahme_gguid)
    {
        return HochwarthIT_BVDGGate::getEIMInterface()->deleteObject('TEILNAHME', "0x$teilnahme_gguid");
    }
    
    public function createLinkBetweenAddressesTeilnehmer($gguid1, $gguid2, $attribute)
    {
    	$link = new LinkObject();
    	$link->objectType1 = "ADDRESS";
    	$link->GGUID1 = $gguid1;
    	$link->objectType2 = "TEILNAHME";
    	$link->GGUID2 = $gguid2;
    	$link->attribute = new StdClass();
    	$link->attribute->key = $attribute;
    	$link->isHierarchy = true;

    	$linkRequest = new SaveLinkRequest();
    	$linkRequest->links = array($link);

    	$linkResponse = HochwarthIT_BVDGGate::getEIMInterface()->execute($linkRequest);

    	return $linkResponse;
    }

    public function createLinkBetweenWettkampfTeilnehmer($gguid1, $gguid2, $attribute)
    {
    	$link = new LinkObject();
    	$link->objectType1 = "WETTKAMPF";
    	$link->GGUID1 = $gguid1;
    	$link->objectType2 = "TEILNAHME";
    	$link->GGUID2 = $gguid2;
    	$link->attribute = new StdClass();
    	$link->attribute->key = $attribute;
    	$link->isHierarchy = true;

    	$linkRequest = new SaveLinkRequest();
    	$linkRequest->links = array($link);

    	$linkResponse = HochwarthIT_BVDGGate::getEIMInterface()->execute($linkRequest);

    	return $linkResponse;
    }
    
    public function getLinkedTeilnehmer($wettkampf_gguid, $fields = '*', $rolle)
    {
    	$sql = "SELECT ".$fields
    	    . " FROM TEILNAHME AS TN"
    	    . " WHERE TN.IsLinkedToWhere(WETTKAMPF: WHERE WETTKAMPF.GGUID = 0x$wettkampf_gguid)";
    	if ($rolle) {
            $sql .= " AND TN.TEILNAHMEROLLE = '$rolle'";
        }
    	$sql = $this->addTeamfilterToSql($sql, $this->objectType);
    	$result = HochwarthIT_BVDGGate::getEIMInterface()->query($sql);

    	return $this->convertResultObject($result);
    }

    public function getLinkedAthlet($teilnahme_gguid, $fields = '*')
    {
    	$sql = "SELECT ".$fields
    	    . " FROM ADDRESS AS A"
    	    . " WHERE A.IsLinkedToWhere(TEILNAHME: WHERE TEILNAHME.GGUID = 0x$teilnahme_gguid)";
    	$sql = $this->addTeamfilterToSql($sql, $this->objectType);
    	$result = HochwarthIT_BVDGGate::getEIMInterface()->query($sql);

    	return $this->convertResultObject($result, 'first');
    }
}