<?php
require_once "AbstractDao.php";

use \de\cas\open\server\api\business\GetLinksRequest;
use \de\cas\open\server\api\business\SaveLinkRequest;
use \de\cas\open\server\api\business\DeleteLinkRequest;
use \de\cas\open\server\api\types\LinkObject;

class WettkampfDao extends AbstractDao
{
    protected $objectType = "WETTKAMPF";
    static private $instance = null;

    /* @return WettkampfDao */
    static function getInstance()
    {
        if (null === self::$instance) {
            $class = get_called_class();
            self::$instance = new $class;
        }
        return self::$instance;
    }

    /*
     *  Erstellt einen neuen Wettkampf
     */
    /* @return \de\cas\open\server\api\types\DataObject */
    public function createWettkampf($wettkampfdata)
    {
    	$wettkampfObj = $this->createObject();
        foreach ($wettkampfdata as $field => $row) {
            $wettkampfObj->setValue($row['value'], $field, $row['fieldType']);
        }
        return $this->save($wettkampfObj);
    }
    
//    public function createLinkBetweenAddressesStartrecht($gguid1, $gguid2, $attribute)
//    {
//    	$link = new LinkObject();
//    	$link->objectType1 = "ADDRESS";
//    	$link->GGUID1 = $gguid1;
//    	$link->objectType2 = "STARTRECHT";
//    	$link->GGUID2 = $gguid2;
//    	$link->attribute = new StdClass();
//    	$link->attribute->key = $attribute;
//    	$link->isHierarchy = true;
//
//    	$linkRequest = new SaveLinkRequest();
//    	$linkRequest->links = array($link);
//
//    	$linkResponse = HochwarthIT_BVDGGate::getEIMInterface()->execute($linkRequest);
//
//    	return $linkResponse;
//    }
    
//    public function getLinkedAthlets($startrecht_gguid, $fields = '*')
//    {
//    	$sql = "SELECT NAME, CHRISTIANNAME, ".$fields
//    	. " FROM ADDRESS AS A"
//    	. " WHERE A.IsLinkedToWhere(STARTRECHT: WHERE STARTRECHT.GGUID = 0x$startrecht_gguid) AND GWSTYPE = 'Gewichtheber'";
//    	$sql = $this->addTeamfilterToSql($sql, $this->objectType);
//    	$options['order'] = array("A.NAME, A.CHRISTIANNAME");
//    	$sql = $this->addOrderByToSql($sql, $options);
//    	$result = HochwarthIT_BVDGGate::getEIMInterface()->query($sql);
//
//    	return $this->convertResultObject($result);
//    }
    
//    public function deleteStartrecht($strt_gguid)
//    {
//    	HochwarthIT_BVDGGate::getEIMInterface()->deleteObject("STARTRECHT", "0x".$strt_gguid);
//    }
    
//    public function getLinkedVereine($startrecht_gguid, $fields = '*')
//    {
//    	$sql = "SELECT ".$fields
//    		. " FROM ADDRESS AS A"
//    			. " WHERE A.IsLinkedToWhere(STARTRECHT: WHERE STARTRECHT.GGUID = 0x$startrecht_gguid) AND GWSTYPE = 'Verein'";
//    			$sql = $this->addTeamfilterToSql($sql, $this->objectType);
//    			$result = HochwarthIT_BVDGGate::getEIMInterface()->query($sql);
//
//    			return $this->convertResultObject($result);
//    }

    public function getLinkedWettkaempfe($verein_gguid, $fields = '*', $linkatt, $option = null)
    {
        $sql = "SELECT ".$fields
            . " FROM WETTKAMPF AS W"
            . " WHERE W.IsLinkedToWhere(ADDRESS: WHERE ADDRESS.GGUID = 0x$verein_gguid; LinkAttribute='$linkatt')";
        if ($option) {
            $sql .= " AND $option";
        }
        $sql = $this->addTeamfilterToSql($sql, $this->objectType);
        $result = HochwarthIT_BVDGGate::getEIMInterface()->query($sql);

        return $this->convertResultObject($result);
    }
}