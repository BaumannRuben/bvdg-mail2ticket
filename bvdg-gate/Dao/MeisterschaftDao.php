<?php
require_once "AbstractDao.php";

use \de\cas\open\server\api\business\GetLinksRequest;
use \de\cas\open\server\api\business\SaveLinkRequest;
use \de\cas\open\server\api\business\DeleteLinkRequest;
use \de\cas\open\server\api\types\LinkObject;

class MeisterschaftDao extends AbstractDao
{
    protected $objectType = "MEISTERSCHAFT";
    static private $instance = null;

    /* @return MeisterschaftDao */
    static function getInstance()
    {
        if (null === self::$instance) {
            $class = get_called_class();
            self::$instance = new $class;
        }
        return self::$instance;
    }

    public function getLinkedMeisterschaft($teilnehmer_gguid, $fields = '*', $type = 'all')
    {
        $sql = "SELECT ".$fields
            . " FROM MEISTERSCHAFT AS M"
            . " WHERE M.IsLinkedToWhere(TEILNAHMEMS: WHERE TEILNAHMEMS.GGUID = 0x$teilnehmer_gguid)";
        $sql = $this->addTeamfilterToSql($sql, $this->objectType);
        $result = HochwarthIT_BVDGGate::getEIMInterface()->query($sql);

        return $this->convertResultObject($result, $type);
    }

}