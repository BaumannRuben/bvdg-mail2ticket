<html>
<body>
<pre>
<?php
ini_set('display_errors',1);
#=== Including bvdg-gate ======
require "BVDGGate.php";

#=== Show all available objects ======
//print_r(HochwarthIT_BVDGGate::getAvailableObjects());exit;

#=== Data Access Objects ======
$eventDao = HochwarthIT_BVDGGate::getEventDao();
$appointmentDao = HochwarthIT_BVDGGate::getAppointmentDao();
$addressDao = HochwarthIT_BVDGGate::getAdressDao();


#=== Addresses ======
$options = array(
    "fields" => array(
        "GGUID",
        "ADDRESSTERM",
        "CHRISTIANNAME",
        "NAME",
        "COMPNAME",
        "STREET1",
        "ZIP1",
        "TOWN1"
    ),
    "limit" => 2,
    //"conditions" => array("CHRISTIANNAME LIKE" => "%Filiale%")
);
#-Erstellung--
/*
$addressdata = array(
  'COMPNAME' => array('value' => 'Hochwarth IT HQ', 'fieldType' => 'STRING'),
  'GWISCOMPANY' => array('value' => '1', 'fieldType' => 'STRING')
);
$newAddress = $addressDao->createAddress($addressdata);
print_r($newAddress);
*/
#-Ausgabe--
//$addresses = $addressDao->find('all', $options);
//$addresses = $addressDao->find('list', array("displayField" => "NAME"));
//print_r($addresses);
#-Adressverlinkungen--
// Beispieladressen //
// C9C85627730C3E428EADF7920414B79C -> Zentrale HQ
// F352F8842848C746BEB528F1AB773E2E -> Filiale 1
// C79B0D0C6EC8DC47B38ED7AEF30BCCF3 -> Filiale 2
// 7BAE35B22BF79349B4674C6B3E38A0C1 -> Filiale 3
//print_r($addressDao->getAddressLinks());                                                                      //alle Verknüpfungen zwischen Adressen
//print_r($addressDao->getLinkedAddresses("756EFBEF26369F49B51A075E9273CBC6", "NAME, CHRISTIANNAME"));          //Adressen, welcher mit einer bestimmter Adresse verknüpft sind
//print_r($addressDao->createLinkBetweenAddresses("7BAE35B22BF79349B4674C6B3E38A0C1","C9C85627730C3E428EADF7920414B79C", "L2UADRHIERARCHY"));   // (hierarchische) Verknüpfung zwischen Adressen erzeugen
//print_r($addressDao->deleteLinkBetweenAddresses("360A691D5115DF42AAA86C9C01CAC308@ADDRESS@756EFBEF26369F49B51A075E9273CBC6@ADDRESS"));        // Verknüpfung löschen
die();


#=== Accounting ======
$accPos = $eventDao->getPrices("FFC128CDF1C63D41B5F7C70DBC714D29");
print_r($accPos);
echo $eventDao->accountingToHtml($accPos);


#=== Events, Appointments, Registrations ======
$events = $eventDao->find('all');
$options = array(
    "link" => "EVENT"
);
$ev_appointments = $appointmentDao->find('all', $options);
$ev_registrations = $eventDao->getRegistrations("E39A64867961B342A51952B349CCEC29");

$options = array(
    "fields" => array(
        "GGUID",
        "KEYWORD"
    )
);
$appointments = $appointmentDao->find('all', $options);
$app_registrations = $appointmentDao->getRegistrations("2321A7EF0DCF564590F059D5BC75382A");
$app2_registrations = $appointmentDao->getRegistrations("638F950340946C4992EC8BDEC63039E2");

$outputs = array(
    array("EVENTS", $events),
    array("EVENT APPOINTMENTS", $ev_appointments),
    array("EVENT REGISTRATIONS", $ev_registrations),
    array("APPOINTMENTS", $appointments),
    array("APPOINTMENT REGISTRATIONS", $app_registrations),
    array("APPOINTMENT REGISTRATIONS 2", $app2_registrations)
);

?>
<?php foreach($outputs as $output): ?>
<h2><?php echo $output[0]; ?></h2>
<?php $output = $output[1]; ?>
<?php if(count($output) == 0){continue;} ?>
<table border="1">
    <thead>
        <tr>
            <th>Zeile</th>
            <?php foreach($output[0] as $field => $data): ?>
                <th>
                    <?php echo $data['displayName']." ($field)"; ?>
                </th>
            <?php endforeach; ?>
        </tr>
    </thead>
    <tbody>
     <?php $count = 0; ?>
        <?php foreach($output as $entry): ?>
            <?php $count ++; ?>
            <tr>
                <td><?php echo $count; ?></td>
                <?php foreach($entry as $data): ?>
                    <td>
                        <?php echo $data['value']; ?>
                    </td>
                <?php endforeach; ?>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php endforeach; ?>
</pre>
</body>
</html>
