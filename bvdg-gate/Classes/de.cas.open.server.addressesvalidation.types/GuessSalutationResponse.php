<?php
// This file has been automatically generated.

namespace de\cas\open\server\addressesvalidation\types {

    /**
     * @package de\cas\open\server\addressesvalidation
     * @subpackage types
     *
     *				Request object for the business operation that
     *				guesses the gender, based on the name and title, and returns the
     *				salutation to
     *				the gender-specific salutation in the language of the
     *				currently logged-in
     *				user.
     *				If no genderguesser plugIn is able to guess
     *				the gender, no salutation is set.
     */
    class GuessSalutationResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var string
         *Guessed gender of first name of the request.
         */
        public $gender;

        /**
         * @var string
         *Guessed salutation for the provided first name of
         *										the request (e.g. Mrs., Mr.).
         */
        public $addressTerm;

        /**
         * @var string
         *Guessed letterhead salutation for the provided
         *										first name of the request (e.g. Dear Mr., Dear Mrs.).
         */
        public $addressLetter;

    }

}
