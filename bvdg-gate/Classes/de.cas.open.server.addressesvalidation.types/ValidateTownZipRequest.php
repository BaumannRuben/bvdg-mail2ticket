<?php
// This file has been automatically generated.

namespace de\cas\open\server\addressesvalidation\types {

    /**
     * @package de\cas\open\server\addressesvalidation
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\RequestObject:<br/>
     *				Returns related address data for an zip code or town name.
     *				See TownZipMapperData for provied town infos.
     *				Throws an IllegalArgumentException if both values (ZIP and town) are provided.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject: GetAddressDataResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetAddressDataResponse
     */
    class ValidateTownZipRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										 Country of ZIP code or town name. Mandatory.
         */
        public $country;

        /**
         * @var string
         *
         *										 Find related address data by using a ZIP code.
         */
        public $zip;

        /**
         * @var string
         *
         *										 Find related address data by using a town name.
         */
        public $town;

    }

}
