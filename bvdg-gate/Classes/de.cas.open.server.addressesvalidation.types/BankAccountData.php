<?php
// This file has been automatically generated.

namespace de\cas\open\server\addressesvalidation\types {

    /**
     * @package de\cas\open\server\addressesvalidation
     * @subpackage types
     *
     *        Container for bank account data.
     */
    class BankAccountData {

        /**
         * @var string
         *
         */
        public $bankAccountNumber;

        /**
         * @var \de\cas\open\server\addressesvalidation\types\BankData
         *
         */
        public $bankData;

    }

}
