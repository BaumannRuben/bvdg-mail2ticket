<?php
// This file has been automatically generated.

namespace de\cas\open\server\addressesvalidation\types {

    /**
     * @package de\cas\open\server\addressesvalidation
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\ResponseObject:
     *				Validates a prefix of a phone number. It's used for input validation.
     *				Only a number is expected, i.e. "721". It's not valid to send vales like "+49 721" or "0721".
     *				\de\cas\open\server\api\types\RequestObject: ValidatePhonePrefixRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see ValidatePhonePrefixRequest
     */
    class ValidatePhonePrefixResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var boolean
         *
         *										TRUE: If a phone prefix validation plug-ins voted true,
         *										else FALSE.
         */
        public $valid;

    }

}
