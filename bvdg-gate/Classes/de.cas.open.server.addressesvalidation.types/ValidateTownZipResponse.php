<?php
// This file has been automatically generated.

namespace de\cas\open\server\addressesvalidation\types {

    /**
     * @package de\cas\open\server\addressesvalidation
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\ResponseObject: Returns related address data
     *				for an zip code or town name. See TownZipMapperData for provided town infos.
     *				Return value may be null!
     *				Corresponding
     *				\de\cas\open\server\api\types\RequestObject: GetCockpitItemsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetCockpitItemsRequest
     */
    class ValidateTownZipResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *										Related address data or null if none were available.
         */
        public $addressData;

    }

}
