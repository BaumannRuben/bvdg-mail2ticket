<?php
// This file has been automatically generated.

namespace de\cas\open\server\addressesvalidation\types {

    /**
     * @package de\cas\open\server\addressesvalidation
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject
     *        of the business operation that validates an IBAN.
     *        Corresponding \de\cas\open\server\api\types\RequestObject:
     *        ValidateIbanRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see ValidateIbanRequest
     */
    class ValidateIbanResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var boolean
         *
         *                    Value indicating whether the given IBAN is valid. Mandatory.
         */
        public $valid;

        /**
         * @var \de\cas\open\server\addressesvalidation\types\BankAccountData
         *
         *                    \de\cas\open\server\addresses\validation\types\BankAccountData
         *                    container containing the bank account data that could be derived from
         *                    the given IBAN.
         *	@see \de\cas\open\server\addresses\validation\types\BankAccountData
         */
        public $bankAccountData;

    }

}
