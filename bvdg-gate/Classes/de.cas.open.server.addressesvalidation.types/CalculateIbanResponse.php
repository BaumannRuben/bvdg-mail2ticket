<?php
// This file has been automatically generated.

namespace de\cas\open\server\addressesvalidation\types {

    /**
     * @package de\cas\open\server\addressesvalidation
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject
     *        of the business	operation that calculates an IBAN from the given
     *        bank data.
     *        Corresponding \de\cas\open\server\api\types\RequestObject:
     *        CalculateIbanRequest.
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see CalculateIbanRequest
     */
    class CalculateIbanResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var string
         *
         *                    The calculated IBAN.
         */
        public $iban;

    }

}
