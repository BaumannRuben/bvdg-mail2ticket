<?php
// This file has been automatically generated.

namespace de\cas\open\server\addressesvalidation\types {

    /**
     * @package de\cas\open\server\addressesvalidation
     * @subpackage types
     *
     *				Request object for the business operation that
     *				guesses the gender, based on the name and title, and returns the
     *				salutation to
     *				the gender-specific salutation in the language of the
     *				currently logged-in
     *				user.
     *				If no genderguesser plugIn is able to guess
     *				the gender, no salutation is set.
     */
    class GuessSalutationRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										The address term, if specified.
         */
        public $term;

        /**
         * @var boolean
         *
         *										Guess the gender to be used for the salutation from the term (if specified) only.
         */
        public $guessFromTermOnly;

        /**
         * @var string
         *
         *										The last name of the contact to guess the salutation form for.
         */
        public $name;

        /**
         * @var string
         *
         *										The title of the contact to guess the salutation form for.
         */
        public $title;

        /**
         * @var string
         *
         *										The first name of the contact to guess the salutation form for.
         */
        public $christianname;

        /**
         * @var string
         *
         *										The previous value of the contact's last name to guess the salutation form for.
         */
        public $oldname;

        /**
         * @var string
         *
         *										The previous value of the contact's letter salutation to guess the salutation form for. This will be persisted (with only the name changed) if there is a value set.
         */
        public $letter;

        /**
         * @var string
         *
         *										Specifies the preferred language for the
         *										salutation translation.
         */
        public $preferredLanguage;

    }

}
