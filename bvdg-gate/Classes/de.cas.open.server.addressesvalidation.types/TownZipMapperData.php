<?php
// This file has been automatically generated.

namespace de\cas\open\server\addressesvalidation\types {

    /**
     * @package de\cas\open\server\addressesvalidation
     * @subpackage types
     *
     *				Container for some address data.
     */
    class TownZipMapperData extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var string
         *
         *										ZIP of an address.
         */
        public $zip;

        /**
         * @var string
         *
         *										Town name of an address.
         */
        public $town;

        /**
         * @var array
         *
         *										Suburbs of an address.
         */
        public $suburbs;

        /**
         * @var string
         *
         *										State of an address.
         */
        public $state;

    }

}
