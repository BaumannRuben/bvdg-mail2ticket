<?php
// This file has been automatically generated.

namespace de\cas\open\server\addressesvalidation\types {

    /**
     * @package de\cas\open\server\addressesvalidation
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\RequestObject:<br/>
     *				Validates a prefix of a phone number. It's used for input validation.
     *				Only a number is expected, i.e. "721". It's not valid to send vales like "+49 721" or "0721".
     *				Corresponding \de\cas\open\server\api\types\ResponseObject: ValidatePhonePrefixResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see ValidatePhonePrefixResponse
     */
    class ValidatePhonePrefixRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										 Country of phone prefix. Mandatory.
         */
        public $country;

        /**
         * @var string
         *
         *										 Phone prefix to validate. Mandatory.
         *										 Only a number is expected, i.e. "721". It's not valid
         *										 to send vales like "+49 721" or "0721".
         */
        public $phonePrefix;

    }

}
