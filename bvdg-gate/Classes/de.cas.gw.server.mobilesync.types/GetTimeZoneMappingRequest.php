<?php
// This file has been automatically generated.

namespace de\cas\gw\server\mobilesync\types {

    /**
     * @package de\cas\gw\server\mobilesync
     * @subpackage types
     *
     *        Reads information about a time zone.
     */
    class GetTimeZoneMappingRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var \de\cas\gw\server\mobilesync\types\TimeZoneMapping
         *
         */
        public $tzMapping;

    }

}
