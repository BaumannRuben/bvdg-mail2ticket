<?php
// This file has been automatically generated.

namespace de\cas\gw\server\mobilesync\types {

    /**
     * @package de\cas\gw\server\mobilesync
     * @subpackage types
     *
     *        Asks for all info about one specific suggest value. Table name and column name must be specified, additionally one combination
     *        of userLanguage+userValue, targetLanguage+targetValue or position[+targetLanguage] must be given.
     */
    class GetSuggestValueRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         */
        public $tableName;

        /**
         * @var string
         *
         */
        public $fieldName;

        /**
         * @var string
         *
         */
        public $queryValue;

        /**
         * @var string
         *
         */
        public $targetLanguage;

    }

}
