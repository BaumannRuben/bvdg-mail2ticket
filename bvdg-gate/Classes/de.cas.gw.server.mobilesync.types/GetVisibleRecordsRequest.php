<?php
// This file has been automatically generated.

namespace de\cas\gw\server\mobilesync\types {

    /**
     * @package de\cas\gw\server\mobilesync
     * @subpackage types
     *
     *        Request to return the records that are currently visible for the state.
     */
    class GetVisibleRecordsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         */
        public $elementType;

        /**
         * @var array
         *
         *                Optional. If empty, column GGUID will be used.
         */
        public $selectColumns;

    }

}
