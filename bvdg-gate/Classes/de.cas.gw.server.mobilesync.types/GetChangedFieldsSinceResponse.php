<?php
// This file has been automatically generated.

namespace de\cas\gw\server\mobilesync\types {

    /**
     * @package de\cas\gw\server\mobilesync
     * @subpackage types
     *
     *        Returns a FieldChanges instance containing the changed fields.
     */
    class GetChangedFieldsSinceResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         */
        public $fieldChanges;

    }

}
