<?php
// This file has been automatically generated.

namespace de\cas\gw\server\mobilesync\types {

    /**
     * @package de\cas\gw\server\mobilesync
     * @subpackage types
     *
     *        Describes a user specific alarm setting.
     */
    class PersonalAlarm {

        /**
         * @var string
         *
         */
        public $tableName;

        /**
         * @var string
         *
         */
        public $tableGUID;

        /**
         * @var string
         *
         */
        public $userGUID;

        /**
         * @var unknown
         *
         */
        public $alarmTimeStamp;

    }

}
