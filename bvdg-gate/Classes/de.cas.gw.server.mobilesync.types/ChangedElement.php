<?php
// This file has been automatically generated.

namespace de\cas\gw\server\mobilesync\types {

    /**
     * @package de\cas\gw\server\mobilesync
     * @subpackage types
     *
     *        Describes the change type of a state element.
     */
    class ChangedElement {

        /**
         * @var string
         *
         */
        public $changeType;

        /**
         * @var \de\cas\gw\server\mobilesync\types\StateElement
         *
         */
        public $stateElement;

    }

}
