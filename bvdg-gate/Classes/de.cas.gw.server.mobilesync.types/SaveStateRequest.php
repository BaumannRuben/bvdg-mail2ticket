<?php
// This file has been automatically generated.

namespace de\cas\gw\server\mobilesync\types {

    /**
     * @package de\cas\gw\server\mobilesync
     * @subpackage types
     *
     *        Save a new state based on the information in the old state identifier and relate all
     *        elements that are passed in as parameter with it.
     */
    class SaveStateRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var \de\cas\gw\server\mobilesync\types\ElementStateIdentifier
         *
         */
        public $oldStateIdentifier;

        /**
         * @var array
         *
         */
        public $elementsKnownForState;

    }

}
