<?php
// This file has been automatically generated.

namespace de\cas\gw\server\mobilesync\types {

    /**
     * @package de\cas\gw\server\mobilesync
     * @subpackage types
     *
     *        Returns a state identifier instance which identifies the newly created state.
     */
    class SaveStateResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\gw\server\mobilesync\types\ElementStateIdentifier
         *
         */
        public $newStateIdentifier;

    }

}
