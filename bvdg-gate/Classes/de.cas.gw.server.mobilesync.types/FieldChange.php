<?php
// This file has been automatically generated.

namespace de\cas\gw\server\mobilesync\types {

    /**
     * @package de\cas\gw\server\mobilesync
     * @subpackage types
     *
     *        Contains a tupel of journal-id, field name and field type.
     */
    class FieldChange {

        /**
         * @var int
         *
         */
        public $journalId;

        /**
         * @var string
         *
         */
        public $fieldName;

        /**
         * @var string
         *
         */
        public $fieldType;

    }

}
