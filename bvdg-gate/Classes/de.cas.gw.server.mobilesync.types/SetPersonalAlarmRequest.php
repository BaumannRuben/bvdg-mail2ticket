<?php
// This file has been automatically generated.

namespace de\cas\gw\server\mobilesync\types {

    /**
     * @package de\cas\gw\server\mobilesync
     * @subpackage types
     *
     *        Sets a user specific alarm on a gW object (appointment, todo, ...).
     */
    class SetPersonalAlarmRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var \de\cas\gw\server\mobilesync\types\PersonalAlarm
         *
         */
        public $personalAlarm;

        /**
         * @var boolean
         *
         */
        public $deleteAlarm;

    }

}
