<?php
// This file has been automatically generated.

namespace de\cas\gw\server\mobilesync\types {

    /**
     * @package de\cas\gw\server\mobilesync
     * @subpackage types
     *
     *        Determines changes that were done to the elements known to the client since the last synchronization.
     *        A ChangeCriteria instance must be supplied to let the change detection know, which changes are relevant for the caller.
     */
    class GetKnownElementsForStateRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var \de\cas\gw\server\mobilesync\types\ElementStateIdentifier
         *
         */
        public $stateIdentifier;

    }

}
