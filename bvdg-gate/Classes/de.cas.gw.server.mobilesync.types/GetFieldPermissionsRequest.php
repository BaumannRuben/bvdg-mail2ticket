<?php
// This file has been automatically generated.

namespace de\cas\gw\server\mobilesync\types {

    /**
     * @package de\cas\gw\server\mobilesync
     * @subpackage types
     *
     *        Reads permissions for the fields of an object type.
     */
    class GetFieldPermissionsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         */
        public $elementType;

        /**
         * @var string
         *
         *                A subtype that further specifies the element type.
         *                For ADDRESS elementType: COMPANY|CONTACT|SINGLEPERSON
         */
        public $addressSubtype;

        /**
         * @var string
         *
         *                OPTIONAL. The GGUID of a record for which the specific field permissions should be returned.
         *                Currently only needed for elementType=ADDRESS, addressSubType=CONTACT
         */
        public $recordGuid;

        /**
         * @var boolean
         *
         */
        public $includeFullAccess;

    }

}
