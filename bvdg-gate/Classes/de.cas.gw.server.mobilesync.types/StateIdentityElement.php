<?php
// This file has been automatically generated.

namespace de\cas\gw\server\mobilesync\types {

    /**
     * @package de\cas\gw\server\mobilesync
     * @subpackage types
     *
     *        A pair of GW and foreign identity and a journal ID.
     */
    class StateIdentityElement {

        /**
         * @var \de\cas\gw\server\mobilesync\types\GWIdentity
         *
         */
        public $gwIdentity;

        /**
         * @var string
         *
         */
        public $foreignIdentity;

        /**
         * @var int
         *
         */
        public $journalId;

    }

}
