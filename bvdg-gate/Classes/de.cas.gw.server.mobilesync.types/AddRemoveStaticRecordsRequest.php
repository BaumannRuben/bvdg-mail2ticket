<?php
// This file has been automatically generated.

namespace de\cas\gw\server\mobilesync\types {

    /**
     * @package de\cas\gw\server\mobilesync
     * @subpackage types
     *
     *        Add record guids to the static records of a user.
     */
    class AddRemoveStaticRecordsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         */
        public $tableName;

        /**
         * @var array
         *
         */
        public $tableGUIDs;

        /**
         * @var boolean
         *
         */
        public $removeRecords;

    }

}
