<?php
// This file has been automatically generated.

namespace de\cas\gw\server\mobilesync\types {

    /**
     * @package de\cas\gw\server\mobilesync
     * @subpackage types
     *
     *        Returns a list of elements that have been changed and that have been determined by the given type of change,
     *        containing the most current journalId and the elementId.
     */
    class GetKnownElementsForStateResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         */
        public $activeSyncMappingElements;

    }

}
