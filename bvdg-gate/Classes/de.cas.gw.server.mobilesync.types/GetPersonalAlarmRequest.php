<?php
// This file has been automatically generated.

namespace de\cas\gw\server\mobilesync\types {

    /**
     * @package de\cas\gw\server\mobilesync
     * @subpackage types
     *
     *        Asks for a user specific alarm on a gW object (appointment, todo, ...). If no user is specified, the alarm for the current user is returned.
     */
    class GetPersonalAlarmRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         */
        public $tableName;

        /**
         * @var string
         *
         */
        public $tableGUID;

        /**
         * @var string
         *
         */
        public $userGUID;

    }

}
