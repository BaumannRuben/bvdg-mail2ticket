<?php
// This file has been automatically generated.

namespace de\cas\gw\server\mobilesync\types {

    /**
     * @package de\cas\gw\server\mobilesync
     * @subpackage types
     *
     *        A tupel of collection ID, element type, client ID and sync key.
     */
    class ElementStateIdentifier extends \de\cas\gw\server\mobilesync\types\StateIdentifier {

        /**
         * @var string
         *
         */
        public $collectionID;

        /**
         * @var string
         *
         */
        public $elementType;

    }

}
