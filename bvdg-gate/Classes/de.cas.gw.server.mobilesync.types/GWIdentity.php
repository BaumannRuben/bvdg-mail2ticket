<?php
// This file has been automatically generated.

namespace de\cas\gw\server\mobilesync\types {

    /**
     * @package de\cas\gw\server\mobilesync
     * @subpackage types
     *
     *        A tupel of element ID and meta information values.
     */
    class GWIdentity {

        /**
         * @var string
         *
         */
        public $elementID;

        /**
         * @var array
         *
         */
        public $metaInfo;

    }

}
