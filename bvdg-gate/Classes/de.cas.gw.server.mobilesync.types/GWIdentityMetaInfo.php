<?php
// This file has been automatically generated.

namespace de\cas\gw\server\mobilesync\types {

    /**
     * @package de\cas\gw\server\mobilesync
     * @subpackage types
     *
     *        A tupel of meta information key and value.
     */
    class GWIdentityMetaInfo {

        /**
         * @var string
         *
         */
        public $key;

        /**
         * @var string
         *
         */
        public $value;

    }

}
