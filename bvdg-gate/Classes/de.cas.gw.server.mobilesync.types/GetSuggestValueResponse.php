<?php
// This file has been automatically generated.

namespace de\cas\gw\server\mobilesync\types {

    /**
     * @package de\cas\gw\server\mobilesync
     * @subpackage types
     *
     *        Returns all info about a specified suggest value.
     */
    class GetSuggestValueResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var string
         *
         */
        public $tableName;

        /**
         * @var string
         *
         */
        public $fieldName;

        /**
         * @var string
         *
         */
        public $queryValue;

        /**
         * @var string
         *
         */
        public $targetLanguage;

        /**
         * @var string
         *
         */
        public $targetValue;

    }

}
