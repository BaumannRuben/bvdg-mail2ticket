<?php
// This file has been automatically generated.

namespace de\cas\gw\server\mobilesync\types {

    /**
     * @package de\cas\gw\server\mobilesync
     * @subpackage types
     *
     *        Describe the changes that are relevant for synchronization.
     */
    class ChangeCriteria {

        /**
         * @var array
         *
         */
        public $relevantFields;

        /**
         * @var boolean
         *
         */
        public $permissionsRelevant;

        /**
         * @var boolean
         *
         */
        public $tagsRelevant;

        /**
         * @var boolean
         *
         */
        public $linksRelevant;

        /**
         * @var boolean
         *
         */
        public $recurrencesRelevant;

    }

}
