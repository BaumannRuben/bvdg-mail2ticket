<?php
// This file has been automatically generated.

namespace de\cas\gw\server\mobilesync\types {

    /**
     * @package de\cas\gw\server\mobilesync
     * @subpackage types
     *
     *        Returns the highest journal id for each of the elements contained in the request.
     *        Elements that cannot be found in the journal are skipped in the response.
     */
    class GetStateForElementsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var array
         *
         */
        public $elementIds;

    }

}
