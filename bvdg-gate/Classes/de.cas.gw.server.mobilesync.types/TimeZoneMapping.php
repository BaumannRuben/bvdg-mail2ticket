<?php
// This file has been automatically generated.

namespace de\cas\gw\server\mobilesync\types {

    /**
     * @package de\cas\gw\server\mobilesync
     * @subpackage types
     *
     *        Describes a time zone with Windows name and Java name.
     */
    class TimeZoneMapping {

        /**
         * @var string
         *
         */
        public $windowsName;

        /**
         * @var string
         *
         */
        public $mapID;

        /**
         * @var string
         *
         */
        public $defaultLocale;

        /**
         * @var string
         *
         */
        public $javaName;

    }

}
