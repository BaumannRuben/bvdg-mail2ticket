<?php
// This file has been automatically generated.

namespace de\cas\gw\server\mobilesync\types {

    /**
     * @package de\cas\gw\server\mobilesync
     * @subpackage types
     *
     *        Returns a user specific alarm on a gW object (appointment, todo, ...).
     */
    class GetPersonalAlarmResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\gw\server\mobilesync\types\PersonalAlarm
         *
         */
        public $personalAlarm;

    }

}
