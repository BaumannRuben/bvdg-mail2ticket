<?php
// This file has been automatically generated.

namespace de\cas\gw\server\mobilesync\types {

    /**
     * @package de\cas\gw\server\mobilesync
     * @subpackage types
     *
     *        A tupel of elements mapping synchronizable elements.
     */
    class ActiveSyncMappingElement {

        /**
         * @var string
         *
         */
        public $recordGuid;

        /**
         * @var string
         *
         */
        public $foreignRecordId;

        /**
         * @var int
         *
         */
        public $journalId;

        /**
         * @var boolean
         *
         */
        public $recurrentEvent;

        /**
         * @var boolean
         *
         */
        public $forceUpdate;

    }

}
