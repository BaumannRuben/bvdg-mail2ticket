<?php
// This file has been automatically generated.

namespace de\cas\gw\server\geodata\types {

    /**
     * @package de\cas\gw\server\geodata
     * @subpackage types
     *
     *        Result object with initial parameters for the area search.
     */
    class GeoDataSessionParams {

        /**
         * @var string
         *
         *                Sets/Returns the  tablename of the data to be displayed in the map.
         *                If the tablename is different from 'ADDRESS', the field 'RelationName'
         *                is mandatory.
         */
        public $TableName;

        /**
         * @var string
         *
         *                Sets/Returns the name of the relation that should be used to find
         *                addresses related to the displayed records.
         */
        public $RelationName;

        /**
         * @var string
         *
         *                Sets/Returns the sql constraint that should be used to qualify the
         *                records that should be displayd.
         */
        public $SqlWhereString;

        /**
         * @var string
         *
         *                Sets/Returns the gguid of a map view in the users navigator
         *                that should be used to initialize the geo session.
         */
        public $ViewGuid;

        /**
         * @var string
         *
         *                Sets/Returns the teamfilter to be used.
         */
        public $Teamfilter;

        /**
         * @var string
         *
         *                Sets/Returns the gui mode.
         */
        public $GuiMode;

    }

}
