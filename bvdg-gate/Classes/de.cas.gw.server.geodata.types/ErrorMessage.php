<?php
// This file has been automatically generated.

namespace de\cas\gw\server\geodata\types {

    /**
     * @package de\cas\gw\server\geodata
     * @subpackage types
     *
     */
    class ErrorMessage {

        /**
         * @var string
         *
         */
        public $ErrorCode;

        /**
         * @var string
         *
         */
        public $AddressCount;

        /**
         * @var string
         *
         */
        public $AddressTypeLabel;

    }

}
