<?php
// This file has been automatically generated.

namespace de\cas\gw\server\geodata\types {

    /**
     * @package de\cas\gw\server\geodata
     * @subpackage types
     *
     *        Describes a rectangle through top-left and bottom-right geocoordinates.
     */
    class InitialMapView {

        /**
         * @var double
         *
         */
        public $TopLeftLatitude;

        /**
         * @var double
         *
         */
        public $TopLeftLongitude;

        /**
         * @var double
         *
         */
        public $BottomRightLatitude;

        /**
         * @var double
         *
         */
        public $BottomRightLongitude;

    }

}
