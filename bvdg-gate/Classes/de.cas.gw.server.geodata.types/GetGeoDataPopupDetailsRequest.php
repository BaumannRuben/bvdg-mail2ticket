<?php
// This file has been automatically generated.

namespace de\cas\gw\server\geodata\types {

    /**
     * @package de\cas\gw\server\geodata
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Gets the content of a cluster-popup.
     *	@see \de\cas\open\server\api\types\RequestObject
     */
    class GetGeoDataPopupDetailsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    Sets/Returns the dataBase for the geodata request.
         */
        public $DataBase;

        /**
         * @var int
         *
         *                    Sets/Returns the geoSessionId for the geodata request.
         */
        public $GeoSessionId;

        /**
         * @var string
         *
         *                    Sets/Returns the AddressId for the geodata request.
         */
        public $AddressId;

    }

}
