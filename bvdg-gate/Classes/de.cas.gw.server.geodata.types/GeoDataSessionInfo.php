<?php
// This file has been automatically generated.

namespace de\cas\gw\server\geodata\types {

    /**
     * @package de\cas\gw\server\geodata
     * @subpackage types
     *
     *        Result object with initial parameters for the area search.
     */
    class GeoDataSessionInfo {

        /**
         * @var int
         *
         *                Sets/Returns the ID of the newly created geo session.
         */
        public $SessionId;

        /**
         * @var string
         *
         *                Sets/Returns the relative URL under which a webpage for displaying
         *                the map content can be found.
         */
        public $Url;

    }

}
