<?php
// This file has been automatically generated.

namespace de\cas\gw\server\geodata\types {

    /**
     * @package de\cas\gw\server\geodata
     * @subpackage types
     *
     *        Contact Information.
     */
    class Contact {

        /**
         * @var \de\cas\gw\server\geodata\types\Location
         *
         */
        public $Location;

        /**
         * @var string
         *
         */
        public $Label;

        /**
         * @var string
         *
         */
        public $ContactType;

        /**
         * @var string
         *
         */
        public $Company;

        /**
         * @var string
         *
         */
        public $ContactDetailLink;

        /**
         * @var string
         *
         */
        public $RouteLink;

    }

}
