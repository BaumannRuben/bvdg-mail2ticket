<?php
// This file has been automatically generated.

namespace de\cas\gw\server\geodata\types {

    /**
     * @package de\cas\gw\server\geodata
     * @subpackage types
     *
     *        Object which defines a category of a cluster.
     */
    class ClusterCategory {

        /**
         * @var int
         *
         */
        public $Id;

        /**
         * @var string
         *
         */
        public $Label;

        /**
         * @var double
         *
         */
        public $Opacity;

        /**
         * @var string
         *
         */
        public $Color;

        /**
         * @var int
         *
         */
        public $BorderWidth;

        /**
         * @var string
         *
         */
        public $BorderColor;

    }

}
