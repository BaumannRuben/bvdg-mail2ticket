<?php
// This file has been automatically generated.

namespace de\cas\gw\server\geodata\types {

    /**
     * @package de\cas\gw\server\geodata
     * @subpackage types
     *
     */
    class AddressType {

        /**
         * @var string
         *
         */
        public $Label;

        /**
         * @var string
         *
         */
        public $Id;

    }

}
