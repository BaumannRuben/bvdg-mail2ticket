<?php
// This file has been automatically generated.

namespace de\cas\gw\server\geodata\types {

    /**
     * @package de\cas\gw\server\geodata
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Gets the initialization parameters for the circumcircle search.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: GetGeoDataInitializationParametersResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetGeoDataInitializationParametersResponse
     */
    class GetGeoDataInitializationParametersRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    Sets/Returns the dataBase for the geodata request.
         */
        public $DataBase;

        /**
         * @var int
         *
         *                    Sets/Returns the geoSessionId for the geodata request.
         */
        public $GeoSessionId;

    }

}
