<?php
// This file has been automatically generated.

namespace de\cas\gw\server\geodata\types {

    /**
     * @package de\cas\gw\server\geodata
     * @subpackage types
     *
     *        Result object with additional information to be displayed in cluster-popups.
     */
    class PopupDetailResult {

        /**
         * @var string
         *
         */
        public $Label;

        /**
         * @var string
         *
         */
        public $Company;

        /**
         * @var string
         *
         */
        public $Street;

        /**
         * @var string
         *
         */
        public $City;

        /**
         * @var boolean
         *
         */
        public $HighAccuracy;

        /**
         * @var string
         *
         */
        public $ContactType;

        /**
         * @var string
         *
         */
        public $ContactDetailLink;

        /**
         * @var string
         *
         */
        public $RouteLink;

    }

}
