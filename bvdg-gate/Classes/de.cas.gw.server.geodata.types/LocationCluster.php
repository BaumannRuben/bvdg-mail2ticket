<?php
// This file has been automatically generated.

namespace de\cas\gw\server\geodata\types {

    /**
     * @package de\cas\gw\server\geodata
     * @subpackage types
     *
     */
    class LocationCluster {

        /**
         * @var double
         *
         */
        public $Latitude;

        /**
         * @var double
         *
         */
        public $Longitude;

        /**
         * @var boolean
         *
         */
        public $IsWithinRadius;

    }

}
