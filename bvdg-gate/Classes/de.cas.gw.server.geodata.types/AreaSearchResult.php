<?php
// This file has been automatically generated.

namespace de\cas\gw\server\geodata\types {

    /**
     * @package de\cas\gw\server\geodata
     * @subpackage types
     *
     *        Result object which contains the Contacts and Clusters in the defined Area.
     */
    class AreaSearchResult {

        /**
         * @var array
         *
         *            User-defined categories into which clusters are classified.
         */
        public $ClusterCategories;

        /**
         * @var string
         *
         *            Headline to be displayed on the category explanation.
         */
        public $ExplanationHeader;

        /**
         * @var array
         *
         *            List of Clusters to be displayed in the map.
         */
        public $ContactClusters;

        /**
         * @var string
         *
         *            Link to be opened to display the result of the area search.
         */
        public $ListLink;

        /**
         * @var string
         *
         *            Warning message to be displayed.
         */
        public $WarningMessage;

        /**
         * @var array
         *
         *            Error messages to be displayed.
         */
        public $ErrorMessages;

        /**
         * @var int
         *
         *            Result count of the area search.
         */
        public $AddressesWithinRadius;

        /**
         * @var string
         *
         *            URL of the heatmap tiles.
         */
        public $TileUrl;

    }

}
