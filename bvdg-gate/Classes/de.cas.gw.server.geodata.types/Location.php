<?php
// This file has been automatically generated.

namespace de\cas\gw\server\geodata\types {

    /**
     * @package de\cas\gw\server\geodata
     * @subpackage types
     *
     *        Geo Location information for one address. Street and City are for tooltip information if it is a Cluster they are not used
     */
    class Location {

        /**
         * @var double
         *
         */
        public $Latitude;

        /**
         * @var double
         *
         */
        public $Longitude;

        /**
         * @var string
         *
         */
        public $Street;

        /**
         * @var string
         *
         */
        public $City;

        /**
         * @var boolean
         *
         */
        public $HighAccuracy;

        /**
         * @var boolean
         *
         */
        public $IsWithinRadius;

    }

}
