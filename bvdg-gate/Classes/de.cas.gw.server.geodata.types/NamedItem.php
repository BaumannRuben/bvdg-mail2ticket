<?php
// This file has been automatically generated.

namespace de\cas\gw\server\geodata\types {

    /**
     * @package de\cas\gw\server\geodata
     * @subpackage types
     *
     *        Sequence of Named items suitable for all kinds of physical name - display name pairs.
     *        Each item consists of the Id, the physical name of the field, and the label, the display name of the field.
     */
    class NamedItem {

        /**
         * @var string
         *
         */
        public $Label;

        /**
         * @var string
         *
         */
        public $Id;

    }

}
