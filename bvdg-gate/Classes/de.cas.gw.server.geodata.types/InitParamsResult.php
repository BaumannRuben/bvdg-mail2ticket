<?php
// This file has been automatically generated.

namespace de\cas\gw\server\geodata\types {

    /**
     * @package de\cas\gw\server\geodata
     * @subpackage types
     *
     *        Result object with initial parameters for the area search.
     */
    class InitParamsResult {

        /**
         * @var string
         *
         */
        public $GuiMode;

        /**
         * @var string
         *
         */
        public $Locale;

        /**
         * @var string
         *
         */
        public $YMApiKey;

        /**
         * @var \de\cas\gw\server\geodata\types\SearchCenter
         *
         */
        public $SearchCenter;

        /**
         * @var \de\cas\gw\server\geodata\types\InitialMapView
         *
         */
        public $InitialMapView;

        /**
         * @var string
         *
         */
        public $WarningMessage;

        /**
         * @var array
         *
         */
        public $ErrorMessages;

        /**
         * @var double
         *
         */
        public $InitialRadius;

        /**
         * @var int
         *
         */
        public $CountInputAddresses;

        /**
         * @var array
         *
         */
        public $Links;

        /**
         * @var string
         *
         */
        public $LinkId;

        /**
         * @var string
         *
         */
        public $SourceDataType;

        /**
         * @var string
         *
         */
        public $SourceDataTypeLabel;

        /**
         * @var string
         *
         */
        public $AggrFunc;

        /**
         * @var array
         *
         */
        public $AggrFields;

        /**
         * @var string
         *
         */
        public $AggrFieldId;

        /**
         * @var string
         *
         */
        public $ViewType;

        /**
         * @var array
         *
         */
        public $Views;

        /**
         * @var string
         *
         */
        public $ViewId;

    }

}
