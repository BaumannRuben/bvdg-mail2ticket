<?php
// This file has been automatically generated.

namespace de\cas\gw\server\geodata\types {

    /**
     * @package de\cas\gw\server\geodata
     * @subpackage types
     *
     */
    class GeoDataSearchInputParameter {

        /**
         * @var double
         *
         */
        public $TopLeftLatitude;

        /**
         * @var double
         *
         */
        public $TopLeftLongitude;

        /**
         * @var double
         *
         */
        public $BottomRightLatitude;

        /**
         * @var double
         *
         */
        public $BottomRightLongitude;

        /**
         * @var double
         *
         */
        public $SearchCenterLatitude;

        /**
         * @var double
         *
         */
        public $SearchCenterLongitude;

        /**
         * @var double
         *
         */
        public $Radius;

        /**
         * @var int
         *
         */
        public $ZoomLevel;

        /**
         * @var int
         *
         */
        public $ClusterLevel;

        /**
         * @var string
         *
         */
        public $ClusterId;

        /**
         * @var string
         *
         */
        public $LinkId;

        /**
         * @var string
         *
         */
        public $AggrFunc;

        /**
         * @var string
         *
         */
        public $AggrFieldId;

        /**
         * @var string
         *
         */
        public $ViewType;

        /**
         * @var string
         *
         */
        public $ViewId;

    }

}
