<?php
// This file has been automatically generated.

namespace de\cas\gw\server\geodata\types {

    /**
     * @package de\cas\gw\server\geodata
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Gets the content of a cluster-popup.
     *	@see \de\cas\open\server\api\types\ResponseObject
     */
    class GetGeoDataPopupDetailsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\gw\server\geodata\types\PopupDetailResult
         *
         *                    Sets/Returns the popup parameters result.
         */
        public $PopupDetailResult;

    }

}
