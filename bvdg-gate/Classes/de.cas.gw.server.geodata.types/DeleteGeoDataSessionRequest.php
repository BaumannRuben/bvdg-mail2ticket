<?php
// This file has been automatically generated.

namespace de\cas\gw\server\geodata\types {

    /**
     * @package de\cas\gw\server\geodata
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Ends a geo session.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: DeleteGeoDataSessionResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see DeleteGeoDataSessionResponse
     */
    class DeleteGeoDataSessionRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var int
         *
         */
        public $GeoSessionId;

    }

}
