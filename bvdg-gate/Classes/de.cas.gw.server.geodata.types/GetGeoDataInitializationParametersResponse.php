<?php
// This file has been automatically generated.

namespace de\cas\gw\server\geodata\types {

    /**
     * @package de\cas\gw\server\geodata
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Gets the initialization parameters for the circumcircle search.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: GetGeoDataInitializationParametersRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetGeoDataInitializationParametersRequest
     */
    class GetGeoDataInitializationParametersResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\gw\server\geodata\types\InitParamsResult
         *
         *                    Sets/Returns the initialization parameters result.
         */
        public $InitParamsResult;

    }

}
