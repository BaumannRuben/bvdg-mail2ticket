<?php
// This file has been automatically generated.

namespace de\cas\gw\server\geodata\types {

    /**
     * @package de\cas\gw\server\geodata
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Gets the contacts in the circumcircle.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: GetGeoDataContactsInAreaResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetGeoDataContactsInAreaResponse
     */
    class GetGeoDataHeatmapRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    Sets/Returns the dataBase for the geodata request.
         */
        public $DataBase;

        /**
         * @var int
         *
         *                    Sets/Returns the geoSessionId for the geodata request.
         */
        public $GeoSessionId;

        /**
         * @var string
         *
         *                    Sets/Returns the TileID for the geodata request.
         */
        public $TileId;

        /**
         * @var string
         *
         *                    Sets/Returns the ViewId for the geodata request.
         */
        public $ViewId;

        /**
         * @var string
         *
         *                    Sets/Returns the aggregation function for the geodata request.
         */
        public $AggrFunc;

        /**
         * @var string
         *
         *                    Sets/Returns the hash for other parameters of the geodata request.
         */
        public $ParamHash;

    }

}
