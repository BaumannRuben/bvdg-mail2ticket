<?php
// This file has been automatically generated.

namespace de\cas\open\server\opportunities\types {

    /**
     * @package de\cas\open\server\opportunities
     * @subpackage types
     *
     *		    \de\cas\open\server\api\types\RequestObject for the businessoperation that returns all
     *		    opportunitypositions to a given opportunity.
     *		    Corresponding \de\cas\open\server\api\types\ResponseObject: GetOpportunityPositionsResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetOpportunityPositionsResponse
     */
    class GetOpportunityPositionsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										Sets/Returns the GGUID of the opportunity whose
         *										positions shall be returned.
         */
        public $gguidOfOpportunity;

        /**
         * @var array
         *
         *										Returns the columns used to sort the result (must be of
         *										same size as {@link #positionsOrderByTypes}.
         */
        public $positionsOrderByColumns;

        /**
         * @var array
         *
         *										Returns the directions used to sort the result (must be
         *										of same size as {@link #positionsOrderByColumns}.
         */
        public $positionsOrderByTypes;

    }

}
