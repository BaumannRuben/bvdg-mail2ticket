<?php
// This file has been automatically generated.

namespace de\cas\open\server\opportunities\types {

    /**
     * @package de\cas\open\server\opportunities
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject for re-calculating fields of a opportunity position
     *        when a field changes. Note that the request does not persist the opportunity position.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: CalculateOpportunityPositionValuesResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see CalculateOpportunityPositionValuesResponse
     */
    class CalculateOpportunityPositionValuesRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var array
         *
         *                    Sets/Returns the opportunity position.
         */
        public $opportunityPositions;

        /**
         * @var string
         *
         *                    Sets/Returns the name of the changed field.
         */
        public $changedFieldName;

        /**
         * @var double
         *
         *                    Sets/Returns the new value of the changed field.
         */
        public $newFieldValue;

        /**
         * @var string
         *
         *                    Sets/Returns the guid of the address the opportunity is related to.
         */
        public $addressGuid;

    }

}
