<?php
// This file has been automatically generated.

namespace de\cas\open\server\opportunities\types {

    /**
     * @package de\cas\open\server\opportunities
     * @subpackage types
     *
     *		    \de\cas\open\server\api\types\ResponseObject for the businessoperation that returns all
     *		    opportunitypositions to a given opportunity.
     *		    Corresponding \de\cas\open\server\api\types\RequestObject: GetOpportunityPositionsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetOpportunityPositionsRequest
     */
    class SaveOpportunityResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\DataObjectTransferable
         *
         *										Sets/Returns the saved opportunity.
         */
        public $opportunity;

        /**
         * @var \de\cas\open\server\api\types\MassQueryResult
         *
         *										Sets/Returns the resultobject containing the saved
         *										opportunitypositions.
         */
        public $positions;

    }

}
