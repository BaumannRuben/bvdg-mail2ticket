<?php
// This file has been automatically generated.

namespace de\cas\open\server\opportunities\types {

    /**
     * @package de\cas\open\server\opportunities
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Gets the list of products that can be added
     *        as positions to an opportunity.
     *        Corresponding \de\cas\open\server\api\types\RequestObject:
     *        GetOpportunityPositionSelectionListRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetOpportunityPositionSelectionListRequest
     */
    class GetOpportunityPositionSelectionListResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\MassQueryResult
         *
         *                    The query result
         */
        public $QueryResult;

    }

}
