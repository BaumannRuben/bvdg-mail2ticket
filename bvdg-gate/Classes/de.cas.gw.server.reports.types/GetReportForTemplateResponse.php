<?php
// This file has been automatically generated.

namespace de\cas\gw\server\reports\types {

    /**
     * @package de\cas\gw\server\reports
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Returns the report for the given report template.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: GetReportForTemplateRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetReportForTemplateRequest
     */
    class GetReportForTemplateResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var unknown
         *
         *                    The report as byte stream.
         */
        public $Report;

        /**
         * @var string
         *
         *                    The file format of the report.
         */
        public $ReportFileFormat;

    }

}
