<?php
// This file has been automatically generated.

namespace de\cas\gw\server\reports\types {

    /**
     * @package de\cas\gw\server\reports
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Returns the report for the given report template.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: GetReportForTemplateResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetReportForTemplateResponse
     */
    class GetReportForTemplateRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    The guid of the record for with a report should be generated.
         */
        public $RecordGuid;

        /**
         * @var string
         *
         *                    The guid of the template to use for the report.
         */
        public $TemplateGuid;

    }

}
