<?php
// This file has been automatically generated.

namespace de\cas\gw\server\reports\types {

    /**
     * @package de\cas\gw\server\reports
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Returns the report that the specified xml describes.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: GetReportFromXmlResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetReportFromXmlResponse
     */
    class GetReportFromXmlRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    Describes the parameters of the report.
         */
        public $XmlDescription;

    }

}
