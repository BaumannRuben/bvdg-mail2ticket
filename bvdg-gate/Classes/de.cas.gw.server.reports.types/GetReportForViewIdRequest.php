<?php
// This file has been automatically generated.

namespace de\cas\gw\server\reports\types {

    /**
     * @package de\cas\gw\server\reports
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Returns the report for the specified view id.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: GetReportForViewIdResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetReportForViewIdResponse
     */
    class GetReportForViewIdRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    The view id of the requested report.
         */
        public $ViewId;

    }

}
