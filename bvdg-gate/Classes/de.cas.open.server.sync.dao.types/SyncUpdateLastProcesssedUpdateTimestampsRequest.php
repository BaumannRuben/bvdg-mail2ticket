<?php
// This file has been automatically generated.

namespace de\cas\open\server\sync\dao\types {

    /**
     * @package de\cas\open\server\sync
     * @subpackage dao\types
     *
     */
    class SyncUpdateLastProcesssedUpdateTimestampsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var \de\cas\open\server\sync\dao\transferables\TransferableSyncSessionContext
         *
         */
        public $syncSessionContext;

        /**
         * @var array
         *
         */
        public $newUpdateTimestamps;

    }

}
