<?php
// This file has been automatically generated.

namespace de\cas\open\server\sync\dao\types {

    /**
     * @package de\cas\open\server\sync
     * @subpackage dao\types
     *
     */
    class SyncGetSyncJobsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         */
        public $leftInstanceId;

        /**
         * @var string
         *
         */
        public $rightInstanceId;

    }

}
