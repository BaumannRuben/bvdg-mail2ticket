<?php
// This file has been automatically generated.

namespace de\cas\open\server\sync\dao\types {

    /**
     * @package de\cas\open\server\sync
     * @subpackage dao\types
     *
     */
    class SyncGetFailedIdsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         */
        public $failedIds;

    }

}
