<?php
// This file has been automatically generated.

namespace de\cas\open\server\sync\dao\types {

    /**
     * @package de\cas\open\server\sync
     * @subpackage dao\types
     *
     */
    class SyncGetPairsByIDsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var array
         *
         */
        public $syncConfigurationIds;

        /**
         * @var array
         *
         */
        public $leftIds;

        /**
         * @var array
         *
         */
        public $rightIds;

        /**
         * @var \de\cas\open\server\sync\dao\transferables\TransferableSyncSessionContext
         *
         */
        public $syncSessionContext;

    }

}
