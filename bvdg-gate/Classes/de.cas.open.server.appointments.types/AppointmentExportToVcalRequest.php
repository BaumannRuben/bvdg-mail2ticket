<?php
// This file has been automatically generated.

namespace de\cas\open\server\appointments\types {

    /**
     * @package de\cas\open\server\appointments
     * @subpackage types
     *\de\cas\open\server\api\types\RequestObject:<br/> TODO [Albina.Pace]: Purpose of the business logic.
     *				<p>Possible fault codes explicitly thrown by this operation are:
     *					<ul>
     *					<li>NO_EXCEPTION_CODE - no explanation</li>
     *					</ul>
     *				</p>
     *				Corresponding \de\cas\open\server\api\types\ResponseObject: AppointmentExportToVcalResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see AppointmentExportToVcalResponse
     */
    class AppointmentExportToVcalRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var \de\cas\open\server\api\types\DataObjectTransferable
         *
         *										The appointment to export to vcal.
         */
        public $appointment;

        /**
         * @var boolean
         *
         *										If set to true, indicates a cancelled appointment,
         *										otherwise a request will be sent.
         */
        public $cancelled;

        /**
         * @var boolean
         *
         *										If set to true, the whole series is exported, otherwise
         *										just the given appointment.
         */
        public $all;

    }

}
