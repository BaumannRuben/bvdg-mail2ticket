<?php
// This file has been automatically generated.

namespace de\cas\open\server\appointments\types {

    /**
     * @package de\cas\open\server\appointments
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Gets appointments extended by the given columns.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: GetExtendedAppointmentsResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetExtendedAppointmentsResponse
     */
    class GetExtendedAppointmentsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var unknown
         *
         *                    Represents the lower bound of the result.
         *                    "... WHERE START_DT >= {startDT} ..."
         */
        public $startDT;

        /**
         * @var unknown
         *
         *                    Represents the upper bound of the result.
         *                    "... AND END_DT <= {endDT} ..."
         */
        public $endDT;

        /**
         * @var array
         *
         *                    TeamFilter entries. If not set, all appointments where the current user has sufficient access rights are shown.
         */
        public $teamFilter;

        /**
         * @var array
         *
         *                    Columns that should be included in the result additional to the standard columns that are provided.
         */
        public $columns;

    }

}
