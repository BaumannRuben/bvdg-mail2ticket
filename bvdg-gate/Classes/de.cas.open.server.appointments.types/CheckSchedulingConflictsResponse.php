<?php
// This file has been automatically generated.

namespace de\cas\open\server\appointments\types {

    /**
     * @package de\cas\open\server\appointments
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\ResponseObject: Checks for overlapping appointments in a given time
     *				period for a number of given users. Will return a list of conflicting
     *				appointments if there are any. Corresponding \de\cas\open\server\api\types\RequestObject: CheckSchedulingConflictsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see CheckSchedulingConflictsRequest
     */
    class CheckSchedulingConflictsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\MassQueryResult
         *
         *										MassQueryResult containing conflicting appointments
         */
        public $conflicts;

    }

}
