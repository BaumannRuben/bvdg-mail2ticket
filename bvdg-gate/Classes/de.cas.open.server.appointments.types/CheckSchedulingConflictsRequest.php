<?php
// This file has been automatically generated.

namespace de\cas\open\server\appointments\types {

    /**
     * @package de\cas\open\server\appointments
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\RequestObject: Checks for overlapping appointments in a given time
     *				period for a number of given users. Will return a list of conflicting
     *				appointments if there are any. Corresponding \de\cas\open\server\api\types\ResponseObject: CheckSchedulingConflictsResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see CheckSchedulingConflictsResponse
     */
    class CheckSchedulingConflictsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var unknown
         *
         *										Start date of the period to be checked (start date of
         *										the appointment)
         */
        public $startDateTime;

        /**
         * @var unknown
         *
         *										End date of the period to be checked (end date of the
         *										appointment)
         */
        public $endDateTime;

        /**
         * @var array
         *
         *										GUID of the activity, should be examined at the intersections. A single GUID can be passed as a binary or string, as comma-separated string several GUIDs, eg.: '0x273846B278364782A678F34561,0x273846B278364782A678F34561'
         */
        public $currentGUID;

        /**
         * @var array
         *
         *										IDs of the participants of the appointment
         */
        public $participantIDs;

        /**
         * @var array
         *
         *										TeamFilter entries.
         */
        public $teamFilter;

        /**
         * @var array
         *
         *										Additional columnnames can be applied which will be
         *										included in the query-result which will be contained in
         *										the Response.
         */
        public $columnNamesToBeIncludedInResponse;

    }

}
