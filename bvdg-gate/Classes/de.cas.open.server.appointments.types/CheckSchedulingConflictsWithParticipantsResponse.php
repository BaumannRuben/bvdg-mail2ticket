<?php
// This file has been automatically generated.

namespace de\cas\open\server\appointments\types {

    /**
     * @package de\cas\open\server\appointments
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Checks for overlapping appointments in a given time
     *        period for a number of given users. Will return a list of conflicting
     *        appointments if there are any, including all participants of an appointment.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: CheckSchedulingConflictsWithParticipantsRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see CheckSchedulingConflictsWithParticipantsRequest
     */
    class CheckSchedulingConflictsWithParticipantsResponse extends \de\cas\open\server\appointments\types\CheckSchedulingConflictsResponse {

    }

}
