<?php
// This file has been automatically generated.

namespace de\cas\open\server\appointments\types {

    /**
     * @package de\cas\open\server\appointments
     * @subpackage types
     *\de\cas\open\server\api\types\RequestObject:<br/> TODO [Albina.Pace]: Imports a Vcal appointment by parsing the given byte array.
     *				<p>Possible fault codes explicitly thrown by this operation are:
     *					<ul>
     *					<li>NO_EXCEPTION_CODE - no explanation</li>
     *					</ul>
     *				</p>
     *				Corresponding \de\cas\open\server\api\types\ResponseObject: AppointmentImportFromVCalResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see AppointmentImportFromVCalResponse
     */
    class AppointmentImportFromVCalRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var unknown
         *
         *										Sets/Returns the content of the vcard.
         */
        public $vcalContent;

        /**
         * @var string
         *The charset to use.
         */
        public $charset;

        /**
         * @var boolean
         *
         *										If true and if foreignEditPermissionRestriction is null,
         *										foreignEditPermissionRestriction is set to PRIVATE.
         *										DEPRECATED, please use foreignEditPermissionRestriction.
         */
        public $private;

        /**
         * @var string
         *
         *										Sets the fepr of created appointments to this value.
         *										There is one exception to this rule: if the property
         *										PRIVATE is set in the iCal file.
         */
        public $foreignEditPermissionRestriction;

        /**
         * @var boolean
         *
         *										If true, the iCal file is only parsed, but the created appointment isn't persistent.
         */
        public $parseOnly;

    }

}
