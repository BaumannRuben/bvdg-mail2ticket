<?php
// This file has been automatically generated.

namespace de\cas\open\server\appointments\types {

    /**
     * @package de\cas\open\server\appointments
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\RequestObject: Searches for a free time period of a given length in a
     *				given timespan for a list of given users. Returns matching time periods found
     *				and a proposed appointment. Corresponding \de\cas\open\server\api\types\ResponseObject: SearchAvailableTimeSlotsResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see SearchAvailableTimeSlotsResponse
     */
    class SearchAvailableTimeSlotsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var double
         *
         *										The duration of the time period to be found
         */
        public $duration;

        /**
         * @var array
         *
         *										List of OIDs of the participants
         */
        public $participantIDs;

        /**
         * @var unknown
         *
         *										Start date of the period to be searched
         */
        public $startOfSearchPeriod;

        /**
         * @var unknown
         *
         *										End date of the period to be searched
         */
        public $endOfSearchPeriod;

        /**
         * @var unknown
         *
         *										Start time of the timespan that will be searched per
         *										day
         */
        public $startOfTimespanPerDay;

        /**
         * @var unknown
         *
         *										End time of the timespan that will be searched per day
         */
        public $endOfTimespanPerDay;

        /**
         * @var boolean
         *
         *										Flag that indicates if holidays (not to be confused
         *										with vacations) are taken into account
         */
        public $checkPublicHolidays;

        /**
         * @var boolean
         *
         *										Flag that indicates if weekends are taken into account
         */
        public $checkWeekends;

        /**
         * @var array
         *
         *										List of OIDs of which one hast to be available for a
         *										time slot.
         */
        public $oneIsMandatoryOIDList;

    }

}
