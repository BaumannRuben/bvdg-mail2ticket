<?php
// This file has been automatically generated.

namespace de\cas\open\server\appointments\types {

    /**
     * @package de\cas\open\server\appointments
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Gets appointments extended by the given columns.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: GetExtendedAppointmentsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetExtendedAppointmentsRequest
     */
    class GetExtendedAppointmentsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\MassQueryResult
         *
         *                    Appointments which are extended by some virtual fields.
         */
        public $appointments;

    }

}
