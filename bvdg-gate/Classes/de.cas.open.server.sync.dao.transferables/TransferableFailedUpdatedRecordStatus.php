<?php
// This file has been automatically generated.

namespace de\cas\open\server\sync\dao\transferables {

    /**
     * @package de\cas\open\server\sync
     * @subpackage dao\transferables
     *
     */
    class TransferableFailedUpdatedRecordStatus extends \de\cas\open\server\sync\dao\transferables\TransferableFailedStatus {

        /**
         * @var string
         *
         */
        public $ownId;

    }

}
