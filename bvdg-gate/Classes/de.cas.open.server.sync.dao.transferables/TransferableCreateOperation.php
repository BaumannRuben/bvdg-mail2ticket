<?php
// This file has been automatically generated.

namespace de\cas\open\server\sync\dao\transferables {

    /**
     * @package de\cas\open\server\sync
     * @subpackage dao\transferables
     *
     */
    class TransferableCreateOperation extends \de\cas\open\server\sync\dao\transferables\TransferableRecordOperation {

        /**
         * @var \de\cas\open\server\sync\dao\transferables\TransferableRecordData
         *
         */
        public $recordData;

        /**
         * @var array
         *
         */
        public $outgoingLinks;

        /**
         * @var array
         *
         */
        public $incomingLinks;

        /**
         * @var array
         *
         */
        public $dependentCreateOperations;

    }

}
