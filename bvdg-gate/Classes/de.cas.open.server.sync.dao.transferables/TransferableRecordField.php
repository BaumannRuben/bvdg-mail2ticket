<?php
// This file has been automatically generated.

namespace de\cas\open\server\sync\dao\transferables {

    /**
     * @package de\cas\open\server\sync
     * @subpackage dao\transferables
     *
     */
    class TransferableRecordField {

        /**
         * @var string
         *
         */
        public $fieldName;

        /**
         * @var unknown
         *
         */
        public $fieldValue;

    }

}
