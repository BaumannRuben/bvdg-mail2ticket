<?php
// This file has been automatically generated.

namespace de\cas\open\server\sync\dao\transferables {

    /**
     * @package de\cas\open\server\sync
     * @subpackage dao\transferables
     *
     */
    class TransferableFailedPair {

        /**
         * @var string
         *
         */
        public $recordId;

        /**
         * @var string
         *
         */
        public $recordChecksum;

        /**
         * @var string
         *
         */
        public $foreignRecordId;

        /**
         * @var string
         *
         */
        public $foreignRecordChecksum;

        /**
         * @var string
         *
         */
        public $failureType;

        /**
         * @var string
         *
         */
        public $failureMessage;

        /**
         * @var array
         *
         */
        public $syncViewIds;

        /**
         * @var array
         *
         */
        public $additionalData;

    }

}
