<?php
// This file has been automatically generated.

namespace de\cas\open\server\sync\dao\transferables {

    /**
     * @package de\cas\open\server\sync
     * @subpackage dao\transferables
     *
     */
    class TransferableSyncConfiguration {

        /**
         * @var string
         *
         */
        public $configurationId;

        /**
         * @var \de\cas\open\server\sync\dao\transferables\TransferableSyncViewDefinition
         *
         */
        public $leftViewDefinition;

        /**
         * @var \de\cas\open\server\sync\dao\transferables\TransferableSyncViewDefinition
         *
         */
        public $rightViewDefinition;

        /**
         * @var string
         *
         */
        public $description;

        /**
         * @var int
         *
         */
        public $direction;

        /**
         * @var int
         *
         */
        public $oid;

    }

}
