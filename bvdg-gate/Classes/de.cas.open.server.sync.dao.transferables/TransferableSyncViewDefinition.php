<?php
// This file has been automatically generated.

namespace de\cas\open\server\sync\dao\transferables {

    /**
     * @package de\cas\open\server\sync
     * @subpackage dao\transferables
     *
     */
    class TransferableSyncViewDefinition {

        /**
         * @var string
         *
         */
        public $syncViewDefinitionId;

        /**
         * @var string
         *
         */
        public $recordType;

        /**
         * @var string
         *
         */
        public $filter;

        /**
         * @var array
         *
         */
        public $fields;

        /**
         * @var string
         *
         */
        public $lastProcessedUpdateTimestamp;

        /**
         * @var boolean
         *
         */
        public $deletedRecordsAware;

        /**
         * @var string
         *
         */
        public $displayName;

    }

}
