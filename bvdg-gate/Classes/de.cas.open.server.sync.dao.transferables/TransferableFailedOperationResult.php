<?php
// This file has been automatically generated.

namespace de\cas\open\server\sync\dao\transferables {

    /**
     * @package de\cas\open\server\sync
     * @subpackage dao\transferables
     *
     */
    class TransferableFailedOperationResult {

        /**
         * @var string
         *
         */
        public $recordGuid;

        /**
         * @var string
         *
         */
        public $failureMessage;

        /**
         * @var string
         *
         */
        public $failureType;

    }

}
