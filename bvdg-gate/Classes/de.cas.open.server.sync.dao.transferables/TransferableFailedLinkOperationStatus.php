<?php
// This file has been automatically generated.

namespace de\cas\open\server\sync\dao\transferables {

    /**
     * @package de\cas\open\server\sync
     * @subpackage dao\transferables
     *
     */
    class TransferableFailedLinkOperationStatus extends \de\cas\open\server\sync\dao\transferables\TransferableFailedStatus {

        /**
         * @var \de\cas\open\server\sync\dao\transferables\TransferableLink
         *
         */
        public $link;

    }

}
