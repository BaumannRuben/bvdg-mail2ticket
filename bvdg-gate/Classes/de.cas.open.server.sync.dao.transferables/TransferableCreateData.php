<?php
// This file has been automatically generated.

namespace de\cas\open\server\sync\dao\transferables {

    /**
     * @package de\cas\open\server\sync
     * @subpackage dao\transferables
     *
     */
    class TransferableCreateData {

        /**
         * @var string
         *
         */
        public $externalRecordId;

        /**
         * @var array
         *
         */
        public $recordFields;

        /**
         * @var array
         *
         */
        public $additionalData;

        /**
         * @var array
         *
         */
        public $syncViewDefinitions;

    }

}
