<?php
// This file has been automatically generated.

namespace de\cas\open\server\sync\dao\transferables {

    /**
     * @package de\cas\open\server\sync
     * @subpackage dao\transferables
     *
     */
    class TransferableUpdateOperation extends \de\cas\open\server\sync\dao\transferables\TransferableRecordOperation {

        /**
         * @var \de\cas\open\server\sync\dao\transferables\TransferableRecordId
         *
         */
        public $recordId;

        /**
         * @var \de\cas\open\server\sync\dao\transferables\TransferableRecordData
         *
         */
        public $recordData;

    }

}
