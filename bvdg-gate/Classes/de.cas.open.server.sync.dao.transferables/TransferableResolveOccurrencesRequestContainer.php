<?php
// This file has been automatically generated.

namespace de\cas\open\server\sync\dao\transferables {

    /**
     * @package de\cas\open\server\sync
     * @subpackage dao\transferables
     *
     */
    class TransferableResolveOccurrencesRequestContainer {

        /**
         * @var \de\cas\open\server\sync\dao\transferables\TransferableRecordId
         *
         */
        public $recordId;

        /**
         * @var int
         *
         */
        public $userId;

    }

}
