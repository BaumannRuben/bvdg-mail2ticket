<?php
// This file has been automatically generated.

namespace de\cas\open\server\sync\dao\transferables {

    /**
     * @package de\cas\open\server\sync
     * @subpackage dao\transferables
     *
     */
    class TransferableDeleteData {

        /**
         * @var \de\cas\open\server\sync\dao\transferables\TransferableRecordId
         *
         */
        public $recordId;

        /**
         * @var \de\cas\open\server\sync\dao\transferables\TransferableRecordId
         *
         */
        public $foreignRecordId;

        /**
         * @var array
         *
         */
        public $syncViewDefinitions;

    }

}
