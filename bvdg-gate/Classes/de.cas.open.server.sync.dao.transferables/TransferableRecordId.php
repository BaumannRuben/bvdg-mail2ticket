<?php
// This file has been automatically generated.

namespace de\cas\open\server\sync\dao\transferables {

    /**
     * @package de\cas\open\server\sync
     * @subpackage dao\transferables
     *
     */
    class TransferableRecordId {

        /**
         * @var string
         *
         */
        public $id;

        /**
         * @var string
         *
         */
        public $recordCheckSum;

        /**
         * @var array
         *
         */
        public $syncViewIds;

        /**
         * @var array
         *
         */
        public $additionalData;

    }

}
