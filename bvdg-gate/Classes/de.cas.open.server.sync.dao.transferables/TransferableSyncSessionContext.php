<?php
// This file has been automatically generated.

namespace de\cas\open\server\sync\dao\transferables {

    /**
     * @package de\cas\open\server\sync
     * @subpackage dao\transferables
     *
     */
    class TransferableSyncSessionContext {

        /**
         * @var string
         *
         */
        public $leftInstanceId;

        /**
         * @var string
         *
         */
        public $rightInstanceId;

        /**
         * @var string
         *
         */
        public $syncSessionId;

    }

}
