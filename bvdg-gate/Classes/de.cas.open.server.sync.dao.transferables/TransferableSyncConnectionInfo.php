<?php
// This file has been automatically generated.

namespace de\cas\open\server\sync\dao\transferables {

    /**
     * @package de\cas\open\server\sync
     * @subpackage dao\transferables
     *
     */
    class TransferableSyncConnectionInfo {

        /**
         * @var string
         *
         */
        public $displayName;

        /**
         * @var string
         *
         */
        public $host;

        /**
         * @var int
         *
         */
        public $port;

        /**
         * @var string
         *
         */
        public $username;

        /**
         * @var string
         *
         */
        public $password;

        /**
         * @var boolean
         *
         */
        public $useHttps;

        /**
         * @var boolean
         *
         */
        public $useProxy;

        /**
         * @var array
         *
         */
        public $parameters;

        /**
         * @var unknown
         *
         */
        public $proxySettings;

        /**
         * @var string
         *
         */
        public $proxyHost;

        /**
         * @var int
         *
         */
        public $proxyPort;

        /**
         * @var string
         *
         */
        public $proxyUsername;

        /**
         * @var string
         *
         */
        public $proxyPassword;

        /**
         * @var int
         *
         */
        public $proxyType;

    }

}
