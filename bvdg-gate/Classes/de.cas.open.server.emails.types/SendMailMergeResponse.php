<?php
// This file has been automatically generated.

namespace de\cas\open\server\emails\types {

    /**
     * @package de\cas\open\server\emails
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Sends a mail merge of the given document GUID from the given user GUID.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: GetArchivedEMailRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetArchivedEMailRequest
     */
    class SendMailMergeResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var boolean
         *Sets/Returns if sending the mail merge was successful
         */
        public $sendSuccessful;

    }

}
