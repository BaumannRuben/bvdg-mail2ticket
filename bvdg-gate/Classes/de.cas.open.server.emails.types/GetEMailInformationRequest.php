<?php
// This file has been automatically generated.

namespace de\cas\open\server\emails\types {

    /**
     * @package de\cas\open\server\emails
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Retrieves information about an e-mail.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: GetEMailInformationResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetEMailInformationResponse
     */
    class GetEMailInformationRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         */
        public $eMailGGUID;

        /**
         * @var boolean
         *
         *                    Flag indicating if inline attachment information shall
         *                    be retrieved (flag set to 'true')
         */
        public $getInlineAttachments;

    }

}
