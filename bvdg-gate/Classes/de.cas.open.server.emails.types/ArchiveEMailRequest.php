<?php
// This file has been automatically generated.

namespace de\cas\open\server\emails\types {

    /**
     * @package de\cas\open\server\emails
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Creates a document from an EMail and archives it.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: ArchiveEMailResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see ArchiveEMailResponse
     */
    class ArchiveEMailRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var unknown
         *
         *                    Sets/Returns the eMail file bytes.
         */
        public $EMailFileContent;

        /**
         * @var array
         *
         *                    DataObjectPermission representing the access
         *                    permissions to this dataobject.
         */
        public $permissions;

        /**
         * @var string
         *
         *                    Foreign edit permission restrictions for the EMAILSTORE
         *                    DataObject.
         */
        public $foreignEditPermissionRestrictions;

    }

}
