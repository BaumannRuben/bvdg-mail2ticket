<?php
// This file has been automatically generated.

namespace de\cas\open\server\emails\types {

    /**
     * @package de\cas\open\server\emails
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Sends a mail merge of the given document GUID from the given user GUID.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: GetArchivedEMailResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetArchivedEMailResponse
     */
    class SendMailMergeRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    Sets/Returns the GGUID of the mail merge document
         */
        public $documentGUID;

        /**
         * @var string
         *
         *                    Sets/Returns the GGUID of the user
         */
        public $userGUID;

        /**
         * @var boolean
         *
         *                    Sets/Returns the testMode option
         */
        public $testMode;

        /**
         * @var boolean
         *
         *                    Sets/Returns the useSendFolder option
         */
        public $useSendFolder;

        /**
         * @var boolean
         *
         *                    Sets/Returns the isPlanned option
         */
        public $isPlanned;

        /**
         * @var boolean
         *
         *                    Sets/Returns the resumed option
         */
        public $resumed;

    }

}
