<?php
// This file has been automatically generated.

namespace de\cas\open\server\emails\types {

    /**
     * @package de\cas\open\server\emails
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Returns the cancel status of the serial E-Mail for the given gguid.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: CancelSerialMailRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see CancelSerialMailRequest
     */
    class CancelSerialMailResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var boolean
         *
         *                    Gets the cancel status.
         */
        public $Canceled;

    }

}
