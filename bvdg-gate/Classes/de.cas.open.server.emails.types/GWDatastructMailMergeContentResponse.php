<?php
// This file has been automatically generated.

namespace de\cas\open\server\emails\types {

    /**
     * @package de\cas\open\server\emails
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Returns the gwdatastruct data content for a mailmerge object
     *        Corresponding \de\cas\open\server\api\types\RequestObject: GWDatastructMailMergeContentRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GWDatastructMailMergeContentRequest
     */
    class GWDatastructMailMergeContentResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var string
         *
         *                    returns the gwdatastruct content
         */
        public $ResponseGWDataStructContent;

    }

}
