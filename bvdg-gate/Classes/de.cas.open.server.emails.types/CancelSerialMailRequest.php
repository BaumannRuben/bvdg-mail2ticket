<?php
// This file has been automatically generated.

namespace de\cas\open\server\emails\types {

    /**
     * @package de\cas\open\server\emails
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Returns the cancel status of the serial E-Mail for the given gguid.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: CancelSerialMailResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see CancelSerialMailResponse
     */
    class CancelSerialMailRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    Sets/Returns the GGUID of the serial e-mail object to cancel.
         */
        public $GGUID;

    }

}
