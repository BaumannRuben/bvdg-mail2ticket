<?php
// This file has been automatically generated.

namespace de\cas\open\server\emails\types {

    /**
     * @package de\cas\open\server\emails
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Returns the send status of the serial e-mail E-Mail of the given gguid.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: GetSerialMailStatusRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetSerialMailStatusRequest
     */
    class GetSerialMailStatusResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var int
         *
         *                    Gets the status of the email.
         */
        public $status;

    }

}
