<?php
// This file has been automatically generated.

namespace de\cas\open\server\emails\types {

    /**
     * @package de\cas\open\server\emails
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Returns an archived EMail for a given GGUID of an
     *        EMailStore object. Corresponding \de\cas\open\server\api\types\RequestObject: GetArchivedEMailRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetArchivedEMailRequest
     */
    class GetArchivedEMailResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var unknown
         *Sets/Returns the file content
         */
        public $fileContent;

        /**
         * @var string
         *
         *                    Sets/Returns the file extension
         */
        public $fileExtension;

    }

}
