<?php
// This file has been automatically generated.

namespace de\cas\open\server\emails\types {

    /**
     * @package de\cas\open\server\emails
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Requests the gwdatastruct data content for a mailmerge object.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: GWDatastructMailMergeContentResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GWDatastructMailMergeContentResponse
     */
    class GWDatastructMailMergeContentRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    Guid of the mailmerge document for which the gwdatastruct content should be received
         */
        public $GUID;

    }

}
