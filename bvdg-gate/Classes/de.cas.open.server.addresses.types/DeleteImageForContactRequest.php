<?php
// This file has been automatically generated.

namespace de\cas\open\server\addresses\types {

    /**
     * @package de\cas\open\server\addresses
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Removes an image for an address. Corresponding \de\cas\open\server\api\types\ResponseObject: DeleteImageForContactResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see DeleteImageForContactResponse
     */
    class DeleteImageForContactRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    Sets/Returns the GGUID of the address the image shall
         *                    be removed for.
         */
        public $AdressGGUID;

    }

}
