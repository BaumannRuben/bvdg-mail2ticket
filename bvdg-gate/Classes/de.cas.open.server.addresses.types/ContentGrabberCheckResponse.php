<?php
// This file has been automatically generated.

namespace de\cas\open\server\addresses\types {

    /**
     * @package de\cas\open\server\addresses
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\ResponseObject: Extracted address-related data from the input-string as DataObject.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: ContentGrabberCheckRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see ContentGrabberCheckRequest
     */
    class ContentGrabberCheckResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\DataObjectTransferable
         *
         *										Aus dem Eingabestring extrahierte Adress-Daten im ADDRESS DataObject.
         */
        public $DataObject;

    }

}
