<?php
// This file has been automatically generated.

namespace de\cas\open\server\addresses\types {

    /**
     * @package de\cas\open\server\addresses
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Removes an image for an address. Corresponding \de\cas\open\server\api\types\RequestObject: DeleteImageForContactRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see DeleteImageForContactRequest
     */
    class DeleteImageForContactResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\DataObjectTransferable
         *
         *                    Sets/Returns the address data object recently modified.
         */
        public $Address;

    }

}
