<?php
// This file has been automatically generated.

namespace de\cas\open\server\addresses\types {

    /**
     * @package de\cas\open\server\addresses
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Retrieves the image bytes for a given address.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: GetImageForContactResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetImageForContactResponse
     */
    class GetImageForContactRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    Sets/Returns the GGUID of the address whose image
         *                    shall be retrieved.
         */
        public $AdressGGUID;

    }

}
