<?php
// This file has been automatically generated.

namespace de\cas\open\server\addresses\types {

    /**
     * @package de\cas\open\server\addresses
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject for the business operation that checks if contact
     *        duplicates exist for a given contact object.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject CheckForContactDuplicatesResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see CheckForContactDuplicatesResponse
     */
    class CheckForContactDuplicatesRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var \de\cas\open\server\api\types\DataObjectTransferable
         *
         *                    Address data object to check. Can be unsaved.
         */
        public $contact;

        /**
         * @var array
         *
         *                    List of necessary fields to visualize duplicates
         */
        public $fields;

        /**
         * @var boolean
         *
         *                    Indicates whether the duplicate check should be executed in system context.
         */
        public $executeInSystemContext;

    }

}
