<?php
// This file has been automatically generated.

namespace de\cas\open\server\addresses\types {

    /**
     * @package de\cas\open\server\addresses
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject for the business operation that checks if contact
     *        duplicates exist for a given contact object. If duplicates are found, the addresses are also marked
     *        as duplicates for later resolution. Performs the check in system context to get all possible
     *        duplicates, without access restrictions.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: CheckContactAndMarkDuplicatesInSystemContextRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see CheckContactAndMarkDuplicatesInSystemContextRequest
     */
    class CheckContactAndMarkDuplicatesInSystemContextResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var boolean
         *
         *                    Returns whether the given address has been marked as a duplicate.
         */
        public $isDuplicate;

    }

}
