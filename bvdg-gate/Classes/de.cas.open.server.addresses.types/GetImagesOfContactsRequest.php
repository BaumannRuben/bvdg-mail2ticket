<?php
// This file has been automatically generated.

namespace de\cas\open\server\addresses\types {

    /**
     * @package de\cas\open\server\addresses
     * @subpackage types
     *
     *		    \de\cas\open\server\api\types\RequestObject: Returns address data, which are
     *		    selected by the query argument. After the retrieval of the MassQueryResult, the business
     *		    operation adds another column called imageData. This column contains the contact image
     *		    as PNG image with a maximum size of 200x200 px. <br/>
     *		    Please always select the column ADDRESS.IMAGEGUID, which is needed to retrieve
     *		    the images.
     *		    Corresponding \de\cas\open\server\api\types\ResponseObject: GetImagesOfContactsResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetImagesOfContactsResponse
     */
    class GetImagesOfContactsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										 The SELECT-statement for the query, please add the column
         *										 ADDRESS.IMAGEGUID.
         */
        public $query;

        /**
         * @var unknown
         *
         *										The first record that shall be returned, that is the offset
         *	 									zero-based of the first record in the result
         *	 									 of the database query.
         */
        public $firstRecord;

        /**
         * @var unknown
         *
         *										The number of records that shall be contained in the result (if the query
         *										returns more rows than this).
         */
        public $recordCount;

        /**
         * @var boolean
         *
         *										If true a SELECT statement is cloned and appended with a UNION so that it
         *										also returns private records of other users.
         */
        public $includePrivateForeignRecords;

        /**
         * @var int
         *
         *										 The minimum permission the current user must have on the record (see
         *										 \de\cas\open\server\api\types\EIMPermissionConstants)
         *	@see \de\cas\open\server\api\types\EIMPermissionConstants
         */
        public $minPermission;

    }

}
