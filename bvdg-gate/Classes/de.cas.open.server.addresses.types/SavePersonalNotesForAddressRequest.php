<?php
// This file has been automatically generated.

namespace de\cas\open\server\addresses\types {

    /**
     * @package de\cas\open\server\addresses
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\RequestObject: Saves, or creates the personal notes for the given address
     *				Corresponding \de\cas\open\server\api\types\ResponseObject: SavePersonalNotesForAddressResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see SavePersonalNotesForAddressResponse
     */
    class SavePersonalNotesForAddressRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										The gguid of the Address
         */
        public $AddressGuid;

        /**
         * @var \de\cas\open\server\api\types\AbstractMassData
         *
         *										Values of the new/updated personal notes
         *										Columns: GGUID, NOTES2, GROUPGGUID
         *										GGUID has to be null if the current row is a new note
         */
        public $PersonalNotes;

    }

}
