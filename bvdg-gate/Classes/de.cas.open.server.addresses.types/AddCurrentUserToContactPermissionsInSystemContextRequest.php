<?php
// This file has been automatically generated.

namespace de\cas\open\server\addresses\types {

    /**
     * @package de\cas\open\server\addresses
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject for the business operation that adds current user to permissions
     *        of given contact. Uses system context to add user without access restrictions.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject AddCurrentUserToAddressPermissionsInSystemContextResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see AddCurrentUserToAddressPermissionsInSystemContextResponse
     */
    class AddCurrentUserToContactPermissionsInSystemContextRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    GGUID of address data object to add current user to permissions
         */
        public $contactGguid;

    }

}
