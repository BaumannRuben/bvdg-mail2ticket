<?php
// This file has been automatically generated.

namespace de\cas\open\server\addresses\types {

    /**
     * @package de\cas\open\server\addresses
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Saves an image for an address. An existing image will be
     *        overwritten. Corresponding \de\cas\open\server\api\types\ResponseObject: SaveImageForContactResponse.<br/>
     *        Throws a BusinessException with the code BUSINESS_IMAGE_TYPE_NOT_SUPPORTED, if the image
     *        type is not supported. Please use PNG or JPG.
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see SaveImageForContactResponse
     */
    class SaveImageForContactRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    Sets/Returns the GGUID of the address the image shall
         *                    be saved for.
         */
        public $AdressGGUID;

        /**
         * @var unknown
         *
         *                    Sets/Returns the bytes of the image to be saved.
         */
        public $ImageBytes;

        /**
         * @var string
         *
         *                    Sets/Returns the file extension for the image, e.g.
         *                    'jpg' or 'bmp'.
         */
        public $ImageFileExtension;

    }

}
