<?php
// This file has been automatically generated.

namespace de\cas\open\server\addresses\types {

    /**
     * @package de\cas\open\server\addresses
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Requests a file with the linked addresses to a specified data record.. Corresponding \de\cas\open\server\api\types\RequestObject: GetLinkedAddressesAsFileRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetLinkedAddressesAsFileRequest
     */
    class GetLinkedAddressesAsFileResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var unknown
         *
         *                    Sets/Returns a the result file.
         */
        public $ResultFile;

    }

}
