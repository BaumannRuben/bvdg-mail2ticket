<?php
// This file has been automatically generated.

namespace de\cas\open\server\addresses\types {

    /**
     * @package de\cas\open\server\addresses
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\ResponseObject:
     *				 Returns the fields that are replicated between a single person contact record and its primary organization and therefore should not be displayed as disabled on client side. The result can be cached as it won't change during runtime.
     *				Corresponding \de\cas\open\server\api\types\RequestObject: GetReplicatedPrimaryOrganizationFieldsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetReplicatedPrimaryOrganizationFieldsRequest
     */
    class GetReplicatedPrimaryOrganizationFieldsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *										Gets/Sets the fields that are replicated between a single person contact record and its primary organization and therefore should not be displayed as disabled on client side.
         */
        public $replicatedFields;

    }

}
