<?php
// This file has been automatically generated.

namespace de\cas\open\server\addresses\types {

    /**
     * @package de\cas\open\server\addresses
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\RequestObject:<br/>
     *				Checks that a merge of an address with a duplicate is possible.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject: ADRIsMergePossibleResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see ADRIsMergePossibleResponse
     */
    class ADRIsMergePossibleRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										 The GGUID of the first address to merge. This is considered to be the SELECTED address.
         *										 Reasons for not being able to merge 1 into 2, and hence not being able to delete 1,
         *										 will refer to 1 as the SELECTED address.
         */
        public $address1Gguid;

        /**
         * @var string
         *
         *										 The GGUID of the second address to merge. This is considered to be the DUPLICATE address.
         *										 Reasons for not being able to merge 2 into 1, and hence not being able to delete 2,
         *										 will refer to 2 as the DUPLICATE address.
         */
        public $address2Gguid;

    }

}
