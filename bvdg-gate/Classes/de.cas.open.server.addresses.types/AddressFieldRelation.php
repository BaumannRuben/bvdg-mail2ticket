<?php
// This file has been automatically generated.

namespace de\cas\open\server\addresses\types {

    /**
     * @package de\cas\open\server\addresses
     * @subpackage types
     *
     *        Simple type that contains values that describes a field relation between organisations and contacts.
     */
    class AddressFieldRelation {

        /**
         * @var string
         *
         *                Returns the field name of the field relation.
         */
        public $fieldName;

        /**
         * @var boolean
         *
         *                Returns whether the field is editable for organisations or not.
         */
        public $editableForOrganisations;

        /**
         * @var boolean
         *
         *                Returns whether the field is editable for contacts or not.
         */
        public $editableForContacts;

        /**
         * @var boolean
         *
         *                Returns whether the field is synchronized between organsiations and contacts or not.
         */
        public $synchronized;

    }

}
