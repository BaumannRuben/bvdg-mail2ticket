<?php
// This file has been automatically generated.

namespace de\cas\open\server\addresses\types {

    /**
     * @package de\cas\open\server\addresses
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\RequestObject: Requests the city name for the given zip code. Corresponding \de\cas\open\server\api\types\ResponseObject: DoZipAddressCheckResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see DoZipAddressCheckResponse
     */
    class DoZipAddressCheckRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										The zip code to search.
         */
        public $Zip;

        /**
         * @var string
         *
         *										Country code. Eg.: DE, AT, CH
         */
        public $AddressFormat;

    }

}
