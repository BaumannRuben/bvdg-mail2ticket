<?php
// This file has been automatically generated.

namespace de\cas\open\server\addresses\types {

    /**
     * @package de\cas\open\server\addresses
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject for the business operation that checks if contact
     *        duplicates exist for a given contact object. If duplicates are found, the addresses are also marked
     *        as duplicates for later resolution. Performs the check in system context to get all possible
     *        duplicates, without access restrictions.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject CheckContactAndMarkDuplicatesInSystemContextResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see CheckContactAndMarkDuplicatesInSystemContextResponse
     */
    class CheckContactAndMarkDuplicatesInSystemContextRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    The GGUID of the address to check for duplicates.
         */
        public $gguid;

    }

}
