<?php
// This file has been automatically generated.

namespace de\cas\open\server\addresses\types {

    /**
     * @package de\cas\open\server\addresses
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\RequestObject: Return with the assigned Kontaktnotizen objects for an address with the given gguid
     *				Corresponding \de\cas\open\server\api\types\ResponseObject: GetExistingPersonalNotesForAddressResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetExistingPersonalNotesForAddressResponse
     */
    class GetExistingPersonalNotesForAddressRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										The gguid of the Address
         */
        public $AddressGuid;

    }

}
