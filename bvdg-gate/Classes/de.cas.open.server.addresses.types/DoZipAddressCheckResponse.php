<?php
// This file has been automatically generated.

namespace de\cas\open\server\addresses\types {

    /**
     * @package de\cas\open\server\addresses
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\ResponseObject: Returns with the city for the given zip code. Corresponding \de\cas\open\server\api\types\RequestObject: DoZipAddressCheckRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see DoZipAddressCheckRequest
     */
    class DoZipAddressCheckResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var string
         *
         *										Returns with the city (Ort) for the given zip code.
         */
        public $City;

        /**
         * @var string
         *
         *										Returns with the area for the given zip code.
         */
        public $Area;

        /**
         * @var string
         *
         *										Returns with the state (Bundesland) for the given zip code.
         */
        public $State;

        /**
         * @var string
         *
         *										Returns with the country for the given zip code.
         */
        public $Country;

    }

}
