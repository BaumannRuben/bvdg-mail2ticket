<?php
// This file has been automatically generated.

namespace de\cas\open\server\addresses\types {

    /**
     * @package de\cas\open\server\addresses
     * @subpackage types
     *
     *	    \de\cas\open\server\api\types\RequestObject:
     *	    Returns the fields that are replicated between a single person contact record and its primary organization and therefore should not be displayed as disabled on client side. The result can be cached as it won't change during runtime.
     *	    Corresponding \de\cas\open\server\api\types\ResponseObject: GetReplicatedPrimaryOrganizationFieldsResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetReplicatedPrimaryOrganizationFieldsResponse
     */
    class GetReplicatedPrimaryOrganizationFieldsRequest extends \de\cas\open\server\api\types\RequestObject {

    }

}
