<?php
// This file has been automatically generated.

namespace de\cas\open\server\addresses\types {

    /**
     * @package de\cas\open\server\addresses
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Requests field relations for a given set of address fields. Corresponding \de\cas\open\server\api\types\ResponseObject: GetAddressFieldRelationsResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetAddressFieldRelationsResponse
     */
    class GetAddressFieldRelationsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var array
         *
         *                    Sets/Returns a set of fields for retreiving the field relations.
         */
        public $Fields;

    }

}
