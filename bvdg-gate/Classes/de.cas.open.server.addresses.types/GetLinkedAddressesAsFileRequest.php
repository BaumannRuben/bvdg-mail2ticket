<?php
// This file has been automatically generated.

namespace de\cas\open\server\addresses\types {

    /**
     * @package de\cas\open\server\addresses
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Requests a file with the linked addresses to a specified data record. \de\cas\open\server\api\types\ResponseObject: GetLinkedAddressesAsFileResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetLinkedAddressesAsFileResponse
     */
    class GetLinkedAddressesAsFileRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var array
         *
         *                    Sets/Returns a set of fields that should be included.
         */
        public $FieldsToInclude;

        /**
         * @var array
         *
         *                    Sets/Returns a set of fields that should not be included.
         */
        public $FieldsToIgnore;

        /**
         * @var string
         *
         *                    Sets/Returns the ObjectType of the link source record.
         */
        public $SourceObjectType;

        /**
         * @var string
         *
         *                    Sets/Returns the GGUID of the link source record.
         */
        public $SourceObjectGuid;

        /**
         * @var boolean
         *
         *                    Sets/Returns whether linked ADDRESSNOTES should be also included.
         */
        public $RequestAddressNotes;

    }

}
