<?php
// This file has been automatically generated.

namespace de\cas\open\server\addresses\types {

    /**
     * @package de\cas\open\server\addresses
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject for the business operation that checks if contact
     *        duplicates exist for a given contact object. Performs the check in system context to get all possible
     *        duplicates, without access restrictions.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: CheckForContactDuplicatesInSystemContextRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see CheckForContactDuplicatesInSystemContextRequest
     */
    class CheckForContactDuplicatesInSystemContextResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\MassQueryResult
         *
         *                    Returns the list of GGUIDs of possible contact duplicates.
         */
        public $duplicateObjects;

    }

}
