<?php
// This file has been automatically generated.

namespace de\cas\open\server\addresses\types {

    /**
     * @package de\cas\open\server\addresses
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Requests field relations for a given set of address fields. Corresponding \de\cas\open\server\api\types\RequestObject: GetAddressFieldRelationsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetAddressFieldRelationsRequest
     */
    class GetAddressFieldRelationsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *                    Sets/Returns a set of field relations.
         */
        public $FieldRelations;

    }

}
