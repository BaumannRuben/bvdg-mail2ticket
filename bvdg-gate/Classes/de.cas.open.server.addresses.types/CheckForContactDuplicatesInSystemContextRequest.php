<?php
// This file has been automatically generated.

namespace de\cas\open\server\addresses\types {

    /**
     * @package de\cas\open\server\addresses
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject for the business operation that checks if contact
     *        duplicates exist for a given contact object. Performs the check in system context to get all possible
     *        duplicates, without access restrictions.
     *        Consider using CheckForContactDuplicatesRequest instead.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject CheckForContactDuplicatesInSystemContextResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see CheckForContactDuplicatesRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see CheckForContactDuplicatesInSystemContextResponse
     */
    class CheckForContactDuplicatesInSystemContextRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var \de\cas\open\server\api\types\DataObjectTransferable
         *
         *                    Address data object to check. Can be unsaved.
         */
        public $contact;

        /**
         * @var array
         *
         *                    List of necessary fields to visualize duplicates
         */
        public $fields;

    }

}
