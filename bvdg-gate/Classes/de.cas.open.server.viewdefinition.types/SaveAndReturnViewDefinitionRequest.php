<?php
// This file has been automatically generated.

namespace de\cas\open\server\viewdefinition\types {

    /**
     * @package de\cas\open\server\viewdefinition
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\RequestObject: Saves a view-definition
     *				together with a
     *				filter-DataObject. Corresponding \de\cas\open\server\api\types\ResponseObject: SaveAndReturnViewDefinitionResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see SaveAndReturnViewDefinitionResponse
     */
    class SaveAndReturnViewDefinitionRequest extends \de\cas\open\server\viewdefinition\types\SaveViewDefinitionRequest {

    }

}
