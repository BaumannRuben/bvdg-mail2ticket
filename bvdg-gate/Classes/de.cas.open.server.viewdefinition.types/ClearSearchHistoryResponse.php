<?php
// This file has been automatically generated.

namespace de\cas\open\server\viewdefinition\types {

    /**
     * @package de\cas\open\server\viewdefinition
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\ResponseObject: Contains nothing.
     *				Corresponding \de\cas\open\server\api\types\RequestObject:
     *				ClearSearchHistoryRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see ClearSearchHistoryRequest
     */
    class ClearSearchHistoryResponse extends \de\cas\open\server\api\types\ResponseObject {

    }

}
