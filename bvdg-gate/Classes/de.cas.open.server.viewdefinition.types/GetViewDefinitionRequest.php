<?php
// This file has been automatically generated.

namespace de\cas\open\server\viewdefinition\types {

    /**
     * @package de\cas\open\server\viewdefinition
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\RequestObject: Gets a ViewDefinition
     *				with the corresponding
     *				filter-dataobject appended. Corresponding
     *				\de\cas\open\server\api\types\ResponseObject: GetViewDefinitionResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetViewDefinitionResponse
     */
    class GetViewDefinitionRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *The ID of the view to be fetched.
         */
        public $viewId;

        /**
         * @var string
         *The type. Will not be interpreted
         *									in the CAS Open server.
         */
        public $type;

    }

}
