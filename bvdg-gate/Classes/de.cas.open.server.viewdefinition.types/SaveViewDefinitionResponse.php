<?php
// This file has been automatically generated.

namespace de\cas\open\server\viewdefinition\types {

    /**
     * @package de\cas\open\server\viewdefinition
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\ResponseObject: Returns nothing.
     *				Corresponding \de\cas\open\server\api\types\RequestObject:
     *				SaveViewDefinitionRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see SaveViewDefinitionRequest
     */
    class SaveViewDefinitionResponse extends \de\cas\open\server\api\types\ResponseObject {

    }

}
