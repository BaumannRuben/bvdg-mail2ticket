<?php
// This file has been automatically generated.

namespace de\cas\open\server\viewdefinition\types {

    /**
     * @package de\cas\open\server\viewdefinition
     * @subpackage types
     * \de\cas\open\server\api\types\RequestObject: Saves the sort order of
     *				the given views in the scope defined by viewType and viewScope
     *				(optional). Corresponding \de\cas\open\server\api\types\ResponseObject: SaveViewDefinitionsSortOrderResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see SaveViewDefinitionsSortOrderResponse
     */
    class SaveViewDefinitionsSortOrderRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var array
         *The IDs
         *										of view definitions in the order that
         *										should be persisted.
         */
        public $viewIds;

        /**
         * @var string
         *The viewType in whose context the sort
         *										order will
         *										be saved.
         */
        public $viewType;

        /**
         * @var string
         *The viewScope in whose
         *										context the sort order will
         *										be saved. (optional)
         */
        public $viewScope;

    }

}
