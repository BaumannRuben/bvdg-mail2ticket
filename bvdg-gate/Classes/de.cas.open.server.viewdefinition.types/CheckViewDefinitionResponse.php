<?php
// This file has been automatically generated.

namespace de\cas\open\server\viewdefinition\types {

    /**
     * @package de\cas\open\server\viewdefinition
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\ResponseObject: Returns information
     *				about where and why a
     *				ViewDefinition is no longer working.
     *				Corresponding \de\cas\open\server\api\types\RequestObject:
     *				CheckViewDefinitionRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see CheckViewDefinitionRequest
     */
    class CheckViewDefinitionResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var string
         *
         *										Sets/Returns the location where the failure
         *										occured.
         */
        public $failureLocation;

        /**
         * @var string
         *
         *										Sets/Returns the detail code which gives some
         *										more
         *										information about the fault.
         */
        public $detailCode;

    }

}
