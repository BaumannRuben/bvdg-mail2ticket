<?php
// This file has been automatically generated.

namespace de\cas\open\server\viewdefinition\types {

    /**
     * @package de\cas\open\server\viewdefinition
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\RequestObject: Gets a single search
     *				history item.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject: GetSearchHistoryItemResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetSearchHistoryItemResponse
     */
    class GetSearchHistoryItemRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										The ID of the search history item to be returned.
         */
        public $viewId;

    }

}
