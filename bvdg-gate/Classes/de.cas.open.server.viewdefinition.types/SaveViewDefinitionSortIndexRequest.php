<?php
// This file has been automatically generated.

namespace de\cas\open\server\viewdefinition\types {

    /**
     * @package de\cas\open\server\viewdefinition
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\RequestObject: Saves the sort-index
     *				for a single view definition.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject: SaveViewDefinitionsSortOrderResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see SaveViewDefinitionsSortOrderResponse
     */
    class SaveViewDefinitionSortIndexRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *The ID of the view definition.
         */
        public $viewId;

        /**
         * @var int
         *The sort index that should be persisted for the
         *										view.
         */
        public $sortIndex;

    }

}
