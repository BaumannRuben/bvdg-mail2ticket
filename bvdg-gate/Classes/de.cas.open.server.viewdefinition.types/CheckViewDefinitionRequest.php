<?php
// This file has been automatically generated.

namespace de\cas\open\server\viewdefinition\types {

    /**
     * @package de\cas\open\server\viewdefinition
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\RequestObject:
     *				Checks a ViewDefinition for its validity and returns a response that contains a
     *				  faultcode and an indicator, where the fault is located at.<br/><b>If there could be no
     *				  error found, the attribute
     *				  {@link CheckViewDefinitionResponse#setDetailCode(String)} and
     *				  {@link CheckViewDefinitionResponse#setFailureLocation(ViewDefinitionFailureLocation)} are
     *				  <code>null</code></b>.
     *				  <p>
     *				  Fault-locations may be:
     *				  <ul>
     *				  <li>{@link de.cas.open.server.viewdefinition.types.ViewDefinitionFailureLocation#FILTER}</li>
     *				  <li>{@link de.cas.open.server.viewdefinition.types.ViewDefinitionFailureLocation#VIEW}</li>
     *				  <li>{@link de.cas.open.server.viewdefinition.types.ViewDefinitionFailureLocation#BOTH}</li>
     *				  <li>{@link de.cas.open.server.viewdefinition.types.ViewDefinitionFailureLocation#UNKNOWN}</li>
     *				  </ul>
     *				  </p>
     *				  <p>
     *				  Possible faultcodes are:
     *				  <ul>
     *				  <li>{@link de.cas.open.server.viewdefinition.constants.ExceptionCode#BUSINESS_VIEWS_NO_RESULT_TYPE_SET}</li>
     *				  <li>{@link de.cas.open.server.viewdefinition.constants.ExceptionCode#BUSINESS_VIEWS_INVALID_RESULT_TYPE_SET}</li>
     *				  <li>{@link de.cas.open.server.viewdefinition.constants.ExceptionCode#BUSINESS_VIEWS_INVALID_COLUMN_SET_FOR_RESULT_TYPE}</li>
     *				  <li>{@link de.cas.open.server.viewdefinition.constants.ExceptionCode#BUSINESS_VIEWS_COLUMN_WIDTH_IS_TOO_SMALL}</li>
     *				  <li>{@link de.cas.open.server.viewdefinition.constants.ExceptionCode#BUSINESS_VIEWS_SORT_ORDER_OF_COLUMN_CANNOT_BE_EMPTY}</li>
     *				  <li>{@link de.cas.open.server.viewdefinition.constants.ExceptionCode#BUSINESS_VIEWS_CANNOT_DIRECTLY_LINK_TO_A_TEMPLATE_FILTER}</li>
     *				  <li>{@link de.cas.open.server.viewdefinition.constants.ExceptionCode#BUSINESS_VIEWS_RESULT_TYPE_OF_VIEW_DIFFERS_FROM_RESULT_TYPE_OF_FILTER}</li>
     *				  <li>{@link de.cas.open.server.viewdefinition.constants.ExceptionCode#BUSINESS_VIEWS_FILTER_DOES_NOT_EXIST}</li>
     *				  <li>{@link de.cas.open.server.viewdefinition.constants.ExceptionCode#BUSINESS_VIEWS_TEMPLATE_FILTER_SET_IS_NOT_A_TEMPLATE_FILTER}</li>
     *				  <li>{@link de.cas.open.server.viewdefinition.constants.ExceptionCode#BUSINESS_VIEWS_TEMPLATE_FILTER_IS_NOT_EQUAL_TO_ATTACHED_FILTER}</li>
     *				  <li>{@link de.cas.open.server.viewdefinition.constants.ExceptionCode#BUSINESS_VIEWS_TEMPLATE_FILTER_REFERENCED_BY_FILTER_DOES_NOT_EXIST}</li>
     *				  <li>{@link de.cas.open.server.viewdefinition.constants.ExceptionCode#BUSINESS_VIEWS_ERROR_IN_PROJECTION}</li>
     *				  <li>{@link de.cas.open.server.viewdefinition.constants.ExceptionCode#BUSINESS_VIEWS_ERROR_IN_ORDERING}</li>
     *				  <li>{@link de.cas.open.server.viewdefinition.constants.ExceptionCode#BUSINESS_VIEWS_ERROR_IN_SELECTION}</li>
     *				  <li>{@link de.cas.open.server.viewdefinition.constants.ExceptionCode#BUSINESS_VIEWS_UNKNOWN_ERROR}</li>
     *				  </ul>
     *				  </p>
     *				Corresponding \de\cas\open\server\api\types\ResponseObject: CheckViewDefinitionResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see ViewDefinition
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see CheckViewDefinitionResponse
     */
    class CheckViewDefinitionRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var \de\cas\open\server\viewdefinition\types\ViewDefinition
         *
         *										Sets/Returns the ViewDefinition to be checked.
         */
        public $viewDefinition;

    }

}
