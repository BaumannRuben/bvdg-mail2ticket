<?php
// This file has been automatically generated.

namespace de\cas\open\server\viewdefinition\types {

    /**
     * @package de\cas\open\server\viewdefinition
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\RequestObject: Gets all views and the
     *				referenced filters for a specific
     *				view type. Corresponding \de\cas\open\server\api\types\ResponseObject: GetViewDefinitionsResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetViewDefinitionsResponse
     */
    class GetViewDefinitionsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										The type of the views to be returned (e.g.
         *										ADDRESS, APPOINTMENT,
         *										MIXEDLIST, ...)
         */
        public $viewType;

        /**
         * @var string
         *
         *										The scope of the views to be returned. May be
         *										null if views without scope should be returned.
         */
        public $viewScope;

        /**
         * @var array
         *
         *										Only view definitions where the current user is
         *										permission owner
         *										with
         *										one of the roles given are considered and
         *										returned.
         */
        public $roles;

    }

}
