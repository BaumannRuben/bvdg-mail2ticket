<?php
// This file has been automatically generated.

namespace de\cas\open\server\viewdefinition\types {

    /**
     * @package de\cas\open\server\viewdefinition
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\RequestObject: Queries the server for
     *				the search history (list of
     *				views). Corresponding \de\cas\open\server\api\types\ResponseObject: GetSearchHistoryResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetSearchHistoryResponse
     */
    class GetSearchHistoryRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										Sets/Returns the view type to get the search
         *										history. (optional if the viewScope property is set)
         */
        public $viewType;

        /**
         * @var string
         *
         *										Sets/Returns the view scope to get the search
         *										history for. (optional if the viewType property is set)
         */
        public $viewScope;

        /**
         * @var int
         *
         *										Sets/Returns the maximum amount of search results
         *										that
         *										should be fetched.
         */
        public $numResults;

    }

}
