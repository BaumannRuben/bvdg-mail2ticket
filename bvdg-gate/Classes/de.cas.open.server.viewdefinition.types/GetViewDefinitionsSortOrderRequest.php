<?php
// This file has been automatically generated.

namespace de\cas\open\server\viewdefinition\types {

    /**
     * @package de\cas\open\server\viewdefinition
     * @subpackage types
     * \de\cas\open\server\api\types\RequestObject: Gets the sort order of
     *				views in the scope defined by viewType and viewScope
     *				(optional).
     *				Corresponding \de\cas\open\server\api\types\ResponseObject:
     *				GetViewDefinitionsSortOrderResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetViewDefinitionsSortOrderResponse
     */
    class GetViewDefinitionsSortOrderRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *The viewType in for which the sort-order should
         *										be fetched.
         */
        public $viewType;

        /**
         * @var string
         *The viewScope for which the sort-order should be
         *										fetched. (optional)
         */
        public $viewScope;

    }

}
