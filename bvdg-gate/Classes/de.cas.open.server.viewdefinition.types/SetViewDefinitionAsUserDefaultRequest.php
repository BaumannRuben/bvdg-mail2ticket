<?php
// This file has been automatically generated.

namespace de\cas\open\server\viewdefinition\types {

    /**
     * @package de\cas\open\server\viewdefinition
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\RequestObject: Sets a view as
     *				userdefault in the scope defined by ViewDefinition#viewType and
     *				ViewDefinition#viewScope.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject: SetViewDefinitionAsUserDefaultResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see SetViewDefinitionAsUserDefaultResponse
     */
    class SetViewDefinitionAsUserDefaultRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *The ID of the view to set as user default.
         */
        public $viewId;

    }

}
