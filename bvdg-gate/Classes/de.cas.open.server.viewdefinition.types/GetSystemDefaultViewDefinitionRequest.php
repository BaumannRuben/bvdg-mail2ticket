<?php
// This file has been automatically generated.

namespace de\cas\open\server\viewdefinition\types {

    /**
     * @package de\cas\open\server\viewdefinition
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\RequestObject: Gets the view
     *				definition that's flagged as being system default for a viewType and
     *				viewScope (optional). An exception with code
     *				VIEWS_SYSTEM_DEFAULT_VIEW_NOT_FOUND may be thrown in case such a
     *				view
     *				definition does not exist.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject: GetSystemDefaultViewDefinitionResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetSystemDefaultViewDefinitionResponse
     */
    class GetSystemDefaultViewDefinitionRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										The type of the view to be returned (e.g.
         *										ADDRESS, APPOINTMENT, TODO, ...)
         */
        public $viewType;

        /**
         * @var string
         *
         *										The scope of the view to be returned.
         */
        public $viewScope;

    }

}
