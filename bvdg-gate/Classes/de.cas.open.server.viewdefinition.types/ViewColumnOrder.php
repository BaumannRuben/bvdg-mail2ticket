<?php
// This file has been automatically generated.

namespace de\cas\open\server\viewdefinition\types {

    /**
     * @package de\cas\open\server\viewdefinition
     * @subpackage types
     *
     *				This class represents a column to be displayed
     *				with a sort-order.
     */
    class ViewColumnOrder {

        /**
         * @var string
         *
         *								The name of the column or field to be displayed.
         */
        public $columnName;

        /**
         * @var string
         *The direction of order (ASC, DESC).
         */
        public $sortOrder;

    }

}
