<?php
// This file has been automatically generated.

namespace de\cas\open\server\viewdefinition\types {

    /**
     * @package de\cas\open\server\viewdefinition
     * @subpackage types
     *
     *					A ViewDefinition that extends the base ViewDefinition class and adds the properties
     *					<code>interval</code>, <code>startDay</code> and <code>displayType</code>.
     *					<br/>
     *					<code>startDay</code> is defined as:
     *					<ul>
     *						<li>1 --> sunday</li>
     *						<li>2 --> monday</li>
     *						<li>3 --> tuesday</li>
     *						<li>4 --> wednesday</li>
     *						<li>5 --> thursday</li>
     *						<li>6 --> friday</li>
     *						<li>7 --> saturday</li>
     *					</ul>
     *	@see ViewDefinition
     */
    class ViewDefinitionCalendar extends \de\cas\open\server\viewdefinition\types\ViewDefinition {

        /**
         * @var int
         *
         *										The calendars interval to be used for this
         *										ViewDefinition. Only intervals greater zero are valid.
         */
        public $interval;

        /**
         * @var int
         *
         *										The day which should be displayed first.<br/>
         *										<code>startDay</code> is defined as one of:
         *										<ul>
         *											<li>1 --> sunday</li>
         *											<li>2 --> monday</li>
         *											<li>3 --> tuesday</li>
         *											<li>4 --> wednesday</li>
         *											<li>5 --> thursday</li>
         *											<li>6 --> friday</li>
         *											<li>7 --> saturday</li>
         *										</ul>
         */
        public $startDay;

        /**
         * @var string
         *
         *										Defines how the calendar should be displayed.
         *										@see ViewDisplayType
         */
        public $displayType;

        /**
         * @var array
         *
         *										Contains a List of user-/group-IDs that should be used for the teamFilter
         *										in the calendar view.
         */
        public $teamFilter;

    }

}
