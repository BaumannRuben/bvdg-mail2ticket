<?php
// This file has been automatically generated.

namespace de\cas\open\server\viewdefinition\types {

    /**
     * @package de\cas\open\server\viewdefinition
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\RequestObject: Saves a view-definition
     *				together with a
     *				filter-DataObject. Corresponding \de\cas\open\server\api\types\ResponseObject: SaveViewDefinitionResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see SaveViewDefinitionResponse
     */
    class SaveViewDefinitionRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var \de\cas\open\server\viewdefinition\types\ViewDefinition
         *
         *										The ViewDefinition to be saved.
         */
        public $viewDefinition;

        /**
         * @var boolean
         *
         *										Defines whether the server should save the filter
         *										that's appended to the ViewDefinition or not.
         */
        public $skipSaveFilter;

    }

}
