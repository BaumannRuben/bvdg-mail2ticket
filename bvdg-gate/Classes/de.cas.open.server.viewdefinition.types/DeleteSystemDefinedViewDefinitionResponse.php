<?php
// This file has been automatically generated.

namespace de\cas\open\server\viewdefinition\types {

    /**
     * @package de\cas\open\server\viewdefinition
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\ResponseObject: Empty response.
     *				Corresponding \de\cas\open\server\api\types\RequestObject:
     *				DeleteSystemDefinedViewDefinitionRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see DeleteSystemDefinedViewDefinitionRequest
     */
    class DeleteSystemDefinedViewDefinitionResponse extends \de\cas\open\server\api\types\ResponseObject {

    }

}
