<?php
// This file has been automatically generated.

namespace de\cas\open\server\viewdefinition\types {

    /**
     * @package de\cas\open\server\viewdefinition
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\ResponseObject: Contains the saved
     *				ViewDefinition that was added as
     *				search history item. Corresponding
     *				\de\cas\open\server\api\types\RequestObject: AddToSearchHistoryRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see AddToSearchHistoryRequest
     */
    class AddToSearchHistoryResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\viewdefinition\types\ViewDefinition
         *
         *										Sets/Returns the view definition that was added
         *										as search history item.
         */
        public $viewDefinition;

    }

}
