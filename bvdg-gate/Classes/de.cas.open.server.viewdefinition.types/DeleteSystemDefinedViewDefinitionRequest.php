<?php
// This file has been automatically generated.

namespace de\cas\open\server\viewdefinition\types {

    /**
     * @package de\cas\open\server\viewdefinition
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\RequestObject: Deletes a view
     *				definition that shipped with the application (system defined). The
     *				calling user must be activated for the operation set 'ViewAdmin'.<br/>
     *				Exception-Codes possibly thrown:<br/>
     *				<ul>
     *					<li>VIEWS_VIEW_NOT_FOUND - In case no view definition exists with the given id.</li>
     *					<li>VIEWS_NOT_A_SYSTEM_DEFINED_VIEW - In case the view to be deleted is not a system defined view.</li>
     *				</ul>
     *				Corresponding \de\cas\open\server\api\types\ResponseObject:
     *				DeleteSystemDefinedViewDefinitionResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see DeleteSystemDefinedViewDefinitionResponse
     */
    class DeleteSystemDefinedViewDefinitionRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										The ID of the system defined view to be deleted.
         */
        public $viewId;

    }

}
