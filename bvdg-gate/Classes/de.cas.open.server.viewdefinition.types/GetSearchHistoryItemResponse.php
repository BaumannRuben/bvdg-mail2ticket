<?php
// This file has been automatically generated.

namespace de\cas\open\server\viewdefinition\types {

    /**
     * @package de\cas\open\server\viewdefinition
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\ResponseObject: Contains the search
     *				history item - if found.
     *				Corresponding \de\cas\open\server\api\types\RequestObject: GetSearchHistoryItemRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetSearchHistoryItemRequest
     */
    class GetSearchHistoryItemResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\viewdefinition\types\ViewDefinition
         *
         *										Sets/Returns the search history item.
         */
        public $viewDefinition;

    }

}
