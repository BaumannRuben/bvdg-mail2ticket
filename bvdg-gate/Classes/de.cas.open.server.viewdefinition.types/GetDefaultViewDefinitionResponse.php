<?php
// This file has been automatically generated.

namespace de\cas\open\server\viewdefinition\types {

    /**
     * @package de\cas\open\server\viewdefinition
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\ResponseObject: Contains the
     *				default
     *				view definition (either system- or user-default). Corresponding
     *				\de\cas\open\server\api\types\RequestObject: GetDefaultViewDefinitionRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetDefaultViewDefinitionRequest
     */
    class GetDefaultViewDefinitionResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\viewdefinition\types\ViewDefinition
         *
         *										The view definition that's flagged as either
         *										being
         *										system default or user default for the given viewType and
         *										viewScope.
         */
        public $viewDefinition;

    }

}
