<?php
// This file has been automatically generated.

namespace de\cas\open\server\viewdefinition\types {

    /**
     * @package de\cas\open\server\viewdefinition
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\ResponseObject: Contains the
     *				system-default
     *				view definition. Corresponding
     *				\de\cas\open\server\api\types\RequestObject: GetSystemDefaultViewDefinitionRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetSystemDefaultViewDefinitionRequest
     */
    class GetSystemDefaultViewDefinitionResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\viewdefinition\types\ViewDefinition
         *
         *										The view definition that's flagged as being
         *										system default for the given viewType and viewScope.
         */
        public $viewDefinition;

    }

}
