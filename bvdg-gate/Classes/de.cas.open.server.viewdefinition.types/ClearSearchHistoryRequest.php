<?php
// This file has been automatically generated.

namespace de\cas\open\server\viewdefinition\types {

    /**
     * @package de\cas\open\server\viewdefinition
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\RequestObject: Clears the search
     *				history for a given viewType and viewScope for the
     *				logged in user.
     *				This operation is irreversible. Corresponding \de\cas\open\server\api\types\ResponseObject: ClearSearchHistoryResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see ClearSearchHistoryResponse
     */
    class ClearSearchHistoryRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										Sets/Returns the view type for which to clear the
         *										history.
         */
        public $viewType;

        /**
         * @var string
         *
         *										Sets/Returns the view scope for which to clear
         *										the
         *										history. (optional)
         */
        public $viewScope;

    }

}
