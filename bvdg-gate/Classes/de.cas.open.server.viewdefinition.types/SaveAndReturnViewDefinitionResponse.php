<?php
// This file has been automatically generated.

namespace de\cas\open\server\viewdefinition\types {

    /**
     * @package de\cas\open\server\viewdefinition
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\ResponseObject: Returns nothing.
     *				Corresponding \de\cas\open\server\api\types\RequestObject:
     *				SaveAndReturnViewDefinitionRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see SaveAndReturnViewDefinitionRequest
     */
    class SaveAndReturnViewDefinitionResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\viewdefinition\types\ViewDefinition
         *
         *										The ViewDefinition that was saved.
         */
        public $viewDefinition;

    }

}
