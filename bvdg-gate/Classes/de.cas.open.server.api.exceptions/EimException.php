<?php

namespace de\cas\open\server\api\exceptions {

    /**
     * @package de\cas\open\server\api
     * @subpackage exceptions
     */
    class EimException extends \Exception {

        /**
         * @var string
         */
        protected $detailCode;

        /**
         * @var integer
         */
        protected $createdOn;

        /**
         * @var string
         */
        protected $serverStackTrace;

        /**
         * @return string
         */
        public function getDetailCode() {
            return $this->detailCode;
        }

        /**
         * @param string $detailCode
         * @return void
         */
        public function setDetailCode($detailCode) {
            $this->detailCode = $detailCode;
        }

        /**
         * @return integer
         */
        public function getCreatedOn() {
            return $this->createdOn;
        }

        /**
         * @param $createdOn integer
         */
        public function setCreatedOn($createdOn) {
            $this->createdOn = $createdOn;
        }

        /**
         * @return string
         */
        public function getServerStackTrace() {
            return $this->serverStackTrace;
        }

        /**
         * @param string $serverStackTrace
         * @return void
         */
        public function setServerStackTrace($serverStackTrace) {
            $this->serverStackTrace = $serverStackTrace;
        }

        /**
         * @param string $message
         * @param string $detailCode
         * @param \Exception $cause
         */
        public function __construct($message = NULL, $detailCode = NULL, $previous = NULL) {
            parent::__construct($message, 0, $previous);

            $this->detailCode = $detailCode;
        }

        public function __toString() {
            return get_class($this) . ': ' . $this->getDetailCode() . ' - ' . $this->getMessage() . ' |' . PHP_EOL .
                   'Stack trace:' . PHP_EOL . $this->getTraceAsString() . ' |' . PHP_EOL .
                   'Server stack trace:' . PHP_EOL . $this->getServerStackTrace() . ' |' . PHP_EOL;
        }
    }
}
