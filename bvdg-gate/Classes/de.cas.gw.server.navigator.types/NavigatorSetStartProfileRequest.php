<?php
// This file has been automatically generated.

namespace de\cas\gw\server\navigator\types {

    /**
     * @package de\cas\gw\server\navigator
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Sets the navigator start profile for a given id.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: NavigatorSetStartProfileResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see NavigatorSetStartProfileResponse
     */
    class NavigatorSetStartProfileRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         */
        public $ProfileID;

    }

}
