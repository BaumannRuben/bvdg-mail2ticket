<?php
// This file has been automatically generated.

namespace de\cas\gw\server\navigator\types {

    /**
     * @package de\cas\gw\server\navigator
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Returns the navigator profile view for a given view id.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: NavigatorGetProfileViewResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see NavigatorGetProfileViewResponse
     */
    class NavigatorGetProfileViewRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *The profile id
         */
        public $ProfileID;

        /**
         * @var string
         *The view id
         */
        public $ViewID;

    }

}
