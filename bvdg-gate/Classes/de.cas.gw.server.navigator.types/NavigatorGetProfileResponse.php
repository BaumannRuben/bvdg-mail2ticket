<?php
// This file has been automatically generated.

namespace de\cas\gw\server\navigator\types {

    /**
     * @package de\cas\gw\server\navigator
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Returns the navigator profile for a given id.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: NavigatorGetProfileRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see NavigatorGetProfileRequest
     */
    class NavigatorGetProfileResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var string
         *The profile id
         */
        public $ProfileID;

        /**
         * @var string
         *
         *                    The navigator profile for a given view id.
         */
        public $Profile;

        /**
         * @var boolean
         *
         *                    Whether the navigator is public
         */
        public $IsPublic;

    }

}
