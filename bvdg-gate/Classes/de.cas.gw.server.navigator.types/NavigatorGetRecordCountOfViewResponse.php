<?php
// This file has been automatically generated.

namespace de\cas\gw\server\navigator\types {

    /**
     * @package de\cas\gw\server\navigator
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Returns the record count of a view in a navigator
     *        Corresponding \de\cas\open\server\api\types\RequestObject: NavigatorGetRecordCountOfViewRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see NavigatorGetRecordCountOfViewRequest
     */
    class NavigatorGetRecordCountOfViewResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var unknown
         *
         *                    The record count
         */
        public $Count;

    }

}
