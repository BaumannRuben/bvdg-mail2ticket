<?php
// This file has been automatically generated.

namespace de\cas\gw\server\navigator\types {

    /**
     * @package de\cas\gw\server\navigator
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Returns the navigator profile for a given id.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: NavigatorGetProfileResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see NavigatorGetProfileResponse
     */
    class NavigatorUpdateProfileRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         */
        public $ProfileID;

        /**
         * @var string
         *
         */
        public $XML;

        /**
         * @var boolean
         *
         */
        public $SetAsStartProfile;

    }

}
