<?php
// This file has been automatically generated.

namespace de\cas\gw\server\navigator\types {

    /**
     * @package de\cas\gw\server\navigator
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Returns the true if the properties were saved successfully.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: NavigatorSaveNodePropertiesRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see NavigatorSaveNodePropertiesRequest
     */
    class NavigatorSaveNodePropertiesResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var boolean
         *
         */
        public $Result;

    }

}
