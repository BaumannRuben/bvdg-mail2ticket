<?php
// This file has been automatically generated.

namespace de\cas\gw\server\navigator\types {

    /**
     * @package de\cas\gw\server\navigator
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Inserts a new navigator profile into the database.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: NavigatorAddProfileResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see NavigatorAddProfileResponse
     */
    class NavigatorAddProfileRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var boolean
         *
         */
        public $IsPublic;

        /**
         * @var boolean
         *
         */
        public $IsStartProfile;

        /**
         * @var string
         *
         */
        public $Profile;

        /**
         * @var string
         *
         */
        public $ProfileType;

    }

}
