<?php
// This file has been automatically generated.

namespace de\cas\gw\server\navigator\types {

    /**
     * @package de\cas\gw\server\navigator
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Returns the records that are contained in a view of a navigator.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: NavigatorGetRecordsOfViewRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see NavigatorGetRecordsOfViewRequest
     */
    class NavigatorGetRecordsOfViewResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\MassQueryResult
         *
         *                    The query result
         */
        public $QueryResult;

    }

}
