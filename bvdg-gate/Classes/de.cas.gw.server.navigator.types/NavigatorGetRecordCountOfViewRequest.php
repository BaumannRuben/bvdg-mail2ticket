<?php
// This file has been automatically generated.

namespace de\cas\gw\server\navigator\types {

    /**
     * @package de\cas\gw\server\navigator
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Returns the record count of a view in a navigator.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: NavigatorGetRecordCountOfViewResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see NavigatorGetRecordCountOfViewResponse
     */
    class NavigatorGetRecordCountOfViewRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    The profile id
         */
        public $ProfileID;

        /**
         * @var string
         *
         *                    The node id
         */
        public $NodeID;

        /**
         * @var string
         *
         *                    An additional WHERE-string
         */
        public $AdditionalWhereString;

    }

}
