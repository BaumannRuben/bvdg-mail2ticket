<?php
// This file has been automatically generated.

namespace de\cas\gw\server\navigator\types {

    /**
     * @package de\cas\gw\server\navigator
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Returns the available navigator profiles for the current user.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: NavigatorGetAvailableProfilesRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see NavigatorGetAvailableProfilesRequest
     */
    class NavigatorGetAvailableProfilesResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         */
        public $Profiles;

        /**
         * @var array
         *
         */
        public $DisplayNames;

    }

}
