<?php
// This file has been automatically generated.

namespace de\cas\gw\server\navigator\types {

    /**
     * @package de\cas\gw\server\navigator
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Returns the link navigator profile for a given name.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: NavigatorGetLinkProfileResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see NavigatorGetLinkProfileResponse
     */
    class NavigatorGetLinkProfileRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    The profile name
         */
        public $ProfileName;

    }

}
