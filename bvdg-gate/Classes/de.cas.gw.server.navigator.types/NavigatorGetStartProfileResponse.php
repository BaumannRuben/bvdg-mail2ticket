<?php
// This file has been automatically generated.

namespace de\cas\gw\server\navigator\types {

    /**
     * @package de\cas\gw\server\navigator
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Returns the ProfileID of the navigator start profile.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: NavigatorGetStartProfileRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see NavigatorGetStartProfileRequest
     */
    class NavigatorGetStartProfileResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var string
         *
         *                    The profile id of the navigator start profile
         */
        public $ProfileID;

    }

}
