<?php
// This file has been automatically generated.

namespace de\cas\gw\server\navigator\types {

    /**
     * @package de\cas\gw\server\navigator
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Returns the navigator profile for a given id and datatype.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: NavigatorGetProfileForDataTypeRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see NavigatorGetProfileForDataTypeRequest
     */
    class NavigatorGetProfileForDataTypeResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var string
         *The profile id
         */
        public $ProfileID;

        /**
         * @var string
         *The datatype
         */
        public $DataType;

        /**
         * @var string
         *
         *                    The navigator profile for a given view id and datatype.
         */
        public $Profile;

        /**
         * @var boolean
         *
         *                    Whether the navigator is public
         */
        public $IsPublic;

    }

}
