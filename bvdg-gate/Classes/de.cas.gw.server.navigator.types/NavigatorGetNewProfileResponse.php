<?php
// This file has been automatically generated.

namespace de\cas\gw\server\navigator\types {

    /**
     * @package de\cas\gw\server\navigator
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Returns a empty navigator profile.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: NavigatorGetNewProfileRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see NavigatorGetNewProfileRequest
     */
    class NavigatorGetNewProfileResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var string
         *
         */
        public $Profile;

    }

}
