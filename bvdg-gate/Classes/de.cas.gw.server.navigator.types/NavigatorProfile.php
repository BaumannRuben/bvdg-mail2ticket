<?php
// This file has been automatically generated.

namespace de\cas\gw\server\navigator\types {

    /**
     * @package de\cas\gw\server\navigator
     * @subpackage types
     *
     *        Represents a navigator profile.
     */
    class NavigatorProfile {

        /**
         * @var string
         *
         *                Sets/Returns the gguid of the navigator profile
         */
        public $GGUID;

        /**
         * @var string
         *
         *                Sets/Returns the display name of the navigator profile
         */
        public $Keyword;

        /**
         * @var string
         *
         *                Sets/Returns the file type of the navigator profile. The file type contains the language of
         *                the navigator profile. Therefor the ISO-Code of the langauge is attached to the file type.
         */
        public $FileType;

        /**
         * @var string
         *
         *                Sets/Returns the ISO-Code for the language of the navigator profile
         */
        public $Language;

        /**
         * @var string
         *
         *                Sets/Returns the name of the user who modified the navigator profile.
         */
        public $UpdateUser;

        /**
         * @var unknown
         *
         *                Sets/Returns update time stamp of the navigator profile.
         */
        public $UpdateTimeStamp;

        /**
         * @var unknown
         *
         *                Sets/Returns insert time stamp of the navigator profile.
         */
        public $InsertTimeStamp;

        /**
         * @var string
         *
         *                Sets/Returns the owner name of the navigator profile.
         */
        public $OwnerName;

        /**
         * @var string
         *
         *                Sets/Returns the owner guid of the navigator profile.
         */
        public $OwnerGuid;

    }

}
