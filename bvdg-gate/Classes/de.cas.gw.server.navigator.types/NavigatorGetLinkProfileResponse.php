<?php
// This file has been automatically generated.

namespace de\cas\gw\server\navigator\types {

    /**
     * @package de\cas\gw\server\navigator
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Returns the link navigator profile for a given name.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: NavigatorGetLinkProfileRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see NavigatorGetLinkProfileRequest
     */
    class NavigatorGetLinkProfileResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var string
         *
         *                    The profile id
         */
        public $ProfileID;

        /**
         * @var string
         *
         *                    The navigator profile
         */
        public $Profile;

        /**
         * @var boolean
         *
         *                    Whether the navigator is public
         */
        public $IsPublic;

    }

}
