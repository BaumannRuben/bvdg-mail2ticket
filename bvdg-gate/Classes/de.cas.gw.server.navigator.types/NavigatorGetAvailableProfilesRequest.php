<?php
// This file has been automatically generated.

namespace de\cas\gw\server\navigator\types {

    /**
     * @package de\cas\gw\server\navigator
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Gets the available navigator profiles for the current user.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: NavigatorGetAvailableProfilesResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see NavigatorGetAvailableProfilesResponse
     */
    class NavigatorGetAvailableProfilesRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         */
        public $ProfileType;

    }

}
