<?php
// This file has been automatically generated.

namespace de\cas\gw\server\navigator\types {

    /**
     * @package de\cas\gw\server\navigator
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Returns the true if the navigator start profile was set successfully.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: NavigatorSetStartProfileResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see NavigatorSetStartProfileResponse
     */
    class NavigatorSetStartProfileResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var boolean
         *
         */
        public $Result;

    }

}
