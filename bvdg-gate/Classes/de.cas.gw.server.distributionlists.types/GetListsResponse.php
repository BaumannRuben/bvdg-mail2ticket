<?php
// This file has been automatically generated.

namespace de\cas\gw\server\distributionlists\types {

    /**
     * @package de\cas\gw\server\distributionlists
     * @subpackage types
     *
     *				Returns the distribution lists.
     */
    class GetListsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\MassQueryResult
         *
         *										MassQueryResult with the following columns:
         *										- GGUID = GGUID
         *										- KEYWORD = Keyword of the distributor
         *										- OWNERNAME = ownername of the distributor
         *										- COUNT - the number of address contained
         */
        public $result;

    }

}
