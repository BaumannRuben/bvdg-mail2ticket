<?php
// This file has been automatically generated.

namespace de\cas\gw\server\distributionlists\types {

    /**
     * @package de\cas\gw\server\distributionlists
     * @subpackage types
     *
     *				Adds address (es) to a distribution list.
     *				See: https://partnerportal.cas.de/SDKDocu/#12761
     */
    class AddItemsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										GGUID of the distribution list record.
         */
        public $DistributionListGGUID;

        /**
         * @var array
         *
         *										GGUID of the address records.
         */
        public $AddressGGUIDs;

    }

}
