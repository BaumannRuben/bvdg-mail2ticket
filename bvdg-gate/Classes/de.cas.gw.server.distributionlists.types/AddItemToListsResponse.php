<?php
// This file has been automatically generated.

namespace de\cas\gw\server\distributionlists\types {

    /**
     * @package de\cas\gw\server\distributionlists
     * @subpackage types
     *
     *				Returns with the result of the AddItems
     */
    class AddItemToListsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *										Indicates if the operation for the address at the specific index was successfull or not.
         */
        public $result;

    }

}
