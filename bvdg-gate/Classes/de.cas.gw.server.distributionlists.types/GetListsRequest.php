<?php
// This file has been automatically generated.

namespace de\cas\gw\server\distributionlists\types {

    /**
     * @package de\cas\gw\server\distributionlists
     * @subpackage types
     *
     *				Supplies included in the distribution lists.
     *				See: https://partnerportal.cas.de/SDKDocu/#12769
     */
    class GetListsRequest extends \de\cas\open\server\api\types\RequestObject {

    }

}
