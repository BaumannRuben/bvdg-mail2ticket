<?php
// This file has been automatically generated.

namespace de\cas\gw\server\distributionlists\types {

    /**
     * @package de\cas\gw\server\distributionlists
     * @subpackage types
     *
     *				Returns with the result of the DeleteItems
     */
    class DeleteItemsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var boolean
         *
         *										True if at least one address could be deleted, otherwise false.
         */
        public $result;

    }

}
