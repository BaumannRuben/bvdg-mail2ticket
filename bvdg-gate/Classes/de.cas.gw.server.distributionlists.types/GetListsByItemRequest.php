<?php
// This file has been automatically generated.

namespace de\cas\gw\server\distributionlists\types {

    /**
     * @package de\cas\gw\server\distributionlists
     * @subpackage types
     *
     *				Supplies included in the distribution of the given address.
     *				See: https://partnerportal.cas.de/SDKDocu/#12769
     */
    class GetListsByItemRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										GGUID of the address record.
         */
        public $AddressGGUID;

    }

}
