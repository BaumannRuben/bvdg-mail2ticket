<?php

namespace de\cas\open\server\api {

    use \de\cas\open\server\api\types\DataObject;
    use \de\cas\open\server\api\types\DataObjectDescription;
    use \de\cas\open\server\api\types\RequestObject;
    use \de\cas\open\server\api\types\ResponseObject;
    use \de\cas\open\server\api\types\MassUpdateData;
    use \de\cas\open\server\api\types\MassQueryResult;
    use \de\cas\open\server\api\types\ManagementRequestObject;
    use \de\cas\open\server\api\types\ManagementResponseObject;

    /**
     * Facade to access our EIM server. This is facade is our API, which can be used by us and our partners.
     *
     * @package de\cas\open\server\api
     */
    interface IEIMInterface {

        /**
         * This method is used to execute business logic on the server. The type of business logic is defined by the FQCN of the RequestObject subclass. This RequestObject also wraps all needed parameters for the business logic implementation.
         *
         * @param RequestObject $requestObject Subclasses of RequestObject defines the business logic to execute.
         * @return ResponseObject the result of the business logic.
         */
        public function execute($requestObject);

        /**
         * Same as execute, but it's usable for business operations which don't need a authentication
         *
         * @param RequestObject $requestObject Subclasses of RequestObject defines the business logic to execute.
         * @return ResponseObject the result of the business logic.
         */
        public function executeUnauthenticated($requestObject);

        /**
         * This method creates a new data object. CAS provides a set of the most common data types (like appointment, address, ...) but also supports the creation of new data types. The object type is specified by string constants, which are listed in the "DataObjectType" enumeration. The created object isn't persistent until the save method is called.
         *
         * @param string $objectType object type to create
         * @return DataObject a DataObject of the specified type
         */
        public function createObject($objectType);

        /**
         * Creates a new (saved) {@link DataObject} from a template. Creating an object from a template is basically a duplication of the template and resetting the flag indicating that the object is a template. This method includes a check that the given template is indeed a template, i.e. the field <code>ISTEMPLATE</code> is <code>true</code>, otherwise a {@link de.cas.open.server.api.exceptions.DataLayerException} with {@link de.cas.open.server.api.constants.ExceptionCode#DATA_LAYER_OBJECT_IS_NOT_A_TEMPLATE} is thrown. The duplication of a template includes the duplication of its links.
         *
         * @param string $objectType the object type
         * @param string $templateGUID the template's GGUID
         * @return DataObject the new {@link DataObject}
         */
        public function createObjectFromTemplate($objectType, $templateGUID);

        /**
         * This method retrieves a data object. If an object can't be found for a given id, a soap fault is returned. In general, a soap fault will be mapped to an exception.
         *
         * @param string $typeName   type of requested object
         * @param string $gguid  GGUID of object to retrieve
         * @param array $requestedFields fields to retrieve (optional)
         * @return DataObject returns the requested DataObject
         */
        public function getObject($typeName, $gguid, $requestedFields = NULL);

        /**
         * This method saves a generic {@link DataObject}. Only after this method, a data object is persistent. That applies for updated as well as for newly created objects. After a successful invocation the object has a assigned id, which can retrieved by the returned object.
         *
         * @param DataObject $dataObject object to save, has to be a GenericDataObject.
         * @return DataObject returns the saved DataObject (in case of new object: the object has now an id)
         */
        public function saveAndReturnObject($dataObject);

        /**
         * Same as {@link #saveAndReturnObject(DataObject)}, but this method doesn't return the saved object. This means you does not get the system fields (GGUID, UpdateTimestamp, ...) of the DataObject
         *
         * @param DataObject $dataObject object to save
         * @return void
         */
        public function saveObject($dataObject);

        /**
         * Saves multiple objects. This methos does not detect any update conflicts.
         *
         * @param MassUpdateData $massUpdateData  the data to save
         * @return array
         *         an {@link IndexedGuidEIMExceptionContainer} for every record that could not be
         *         saved to the database. The {@link IndexedGuidEIMExceptionContainer}s cover only
         *         exceptions that come from insufficient permissions. All other failures will be
         *         signaled using a {@link DataLayerException}.
         */
        public function saveMultipleObjects($massUpdateData);

        /**
         * This method returns an echo and it's only an "availability" test service.
         *
         * @param string $echoString string which must be returned again
         * @return string the input string
         */
        public function echo_($echoString);

        /**
         * This method deletes a data object. After this method, the object is deleted but still available in the recycle bin.
         *
         * @param string $typeName  object to delete
         * @param string $gguid  GGUID of object which will be deleted
         * @return void
         */
        public function deleteObject($typeName, $gguid);

        /**
         * This method undeletes a data object which was previously tagged as deleted (placed in the recycle bin).
         *
         * @param string $typeName  object to undelete
         * @param string $gguid GGUID of object which will be undeleted
         * @return void
         */
        public function undeleteObject($typeName, $gguid);

        /**
         * This method purges a data object. It will be removed unrecoverably.
         *
         * @param string $typeName  object to purge
         * @param string $gguid GGUID of object which will be purged
         * @return void
         */
        public function purgeObject($typeName, $gguid);

        /**
         * Executes a query (<strong>only SELECT</strong>) and returnes a {@link MassQueryResult} object containing an entry for each returned row and the types of the fields in the rows. See <a href="../{@docRoot} /CAS_SQL.html /CAS_SQL.html</a> for the specified SQL subset.
         *
         * @param string $query The SELECT-statement for the query.
         * @param boolean $includePrivateForeignRecords If true a SELECT statement is cloned and appended with a UNION so that it also returns private records of other users. (optional)
         * @param integer $minPermission  the minimum permission the current user must have on the record (see {@link de.cas.open.server.api.types.EIMPermissionConstants}) (optional)
         * @return MassQueryResult a {@link MassQueryResult} object containing an entry for each returned row and the types of the fields in the rows
         */
        public function query($query, $includePrivateForeignRecords = NULL, $minPermission = NULL);

        /**
         * Executes a query (<strong>only SELECT</strong>) and returnes a {@link MassQueryResult} object containing an entry for each returned row and the types of the fields in the rows. See <a href="../{@docRoot} /CAS_SQL.html /CAS_SQL.html</a> for the specified SQL subset.
         *
         * @param string $query The SELECT-statement for the query.
         * @param integer $firstRecord The first record that shall be returned, that is the offset (<strong>zero-based</strong>) of the first record in the result of the database query. (optional)
         * @param integer $recordCount  The number of records that shall be contained in the result (if the query returns more rows than this). (optional)
         * @param boolean $includePrivateForeignRecords If true a SELECT statement is cloned and appended with a UNION so that it also returns private records of other users. (optional)
         * @param integer $minPermission  the minimum permission the current user must have on the record (see {@link de.cas.open.server.api.types.EIMPermissionConstants}) (optional)
         * @return MassQueryResult a {@link MassQueryResult} object containing an entry for each returned row and the types of the fields in the rows
         */
        public function queryPaged($query, $firstRecord, $recordCount, $includePrivateForeignRecords = NULL, $minPermission = NULL);

        /**
         * Returns the number of records a call of the method {@link #query(String)} would return. This is useful if you want to retrieve the data using {@link #query(String, long, long)} (paging).
         *
         * @param string $query The SELECT-statement for the query.
         * @return integer the number of records
         */
        public function getQueryRecordCount($query);

        /**
         * This method returns meta information for a DataObject type. It contains all field names and the type of the fields. Additionally, there are fields like 'mandatory' or 'hidden'.
         *
         * @param string $typeName object to describe
         * @return DataObjectDescription for the given type
         */
        public function getObjectDescription($typeName);

        /**
         * Returns a list with all available DataObject types (i.e. Appointment, Document, Contact, ...)
         *
         * @return array returns a list of type names
         */
        public function getAvailableObjectNames();

        /**
         * Returns the version number of the server.
         *
         * @return string version number as string: major.minor.build
         */
        public function getVersion();

        /**
         * Duplicates the <code>objectToDuplicate</code> including all permissions, tags and links. It is ensured that the user has the permission to write the object after the duplication.
         *
         * @param string $objectType  type of object that will be duplicated
         * @param string $gguid  GGUID of object that will be duplicated
         * @param boolean $duplicateAsTemplate   if <code>true</code> the duplicate will be a template (optional)
         * @param boolean $copyLinks   if <code>true</code> all links (including those that the linkpartner may not read) shall be copied to the duplicate (optional)
         * @return DataObject the duplicated and saved object
         */
        public function duplicateObject($objectType, $gguid, $duplicateAsTemplate = FALSE, $copyLinks = FALSE);

        /**
         * This method is used to execute management operations on the server. The type of business logic is defined by the FQCN of the ManagementRequestObject subclass. This ManagementRequestObject also wrapps all needed parameters for the business logic implementation.
         *
         * @param ManagementRequestObject requestObject Subclasses of ManagementRequestObject defines thes business logic to exceute.
         * @return ManagementResponseObject the result of the business logic.
         */
        public function executeManagementOperation($requestObject);

        /**
         * Returns the URL of the server (RMI or webserice).
         *
         * @return string the URL of the server
         */
        public function getServerUrl();
    }
}
