<?php
// This file has been automatically generated.

namespace de\cas\open\server\publicholiday\types {

    /**
     * @package de\cas\open\server\publicholiday
     * @subpackage types
     *\de\cas\open\server\api\types\RequestObject:<br/> Retrieves all countries, states and regions that have
     *			public holidays stored in the database.
     *			<br/>Corresponding \de\cas\open\server\api\types\ResponseObject: GetAvailableCountriesStatesRegionsResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetAvailableCountriesStatesRegionsResponse
     */
    class GetAvailableCountriesStatesRegionsRequest extends \de\cas\open\server\api\types\RequestObject {

    }

}
