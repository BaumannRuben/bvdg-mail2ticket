<?php
// This file has been automatically generated.

namespace de\cas\open\server\publicholiday\types {

    /**
     * @package de\cas\open\server\publicholiday
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\ResponseObject: Retrieves all public
     *				holidays within a given timespan
     *				for a given country/state/region.
     *				Corresponding \de\cas\open\server\api\types\RequestObject:
     *				GetPublicHolidaysInTimespanRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetPublicHolidaysInTimespanRequest
     */
    class GetPublicHolidaysInTimespanResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *										Sets/Returns the retrieved public holidays.
         */
        public $publicHolidays;

    }

}
