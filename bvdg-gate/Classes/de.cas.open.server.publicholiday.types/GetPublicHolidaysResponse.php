<?php
// This file has been automatically generated.

namespace de\cas\open\server\publicholiday\types {

    /**
     * @package de\cas\open\server\publicholiday
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\ResponseObject: Retrieves all public
     *				holidays for a given year in a
     *				given country/state/region.
     *				Corresponding \de\cas\open\server\api\types\RequestObject:
     *				GetPublicHolidaysRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetPublicHolidaysRequest
     */
    class GetPublicHolidaysResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *										Sets/Returns the retrieved public holidays.
         */
        public $publicHolidays;

    }

}
