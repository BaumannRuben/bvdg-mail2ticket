<?php
// This file has been automatically generated.

namespace de\cas\open\server\publicholiday\types {

    /**
     * @package de\cas\open\server\publicholiday
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\RequestObject: Retrieves all public
     *				holidays for a given year in a
     *				given country/state/region.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject:
     *				GetPublicHolidaysResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetPublicHolidaysResponse
     */
    class GetPublicHolidaysRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var int
         *
         *										The year the public holidays shall be returned
         *										for.
         */
        public $year;

        /**
         * @var string
         *
         *										The country the public holidays shall be returned
         *										for.
         */
        public $country;

        /**
         * @var string
         *
         *										The state the public holidays shall be returned
         *										for. If
         *										null, all states in the country will be returned, if
         *										any exist.
         */
        public $state;

        /**
         * @var string
         *
         *										The region the public holidays shall be returned
         *										for.
         *										If null, all regions in states of the country will be
         *										returned, if any exist.
         */
        public $region;

    }

}
