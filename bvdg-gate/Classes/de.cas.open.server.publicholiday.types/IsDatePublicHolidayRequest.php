<?php
// This file has been automatically generated.

namespace de\cas\open\server\publicholiday\types {

    /**
     * @package de\cas\open\server\publicholiday
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\RequestObject: Checks if a given date
     *				is a public holiday in the given
     *				country/state/region. Corresponding
     *				\de\cas\open\server\api\types\ResponseObject: IsDatePublicHolidayResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see IsDatePublicHolidayResponse
     */
    class IsDatePublicHolidayRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var unknown
         *Date to be checked.
         */
        public $date;

        /**
         * @var string
         *
         *										Country of the possible public holiday.
         */
        public $country;

        /**
         * @var string
         *
         *										State of the possible public holiday.
         */
        public $state;

        /**
         * @var string
         *
         *										Region of the possible public holiday.
         */
        public $region;

    }

}
