<?php
// This file has been automatically generated.

namespace de\cas\open\server\publicholiday\types {

    /**
     * @package de\cas\open\server\publicholiday
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject:Retrieves the country code for
     *      public holidays configured for the current user.
     *			Corresponding \de\cas\open\server\api\types\ResponseObject: GetCurrentUsersPublicHolidayCountryCodeResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetCurrentUsersPublicHolidayCountryCodeResponse
     */
    class GetCurrentUsersPublicHolidayCountryCodeRequest extends \de\cas\open\server\api\types\RequestObject {

    }

}
