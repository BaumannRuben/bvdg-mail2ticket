<?php

namespace de\cas\open\server\api\eiminterface {

    use \de\cas\open\server\api\IEIMInterface;
    use \de\cas\open\server\api\eimws\EIMServicePortTypeClient;
    use \de\cas\open\server\api\types\DataObjectTransferable;
    use \de\cas\open\server\api\types\DataObject;
    use \de\cas\open\server\api\types\DataObjectDescriptionTransferable;
    use \de\cas\open\server\api\types\DataObjectDescription;

    /**
     * EIMWebServiceInterface
     *
     * @package de\cas\open\server\api
     * @subpackage eiminterface
     */
    class EIMWebServiceInterface implements IEIMInterface {

        /**
         * @var \de\cas\open\server\api\eimws\EIMServicePortTypeClient
         */
        private $service;

        /**
         * @return \de\cas\open\server\api\eimws\EIMServicePortTypeClient
         */
        public function getService() {
            return $this->service;
        }

        /**
         * @param \de\cas\open\server\api\eimws\EIMServicePortTypeClient $service
         * @return void
         */
        public function setService(EIMServicePortTypeClient $service) {
            $this->service = $service;
        }

        /**
         * @param \de\cas\open\server\api\eimws\EIMServicePortTypeClient $service
         */
        public function __construct(EIMServicePortTypeClient $service) {
            $this->setService($service);
        }

        /**
         * Extracts a value from an object.
         *
         * @param object|array $object
         * @return mixed
         */
        protected static function dismantle($object) {
            foreach ($object as $value) {
                return $value;
            }
            return NULL;
        }

        public function createObject($objectType) {
            $this->assertArgumentNotNull('$objectType', $objectType);
            // DataObjectTransferable
            $transferable = self::dismantle($this->getService()->createObject(array('typeName' => $objectType)));
            // DataObject
            return DataObject::fromDataObjectTransferable($transferable);
        }

        public function createObjectFromTemplate($objectType, $templateGUID) {
            $this->assertArgumentNotNull('$objectType', $objectType);
            $this->assertArgumentNotNull('$templateGUID', $templateGUID);
            // DataObjectTransferable
            $transferable = self::dismantle($this->getService()->createObjectFromTemplate(array(
                    'typeName' => $objectType,
                    'gguid' => $templateGUID,
                )));
            // DataObject
            return DataObject::fromDataObjectTransferable($transferable);
        }

        public function deleteObject($typeName, $gguid) {
            $this->assertArgumentNotNull('$typeName', $typeName);
            $this->assertArgumentNotNull('$gguid', $gguid);
            $this->getService()->deleteObject(array(
                    'typeName' => $typeName,
                    'gguid' => $gguid,
                ));
        }

        public function deleteMultipleObjects($typeName, $gguids) {
            $this->assertArgumentNotNull('$typeName', $typeName);
            $this->assertArgumentArray('$gguids', $gguids);
            $this->getService()->deleteMultipleObjects(array(
                    'objectType' => $typeName,
                    'gguids' => $gguids,
                ));
        }

        public function undeleteObject($typeName, $gguid) {
            $this->assertArgumentNotNull('$typeName', $typeName);
            $this->assertArgumentNotNull('$gguid', $gguid);
            $this->getService()->undeleteObject(array(
                    'typeName' => $typeName,
                    'gguid' => $gguid,
                ));
        }

        public function undeleteMultipleObjects($typeName, $gguids) {
            $this->assertArgumentNotNull('$typeName', $typeName);
            $this->assertArgumentArray('$gguids', $gguids);
            $this->getService()->undeleteMultipleObjects(array(
                    'objectType' => $typeName,
                    'gguids' => $gguids,
                ));
        }

        public function purgeObject($typeName, $gguid) {
            $this->assertArgumentNotNull('$typeName', $typeName);
            $this->assertArgumentNotNull('$gguid', $gguid);
            $this->getService()->purgeObject(array(
                    'typeName' => $typeName,
                    'gguid' => $gguid,
                ));
        }

        public function purgeMultipleObjects($typeName, $gguids) {
            $this->assertArgumentNotNull('$typeName', $typeName);
            $this->assertArgumentArray('$gguids', $gguids);
            $this->getService()->purgeMultipleObjects(array(
                    'objectType' => $typeName,
                    'gguids' => $gguids,
                ));
        }

        public function echo_($echoString) {
            return self::dismantle($this->getService()->echo(array('echoString' => $echoString)));
        }

        public function execute($requestObject) {
            $this->assertArgumentNotNull('$requestObject', $requestObject);
            return self::dismantle($this->getService()->execute(array('requestObject' => $requestObject)));
        }

        public function executeUnauthenticated($requestObject) {
            $this->assertArgumentNotNull('$requestObject', $requestObject);
            return self::dismantle($this->getService()->executeUnauthenticated(array('requestObject' => $requestObject)));
        }

        public function executeManagementOperation($requestObject) {
            $this->assertArgumentNotNull('$requestObject', $requestObject);
            return self::dismantle($this->getService()->executeManagementOperation(array('requestObject' => $requestObject)));
        }

        public function getAvailableObjectNames() {
            return self::dismantle($this->getService()->getAvailableObjectNames());
        }

        public function getObject($typeName, $gguid, $requestedFields = NULL) {
            $this->assertArgumentNotNull('$typeName', $typeName);
            $this->assertArgumentNotNull('$gguid', $gguid);
            if (is_null($requestedFields)) {
                // DataObjectTransferable
                $transferable = self::dismantle($this->getService()->getObject(array(
                        'typeName' => $typeName,
                        'GGUID' => $gguid,
                    )));
            } else {
                $this->assertArgumentArray('$requestedFields', $requestedFields);
                // DataObjectTransferable
                $transferable = self::dismantle($this->getService()->getPartialObject(array(
                        'typeName' => $typeName,
                        'GGUID' => $gguid,
                        'requestedFields' => $requestedFields,
                    )));
            }
            // DataObject
            return DataObject::fromDataObjectTransferable($transferable);
        }

        public function query($query, $includePrivateForeignRecords = NULL, $minPermission = NULL) {
            $this->assertArgumentNotNull('$query', $query);

            if (is_null($includePrivateForeignRecords) && is_null($minPermission)) {
                return self::dismantle($this->getService()->query(array('expression' => $query)));

            } else {
                if (is_null($includePrivateForeignRecords))
                    $includePrivateForeignRecords = FALSE;
                if (is_null($minPermission))
                    $minPermission = -1;
                $this->assertArgumentBoolean('$includePrivateForeignRecords', $includePrivateForeignRecords);
                $this->assertArgumentInteger('$minPermission', $minPermission);

                return self::dismantle($this->getService()->queryWithPermission(array(
                        'expression' => $query,
                        'includePrivateForeignRecords' => $includePrivateForeignRecords,
                        'minPermission' => $minPermission,
                    )));
            }
        }

        public function queryPaged($query, $firstRecord, $recordCount, $includePrivateForeignRecords = NULL, $minPermission = NULL) {
            $this->assertArgumentNotNull('$query', $query);

            $this->assertArgumentInteger('$firstRecord', $firstRecord);
            $this->assertArgumentInteger('$recordCount', $recordCount);

            if (is_null($includePrivateForeignRecords) && is_null($minPermission)) {
                return self::dismantle($this->getService()->queryPaged(array(
                        'expression' => $query,
                        'firstRecord' => $firstRecord,
                        'recordCount' => $recordCount,
                    )));

            } else {
                if (is_null($includePrivateForeignRecords))
                    $includePrivateForeignRecords = FALSE;
                if (is_null($minPermission))
                    $minPermission = -1;
                $this->assertArgumentBoolean('$includePrivateForeignRecords', $includePrivateForeignRecords);
                $this->assertArgumentInteger('$minPermission', $minPermission);

                return self::dismantle($this->getService()->queryPagedWithPermission(array(
                        'expression' => $query,
                        'firstRecord' => $firstRecord,
                        'recordCount' => $recordCount,
                        'includePrivateForeignRecords' => $includePrivateForeignRecords,
                        'minPermission' => $minPermission,
                    )));
            }
        }

        public function getQueryRecordCount($query) {
            $this->assertArgumentNotNull('$query', $query);
            return self::dismantle($this->getService()->getQueryRecordCount(array('expression' => $query)));
        }

        public function getObjectDescription($typeName) {
            $this->assertArgumentNotNull('$typeName', $typeName);
            // DataObjectDescriptionTransferable
            $transferable = self::dismantle($this->getService()->getObjectDescription(array('typeName' => $typeName)));
            // DataObjectDescription
            return DataObjectDescription::fromDataObjectDescriptionTransferable($transferable);
        }

        public function saveAndReturnObject($dataObject) {
            $this->assertArgumentNotNull('$dataObject', $dataObject);
            // DataObjectTransferable
            $transferable = $dataObject->toDataObjectTransferable();
            $transferable = self::dismantle($this->getService()->saveAndReturnObject(array('dataObject' => $transferable)));
            // DataObject
            return DataObject::fromDataObjectTransferable($transferable);
        }

        public function saveObject($dataObject) {
            $this->assertArgumentNotNull('$dataObject', $dataObject);
            // DataObjectTransferable
            $transferable = $dataObject->toDataObjectTransferable();
            $this->getService()->saveObject(array('dataObject' => $transferable));
        }

        public function saveMultipleObjects($data) {
            $this->assertArgumentNotNull('$data', $data);
            return self::dismantle($this->getService()->saveMultipleObjects(array('data' => $data)));
        }

        public function duplicateObject($objectType, $gguid, $duplicateAsTemplate = FALSE, $copyLinks = FALSE) {
            $this->assertArgumentNotNull('$objectType', $objectType);
            $this->assertArgumentNotNull('$gguid', $gguid);
            $this->assertArgumentBoolean('$duplicateAsTemplate', $duplicateAsTemplate);
            $this->assertArgumentBoolean('$copyLinks', $copyLinks);
            // DataObjectTransferable
            $transferable = self::dismantle($this->getService()->duplicateObject(array(
                    'typeName' => $objectType,
                    'GGUID' => $gguid,
                    'duplicateAsTemplate' => $duplicateAsTemplate,
                    'copyLinks' => $copyLinks,
                )));
            // DataObject
            return DataObject::fromDataObjectTransferable($transferable);
        }

        public function getVersion() {
            return self::dismantle($this->getService()->getVersion());
        }

        protected function assertArgumentNotNull($argName, $arg) {
            if (is_null($arg)) {
                throw new \InvalidArgumentException("Argument $argName must not be null.");
            }
        }

        protected function assertArgumentBoolean($argName, $arg) {
            if (!is_bool($arg)) {
                throw new \InvalidArgumentException("Argument $argName must be boolean.");
            }
        }

        protected function assertArgumentInteger($argName, $arg) {
            if (!is_int($arg)) {
                throw new \InvalidArgumentException("Argument $argName must be an integer.");
            }
        }

        protected function assertArgumentArray($argName, $arg) {
            if (!is_array($arg)) {
                throw new \InvalidArgumentException("Argument $argName must be an array.");
            }
        }

        /**
         * @see \de\cas\open\server\api\IEIMInterface::getServerUrl()
         */
        public function getServerUrl() {
            return $this->getService()->getServerUrl();
        }
    }
}
