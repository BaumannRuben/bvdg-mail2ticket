<?php

namespace de\cas\open\server\api\eiminterface {

    use \de\cas\open\server\api\IEIMInterface;
    use \de\cas\open\server\api\eiminterface\EIMWebServiceInterface;
    use \de\cas\open\server\api\eiminterface\interceptors\WsSecurityHeaderInterceptor;
    use \de\cas\open\server\api\eiminterface\interceptors\ProductKeyWebServiceHeaderInterceptor;
    use \de\cas\open\server\api\eiminterface\security\ISecurityTokenProvider;
    use \de\cas\open\server\api\eimws\EIMServicePortTypeClient;

    /**
     * Creates an connection to a CAS genesisWorld or Open server.
     *
     * @package de\cas\open\server\api
     * @subpackage eiminterface
     *
     * @see \de\cas\open\server\api\IEIMInterface
     */
    class EIMInterfaceFactory {

        private $eimInterface;
        private $securityTokenProvider;

        /**
         * Gets the EIMInterface used to communicate with the server.
         *
         * @return \de\cas\open\server\api\IEIMInterface
         */
        public function getEIMInterface() {
            return $this->eimInterface;
        }

        /**
         * Gets the ISecurityTokenProvider that is used for request authentication.
         *
         * @return \de\cas\open\server\api\eiminterface\security\ISecurityTokenProvider
         */
        public function getSecurityTokenProvider() {
            return $this->securityTokenProvider;
        }

        /**
         * Creates an connection to a CAS genesisWorld or Open server.
         *
         * @param \de\cas\open\server\api\eiminterface\security\ISecurityTokenProvider $tokenProvider is used to provide the security token for request authentication
         * @param string $url URL of the webservice
         * @param string $productKey product key
         * @param callback $debugLoggingCallback
         * @param array $soapOptions
         */
        public function __construct(ISecurityTokenProvider $tokenProvider = NULL, $url, $productKey, $debugLoggingCallback = NULL, $soapOptions = NULL) {
            if (is_null($url)) {
                throw new \InvalidArgumentException('URL must not be null');
            }

            $this->securityTokenProvider = $tokenProvider;

            $service = new EIMServicePortTypeClient($url, $debugLoggingCallback, $soapOptions);

            array_push($service->getMessageInspectors(), new WsSecurityHeaderInterceptor($this->getSecurityTokenProvider()));
            array_push($service->getMessageInspectors(), new ProductKeyWebServiceHeaderInterceptor($productKey));

            $this->eimInterface = new EIMWebServiceInterface($service);
        }
    }
}
