<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *				Meta information object that describes a Field of a DataObject.
     */
    class FieldDescription {

        /**
         * @var int
         *
         *								Returns the field's ID. You need the ID
         *								to define a ColumnAccessRestriction.
         *	@see ColumnAccessRestriction
         */
        public $id;

        /**
         * @var string
         *
         *								The name of the field
         */
        public $name;

        /**
         * @var string
         *
         *								The type of the described field (e.g.
         *								StringField)
         */
        public $fieldType;

        /**
         * @var boolean
         *
         *								Sets/Returns if the field is mandatory.
         *								That means it must be set before the
         *								DataObject can be saved, otherwise the
         *								application server will throw an
         *								exception. The server does not throw
         *								an exception if the field is defined
         *								by the customer as not every client
         *								(e.g. mobile sync, mobile access)
         *								supports custom fields.
         */
        public $mandatory;

        /**
         * @var boolean
         *
         *								Sets/Returns if the field is
         *								recommended. That means it should be
         *								set, but it has no to be set. The client
         *								may warn the user.
         */
        public $recommended;

        /**
         * @var boolean
         *
         *								If true the field is hidden. In teamCRM
         *								DataObjects are not user specific thus
         *								this field is always true. In EIM
         *								DataObjects are user specific, that
         *								means if the user must not read that field
         *								this parameter is set to false and the
         *								Field does not occur in the DataObject.
         */
        public $hidden;

        /**
         * @var boolean
         *
         *								If true the user may update the field.
         *								In teamCRM DataObjects are not user
         *								specific thus this field is always set
         *								to true. In EIM DataObjects are user
         *								specific, that means if the user must not
         *								update that field this parameter is set
         *								to false, any attempt to save a not
         *								updateable field will be ignored.
         */
        public $updateable;

        /**
         * @var boolean
         *
         *								If true the described Field is a
         *								systemfield which means that changes may
         *								only be done by the server's
         *								businesslogic. Any changes made outside
         *								the server will be ignored.
         */
        public $systemField;

        /**
         * @var boolean
         *
         *								If true the field may be updated using a
         *								mass operation.
         */
        public $allowedForMassUpdate;

        /**
         * @var string
         *
         *								Contains the long-name of the field that
         *								will be displayed. The language of the
         *								displayname is according to the locale
         *								set.
         */
        public $displayname;

        /**
         * @var string
         *
         *								Contains the short-name of the field
         *								that will be displayed. The language of
         *								the displayname is according to the
         *								locale set.
         */
        public $displaynameShort;

        /**
         * @var boolean
         *
         *								Indicates if this column may be
         *								duplicated, e.g. the GUID must not be
         *								duplicated. true by default.
         */
        public $duplicatable;

        /**
         * @var boolean
         *
         *								Indicates if this column is a custom
         *								column that is not defined by CAS Open.
         */
        public $custom;

        /**
         * @var int
         *
         *								Returns the current edit permission the user has (see {@link
         *								de.cas.open.server.api.types.EIMPermissionConstants#COL_ACCESS_READ_DENIED}
         *								or
         *								{@link
         *								de.cas.open.server.api.types.EIMPermissionConstants#COL_ACCESS_UPDATE_DENIED}
         *								or {@link
         *								de.cas.open.server.api.types.EIMPermissionConstants#COL_ACCESS_FULL_ACCESS}).
         */
        public $accessPermission;

        /**
         * @var int
         *
         *								Returns the minimal edit permission that can be assigned to this field by
         *								an administrator (see {@link
         *								de.cas.open.server.api.types.EIMPermissionConstants#COL_ACCESS_READ_DENIED}
         *								or
         *								{@link
         *								de.cas.open.server.api.types.EIMPermissionConstants#COL_ACCESS_UPDATE_DENIED}
         *								or {@link
         *								de.cas.open.server.api.types.EIMPermissionConstants#COL_ACCESS_FULL_ACCESS}).
         */
        public $minAccessPermission;

        /**
         * @var boolean
         *
         *                Indicates whether this column may be edited in a list view.
         */
        public $inListEditable;

        /**
         * @var string
         *
         *                Returns a suffix that should be appendend when
         *                displaying the value, e. g. '%' for percentual values.
         */
        public $suffix;

        /**
         * @var string
         *
         *                Additional information the client may need to format a field's value.
         *                Example: Duration, PersonDaysCAS, PersonDaysITD
         */
        public $formatOption;

    }

}
