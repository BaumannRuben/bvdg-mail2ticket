<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *				Represents a resource bundle with its associated key-value pairs.
     */
    class EIMResourceBundleTransferable {

        /**
         * @var string
         *
         *								Sets/Returns the resource bundles name.
         */
        public $bundleName;

        /**
         * @var \de\cas\open\server\api\types\EIMLocale
         *
         *								The locale that represents the best match that could be made
         *								while searching for a specific resource bundle. e.g. If it was
         *								searched for a resource bundle for locale de_DE_HE and there
         *								was at least one resource found that directly matched de_DE_HE,
         *								this locale will represent this match. If no resources where
         *								found matching de_DE_HE but only resources that matched de_DE,
         *								this locale will respresent only de_DE.. and so forth.
         */
        public $bestMatchedLocale;

        /**
         * @var array
         *
         *								Returns a list of Resource-objects which belong to this
         *								resource bundle.
         */
        public $resources;

    }

}
