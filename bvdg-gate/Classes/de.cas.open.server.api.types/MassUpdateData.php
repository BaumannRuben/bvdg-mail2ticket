<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *				Container for the data needed for a batch INSERT/UPDATE. If you set a
     *				permission it is used for all objects to save. You can either replace the
     *				existing permissions or add new ones depending on the {@link
     *				#replacePermissions} property. Make sure to set {@link #columnNames} and {@link
     *				#types} according to the data in the rows. You have to set the {@link
     *				#descriptionVersion} See \de\cas\eim\api\util\MassDataUtil for utility
     *				functions to create a MassUpdateData object.
     *	@see \de\cas\eim\api\util\MassDataUtil
     */
    class MassUpdateData extends \de\cas\open\server\api\types\TypedMassData {

        /**
         * @var string
         *
         *										Sets/Returns the new foreign edit permission
         *										restriction (i.e. the maximum permission a user can
         *										inherit by foreign edit permission).
         */
        public $foreignEditPermissionRestriction;

        /**
         * @var array
         *
         *										Returns the list of DataObjectPermissions that either
         *										replace (INSERT or if {@link #replacePermissions} is
         *										<code>true</code>) the existing permissions
         *										or that will be added to the existing (if {@link
         *										#replacePermissions} is
         *										<code>false</code>).
         */
        public $permissions;

        /**
         * @var array
         *
         *										Returns the list of Tags that either
         *										replace (INSERT or if {@link #replaceTags} is
         *										<code>true</code>) the existing tags
         *										or that will be added to the existing (if {@link
         *										#replaceTags} is
         *										<code>false</code>).
         */
        public $tags;

        /**
         * @var unknown
         *
         *										Sets/Returns the Unix timestamp when the \de\cas\open\server\api\types\DataObjectDescription on which
         *										this \de\cas\open\server\api\types\DataObject is based
         *										was read from the database. This timestamp allows it to
         *										determine if a \de\cas\open\server\api\types\DataObject
         *										that is based on this description or this description
         *										itself is out-of-date.
         *	@see \de\cas\open\server\api\types\DataObjectDescription
         *	@see \de\cas\open\server\api\types\DataObject
         */
        public $descriptionVersion;

        /**
         * @var boolean
         *
         *										Sets/Returns the flag that indicates if the existing
         *										permissions will be replaced by the permissions given
         *										in {@link #permissions} or if the permissions will be
         *										added to the existing ones. In case of a batch INSERT
         *										this flag will be neglected.
         */
        public $replacePermissions;

        /**
         * @var boolean
         *
         *										Sets/Returns the flag that indicates if the existing
         *										tags will be replaced by the tags given
         *										in {@link #tags} or if the tags will be
         *										added to the existing ones. In case of a batch INSERT
         *										this flag will be neglected.
         */
        public $replaceTags;

    }

}
