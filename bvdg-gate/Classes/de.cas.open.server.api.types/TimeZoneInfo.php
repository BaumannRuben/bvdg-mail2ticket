<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *				This is a container class which holds information about a specific timezone. It
     *				contains a timezone-ID (e.g. Europe/Berlin, see
     *				http://www.twinsun.com/tz/tz-link.htm), a localized display-name (e.g. Berlin)
     *				and the GMT offset display string (e.g. GMT-06:00).
     */
    class TimeZoneInfo {

        /**
         * @var \de\cas\open\server\api\types\KeyAndDisplayValue
         *
         *								The ID of the represented timezone (e.g. "Europe/Berlin", see
         *								http://www.twinsun.com/tz/tz-link.htm) and the display name.
         */
        public $name;

        /**
         * @var int
         *
         *								The raw offset to GMT (DST are ignored).
         */
        public $rawOffset;

    }

}
