<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *        Request Object to save a synced record.
     *        Note that the guid of an appointment might change during an update, so
     *        be sure to check the guid of the returned dataobject in the corresponding \de\cas\open\server\api\types\ResponseObject:
     *        SaveAndReturnSyncRecordResponse.
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see SaveAndReturnSyncRecordResponse
     */
    class SaveAndReturnSyncRecordRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var \de\cas\open\server\api\types\DataObjectTransferable
         *
         *                    The DataObject to be saved.
         */
        public $DataObject;

        /**
         * @var \de\cas\open\server\api\types\SyncRecordPermissions
         *
         *                    The permissions that should be set for the DataObject.
         *                    Note that the fields "isOrganizer" and "isParticipantEditingAllowed" are not
         *                    evaluated while saving the record.
         */
        public $Permissions;

        /**
         * @var boolean
         *
         *                    Should be set if the request was made for an user interaction.
         *                    If a synchronisation sendthis request this flag should not be set or false
         */
        public $isUserRequest;

    }

}
