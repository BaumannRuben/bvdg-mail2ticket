<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *		    A DataObjectTransferable is a bean which can contain properies of all data objects in EIM products (e.g. Appointment).
     */
    class DataObjectTransferable {

        /**
         * @var array
         *
         *		            Returns the names of the fields.
         */
        public $fields;

        /**
         * @var array
         *
         *					      Returns the names of the fields that have
         *					      null-values.
         */
        public $nullFields;

        /**
         * @var string
         *
         *								A DataObjectTransferable is a generic object. This
         *								property
         *								describes the type of the object, i.e. APPOINTMENT.
         */
        public $objectType;

        /**
         * @var string
         *
         *								Returns/Sets the clientprefix (German: "Mandant") the
         *								object belongs to.
         */
        public $client;

        /**
         * @var string
         *
         *								Sets/Returns the new foreign edit permission restriction (i.e.
         *								the maximum permission a user can inherit by foreign edit
         *								permission).
         */
        public $foreignEditPermissionRestriction;

        /**
         * @var array
         *A list of DataObjectPermissions
         */
        public $permissions;

        /**
         * @var array
         *
         *								Returns the list of dirty fieldnames (i.e. the names of the
         *								fields that must be saved).
         */
        public $dirtyFields;

        /**
         * @var array
         *
         *								Returns the List of assigned Tags (system- and usertags).
         */
        public $localizedTags;

        /**
         * @var unknown
         *
         *								Sets/Returns the Unix timestamp when the \de\cas\open\server\api\types\DataObjectDescription on which this
         *								\de\cas\open\server\api\types\DataObject is based was read from
         *								the database. This timestamp allows it to determine if a \de\cas\open\server\api\types\DataObject that is based on this
         *								description or this description itself is out-of-date.
         *	@see \de\cas\open\server\api\types\DataObjectDescription
         *	@see \de\cas\open\server\api\types\DataObject
         */
        public $descriptionVersion;

        /**
         * @var boolean
         *
         *								A flag that indicates, if a user is allowed to save this \de\cas\open\server\api\types\DataObject. Because this is a
         *								per-user-flag, it must be set every time, the \de\cas\open\server\api\types\DataObject is passed to the
         *								client.<br> <strong>It does not serve as an
         *								indicator for the server! It has only to be set from the
         *								server.</strong>
         *	@see \de\cas\open\server\api\types\DataObject
         */
        public $writable;

        /**
         * @var boolean
         *
         *								A flag that indicates, if the tags where altered.
         */
        public $tagsDirty;

        /**
         * @var boolean
         *
         *								A flag that indicates, if the permissions where altered.
         */
        public $permissionsDirty;

    }

}
