<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *        Request Object to retrieve all participants of a record
     *        when ExchangeSync is active.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject:
     *        GetSyncRecordPermissionsResponse
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetSyncRecordPermissionsResponse
     */
    class GetSyncRecordPermissionsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    Gets/Sets the tablename of the record.
         */
        public $tableName;

        /**
         * @var string
         *
         *                    Gets/Sets the Guid of the record.
         */
        public $tableGuid;

    }

}
