<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *				Represents a permission on a DataObjectType, i.e. to all objects of this type in general.
     *				See \de\cas\open\server\dataaccess\PermissionsProvider  for details.
     *	@see \de\cas\open\server\dataaccess\PermissionsProvider
     */
    class DataObjectTypePermission {

        /**
         * @var string
         *
         *								Returns the GGUID of the permission.
         */
        public $GGUID;

        /**
         * @var int
         *
         *								Returns the ID of the permissions owner to whom the permission is
         *								granted. Positive IDs: users, negative IDs: groups
         */
        public $accessorID;

        /**
         * @var int
         *
         *								Returns the granted permission.
         *								@see de.cas.open.server.api.types.EIMPermissionConstants.DATAOBJECTTYPE_NO_ACCESS
         *								@see de.cas.open.server.api.types.EIMPermissionConstants.DATAOBJECTTYPE_READ
         *								@see de.cas.open.server.api.types.EIMPermissionConstants.DATAOBJECTTYPE_INSERT
         *								@see de.cas.open.server.api.types.EIMPermissionConstants.DATAOBJECTTYPE_EDIT
         *								@see de.cas.open.server.api.types.EIMPermissionConstants.DATAOBJECTTYPE_DELETE
         *								@see de.cas.open.server.api.types.EIMPermissionConstants.DATAOBJECTTYPE_FULL_ACCESS
         */
        public $editPermission;

        /**
         * @var string
         *
         *								Returns the objecttype (e.g. "Address").
         */
        public $objectType;

    }

}
