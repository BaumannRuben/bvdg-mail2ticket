<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *				Decribes a link between an user oid and an user name.
     */
    class IdToNameElement {

        /**
         * @var int
         *Sets/Returns the user ID.
         */
        public $oid;

        /**
         * @var string
         *Sets/Returns the user name.
         */
        public $userName;

    }

}
