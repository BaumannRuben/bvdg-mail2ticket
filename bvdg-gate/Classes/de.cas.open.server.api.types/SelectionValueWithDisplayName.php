<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *				A value in a SelectionValueListField.
     *	@see SelectionValueListField
     */
    class SelectionValueWithDisplayName extends \de\cas\open\server\datadefinition\types\SelectionValue {

        /**
         * @var string
         *
         *										Returns the displayname. The displayname is translated
         *										and set when retrieving an object via the
         *										\de\cas\open\server\api\eiminterface\EIMInterface.
         *	@see \de\cas\open\server\api\eiminterface\EIMInterface
         */
        public $displayName;

    }

}
