<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *				This container transports a GUID, an \de\cas\open\server\api\constants\ExceptionCode and a message. Its simply a
     *				container without semantic.
     *	@see \de\cas\open\server\api\constants\ExceptionCode
     */
    class GuidEIMExceptionContainer {

        /**
         * @var string
         *Sets/Returns the GUID.
         */
        public $guid;

        /**
         * @var string
         *
         *								Sets/Returns the \de\cas\open\server\api\constants\ExceptionCode of the
         *								exception that occured if the record could not be saved.
         *	@see \de\cas\open\server\api\constants\ExceptionCode
         */
        public $exceptionCode;

        /**
         * @var string
         *
         *								Sets/Returns the message of the exception that occured if the
         *								record could not be saved.
         */
        public $exceptionMessage;

    }

}
