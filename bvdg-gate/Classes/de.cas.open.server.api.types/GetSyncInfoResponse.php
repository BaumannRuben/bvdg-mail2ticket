<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *        Response Object fpr corresponding \de\cas\open\server\api\types\RequestObject: GetSyncInfoRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetSyncInfoRequest
     */
    class GetSyncInfoResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\SyncInfo
         *
         */
        public $SyncInfo;

    }

}
