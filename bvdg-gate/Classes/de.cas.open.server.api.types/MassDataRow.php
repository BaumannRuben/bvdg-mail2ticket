<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *				Represents a single row in a MassQueryResult object.
     */
    class MassDataRow {

        /**
         * @var boolean
         *1
         *
         *								This is an unused dummy field which acts as a workaround for a .NET bug.
         */
        public $dotnetbugfix;

        /**
         * @var array
         *
         *								Returns the values in a row returned by the query.
         */
        public $values;

    }

}
