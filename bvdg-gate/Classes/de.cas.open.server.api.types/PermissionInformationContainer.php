<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *				Represents an object to be used by the client to display information about
     *				permissions set on a certain DataObject. A PermissionInformationSupplier holds
     *				information on the permission-owner the UID ( > 0), the level of the granted
     *				permission and the source of the granted permission (if the permission is a
     *				foreign-edit-permission or not).
     */
    class PermissionInformationContainer {

        /**
         * @var int
         *
         *								Either the UID (for Users) or GID (for Groups).
         */
        public $accessorId;

        /**
         * @var int
         *The granted permission.
         */
        public $editPermission;

        /**
         * @var boolean
         *
         *								If the editPermission results from a foreignEditPermission.
         */
        public $foreignEditPermission;

    }

}
