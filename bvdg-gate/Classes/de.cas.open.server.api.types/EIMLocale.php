<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *Represents a locale.
     */
    class EIMLocale {

        /**
         * @var string
         *
         *								Sets/Returns the language. May be null.
         */
        public $language;

        /**
         * @var string
         *Sets/Returns the country. May be null.
         */
        public $country;

        /**
         * @var string
         *Sets/Returns the variant. May be null.
         */
        public $variant;

    }

}
