<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *        Describes a type/status field.
     */
    class TypeStatusSuggestFieldDescription extends \de\cas\open\server\api\types\StringFieldDescription {

        /**
         * @var array
         *
         */
        public $TypeSuggestValues;

    }

}
