<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *				This container contains the index of the failed record beside the properties of
     *				GuidEIMExceptionContainer.
     *	@see GuidEIMExceptionContainer
     */
    class IndexedGuidEIMExceptionContainer extends \de\cas\open\server\api\types\GuidEIMExceptionContainer {

        /**
         * @var int
         *
         *										Sets/Returns the message of the exception that occured
         *										if the record could not be saved.
         */
        public $rowIndex;

    }

}
