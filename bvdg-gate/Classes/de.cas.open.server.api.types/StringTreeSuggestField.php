<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *        A container for a String tree suggest value in a generic DataObject.
     */
    class StringTreeSuggestField extends \de\cas\open\server\api\types\Field {

        /**
         * @var array
         *The selected paths. (The nodes along the path and the last node are both selected)
         */
        public $selectedPaths;

    }

}
