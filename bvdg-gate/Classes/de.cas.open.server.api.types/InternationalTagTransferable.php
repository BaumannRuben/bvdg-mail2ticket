<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *				Transferable version of the \de\cas\open\server\api\types\InternationalTag.
     *	@see \de\cas\open\server\api\types\InternationalTag
     */
    class InternationalTagTransferable extends \de\cas\open\server\api\types\Tag {

        /**
         * @var array
         *
         *										Holds a list of locales for which are translations
         *										available.
         */
        public $locales;

        /**
         * @var array
         *
         *										Holds a list of values. This lists order is strongly
         *										coupled to the locales-list.
         */
        public $values;

    }

}
