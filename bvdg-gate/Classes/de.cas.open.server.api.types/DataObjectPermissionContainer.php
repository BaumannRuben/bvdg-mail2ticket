<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *				This class serves as a container for DataObjectPermission objects.
     */
    class DataObjectPermissionContainer {

        /**
         * @var boolean
         *1
         *
         *								This is an unused dummy field which acts as a workaround for a .NET bug.
         */
        public $dotnetbugfix;

        /**
         * @var array
         *
         *								Gets the permissions this instance holds.
         */
        public $permissions;

    }

}
