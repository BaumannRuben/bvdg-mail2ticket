<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *CAS Open currency object
     */
    class Currency {

        /**
         * @var string
         *Sets/Returns the GGUID.
         */
        public $GGUID;

        /**
         * @var string
         *Sets/Returns the currency code.
         */
        public $currencyCode;

        /**
         * @var string
         *Sets/Returns the currency name e.g. Dollars.
         */
        public $currencyName;

        /**
         * @var string
         *Sets/Returns the country code of the currency.
         */
        public $countryCode;

        /**
         * @var string
         *Sets/Returns the language code of the currency.
         */
        public $languageCode;

        /**
         * @var string
         *Sets/Returns the country name of the currency.
         */
        public $countryName;

        /**
         * @var string
         *Sets/Returns the currency symbol e.g. €.
         */
        public $currencySymbol;

        /**
         * @var int
         *Sets/Returns precision of the fractional unit.
         */
        public $FractionalPrecision;

        /**
         * @var string
         *Sets/Returns the pattern for the currency formatting
         */
        public $formatPatterns;

    }

}
