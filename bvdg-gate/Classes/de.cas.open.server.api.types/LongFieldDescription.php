<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *Describes a LongField
     */
    class LongFieldDescription extends \de\cas\open\server\api\types\FieldDescription {

        /**
         * @var unknown
         *
         *                    Sets/Gets the minimum value of the LongField.
         */
        public $minValue;

        /**
         * @var unknown
         *
         *                    Sets/Gets the maximum value of the LongField.
         */
        public $maxValue;

    }

}
