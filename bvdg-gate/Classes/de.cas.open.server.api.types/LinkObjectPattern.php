<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     */
    class LinkObjectPattern {

        /**
         * @var string
         *
         *            Sets/Returns the GGUID that identifies this link
         */
        public $GGUID;

        /**
         * @var string
         *
         *                Sets/Returns the Type of the first object to link.
         */
        public $objectType1;

        /**
         * @var string
         *
         *                Sets/Returns the Type of the second object to link.
         */
        public $objectType2;

        /**
         * @var \de\cas\open\server\api\types\KeyAndDisplayValue
         *
         *                Sets/Returns the role of the first data object in the link
         */
        public $Role1;

        /**
         * @var \de\cas\open\server\api\types\KeyAndDisplayValue
         *
         *                Sets/Returns the role of the first data object in the link
         */
        public $Role2;

        /**
         * @var \de\cas\open\server\api\types\KeyAndDisplayValue
         *
         *                Sets/Returns the attribute of the link (e.g. "Business
         *                Partner").
         */
        public $attribute;

        /**
         * @var unknown
         *
         *                Sets/Returns the start date of the period the link is valid in.
         */
        public $isValidFrom;

        /**
         * @var unknown
         *
         *                Sets/Returns the end date of the period the link is valid in.
         */
        public $isValidTo;

        /**
         * @var string
         *
         *                Sets/Returns the direction of a link (LEFT_TO_RIGHT or
         *                BIDIRECTIONAL)
         */
        public $linkDirection;

        /**
         * @var boolean
         *
         *                Sets/Returns the flag that indicates that this is a
         *                hierarchical link.
         */
        public $isHierarchy;

        /**
         * @var string
         *
         *                Sets/Returns the user that inserted the link.
         */
        public $InsertUser;

        /**
         * @var string
         *
         *                Sets/Returns the user that last modified the link.
         */
        public $UpdateUser;

        /**
         * @var unknown
         *
         *                Sets/Returns the timestamp the link was created on.
         */
        public $InsertTimestamp;

        /**
         * @var unknown
         *
         *                Sets/Returns the timestamp the link was last modified.
         */
        public $UpdateTimestamp;

        /**
         * @var string
         *
         *                Sets/Returns the value of the additional field #1.
         */
        public $additionalField1;

        /**
         * @var string
         *
         *                Sets/Returns the value of the additional field #2.
         */
        public $additionalField2;

    }

}
