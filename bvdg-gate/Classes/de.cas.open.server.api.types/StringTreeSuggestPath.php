<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *Describes a path in a tree, where the nodes are identified by the string value.
     */
    class StringTreeSuggestPath {

        /**
         * @var boolean
         *1
         *
         *              This is an unused dummy field which acts as a workaround for a .NET bug.
         */
        public $dotnetbugfix;

        /**
         * @var array
         *
         *                The segments of the path, represented by the string values of the involved nodes.
         */
        public $segments;

    }

}
