<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *				A value in a SelectionValueListField with a translation for multiple languages.
     *				To be used when managing possible values.
     *	@see SelectionValueListField
     */
    class SelectionValueWithMultipleLanguages extends \de\cas\open\server\datadefinition\types\SelectionValue {

        /**
         * @var array
         *
         *										Holds a list of locales for which are translations
         *										available. This lists order is strongly
         *										coupled to the {@link #values}-list.
         */
        public $locales;

        /**
         * @var array
         *
         *										Holds a list of values. This lists order is strongly
         *										coupled to the {@link #locales}-list.
         */
        public $displayNames;

    }

}
