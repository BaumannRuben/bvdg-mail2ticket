<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *				Represents a permission that is granted to a
     *				user/group to invoke a \de\cas\open\server\business\BusinessOperation.
     *				The PrivilegedOperationSets needing a PrivilegedOperationSetPermission can be configured in the database table
     *				<code>PrivilegedOperations</code>.
     *	@see \de\cas\open\server\business\BusinessOperation
     *	@see PrivilegedOperationSet
     *	@see PrivilegedOperationSetPermission
     */
    class PrivilegedOperationSetPermission {

        /**
         * @var string
         *
         *								Returns the GGUID of the permission record in the database.
         *								Will be set automatically when saving the permission.
         */
        public $GGUID;

        /**
         * @var string
         *
         *								Returns the GGUID of the PrivilegedOperationSet
         *								whose invocation is granted by this
         *								PrivilegedOperationSetPermission.
         *	@see PrivilegedOperationSet
         *	@see PrivilegedOperationSetPermission
         */
        public $privilegedOperationSetGUID;

        /**
         * @var int
         *
         *								Returns OID/GID of the user/group, the permission is granted to.
         */
        public $permissionOwner;

    }

}
