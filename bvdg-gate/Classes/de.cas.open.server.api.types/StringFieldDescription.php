<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *Describes a StringField
     */
    class StringFieldDescription extends \de\cas\open\server\api\types\FieldDescription {

        /**
         * @var int
         *
         *										The maximum length of this Field that can be stored in
         *										the database
         */
        public $maxLength;

    }

}
