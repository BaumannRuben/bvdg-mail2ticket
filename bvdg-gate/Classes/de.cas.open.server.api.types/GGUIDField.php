<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *Represents a GGUID (Genesis World GUID)
     */
    class GGUIDField extends \de\cas\open\server\api\types\Field {

        /**
         * @var string
         *Value of the field
         */
        public $value;

    }

}
