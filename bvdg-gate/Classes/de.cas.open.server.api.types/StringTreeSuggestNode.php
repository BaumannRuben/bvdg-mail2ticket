<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *Describes a Node in a StringTreeSuggestDescription
     */
    class StringTreeSuggestNode {

        /**
         * @var string
         *
         *                The string value of the node.
         */
        public $value;

        /**
         * @var array
         *
         *              The children nodes of this node.
         */
        public $children;

        /**
         * @var boolean
         *
         *                Describes whether this node is selectable.
         */
        public $selectable;

        /**
         * @var boolean
         *
         *                Describes whether the children nodes are multiselectable or single selectable.
         */
        public $childrenMultiSelect;

    }

}
