<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *Represents a resource.
     */
    class Resource {

        /**
         * @var string
         *Sets/Returns the resources key.
         */
        public $resourceKey;

        /**
         * @var string
         *
         *								Sets/Returns the resources value. May be null.
         */
        public $resourceValue;

        /**
         * @var string
         *
         *								Sets/Returns the name of the resource bundle to which this
         *								resource belongs.
         */
        public $parentBundleName;

    }

}
