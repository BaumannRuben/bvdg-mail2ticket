<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *				A container for a double value in a generic DataObject
     */
    class DoubleField extends \de\cas\open\server\api\types\Field {

        /**
         * @var double
         *The double value of this field
         */
        public $value;

    }

}
