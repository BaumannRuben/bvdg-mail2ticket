<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *				Provides information on a set of privileged
     *				\de\cas\open\server\business\BusinessOperations. You can define these
     *				sets by inserting a record in the database table
     *				<code>PrivilegedOperations</code>, whereas the fully qualified
     *				class names of the \de\cas\open\server\api\types\RequestObject belonging
     *				to this PrivilegedOperationSet are comma separated.
     *	@see \de\cas\open\server\business\BusinessOperation
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see PrivilegedOperationSet
     */
    class PrivilegedOperationSet {

        /**
         * @var string
         *
         *								Returns the GGUID of the permission record in the database.
         *								Will be set automatically when saving the permission.
         */
        public $GGUID;

        /**
         * @var string
         *
         *								Returns the set's name.
         */
        public $name;

        /**
         * @var string
         *
         *								Returns the resource key used to retrieve the localized name.
         */
        public $resourceKey;

        /**
         * @var string
         *
         *								Returns the set's localized name.
         */
        public $localizedName;

        /**
         * @var array
         *
         *								Returns the fully qualified class names of the
         *								RequestObjects invoking the
         *								\de\cas\open\server\business\BusinessOperations
         *								belonging to this PrivilegedOperationSet.
         *	@see RequestObject
         *	@see \de\cas\open\server\business\BusinessOperation
         *	@see PrivilegedOperationSet
         */
        public $requestObjectClassNames;

    }

}
