<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *Describes an IntField
     */
    class IntFieldDescription extends \de\cas\open\server\api\types\FieldDescription {

        /**
         * @var int
         *
         *										Sets/Gets the maximum length of the IntField, i.e. the
         *										number of digits.
         */
        public $maxLength;

        /**
         * @var int
         *
         *                    Sets/Gets the minimum value of the IntField.
         */
        public $minValue;

        /**
         * @var int
         *
         *                    Sets/Gets the maximum value of the IntField.
         */
        public $maxValue;

    }

}
