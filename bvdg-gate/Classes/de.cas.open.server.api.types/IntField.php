<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *				A container for an int value in a generic DataObject
     */
    class IntField extends \de\cas\open\server\api\types\Field {

        /**
         * @var int
         *The int value of this field
         */
        public $value;

    }

}
