<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *Represents a tag group.
     */
    class TagGroup {

        /**
         * @var string
         *Sets/Gets the GGUID of the tag group
         */
        public $GGUID;

        /**
         * @var string
         *
         *								Sets/Gets the cardinality rule of the tag group
         */
        public $cardinality;

        /**
         * @var string
         *
         *								Sets/Gets the default tag GUID. Necessary if the setting of a
         *								tag in the tag group is mandatory.
         */
        public $defaultTagGUID;

        /**
         * @var boolean
         *
         *								Flag that indicates if the group is defined by the system and
         *								cannot be deleted.
         */
        public $systemDefined;

        /**
         * @var boolean
         *
         *								Flag that indicates if the group is editable by a user.
         */
        public $userEditable;

    }

}
