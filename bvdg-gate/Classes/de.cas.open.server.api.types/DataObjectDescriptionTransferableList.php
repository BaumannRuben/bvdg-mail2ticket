<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *        A list of DataObjectDescriptionTransferable instances.
     */
    class DataObjectDescriptionTransferableList {

        /**
         * @var array
         *
         *                The contained DataObjectDescriptionTransferables.
         */
        public $dataObjectDescriptions;

    }

}
