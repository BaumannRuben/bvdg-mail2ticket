<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *Contains the localized value of a TagGroup.
     */
    class LocalizedTagGroup extends \de\cas\open\server\api\types\TagGroup {

        /**
         * @var string
         *
         *										Sets/Gets the value of the localized string.
         */
        public $localizedValue;

        /**
         * @var array
         *
         *										List containing Tags that belong to the group.
         */
        public $tags;

    }

}
