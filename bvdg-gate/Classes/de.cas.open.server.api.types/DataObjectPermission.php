<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *				Represents the right and/or the role a rightsowner may have on a single
     *				DataObject.
     */
    class DataObjectPermission {

        /**
         * @var string
         *
         *								Sets/Returns the GGUID of this permission in the ORel-table.
         *								This field is managed by the server and <strong>must not
         *								be set by the client.</strong>
         */
        public $GGUID;

        /**
         * @var int
         *
         *								The database internal ID (OID or GID) of this permissionowners
         */
        public $rightOwnerID;

        /**
         * @var string
         *
         *                Returns the GGUID of the permission owner. Will be set by the
         *                CAS Open server according to the value in {@link #rightOwnerID}.
         */
        public $OwnerGUID;

        /**
         * @var int
         *The assigned right
         */
        public $accessRight;

        /**
         * @var \de\cas\open\server\api\types\KeyAndDisplayValue
         *
         *								The role of the permissionowners (e.g. Participant)
         */
        public $role;

    }

}
