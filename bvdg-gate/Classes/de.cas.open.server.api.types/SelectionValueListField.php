<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *				A field that holds a set of selected values.
     */
    class SelectionValueListField extends \de\cas\open\server\api\types\Field {

        /**
         * @var array
         *The list of selected values.
         */
        public $values;

    }

}
