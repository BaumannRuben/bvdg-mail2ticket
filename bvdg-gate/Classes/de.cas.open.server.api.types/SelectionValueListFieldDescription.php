<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *Describes a SelectionValueListField
     */
    class SelectionValueListFieldDescription extends \de\cas\open\server\api\types\FieldDescription {

        /**
         * @var string
         *
         *										Sets/Gets the cardinality rule of the selection value list
         */
        public $cardinality;

        /**
         * @var string
         *
         *										Sets/Gets the default selection value GUID. Necessary if the cardinality
         *										restriction of a selection value list must contain at least one element.
         */
        public $defaultValueGUID;

        /**
         * @var array
         *
         *										The list of selection values that are allowed to appear in this
         *										selection value list field.
         */
        public $possibleValues;

        /**
         * @var int
         *
         *										Returns the length of the field that stores the given GGUIDs in binary form (to be displayed in MassQueryResults).
         */
        public $lengthOfBinaryShadowField;

    }

}
