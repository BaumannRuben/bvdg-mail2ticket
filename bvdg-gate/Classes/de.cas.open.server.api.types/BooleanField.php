<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *				A container for a boolean value in a generic DataObject
     */
    class BooleanField extends \de\cas\open\server\api\types\Field {

        /**
         * @var boolean
         *
         *										The boolean value of this field
         */
        public $value;

    }

}
