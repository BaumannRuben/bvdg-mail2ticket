<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *        An EIMProperty is a container for a key/value pair used
     *        in \de\cas\open\server\api\types\EIMPropertiesTransferable
     *	@see \de\cas\open\server\api\types\EIMPropertiesTransferable
     */
    class EIMProperty {

        /**
         * @var string
         *
         *								The key under which this property is known
         */
        public $key;

        /**
         * @var string
         *The value of the property
         */
        public $value;

        /**
         * @var string
         *
         *								The group to which this property belongs.
         */
        public $group;

        /**
         * @var boolean
         *
         *								If this property is a default property (and thus cannot be
         *								altered directly).
         */
        public $defaultProperty;

    }

}
