<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *				Client object with an additional field for the last login date/time.
     */
    class ClientWithLastLoginDate extends \de\cas\open\server\api\types\Client {

        /**
         * @var unknown
         *
         *								Sets/Gets the last login time of a user of the client.
         */
        public $LastLoginDateTime;

    }

}
