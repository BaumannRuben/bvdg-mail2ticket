<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *		    A EIMPropertiesTransferable object is a bean which can contain
     *		    properies to be transferred over the webservice.
     */
    class EIMPropertiesTransferable {

        /**
         * @var array
         *A list of EIMProperty-objects
         */
        public $properties;

    }

}
