<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *        Response Object which contains information about the
     *        external owners of a synced record.
     *        Corresponding \de\cas\open\server\api\types\RequestObject:
     *        GetSyncRecordPermissionsRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetSyncRecordPermissionsRequest
     */
    class GetSyncRecordPermissionsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\SyncRecordPermissions
         *
         *                    Gets/sets the permissions of the synchronized record.
         */
        public $SyncRecordPermissions;

    }

}
