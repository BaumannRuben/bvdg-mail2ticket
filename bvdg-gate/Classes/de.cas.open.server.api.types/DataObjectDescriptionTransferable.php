<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *Describes a DataObjectTransferable
     */
    class DataObjectDescriptionTransferable {

        /**
         * @var string
         *
         *								Returns/Sets the client (German:
         *								"Mandant") the description
         *								belongs to.
         */
        public $client;

        /**
         * @var boolean
         *
         *								Sets/Returns the flag that indicates if
         *								the ownercheck for objects belonging to
         *								this service is enabled.
         */
        public $userSensitive;

        /**
         * @var array
         *
         *		            The field descriptions of the DataObjectTransferable.
         */
        public $fields;

        /**
         * @var string
         *
         *		            The object type of the DataObjectTransferable.
         */
        public $descriptionObjectType;

        /**
         * @var string
         *
         */
        public $language;

        /**
         * @var array
         *
         *								Returns the List of TagGroups that are
         *								available for this type of
         *								DataObjectDescription.
         */
        public $localizedTagGroups;

        /**
         * @var unknown
         *
         *								Sets/Returns the Unix timestamp when the
         *								\de\cas\open\server\api\types\DataObjectDescription
         *								was read from the database. This
         *								timestamp allows it to determine if a
         *								\de\cas\open\server\api\types\DataObject
         *								that is based on this description or
         *								this description itself is out-of-date.
         *	@see \de\cas\open\server\api\types\DataObjectDescription
         *	@see \de\cas\open\server\api\types\DataObject
         */
        public $descriptionVersion;

        /**
         * @var boolean
         *
         */
        public $taggable;

        /**
         * @var boolean
         *
         */
        public $linkable;

        /**
         * @var boolean
         *
         *								Sets/Returns the flag that indicates if
         *								dataobjects belonging to this services
         *								may be moved to the recycle bin or can
         *								just be purged (see {@link
         *								de.cas.open.server.dataaccess.AbstractDataObjectDao#deleteObject(String,
         *								String)}).
         */
        public $rBinActivated;

        /**
         * @var boolean
         *
         *								Sets/Returns the flag that indicates if
         *								dataobjects belonging to this services
         *								may be saved as templates.
         */
        public $templActivated;

        /**
         * @var boolean
         *
         *								Indicates that the object type can be used in a mixedList
         *								(i.e. the SQL pseudo table "ObjectTypes").
         */
        public $usedInMixedList;

        /**
         * @var string
         *
         *								Sets/Returns the GGUID of this
         *								objecttype. This ID is used to define
         *								foreign edit permissions. (see {@link
         *								de.cas.open.server.api.types.ForeignEditPermission#setServiceID(String)})
         */
        public $serviceID;

        /**
         * @var \de\cas\open\server\datadefinition\types\ObjectTypeDependency
         *
         *								Returns the superior objecttype if any.
         */
        public $superiorTypeDependency;

        /**
         * @var array
         *
         *								Returns the dependent objecttypes if
         *								any.
         */
        public $dependentTypeDependencies;

        /**
         * @var int
         *
         *								Returns the permission the current user has on objects of the type
         *								described by this DataObjectDescription.
         */
        public $dataObjectTypePermission;

        /**
         * @var \de\cas\open\server\api\types\PrimaryLinkParents
         *
         *		            Returns the possible primary link dataobject types and which
         *		            of them are mandatory.
         */
        public $primaryLinkParents;

    }

}
