<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *				This is the transferable version of the class InternationalTagGroup.
     */
    class InternationalTagGroupTransferable extends \de\cas\open\server\api\types\TagGroup {

        /**
         * @var array
         *
         *										Holds a list of locales for which are translations
         *										available.
         */
        public $locales;

        /**
         * @var array
         *
         *										Holds a list of values. This lists order is strongly
         *										coupled to the locales-list.
         */
        public $values;

    }

}
