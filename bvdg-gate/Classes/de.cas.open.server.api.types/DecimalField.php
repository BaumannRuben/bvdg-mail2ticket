<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *				A container for a BigDecimal value in a generic DataObject.
     */
    class DecimalField extends \de\cas\open\server\api\types\Field {

        /**
         * @var unknown
         *
         *										The BigDecimal value of this field.
         */
        public $value;

    }

}
