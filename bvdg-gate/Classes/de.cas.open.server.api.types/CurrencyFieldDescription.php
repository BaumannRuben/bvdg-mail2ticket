<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *Describes a CurrencyField.
     *	@see CurrencyField
     */
    class CurrencyFieldDescription extends \de\cas\open\server\api\types\DecimalFieldDescription {

    }

}
