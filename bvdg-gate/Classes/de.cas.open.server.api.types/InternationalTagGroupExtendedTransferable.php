<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *				A list of tags available at the group as well as a list of object types the
     *				group is available for.
     */
    class InternationalTagGroupExtendedTransferable extends \de\cas\open\server\api\types\InternationalTagGroupTransferable {

        /**
         * @var array
         *
         *										List containing Tags that belong to the group.
         */
        public $tags;

        /**
         * @var array
         *
         *										List containing the object types the group is available
         *										at.
         */
        public $objectTypes;

    }

}
