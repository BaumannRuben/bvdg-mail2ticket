<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     */
    class PrimaryLinkParents {

        /**
         * @var array
         *
         *                Returns a list of possible primary link dataobject types.
         */
        public $parentObjectTypes;

        /**
         * @var array
         *
         *                Returns a list of mandatory parent dataobject types for primary links.
         */
        public $mandatoryParentObjectTypes;

    }

}
