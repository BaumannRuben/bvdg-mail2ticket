<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *				A Field is a container for a typed value in a generic DataObject
     */
    class Field {

        /**
         * @var string
         *The name of the field
         */
        public $name;

        /**
         * @var string
         *The type of the field
         */
        public $fieldType;

    }

}
