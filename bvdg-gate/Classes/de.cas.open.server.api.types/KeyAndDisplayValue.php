<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *				Represents a combination of a key and its translated display value. The display
     *				value may be used by a client but is not used by the server internally (so not
     *				providing the display value is valid).
     */
    class KeyAndDisplayValue {

        /**
         * @var string
         *
         *								Sets/Returns the key of this element. The key has to be
         *								provided as it is evaluated by the server.
         */
        public $key;

        /**
         * @var string
         *
         *								Sets/Returns the display value of this element. The display
         *								value may be used by a client but is not used by the server
         *								internally (so not providing the display value is valid).
         */
        public $displayValue;

    }

}
