<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *				Tag containing frequency of usage (user-readable and system-readable).
     */
    class CountedLocalizedTag extends \de\cas\open\server\api\types\LocalizedTag {

        /**
         * @var int
         *
         *										Sets/Gets the system-readable count of the tag.
         */
        public $totalCount;

        /**
         * @var int
         *
         *										Sets/Gets the user-readable count of the tag.
         */
        public $ReadableCount;

    }

}
