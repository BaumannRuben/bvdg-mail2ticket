<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *        Represents a key/value pair.
     */
    class KeyValuePair {

        /**
         * @var string
         *
         *                The key identifying the value.
         */
        public $key;

        /**
         * @var string
         *
         *                The value belonging to the key.
         */
        public $value;

    }

}
