<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *Represents several records of an objecttype.
     */
    class TypedMassData extends \de\cas\open\server\api\types\AbstractMassData {

        /**
         * @var string
         *
         *										The objecttype of the records (e.g. ADDRESS).
         */
        public $objectType;

    }

}
