<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *Describes a DoubleField
     */
    class DoubleSuggestFieldDescription extends \de\cas\open\server\api\types\DoubleFieldDescription {

        /**
         * @var array
         *
         *                    The suggested values.
         */
        public $suggestedValues;

        /**
         * @var boolean
         *
         *                    Whether the user can freely edit the value of the field
         *                    or he is only allowed to choose from the predefined set of suggested values.
         */
        public $customValueAllowed;

    }

}
