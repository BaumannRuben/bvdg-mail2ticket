<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *        Describes the suggest values for the type field.
     */
    class TypeSuggestValue {

        /**
         * @var string
         *
         *                The localized type value.
         */
        public $value;

        /**
         * @var array
         *
         *                The mandatory fields for records of this type/status combination.
         */
        public $mandatoryFields;

        /**
         * @var array
         *
         *                The possible status values for this type value.
         */
        public $statusSuggestValues;

    }

}
