<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *Represents a database server.
     */
    class DatabaseServer {

        /**
         * @var string
         *
         *								Sets/Returns the databaseserver's GGUID.
         */
        public $GGUID;

        /**
         * @var unknown
         *
         *								Sets/Returns the point of time when the record was inserted
         *								into the database.
         */
        public $InsertTimestamp;

        /**
         * @var unknown
         *
         *								Sets/Returns the point of time when the record was updated in
         *								the database.
         */
        public $UpdateTimestamp;

        /**
         * @var string
         *
         *								Sets/Returns the server's name in the DNS or its IP address.
         */
        public $Server;

        /**
         * @var int
         *Sets/Returns the server's port.
         */
        public $Port;

        /**
         * @var string
         *
         *								Sets/Returns the user to access the database.
         */
        public $User;

        /**
         * @var int
         *
         *								Sets/Returns server's capacity (i.e. the maximum number of
         *								client schemas).
         */
        public $Capacity;

        /**
         * @var string
         *
         *								Sets/Returns the password to access the database.
         */
        public $Password;

        /**
         * @var boolean
         *
         *								Sets/Returns the flag that indicates if the databaseserver is
         *								deactivated.
         */
        public $Deactivated;

    }

}
