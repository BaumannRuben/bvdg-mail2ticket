<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *		    This is a containter object which holds a string list and a type property.
     */
    class TypedStringList {

        /**
         * @var string
         *
         *			          This is an additional type to describe the list content.
         */
        public $type;

        /**
         * @var array
         *The content as string list.
         */
        public $strings;

    }

}
