<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *Represents a ForeignEditPermission that is granted by or to a group and
     *				therefore inherited to its members.
     *	@see ForeignEditPermission
     */
    class InheritedForeignEditPermission extends \de\cas\open\server\api\types\ForeignEditPermission {

        /**
         * @var int
         *
         *										Returns the original/actual accessor, i.e. the
         *										user/group the permission was originally granted to.
         */
        public $originalAccessor;

        /**
         * @var int
         *
         *										Returns the original/actual granter, i.e. the
         *										user/group the permission was originally granted by.
         */
        public $originalGranter;

    }

}
