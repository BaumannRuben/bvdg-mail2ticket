<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *				Represents the result of a journal query for several rows. A
     *				MassQueryResultObject has a list of types and a list of rows, whereas each row
     *				consists of several fields containing the values. The elements of types reflect
     *				the type of the corresponding field.
     */
    class JournalQueryResult {

        /**
         * @var array
         *Returns the names of the columns
         */
        public $columnNames;

        /**
         * @var array
         *
         *								Returns the rows returned by the query.
         */
        public $rows;

        /**
         * @var array
         *
         *								Returns the displayNames returned by the query, i.e. the
         *								localized names of the fields to be displayed.
         */
        public $displayNames;

        /**
         * @var array
         *
         *								Returns the types of the fields in "rows".
         */
        public $types;

    }

}
