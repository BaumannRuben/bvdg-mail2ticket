<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *				Adds the username to the User object. The username should not
     *				be accessible to non administrators.
     *	@see User
     */
    class UserWithUserName extends \de\cas\open\server\api\types\User {

        /**
         * @var string
         *
         *										The user name (= the login name)
         *										of the user (case-insensitiv),
         *										i.e. "Robert.Glaser"
         */
        public $loginName;

        /**
         * @var string
         *
         *										This GGUID is set when the user
         *										requests a reset of the
         *										password. See \de\cas\open\server\management\internal\users\MIGetResetPasswordGUIDImpl
         *										for details.
         *	@see \de\cas\open\server\management\internal\users\MIGetResetPasswordGUIDImpl
         */
        public $resetPasswordGUID;

        /**
         * @var array
         *
         *										Returns additional properties (such as
         *										properties contributed by a partner).
         */
        public $additionalProperties;

    }

}
