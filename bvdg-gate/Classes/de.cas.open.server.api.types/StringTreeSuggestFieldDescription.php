<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *Describes a String Tree Field.
     */
    class StringTreeSuggestFieldDescription extends \de\cas\open\server\api\types\StringFieldDescription {

        /**
         * @var \de\cas\open\server\api\types\StringTreeSuggestNode
         *
         *                    The root node.
         */
        public $rootNode;

    }

}
