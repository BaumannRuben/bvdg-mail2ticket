<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *        Request Object for querying information about record synchronization.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject:
     *        GetSyncInfoResponse.
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetSyncInfoResponse
     */
    class GetSyncInfoRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    The DataObjectType for which sync information should be returned.
         */
        public $DataObjectType;

    }

}
