<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *		    \de\cas\open\server\api\types\RequestObject:
     *		    It's the base type for all business operation
     *		    request objects. Corresponding
     *		    \de\cas\open\server\api\types\ResponseObject: \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     */
    abstract class RequestObject {

    }

}
