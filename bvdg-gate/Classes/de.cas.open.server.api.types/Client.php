<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     */
    class Client {

        /**
         * @var string
         *
         *								Sets/Returns the client's GGUID.
         */
        public $GGUID;

        /**
         * @var unknown
         *
         *								Sets/Returns the point of time when the
         *								client was inserted into the database.
         */
        public $InsertTimestamp;

        /**
         * @var unknown
         *
         *								Sets/Returns the point of time when the
         *								client was updated in the database.
         */
        public $UpdateTimestamp;

        /**
         * @var string
         *
         *								Sets/Returns the client's number in an
         *								external system.
         */
        public $ClientNo;

        /**
         * @var string
         *
         *								Sets/Returns the client's name.
         */
        public $ClientName;

        /**
         * @var string
         *
         *								Sets/Returns the client's prefix that
         *								must be unique but does not have to be
         *								equal with the clients database schema.
         */
        public $ClientPrefix;

        /**
         * @var boolean
         *
         *								Sets/Returns if the client is disabled,
         *								i.e. users of this client may not logon.
         */
        public $disabled;

        /**
         * @var string
         *
         *								Sets/Returns the path to the client's
         *								document store.
         */
        public $DocumentStorePath;

        /**
         * @var int
         *
         *								Sets/Returns the quota of client's
         *								document store.
         */
        public $Quota;

        /**
         * @var string
         *
         *								Sets/Returns the GUID of the
         *								DatabaseServer that holds the client's
         *								database schema.
         */
        public $DBServerGUID;

        /**
         * @var string
         *
         *								Sets/Returns the client's database
         *								schema.
         */
        public $DBSchema;

        /**
         * @var string
         *
         *								Sets/Returns the GUID used to activate a
         *								client, i.e. to verify the contact
         *								person's email address.
         */
        public $ActivationGUID;

        /**
         * @var string
         *
         *								Sets/Returns the contactperson's name.
         */
        public $ContactPersonName;

        /**
         * @var string
         *
         *								Sets/Returns the contactperson's
         *								christianname.
         */
        public $ContactPersonChristianName;

        /**
         * @var string
         *
         *								Sets/Returns the contactperson's email.
         */
        public $ContactPersonEmail;

        /**
         * @var string
         *
         *								Sets/Returns the contactperson's title.
         */
        public $ContactPersonTitle;

        /**
         * @var string
         *
         *								Sets/Returns the contactperson's address
         *								term (e.g. Mr, Mrs etc.).
         */
        public $ContactAddressTerm;

        /**
         * @var string
         *
         *								Sets/Returns the company's name.
         */
        public $CompName;

        /**
         * @var string
         *
         *								Sets/Returns the client's street.
         */
        public $Street;

        /**
         * @var string
         *
         *								Sets/Returns the client's country..
         */
        public $Country;

        /**
         * @var string
         *
         *								Sets/Returns the client's language.
         */
        public $Language;

        /**
         * @var string
         *
         *								Sets/Returns the client's zip.
         */
        public $Zip;

        /**
         * @var string
         *
         *								Sets/Returns the client's town.
         */
        public $Town;

        /**
         * @var string
         *
         *								Sets/Returns the client's phone number.
         */
        public $Phone;

        /**
         * @var string
         *
         *								Sets/Returns the client's fax number.
         */
        public $Fax;

        /**
         * @var string
         *
         *								Sets/Returns the client's bank account
         *								number.
         */
        public $BankAccountNo;

        /**
         * @var string
         *
         *								Sets/Returns the client's bank
         *								identification code.
         */
        public $BIC;

        /**
         * @var string
         *
         *								Sets/Returns the client's phone prefix
         *								(like area prefix) for authentication
         *								via DNIS (called phone number).
         */
        public $phonePrefix;

        /**
         * @var string
         *
         *								Sets/Returns the invoice person's title.
         */
        public $InvoiceTitle;

        /**
         * @var string
         *
         *								Sets/Returns the invoice person's
         *								address term (e.g. Mr, Mrs etc).
         */
        public $InvoiceAddressTerm;

        /**
         * @var string
         *
         *								Sets/Returns the name of the person that
         *								receives the invoice.
         */
        public $InvoiceName;

        /**
         * @var string
         *
         *								Sets/Returns the christianname of the
         *								person that receives the invoice.
         */
        public $InvoiceChristianName;

        /**
         * @var string
         *
         *								Sets/Returns the street of the person
         *								that receives the invoice.
         */
        public $InvoiceStreet;

        /**
         * @var string
         *
         *								Sets/Returns the zip code of the person
         *								that receives the invoice.
         */
        public $InvoiceZip;

        /**
         * @var string
         *
         *								Sets/Returns the town of the person that
         *								receives the invoice.
         */
        public $InvoiceTown;

        /**
         * @var string
         *
         *								Sets/Returns the country of the person
         *								that receives the invoice.
         */
        public $InvoiceCountry;

        /**
         * @var string
         *
         *								Sets/Returns the company name of the
         *								person that receives the invoice.
         */
        public $InvoiceCompany;

        /**
         * @var boolean
         *
         *								If the invoice address is used.
         */
        public $useInvoiceAddress;

        /**
         * @var string
         *
         *								Sets/Gets the name of the bank account
         *								holder.
         */
        public $BankAccountHolder;

        /**
         * @var string
         *
         *								Sets/Gets the partner's GGUID.
         *								A partner can have a registration
         *								page of his own and might have a
         *								labeled application.
         */
        public $PartnerGUID;

        /**
         * @var string
         *
         *								Sets/Gets the currency iso code of the client.
         */
        public $Currency;

        /**
         * @var string
         *
         *								Sets/Gets the partner campaign GGUID.
         *								A partner can have multiple campaigns with registration pages.
         */
        public $CampaignGUID;

        /**
         * @var string
         *
         *								TAX ID of the company if applicable.
         */
        public $TaxId;

    }

}
