<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *				This is a container class which holds a number of TimeZoneInfo-objects which
     *				are related to the continent represented by an instance of this class.
     */
    class TimeZoneContinentInfo {

        /**
         * @var \de\cas\open\server\api\types\KeyAndDisplayValue
         *
         *								The ID of the represented continent (e.g. "Europe") and the
         *								display name.
         */
        public $name;

        /**
         * @var array
         *
         *								A list of TimeZoneInfo-instances which relate to this
         *								continent.
         */
        public $timeZones;

    }

}
