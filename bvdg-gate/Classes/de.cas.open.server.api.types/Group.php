<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *A group which can contain PermissionOwner.
     */
    class Group extends \de\cas\open\server\api\types\PermissionsOwner {

        /**
         * @var boolean
         *
         *										Indicates if the group will be shown in
         *										dialogs other than the administration
         *										dialog. This useful for groups that simply
         *										exist to specify permissions but are not
         *										to be chosen as a permission owner of a
         *										record.
         */
        public $hidden;

        /**
         * @var array
         *A list of group members.
         */
        public $members;

    }

}
