<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *Describes an UserIdField
     *	@see UserIdField
     */
    class UserIdFieldDescription extends \de\cas\open\server\api\types\IntFieldDescription {

    }

}
