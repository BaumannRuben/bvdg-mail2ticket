<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *				A container for a long value in a generic DataObject
     */
    class LongField extends \de\cas\open\server\api\types\Field {

        /**
         * @var unknown
         *The long value of this field
         */
        public $value;

    }

}
