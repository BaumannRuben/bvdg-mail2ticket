<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *        Holds information about the sync service for a given DataObjectType.
     */
    class SyncInfo {

        /**
         * @var string
         *
         *                DataObjectType the syncInfo refers to.
         */
        public $DataObjectType;

        /**
         * @var boolean
         *
         *                If true, the \de\cas\open\server\api\types\RequestObject: SaveSyncRecordRequest must be used for saving this type of DataObjects.
         *	@see \de\cas\open\server\api\types\RequestObject
         *	@see SaveSyncRecordRequest
         */
        public $UseOfSyncRecordServiceRequired;

        /**
         * @var int
         *
         *                Access right to the data object type. Note that in case the SyncRecordServiceRequired is true,
         *                this value is more reliable than the permission in the DataObjectDescription, because the
         *                DataObjectDescription might then be generally limited to read access in order to prevent clients
         *                which are not aware of the SaveSyncRecordRequest from saving dataObjects.
         *	@see SaveSyncRecordRequest
         */
        public $DataObjectTypePermission;

    }

}
