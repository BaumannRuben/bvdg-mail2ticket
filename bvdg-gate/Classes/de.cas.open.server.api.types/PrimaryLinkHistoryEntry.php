<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     */
    class PrimaryLinkHistoryEntry {

        /**
         * @var string
         *
         *                Returns the GGUID of the parent object.
         */
        public $parentObjectGguid;

        /**
         * @var string
         *
         *                Returns the table name of the parent object.
         */
        public $parentObjectTableName;

        /**
         * @var string
         *
         *                Returns the primary linked address data object.
         */
        public $parentObjectAddress;

        /**
         * @var string
         *
         *                Returns the primary linked project data object.
         */
        public $parentObjectProject;

        /**
         * @var string
         *
         *                Returns the primary linked task data object.
         */
        public $parentObjectTask;

    }

}
