<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\types {

    /**
     * @package de\cas\open\server\api
     * @subpackage types
     *
     *				A container for a XMLGregorianCalendar value in a generic DataObject
     */
    class DateField extends \de\cas\open\server\api\types\Field {

        /**
         * @var unknown
         *
         *										The XMLGregorianCalendar value of this field
         */
        public $value;

    }

}
