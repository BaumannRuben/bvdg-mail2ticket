<?php

namespace de\cas\open\server\api\eimws {

    use \de\cas\gw\php\api\SoapClassMap;
    use \de\cas\open\server\api\exceptions\DataLayerException;
    use \de\cas\open\server\api\exceptions\BusinessException;
    use \de\cas\open\server\api\exceptions\ManagementException;

    /**
     * EIMServicePortTypeClient
     *
     * @package de\cas\open\server\api
     * @subpackage eimws
     *
     * @see \de\cas\open\server\api\eiminterface\EIMInterfaceFactory
     * @see \de\cas\open\server\api\eiminterface\EIMWebServiceInterface
     */
    class EIMServicePortTypeClient extends \SoapClient {

        const SESSION_TIMEOUT_MAX_RETRY = 1;
        const HTTP_CONNECTION_MAX_RETRY = 4;


        private $serverUrl;

        private $messageInspectors;

        private $sessionTimeoutCallback;

        private $debugLoggingCallback;


        /**
         * @return string
         */
        public function getServerUrl() {
            return $this->serverUrl;
        }

        /**
         * @return array
         */
        public function &getMessageInspectors() {
            return $this->messageInspectors;
        }

        /**
         * @return callback
         */
        public function getSessionTimeoutCallback() {
            return $this->sessionTimeoutCallback;
        }

        /**
         * @param callback $sessionTimeoutCallback
         */
        public function setSessionTimeoutCallback($sessionTimeoutCallback) {
            $this->sessionTimeoutCallback = $sessionTimeoutCallback;
        }

        /**
         * @return callback
         */
        public function getDebugLoggingCallback() {
            return $this->debugLoggingCallback;
        }

        /**
         * @param callback $debugLoggingCallback
         */
        public function setDebugLoggingCallback($debugLoggingCallback) {
            $this->debugLoggingCallback = $debugLoggingCallback;
        }

        static private function ensureArray(&$var) {
            if (!is_array($var)) {
                $var = (array) $var;
            }
        }

        protected function getWsdlUrl() {
            return preg_replace('/(?:[?#].*)?$/s', '?wsdl', $this->getServerUrl(), 1);
        }

        protected function convertEIMFault(\SoapFault $soapFault) {
            if (!empty($soapFault->detail)) {
                foreach ($soapFault->detail as $localName => $faultObj) {
                    switch ($localName) {
                        case 'DataLayerFault':
                            $convertedException = new DataLayerException($faultObj->message, $faultObj->code, $soapFault);
                            break;
                        case 'BusinessFault':
                            $convertedException = new BusinessException($faultObj->message, $faultObj->code, $soapFault);
                            break;
                        case 'ManagementFault':
                            $convertedException = new ManagementException($faultObj->message, $faultObj->code, $soapFault);
                            break;
                    }
                    if (!empty($convertedException)) {
                        $convertedException->setCreatedOn($faultObj->createdOn);
                        $convertedException->setServerStackTrace($faultObj->serverStackTrace);
                        return $convertedException;
                    }
                }
            }
            return $soapFault;
        }

        /**
         * @param string $serverUrl
         * @param callback $debugLoggingCallback
         * @param array $soapOptions
         */
        public function __construct($serverUrl, $debugLoggingCallback = NULL, $soapOptions = NULL) {
            $this->serverUrl = (string) $serverUrl;
            $this->messageInspectors = array();
            $this->sessionTimeoutCallback = NULL;
            $this->debugLoggingCallback = $debugLoggingCallback;

            try {
                $defaultOptions = array(
                        'trace' => TRUE,
                        'exceptions' => TRUE,
                        'cache_wsdl' => WSDL_CACHE_NONE,
                        'features' => SOAP_SINGLE_ELEMENT_ARRAYS,
                        'classmap' => SoapClassMap::$map,
                        'location' => $this->getServerUrl(),        // explicitly set location to avoid hostname problems on the internet
                    );

                if (!is_array($soapOptions)) {
                    $soapOptions = array();
                }
                foreach ($soapOptions as &$optionValue) {
                    if (is_numeric($optionValue) && $optionValue == (integer) $optionValue)
                        $optionValue = (integer) $optionValue;
                }
                $soapOptions += $defaultOptions;

                $wsdlUrl = $this->getWsdlUrl($this->getServerUrl());
                // Connect to the web service and parse the WSDL document
                parent::__construct($wsdlUrl, $soapOptions);

                // Notify the callback
                if (!empty($debugLoggingCallback))
                    call_user_func($debugLoggingCallback, 'SOAP initialized. WSDL:' . PHP_EOL . $wsdlUrl, 'cas', 0, array('Functions' => $this->__getFunctions(), 'Types' => count($this->__getTypes())));

            } catch (\SoapFault $soapFault) {
                if (!empty($debugLoggingCallback))
                    call_user_func($debugLoggingCallback, 'SOAP initialization failed. WSDL:' . PHP_EOL . $wsdlUrl . ';' . PHP_EOL . $soapFault->getMessage(), 'cas', 1);

                trigger_error($soapFault->__toString(), E_USER_ERROR);
                throw $soapFault;
            }
        }

        private function getSoapTraceData() {
            return array(
                    'RequestHeaders' => $this->__getLastRequestHeaders(),
                    'Request' => substr($this->__getLastRequest(), 0, 4096),
                    'ResponseHeaders' => $this->__getLastResponseHeaders(),
                    'Response' => substr($this->__getLastResponse(), 0, 4096),
                );
        }

        protected function logSoapCall($function_name, $arguments, $result, $attempt = 0, $start_time = NULL) {
            // Notify the callback
            $callback = $this->getDebugLoggingCallback();
            if (empty($callback))
                return;

            $time_delta = $start_time !== NULL ? microtime(TRUE) - $start_time : NULL;

            $string = "SOAP call: $function_name";
            if ($attempt != 0)
                $string .= " (retry $attempt)";
            $severity = 0;

            if (isset($arguments[0]) && is_array($arguments[0])) {
                $content = array();
                foreach ($arguments[0] as $value) {
                    $content[] = is_object($value) ? get_class($value) : gettype($value);
                }
                $string .= ':' . PHP_EOL . implode(', ', $content);
            }

            if ($result instanceof \stdClass) {
                $content = array();
                foreach (get_object_vars($result) as $value) {
                    $content[] = is_object($value) ? get_class($value) : gettype($value);
                }
                $string .= ' ->' . PHP_EOL . implode(', ', $content);
            } elseif ($result instanceof \SoapFault) {
                $string .= ' -' . PHP_EOL;
                if (isset($result->detail) && $result->detail instanceof \stdClass) {
                    $content = array();
                    foreach (get_object_vars($result->detail) as $key => $value) {
                        $content[] = $key . (isset($value->code) ? ' [' . $value->code . ']' : '');
                    }
                    $string .= implode(', ', $content) . ':' . PHP_EOL;
                }
                $string .= $result->getMessage();
                $severity = 1;
            }

            if ($time_delta !== NULL) {
                $string .= sprintf(' [%.3f s]', $time_delta);
            }

            call_user_func($callback, $string, 'cas', $severity, $this->getSoapTraceData());
        }

        protected function doSoapCall($function_name, $arguments, $options = null, $input_headers = null, &$output_headers = null) {
            // Allow all message inspectors to modify the request before sending
            foreach ($this->getMessageInspectors() as $inspector) {
                self::ensureArray($arguments);
                self::ensureArray($options);
                self::ensureArray($input_headers);

                $inspector->beforeSendRequest($function_name, $arguments, $options, $input_headers);
            }

            for ($conn_attempt = 0; ; ++$conn_attempt) {

                $start_time = microtime(TRUE);
                try {
                    // Send a SOAP message to the web service and receive a reply
                    $result = parent::__soapCall($function_name, $arguments, $options, $input_headers, $output_headers);

                    $this->logSoapCall($function_name, $arguments, $result, $conn_attempt, $start_time);
                    break;

                } catch (\SoapFault $soapFault) {
                    $this->logSoapCall($function_name, $arguments, $soapFault, $conn_attempt, $start_time);

                    // Connection problems are marked with the faultcode 'HTTP'. Retry! (Unless we have connected and possibly created new records...)
                    if ($soapFault->faultcode === 'HTTP' && $soapFault->getMessage() === 'Could not connect to host' &&
                            $conn_attempt < self::HTTP_CONNECTION_MAX_RETRY) {
                        continue;
                    }
                    throw $soapFault;
                }
            }

            // Notify all message inspectors of the result after receiving
            foreach ($this->getMessageInspectors() as $inspector) {
                self::ensureArray($output_headers);

                $inspector->afterReceiveReply($result, $output_headers);
            }

            return $result;
        }

        public function __soapCall($function_name, $arguments, $options = null, $input_headers = null, &$output_headers = null) {
            for ($attempt = 0; ; ++$attempt) {
                try {
                    return $this->doSoapCall($function_name, $arguments, $options, $input_headers, $output_headers);

                } catch (\SoapFault $soapFault) {
                    $eimException = $this->convertEIMFault($soapFault);

                    // Handle session timeouts gracefully
                    if ($eimException instanceof DataLayerException && (
                            $eimException->getDetailCode() === 'SECURITY_SESSION_TIMED_OUT' ||
                            $eimException->getMessage() === 'sessionTicket' ||
                            $eimException->getMessage() === 'Could not parse UsernameToken in security header')) {
                        // Notify the callback
                        $callback = $this->getSessionTimeoutCallback();
                        if (!empty($callback)) {
                            $retry = call_user_func($callback, $eimException);
                            // Do we want to do a new login and retry the SOAP request?
                            if (!empty($retry) && $attempt < self::SESSION_TIMEOUT_MAX_RETRY) {
                                continue;
                            }
                        }
                    }

//                    trigger_error($eimException->__toString(), E_USER_ERROR);
                    throw $eimException;
                }
            }
        }

        public function __call($function_name, $arguments) {
            if (substr($function_name, 0, 2) === '__') {
                return parent::__call($function_name, $arguments);
            } else {
                return $this->__soapCall($function_name, $arguments);
            }
        }
    }
}
