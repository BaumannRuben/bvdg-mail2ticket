<?php
// This file has been automatically generated.

namespace de\cas\open\server\datadefinition\types {

    /**
     * @package de\cas\open\server\datadefinition
     * @subpackage types
     *
     */
    class SelectionValueListColumnMetaInformation extends \de\cas\open\server\datadefinition\types\ColumnMetaInformation {

        /**
         * @var array
         *
         */
        public $value;

    }

}
