<?php
// This file has been automatically generated.

namespace de\cas\open\server\datadefinition\types {

    /**
     * @package de\cas\open\server\datadefinition
     * @subpackage types
     *
     */
    class AlteringDataTypeDefinition extends \de\cas\open\server\datadefinition\types\DataTypeDefinitionBase {

        /**
         * @var unknown
         *
         */
        public $fieldsToAlter;

        /**
         * @var array
         *
         */
        public $fieldToAlter;

        /**
         * @var array
         *
         */
        public $valueListFieldToAlter;

        /**
         * @var \de\cas\open\server\datadefinition\types\MixedListMapping
         *
         *										Defines which column of this type are mapped to
         *										which column of the MixedList. The MixedList is used in results
         *										that contain more records of more than one objecttype such as
         *										the dossier and search results.
         *										If you define a mixedListMapping here, then it replaces an
         *										existing one completely.
         */
        public $mixedListMapping;

    }

}
