<?php
// This file has been automatically generated.

namespace de\cas\open\server\datadefinition\types {

    /**
     * @package de\cas\open\server\datadefinition
     * @subpackage types
     *
     */
    class AlterableSelectionValueListColumnMetaInformation extends \de\cas\open\server\datadefinition\types\AlterableColumnMetaInformation {

        /**
         * @var array
         *
         */
        public $value;

        /**
         * @var array
         *
         */
        public $alterValue;

        /**
         * @var array
         *
         */
        public $deleteValue;

    }

}
