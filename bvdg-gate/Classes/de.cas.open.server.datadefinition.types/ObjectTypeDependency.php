<?php
// This file has been automatically generated.

namespace de\cas\open\server\datadefinition\types {

    /**
     * @package de\cas\open\server\datadefinition
     * @subpackage types
     *Represents a dependency between two
     *				objecttypes.
     */
    class ObjectTypeDependency {

        /**
         * @var string
         *Sets/Returns the
         *								objecttype that has
         *								dependent objecttypes.
         */
        public $superiorObjectType;

        /**
         * @var string
         *Sets/Returns the
         *								dependent objecttype.
         */
        public $dependentObjectType;

        /**
         * @var string
         *Sets/Returns the name of the superior
         *								type's foreign key column
         *								in the dependent object.
         */
        public $superiorForeignKeyColumn;

    }

}
