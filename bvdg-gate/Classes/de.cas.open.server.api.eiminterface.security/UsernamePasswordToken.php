<?php

namespace de\cas\open\server\api\eiminterface\security {

    use \de\cas\open\server\api\eiminterface\security\ISecurityToken;

    /**
     * Implementation of ISecurityToken for authentication via username and password.
     * <code>
     * &lt;?xml version="1.0" ?&gt;
     * &lt;wsse:UsernameToken
     *         xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"
     *         xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"
     *         wsu:Id="XWSSGID-1247233556220-1736697920"&gt;
     *         &lt;wsse:Username&gt;teamCRM1/Robert Glaser&lt;/wsse:Username&gt;
     *         &lt;wsse:Password
     *           Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText"&gt;9638?Phone&lt;/wsse:Password&gt;
     *       &lt;/wsse:UsernameToken&gt;
     * </code>
     *
     * @package de\cas\open\server\api
     * @subpackage eiminterface\security
     */
    class UsernamePasswordToken implements ISecurityToken {

        const wsseNamespace = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';
        const wsuNamespace  = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd';

        private $username;
        private $password;

        /**
         * Gets the current username.
         */
        public function getUsername() {
            return $this->username;
        }

        /**
         * Sets the current username.
         */
        public function setUsername($username) {
            $this->username = (string) $username;
        }

        /**
         * Gets the current password.
         */
        public function getPassword() {
            return $this->password;
        }

        /**
         * Sets the current password.
         */
        public function setPassword($password) {
            $this->password = (string) $password;
        }

        /**
         * A UsernamePasswordToken with an empty username and an empty password.
         *
         * @return \de\cas\open\server\api\eiminterface\security\UsernamePasswordToken
         */
        public static function getEmptyToken() {
            return new static('', '');
        }

        /**
         * Creates a new instance of UsernamePasswordToken.
         *
         * @param string $username the current username
         * @param string $password the current password
         */
        public function __construct($username, $password) {
            $this->setUsername($username);
            $this->setPassword($password);
        }

        /**
         * Returns the WS-Security SOAP header.
         *
         * @return \SoapHeader
         * @see \de\cas\open\server\api\eiminterface\security\ISecurityToken::getSecurityHeader()
         */
        public function getSecurityHeader() {
            $document = new \DOMDocument();

            $usernameToken = $document->createElementNS(self::wsseNamespace, 'wsse:UsernameToken');
            $document->appendChild($usernameToken);

            $wsuIdAttrib = $document->createAttributeNS(self::wsuNamespace, 'wsu:Id');
            $wsuIdAttrib->value = 'XWSSGID-1247233556220-1736697920';
            $usernameToken->appendChild($wsuIdAttrib);

            $username = $document->createElementNS(self::wsseNamespace, 'wsse:Username');
            $usernameToken->appendChild($username);

            $username->appendChild($document->createTextNode($this->getUsername()));

            $password = $document->createElementNS(self::wsseNamespace, 'wsse:Password');
            $usernameToken->appendChild($password);

            $pwTypeAttrib = $document->createAttribute('Type');
            $pwTypeAttrib->value = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText';
            $password->appendChild($pwTypeAttrib);

            $password->appendChild($document->createTextNode($this->getPassword()));

            $usernameTokenXML = $document->saveXML($usernameToken);

            $soapVarUsernameToken = new \SoapVar(
                    $usernameTokenXML,
                    XSD_ANYXML
            );

            $soapVarSecurity = new \SoapVar(
                (object) array(
                    'UsernameToken' => $soapVarUsernameToken,
                ),
                SOAP_ENC_OBJECT,
                NULL,
                self::wsseNamespace,
                'Security',
                self::wsseNamespace
            );

            $soapHeaderSecurity = new \SoapHeader(
                self::wsseNamespace,
                'Security',
                $soapVarSecurity,
                true
            );

            return $soapHeaderSecurity;
        }
    }
}
