<?php
// This file has been automatically generated.

namespace de\cas\open\server\systemnews\types {

    /**
     * @package de\cas\open\server\systemnews
     * @subpackage types
     *\de\cas\open\server\api\types\ResponseObject:<br/> Returns the currently valid system news for
     *				the client of the authenticated user, ordered by ValidFrom-Property.
     *				Corresponding \de\cas\open\server\api\types\RequestObject: GetSystemNewsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetSystemNewsRequest
     */
    class GetSystemNewsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *										Gets the reference to the List of SystemNews
         *										objects
         */
        public $systemNews;

    }

}
