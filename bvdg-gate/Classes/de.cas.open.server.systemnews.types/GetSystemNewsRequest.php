<?php
// This file has been automatically generated.

namespace de\cas\open\server\systemnews\types {

    /**
     * @package de\cas\open\server\systemnews
     * @subpackage types
     *\de\cas\open\server\api\types\RequestObject:<br/> Returns the currently valid system news for
     *				the client of the authenticated user, ordered by ValidFrom-Property.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject: GetSystemNewsResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetSystemNewsResponse
     */
    class GetSystemNewsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *Gets/sets the client prefix.
         */
        public $clientPrefix;

        /**
         * @var string
         *Gets/sets the language of the news.
         */
        public $language;

        /**
         * @var string
         *Gets/sets the customer group.
         */
        public $customerGroup;

    }

}
