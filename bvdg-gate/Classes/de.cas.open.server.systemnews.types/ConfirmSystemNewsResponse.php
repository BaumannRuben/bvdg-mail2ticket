<?php
// This file has been automatically generated.

namespace de\cas\open\server\systemnews\types {

    /**
     * @package de\cas\open\server\systemnews
     * @subpackage types
     *\de\cas\open\server\api\types\ResponseObject:<br/> Confirms the system news with
     *        the given guids.
     *				Corresponding \de\cas\open\server\api\types\RequestObject: ConfirmSystemNewsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see ConfirmSystemNewsRequest
     */
    class ConfirmSystemNewsResponse extends \de\cas\open\server\api\types\ResponseObject {

    }

}
