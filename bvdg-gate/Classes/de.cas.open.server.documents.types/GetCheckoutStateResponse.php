<?php
// This file has been automatically generated.

namespace de\cas\open\server\documents\types {

    /**
     * @package de\cas\open\server\documents
     * @subpackage types
     *
     *        Response object for the business operation that returns checkoutUserGuid
     *        and checkoutUserDescription of a document.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: GetCheckoutStateResponse
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetCheckoutStateResponse
     */
    class GetCheckoutStateResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var string
         *
         *                    Returns the value of the field checkoutUserGuid
         */
        public $checkoutUserGuid;

        /**
         * @var string
         *
         *                    Returns the value of the field checkoutUserDescription
         */
        public $checkoutUserDescription;

    }

}
