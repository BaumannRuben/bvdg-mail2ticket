<?php
// This file has been automatically generated.

namespace de\cas\open\server\documents\types {

    /**
     * @package de\cas\open\server\documents
     * @subpackage types
     *
     *	      DEPRECATED! Please use CheckInFileResponse.
     *	      \de\cas\open\server\api\types\ResponseObject: Adds a document to the
     *	      documentstore for the first time or checks it in in a changed version. Indicates if
     *	      the operation was successful.
     *	      Exceptions: DATA_LAYER.FIELD_NOT_ALTERABLE (if someone tries to reset the gwAutoVersion field),
     *	      BUSINESS_FILE_TYPE_CHANGED (if the user tries to change the file type).
     *	      Corresponding \de\cas\open\server\api\types\RequestObject: CheckInFileAndSaveDocumentRequest
     *	@see CheckInFileResponse
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see CheckInFileAndSaveDocumentRequest
     */
    class CheckInFileAndSaveDocumentResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var string
         *
         *										The GGUID of the document data object which describes
         *										the file.
         */
        public $GGUID;

    }

}
