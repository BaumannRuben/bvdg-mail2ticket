<?php
// This file has been automatically generated.

namespace de\cas\open\server\documents\types {

    /**
     * @package de\cas\open\server\documents
     * @subpackage types
     *
     *		    DEPRECATED! Please use CheckOutFileRequest. \de\cas\open\server\api\types\ResponseObject Checks out a document from the server.
     *		    Corresponding \de\cas\open\server\api\types\RequestObject: CheckOutFileAndSaveDocumentRequest
     *	@see CheckOutFileRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see CheckOutFileAndSaveDocumentRequest
     */
    class CheckOutFileAndSaveDocumentResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var string
         *
         *										Sets/Returns the GGUID of the corresponding
         *										documentobject (\de\cas\open\server\api\types\DataObject).
         *	@see \de\cas\open\server\api\types\DataObject
         */
        public $GGUID;

        /**
         * @var string
         *
         *										The file type of the document. The type can be
         *										different if versioning is enabled. That's why it is
         *										returned for every checkout request.
         */
        public $fileType;

        /**
         * @var unknown
         *
         *										Sets/Returns the document content
         */
        public $documentContent;

    }

}
