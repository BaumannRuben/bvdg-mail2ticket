<?php
// This file has been automatically generated.

namespace de\cas\open\server\documents\types {

    /**
     * @package de\cas\open\server\documents
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Resets the checkout state of a file if the user
     *        holds the file lock or has edit permissions at least. An admin can reset file locks of other users.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: ResetCheckOutStateResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see ResetCheckOutStateResponse
     */
    class ResetCheckOutStateRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *								    The GGUID of the document \de\cas\open\server\api\types\DataObject to reset.
         *	@see \de\cas\open\server\api\types\DataObject
         */
        public $GGUID;

        /**
         * @var \de\cas\open\server\api\types\DataObjectTransferable
         *
         *										This data object reference can be used to save
         *										modifications of the client. This field is optional.
         */
        public $dataObjectToSave;

    }

}
