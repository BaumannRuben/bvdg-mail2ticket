<?php
// This file has been automatically generated.

namespace de\cas\open\server\documents\types {

    /**
     * @package de\cas\open\server\documents
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject:  This operation extracts the mhtml part of a specified mailmerge document.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: GetMailMergeBodyRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetMailMergeBodyRequest
     */
    class GetMailMergeBodyResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var unknown
         *
         *                    Sets/Returns the mhtml content in a zip file.
         */
        public $zipFile;

    }

}
