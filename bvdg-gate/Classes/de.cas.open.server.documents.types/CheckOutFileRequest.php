<?php
// This file has been automatically generated.

namespace de\cas\open\server\documents\types {

    /**
     * @package de\cas\open\server\documents
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Checks out a document from the server. Corresponding
     *        \de\cas\open\server\api\types\ResponseObject: CheckOutFileResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see CheckOutFileResponse
     */
    class CheckOutFileRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *GGUID of requested document.
         */
        public $GGUID;

        /**
         * @var \de\cas\open\server\api\types\DataObjectTransferable
         *
         *										This data object reference can be used to save
         *										modifications of the client. This field is optional.
         */
        public $dataObjectToSave;

        /**
         * @var boolean
         *
         *										Indicates if the document shall be checked out for
         *										reading only, it won't be locked if set to true. If
         *										false, the document will be locked.
         */
        public $readOnly;

        /**
         * @var int
         *
         *										This property is used to request a specific version of
         *										the document. If this value is not set, the most recent
         *										version will be returned.
         */
        public $versionNumber;

    }

}
