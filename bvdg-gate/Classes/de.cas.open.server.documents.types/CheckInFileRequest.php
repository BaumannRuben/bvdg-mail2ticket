<?php
// This file has been automatically generated.

namespace de\cas\open\server\documents\types {

    /**
     * @package de\cas\open\server\documents
     * @subpackage types
     *
     *	      \de\cas\open\server\api\types\RequestObject: Adds a document to the
     *	      documentstore for the first time or updates it. The document record must be
     *	      added to the database before calling this method.
     *	      Exceptions: DATA_LAYER.FIELD_NOT_ALTERABLE (if someone tries to reset the gwAutoVersion
     *	      field), BUSINESS_FILE_TYPE_CHANGED (if the user tries to change the file type).
     *	      Corresponding \de\cas\open\server\api\types\ResponseObject: CheckInFileResponse.
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see CheckInFileResponse
     */
    class CheckInFileRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var \de\cas\open\server\api\types\DataObjectTransferable
         *
         *									  This data object reference can be used to save
         *									  modifications of the client. When creating a new document this
         *									  has to be set.
         */
        public $dataObjectToSave;

        /**
         * @var string
         *
         *									  Sets/Returns the GGUID of the corresponding \de\cas\open\server\api\types\DataObject. When creating a new
         *									  document this should be null.
         *	@see \de\cas\open\server\api\types\DataObject
         */
        public $documentGGUID;

        /**
         * @var string
         *
         *										Sets/Returns the type of the file (e.g. "TXT", "DOC",
         *										...).
         */
        public $fileType;

        /**
         * @var boolean
         *
         *										Sets/Returns the {@link #keepCheckedOut} flag that
         *										indicates that the document shall still be locked by
         *										this user after the check-in. If true, other users
         *										cannot check-out the document for write access. The
         *										default value is false.
         */
        public $keepCheckedOut;

        /**
         * @var boolean
         *
         *										If set to true, a new version of the document is
         *										created (it is possible to restore the old document
         *										content). The default value is false. If
         *										auto-versioning is enabled, this argument will be
         *										ignored.
         */
        public $createNewVersion;

        /**
         * @var string
         *
         *										If versioning is enabled, this field contains a user
         *										comment which should describe the version. A maximum of
         *										100 varchars is the limit.
         */
        public $versionComment;

        /**
         * @var unknown
         *
         *										Sets/Returns the content of the document.
         */
        public $documentContent;

    }

}
