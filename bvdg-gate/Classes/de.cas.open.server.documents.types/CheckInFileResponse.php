<?php
// This file has been automatically generated.

namespace de\cas\open\server\documents\types {

    /**
     * @package de\cas\open\server\documents
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Adds a document to the
     *        documentstore for the first time or checks it in in a changed version. Indicates if
     *        the operation was successful. Corresponding \de\cas\open\server\api\types\RequestObject: CheckInFileRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see CheckInFileRequest
     */
    class CheckInFileResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\DataObjectTransferable
         *
         *										This data object reference can be used to get the saved
         *										data object.
         */
        public $savedDataObject;

    }

}
