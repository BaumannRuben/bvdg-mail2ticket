<?php
// This file has been automatically generated.

namespace de\cas\open\server\documents\types {

    /**
     * @package de\cas\open\server\documents
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Resets the checkout state of a file if the user
     *        holds the file lock. An admin can reset file locks of other users. Corresponding
     *        \de\cas\open\server\api\types\ResponseObject: ResetCheckOutStateAndSaveDocumentResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see ResetCheckOutStateAndSaveDocumentResponse
     */
    class ResetCheckOutStateAndSaveDocumentRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var \de\cas\open\server\api\types\DataObjectTransferable
         *
         *								    Sets/Returns the corresponding document
         *								    \de\cas\open\server\api\types\DataObject.
         *	@see \de\cas\open\server\api\types\DataObject
         */
        public $documentDataObject;

    }

}
