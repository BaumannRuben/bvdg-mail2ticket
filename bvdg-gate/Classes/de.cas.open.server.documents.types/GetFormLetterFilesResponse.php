<?php
// This file has been automatically generated.

namespace de\cas\open\server\documents\types {

    /**
     * @package de\cas\open\server\documents
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\ResponseObject: This business operation creates four files for the CAS
     *				Word plugin. The files are containing the data needed to create form
     *				letters(serveral letters created by one template document). It is important to
     *				release the checkout lock if the form letter editing is finished. Corresponding
     *				\de\cas\open\server\api\types\RequestObject: GetFormLetterFilesRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetFormLetterFilesRequest
     */
    class GetFormLetterFilesResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var unknown
         *
         *										Sets/Returns the dictionary file content. It contains
         *										the translated address field names. Charset: UTF-16LE
         *										File name: 0xGGUID.dictionary
         */
        public $dictionaryFile;

        /**
         * @var unknown
         *
         *										Sets/Returns the data file content. It contains the
         *										values of the linked address records. Charset: UTF-16LE
         *										File name: 0xGGUID.csv
         */
        public $dataFile;

        /**
         * @var unknown
         *
         *										Sets/Returns the INI file content. It contains some
         *										additional information for the word plugin (i.e.
         *										application name, document record values, ...). File
         *										name: keyword.ini Note: Filename should be keyword. If
         *										the keyword contains special character which cannot be
         *										saved to disc, the modified filename must be inserted
         *										in the property "docName". Charset: UTF-8
         */
        public $iniFile;

        /**
         * @var string
         *
         *										Filename of the document to use. If the keyword
         *										contains special character which cannot be saved to
         *										disc, the modified filename must be inserted in the
         *										property "docName".
         */
        public $documentFilename;

    }

}
