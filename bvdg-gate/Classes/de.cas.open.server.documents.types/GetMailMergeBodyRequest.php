<?php
// This file has been automatically generated.

namespace de\cas\open\server\documents\types {

    /**
     * @package de\cas\open\server\documents
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: This operation extracts the mhtml part of a specified mailmerge document
     *        Corresponding
     *        \de\cas\open\server\api\types\ResponseObject: GetMailMergeBodyResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetMailMergeBodyResponse
     */
    class GetMailMergeBodyRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    GGUID of the document record
         */
        public $docGguid;

        /**
         * @var boolean
         *
         *                    Indicates if the document shall be checked out for reading only,
         *                    it won't be locked if set to true. If false, the document will be locked.
         */
        public $readOnly;

    }

}
