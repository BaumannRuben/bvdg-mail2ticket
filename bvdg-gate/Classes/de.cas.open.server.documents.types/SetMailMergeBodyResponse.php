<?php
// This file has been automatically generated.

namespace de\cas\open\server\documents\types {

    /**
     * @package de\cas\open\server\documents
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject:  This operation extracts the mhtml part of a specified mailmerge document.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: SetMailMergeBodyRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see SetMailMergeBodyRequest
     */
    class SetMailMergeBodyResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var boolean
         *
         *                    Returns true if the mailmerge document was changed successfull
         */
        public $mailMergeChanged;

    }

}
