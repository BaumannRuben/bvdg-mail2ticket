<?php
// This file has been automatically generated.

namespace de\cas\open\server\documents\types {

    /**
     * @package de\cas\open\server\documents
     * @subpackage types
     *
     *        DEPRECATED! Please use CheckInFileRequest.
     *        \de\cas\open\server\api\types\RequestObject: Adds a document to the
     *        documentstore for the first time or updates it. The document record must be
     *        added to the database before calling this method since you have to pass the
     *        GGUID of the corresponding \de\cas\open\server\api\types\DataObject.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: CheckInFileAndSaveDocumentResponse
     *	@see CheckInFileRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\DataObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see CheckInFileAndSaveDocumentResponse
     */
    class CheckInFileAndSaveDocumentRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var \de\cas\open\server\api\types\DataObjectTransferable
         *
         *                    Sets/Returns the corresponding document object
         *                    \de\cas\open\server\api\types\DataObject.
         *	@see \de\cas\open\server\api\types\DataObject
         */
        public $documentDataObject;

        /**
         * @var string
         *
         *                    Sets/Returns the type of the file (e.g. "TXT", "DOC",
         *                    ...).
         */
        public $fileType;

        /**
         * @var boolean
         *
         *                    Sets/Returns the {@link #keepCheckedOut} flag that
         *                    indicates that the document shall still be locked by
         *                    this user after the check-in. If true, other users
         *                    cannot check-out the document for write access. The
         *                    default value is false.
         */
        public $keepCheckedOut;

        /**
         * @var boolean
         *
         *                    If set to true, a new version of the document is
         *                    created (it is possible to restore the old document
         *                    content). If auto-versioning is enabled, this argument
         *                    will be ignored.
         */
        public $createNewVersion;

        /**
         * @var string
         *
         *                    If versioning is enabled, this field contains a user
         *                    comment which should describe the version.
         */
        public $versionComment;

        /**
         * @var unknown
         *
         *                    Sets/Returns the content of the document.
         */
        public $documentContent;

    }

}
