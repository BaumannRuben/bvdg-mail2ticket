<?php
// This file has been automatically generated.

namespace de\cas\open\server\documents\types {

    /**
     * @package de\cas\open\server\documents
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\RequestObject: This operation set the html content of a mailmerge document
     *				Corresponding
     *				\de\cas\open\server\api\types\ResponseObject: SetMailMergeDataResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see SetMailMergeDataResponse
     */
    class SetMailMergeDataRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var unknown
         *
         *										Content of the mailmerge document
         */
        public $htmlContent;

        /**
         * @var string
         *
         *										GGUID of the document record
         */
        public $docGguid;

    }

}
