<?php
// This file has been automatically generated.

namespace de\cas\open\server\documents\types {

    /**
     * @package de\cas\open\server\documents
     * @subpackage types
     *
     *				TODO...What does the business logic? Corresponding \de\cas\open\server\api\types\ResponseObject:
     *				GetDocumentVersionsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetDocumentVersionsRequest
     */
    class GetDocumentVersionsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var string
         *
         *										Sets/Returns the GGUID of the corresponding document
         *										record. All document versions of this document record
         *										Gguid are retrieved.
         */
        public $documentGGUID;

        /**
         * @var array
         *
         *										Container object which holds all document versions.
         */
        public $versions;

    }

}
