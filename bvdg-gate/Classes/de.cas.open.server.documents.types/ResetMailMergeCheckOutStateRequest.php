<?php
// This file has been automatically generated.

namespace de\cas\open\server\documents\types {

    /**
     * @package de\cas\open\server\documents
     * @subpackage types
     *
     *        Resets the checkout state of a file mailmerge file
     *        Corresponding \de\cas\open\server\api\types\RequestObject: ResetMailMergeCheckOutStateResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see ResetMailMergeCheckOutStateResponse
     */
    class ResetMailMergeCheckOutStateRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    The GGUID of the document data object to reset.
         */
        public $GGUID;

    }

}
