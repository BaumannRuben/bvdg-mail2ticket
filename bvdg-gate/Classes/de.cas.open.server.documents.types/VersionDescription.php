<?php
// This file has been automatically generated.

namespace de\cas\open\server\documents\types {

    /**
     * @package de\cas\open\server\documents
     * @subpackage types
     *
     *				This element contains a description for every document version. Corresponding
     *				GetDocumentVersionsResponse
     *	@see GetDocumentVersionsResponse
     */
    class VersionDescription {

        /**
         * @var string
         *
         *								The check-in comment for a specific document version.
         */
        public $versionComment;

        /**
         * @var int
         *
         *								The version number of a document version.
         */
        public $version;

        /**
         * @var string
         *
         *								This document version has been check-in by this user.
         */
        public $checkedInByUser;

        /**
         * @var unknown
         *
         *								This document version has been checked-in at this date and
         *								time.
         */
        public $checkedInDate;

    }

}
