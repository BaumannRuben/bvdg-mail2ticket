<?php
// This file has been automatically generated.

namespace de\cas\open\server\documents\types {

    /**
     * @package de\cas\open\server\documents
     * @subpackage types
     *
     *        Request object for the business operation that returns checkoutUserGuid
     *        and checkoutUserDescription of a document.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: GetCheckoutStateRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetCheckoutStateRequest
     */
    class GetCheckoutStateRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    The GGUID of the corresponding document record.
         */
        public $documentGuid;

    }

}
