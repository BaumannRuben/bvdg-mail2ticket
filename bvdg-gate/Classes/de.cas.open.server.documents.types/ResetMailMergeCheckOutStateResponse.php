<?php
// This file has been automatically generated.

namespace de\cas\open\server\documents\types {

    /**
     * @package de\cas\open\server\documents
     * @subpackage types
     *
     *        No return value at reseting checkout state. Corresponding \de\cas\open\server\api\types\ResponseObject: ResetMailMergeCheckOutStateRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see ResetMailMergeCheckOutStateRequest
     */
    class ResetMailMergeCheckOutStateResponse extends \de\cas\open\server\api\types\ResponseObject {

    }

}
