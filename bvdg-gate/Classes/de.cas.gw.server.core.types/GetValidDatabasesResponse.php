<?php
// This file has been automatically generated.

namespace de\cas\gw\server\core\types {

    /**
     * @package de\cas\gw\server\core
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Retrieves all valid databases from the server.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: GetValidDatabasesRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetValidDatabasesRequest
     */
    class GetValidDatabasesResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var string
         *
         *                    The default database.
         */
        public $DefaultDatabase;

        /**
         * @var array
         *
         *                    The valid databases.
         */
        public $Databases;

    }

}
