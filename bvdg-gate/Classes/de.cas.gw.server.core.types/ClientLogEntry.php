<?php
// This file has been automatically generated.

namespace de\cas\gw\server\core\types {

    /**
     * @package de\cas\gw\server\core
     * @subpackage types
     *
     *				An log entry sent by the client to the server in order to log messages also on the server.
     */
    class ClientLogEntry {

        /**
         * @var string
         *
         */
        public $Message;

        /**
         * @var unknown
         *
         */
        public $TimeStamp;

        /**
         * @var \de\cas\gw\server\core\types\ClientExceptionWrapper
         *
         */
        public $Exception;

        /**
         * @var string
         *
         */
        public $Level;

        /**
         * @var string
         *
         */
        public $Source;

    }

}
