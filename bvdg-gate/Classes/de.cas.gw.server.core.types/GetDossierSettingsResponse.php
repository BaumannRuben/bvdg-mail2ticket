<?php
// This file has been automatically generated.

namespace de\cas\gw\server\core\types {

    /**
     * @package de\cas\gw\server\core
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Retrieves all dossier settings for the supplied DataObjectType and the current user.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: GetDossierSettingsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetDossierSettingsRequest
     */
    class GetDossierSettingsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\gw\server\core\types\DossierSettings
         *
         */
        public $DossierSettings;

    }

}
