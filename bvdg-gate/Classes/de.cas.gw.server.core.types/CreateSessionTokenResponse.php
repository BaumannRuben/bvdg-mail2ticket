<?php
// This file has been automatically generated.

namespace de\cas\gw\server\core\types {

    /**
     * @package de\cas\gw\server\core
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Returns a Token for the current sessionTicket
     *        with the given type.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: CreateSessionTokenRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see CreateSessionTokenRequest
     */
    class CreateSessionTokenResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var string
         *
         */
        public $token;

    }

}
