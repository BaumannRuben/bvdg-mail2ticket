<?php
// This file has been automatically generated.

namespace de\cas\gw\server\core\types {

    /**
     * @package de\cas\gw\server\core
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Retrieves the windows authentication settings for the provided Username, SID and DB.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: GetWindowsAuthenticationSettingsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetWindowsAuthenticationSettingsRequest
     */
    class GetWindowsAuthenticationSettingsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var boolean
         *
         *                    Is windows authentication enabled for the given SID and DB.
         */
        public $WindowsAuthenticationEnabled;

        /**
         * @var boolean
         *
         *                    Is windows authentication forced for the given Username and DB.
         */
        public $WindowsAuthenticationForced;

    }

}
