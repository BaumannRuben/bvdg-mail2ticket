<?php
// This file has been automatically generated.

namespace de\cas\gw\server\core\types {

    /**
     * @package de\cas\gw\server\core
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Performs a Logout at the GenesisWorld Server.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: LogoutResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see LogoutResponse
     */
    class LogoutRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    The sessionTicket for the logout.
         */
        public $SessionTicket;

    }

}
