<?php
// This file has been automatically generated.

namespace de\cas\gw\server\core\types {

    /**
     * @package de\cas\gw\server\core
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Returns if current user is allowed to use Smartaccess
     *        Corresponding \de\cas\open\server\api\types\RequestObject: IsSmartAccessAllowedRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see IsSmartAccessAllowedRequest
     */
    class IsSmartAccessAllowedResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var boolean
         *
         *                    Returns <code>true</code>
         *                    if access is allowed for the current user,
         *                    otherwise <code>false</code>.
         */
        public $accessAllowed;

    }

}
