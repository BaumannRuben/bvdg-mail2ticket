<?php
// This file has been automatically generated.

namespace de\cas\gw\server\core\types {

    /**
     * @package de\cas\gw\server\core
     * @subpackage types
     *
     *				An object that wraps around an exception object in order to serialize important exception attributes.
     */
    class ClientExceptionWrapper {

        /**
         * @var string
         *
         */
        public $Message;

        /**
         * @var string
         *
         */
        public $TypeName;

        /**
         * @var string
         *
         */
        public $Source;

        /**
         * @var string
         *
         */
        public $StackTrace;

    }

}
