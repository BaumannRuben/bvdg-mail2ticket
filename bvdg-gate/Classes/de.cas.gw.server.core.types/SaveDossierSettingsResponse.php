<?php
// This file has been automatically generated.

namespace de\cas\gw\server\core\types {

    /**
     * @package de\cas\gw\server\core
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Saves all dossier settings for the supplied DataObjectType and the current user.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: SaveDossierSettingsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see SaveDossierSettingsRequest
     */
    class SaveDossierSettingsResponse extends \de\cas\open\server\api\types\ResponseObject {

    }

}
