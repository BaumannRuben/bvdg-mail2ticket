<?php
// This file has been automatically generated.

namespace de\cas\gw\server\core\types {

    /**
     * @package de\cas\gw\server\core
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Returns the a list of AutoNumberInfo for the given object types.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: GetAutoNumberFieldsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see AutoNumberInfo
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetAutoNumberFieldsRequest
     */
    class GetAutoNumberFieldsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         */
        public $autoNumberInfos;

    }

}
