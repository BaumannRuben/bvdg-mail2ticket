<?php
// This file has been automatically generated.

namespace de\cas\gw\server\core\types {

    /**
     * @package de\cas\gw\server\core
     * @subpackage types
     *
     *        The primary link relevant fields to show in the overview and the wizard. These fields can be configured
     *        in the primary link settings in the Management-Console.
     */
    class PrimaryLinkFields {

        /**
         * @var array
         *
         */
        public $addressOverviewDefaultFields;

        /**
         * @var array
         *
         */
        public $projectOverviewDefaultFields;

        /**
         * @var array
         *
         */
        public $taskOverviewDefaultFields;

        /**
         * @var array
         *
         */
        public $addressOverviewAdditionalFields;

        /**
         * @var array
         *
         */
        public $projectOverviewAdditionalFields;

        /**
         * @var array
         *
         */
        public $taskOverviewAdditionalFields;

        /**
         * @var array
         *
         */
        public $addressSearchDefaultFields;

        /**
         * @var array
         *
         */
        public $projectSearchDefaultFields;

        /**
         * @var array
         *
         */
        public $taskSearchDefaultFields;

        /**
         * @var array
         *
         */
        public $addressSearchAdditionalFields;

        /**
         * @var array
         *
         */
        public $projectSearchAdditionalFields;

        /**
         * @var array
         *
         */
        public $taskSearchAdditionalFields;

    }

}
