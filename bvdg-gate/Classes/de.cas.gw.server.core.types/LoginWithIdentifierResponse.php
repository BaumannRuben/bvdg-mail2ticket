<?php
// This file has been automatically generated.

namespace de\cas\gw\server\core\types {

    /**
     * @package de\cas\gw\server\core
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Performs a Login at the GenesisWorld Server using an Identifier.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: LoginWithIdentifierRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see LoginWithIdentifierRequest
     */
    class LoginWithIdentifierResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var string
         *
         *                    The returned sessionTicket for the login.
         */
        public $SessionTicket;

    }

}
