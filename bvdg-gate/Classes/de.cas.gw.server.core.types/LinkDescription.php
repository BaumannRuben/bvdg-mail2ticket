<?php
// This file has been automatically generated.

namespace de\cas\gw\server\core\types {

    /**
     * @package de\cas\gw\server\core
     * @subpackage types
     *
     *        Describes a link. The roles are translated to the session's locale.
     */
    class LinkDescription {

        /**
         * @var string
         *
         */
        public $attribute;

        /**
         * @var string
         *
         */
        public $objectTypeLeft;

        /**
         * @var string
         *
         */
        public $objectTypeRight;

        /**
         * @var boolean
         *
         */
        public $directed;

        /**
         * @var boolean
         *
         */
        public $deactivated;

        /**
         * @var boolean
         *
         */
        public $cardinalityLeftUnbounded;

        /**
         * @var boolean
         *
         */
        public $cardinalityRightUnbounded;

        /**
         * @var string
         *
         */
        public $roleLeft;

        /**
         * @var string
         *
         */
        public $roleRight;

    }

}
