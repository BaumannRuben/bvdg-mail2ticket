<?php
// This file has been automatically generated.

namespace de\cas\gw\server\core\types {

    /**
     * @package de\cas\gw\server\core
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Returns a auto generated number for the given field.
     *        \de\cas\open\server\api\types\ResponseObject: GetAutoNumberResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetAutoNumberResponse
     */
    class GetAutoNumberRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         */
        public $tableName;

        /**
         * @var string
         *
         */
        public $fieldName;

    }

}
