<?php
// This file has been automatically generated.

namespace de\cas\gw\server\core\types {

    /**
     * @package de\cas\gw\server\core
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Returns the AuthenticateServerState.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: GetAuthenticateServerStateResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetAuthenticateServerStateResponse
     */
    class GetAuthenticateServerStateRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         */
        public $seed;

    }

}
