<?php
// This file has been automatically generated.

namespace de\cas\gw\server\core\types {

    /**
     * @package de\cas\gw\server\core
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Gets a file containing a query result. Corresponding
     *        \de\cas\open\server\api\types\ResponseObject: QueryAsFileResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see QueryAsFileResponse
     */
    class QueryAsFileRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *The query to execute.
         */
        public $query;

        /**
         * @var int
         *
         *                    The number of records that shall be contained in the result (if the query returns more rows than this).
         */
        public $maxRecourdCount;

        /**
         * @var boolean
         *
         *                    If true a SELECT statement is cloned and appended
         *                    with a UNION so that it also
         *                    returns private records of other users.
         */
        public $includePrivateForeignRecords;

        /**
         * @var string
         *The query to execute.
         */
        public $filetype;

        /**
         * @var array
         *The fields that should'n be in the file.
         */
        public $fieldsToIgnore;

    }

}
