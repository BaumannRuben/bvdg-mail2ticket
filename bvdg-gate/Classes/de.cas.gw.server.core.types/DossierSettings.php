<?php
// This file has been automatically generated.

namespace de\cas\gw\server\core\types {

    /**
     * @package de\cas\gw\server\core
     * @subpackage types
     *
     *        Container for dossier related settings.
     */
    class DossierSettings {

        /**
         * @var array
         *
         */
        public $LinkObjects;

        /**
         * @var int
         *
         */
        public $HistoryPanelLeftWidth;

        /**
         * @var boolean
         *
         */
        public $HistoryOnlyMyLinks;

        /**
         * @var string
         *
         */
        public $StartProfile;

        /**
         * @var int
         *
         */
        public $LinkObjectsBack;

        /**
         * @var int
         *
         */
        public $LinkObjectsForward;

    }

}
