<?php
// This file has been automatically generated.

namespace de\cas\gw\server\core\types {

    /**
     * @package de\cas\gw\server\core
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Returns the primary linked parent objects of the data object identified
     *        by its object type and guid.
     *        \de\cas\open\server\api\types\ResponseObject: GetPrimaryLinkedParentsResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetPrimaryLinkedParentsResponse
     */
    class GetPrimaryLinkedParentsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         */
        public $objectType;

        /**
         * @var string
         *
         */
        public $guid;

    }

}
