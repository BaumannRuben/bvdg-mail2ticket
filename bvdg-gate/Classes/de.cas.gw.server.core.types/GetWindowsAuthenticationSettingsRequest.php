<?php
// This file has been automatically generated.

namespace de\cas\gw\server\core\types {

    /**
     * @package de\cas\gw\server\core
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Retrieves the windows authentication settings for the provided Username, SID and DB.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: GetWindowsAuthenticationSettingsResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetWindowsAuthenticationSettingsResponse
     */
    class GetWindowsAuthenticationSettingsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    The Username for which to retrieve the authentication settings.
         */
        public $Username;

        /**
         * @var string
         *
         *                    The SID for which to retrieve the authentication settings.
         */
        public $SID;

        /**
         * @var string
         *
         *                    The Database from which the authentication settings should be loaded.
         */
        public $Database;

    }

}
