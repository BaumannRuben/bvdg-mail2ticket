<?php
// This file has been automatically generated.

namespace de\cas\gw\server\core\types {

    /**
     * @package de\cas\gw\server\core
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Performs a Logout at the GenesisWorld Server.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: LogoutRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see LogoutRequest
     */
    class LogoutResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var boolean
         *
         *                    Returns whether the logout was successful.
         */
        public $Successful;

    }

}
