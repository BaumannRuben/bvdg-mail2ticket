<?php
// This file has been automatically generated.

namespace de\cas\gw\server\core\types {

    /**
     * @package de\cas\gw\server\core
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject:Gets a file containing a query result.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: QueryAsFileRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see QueryAsFileRequest
     */
    class QueryAsFileResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var unknown
         *
         *                    The query as file.
         */
        public $resultFile;

    }

}
