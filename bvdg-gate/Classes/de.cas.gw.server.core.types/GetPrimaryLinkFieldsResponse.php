<?php
// This file has been automatically generated.

namespace de\cas\gw\server\core\types {

    /**
     * @package de\cas\gw\server\core
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Returns the primary link fields.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: GetPrimaryLinkFieldsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetPrimaryLinkFieldsRequest
     */
    class GetPrimaryLinkFieldsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\gw\server\core\types\PrimaryLinkFields
         *
         */
        public $primaryLinkFields;

    }

}
