<?php
// This file has been automatically generated.

namespace de\cas\gw\server\core\types {

    /**
     * @package de\cas\gw\server\core
     * @subpackage types
     *
     *				\de\cas\open\server\api\types\RequestObject: Sends a ClientLogEntry to the server.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject: LoggingResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see LoggingResponse
     */
    class LoggingRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var \de\cas\gw\server\core\types\ClientLogEntry
         *
         */
        public $LogEntry;

    }

}
