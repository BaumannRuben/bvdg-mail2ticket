<?php
// This file has been automatically generated.

namespace de\cas\gw\server\core\types {

    /**
     * @package de\cas\gw\server\core
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Returns the primary link fields.
     *        \de\cas\open\server\api\types\ResponseObject: GetPrimaryLinkFieldsResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetPrimaryLinkFieldsResponse
     */
    class GetPrimaryLinkFieldsRequest extends \de\cas\open\server\api\types\RequestObject {

    }

}
