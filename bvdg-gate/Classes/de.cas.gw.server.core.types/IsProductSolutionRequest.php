<?php
// This file has been automatically generated.

namespace de\cas\gw\server\core\types {

    /**
     * @package de\cas\gw\server\core
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject that returns wheter
     *        a customized solution or product is available.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: IsProductSolutionResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see IsProductSolutionResponse
     */
    class IsProductSolutionRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         */
        public $ProductName;

    }

}
