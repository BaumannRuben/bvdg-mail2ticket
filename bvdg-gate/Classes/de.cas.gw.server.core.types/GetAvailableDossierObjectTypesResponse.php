<?php
// This file has been automatically generated.

namespace de\cas\gw\server\core\types {

    /**
     * @package de\cas\gw\server\core
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Retrieves all object types that can be used in the dossier.
     *        The resulting object list contains all types that the admin configured with regard to the object type permissions of the current user.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: GetAvailableDossierObjectTypesRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetAvailableDossierObjectTypesRequest
     */
    class GetAvailableDossierObjectTypesResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         */
        public $ObjectTypes;

    }

}
