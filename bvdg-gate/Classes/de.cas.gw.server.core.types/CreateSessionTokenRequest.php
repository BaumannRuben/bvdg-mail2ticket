<?php
// This file has been automatically generated.

namespace de\cas\gw\server\core\types {

    /**
     * @package de\cas\gw\server\core
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: Returns a Token for the current sessionTicket
     *        with the given type for the given view type.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: CreateSessionTokenResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see CreateSessionTokenResponse
     */
    class CreateSessionTokenRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var int
         *
         */
        public $usages;

        /**
         * @var unknown
         *
         */
        public $expirationTime;

    }

}
