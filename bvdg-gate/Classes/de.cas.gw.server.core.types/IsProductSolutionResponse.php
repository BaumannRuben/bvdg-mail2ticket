<?php
// This file has been automatically generated.

namespace de\cas\gw\server\core\types {

    /**
     * @package de\cas\gw\server\core
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject that returns wheter
     *        a customized solution or product is available.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: IsProductSolutionRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see IsProductSolutionRequest
     */
    class IsProductSolutionResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var boolean
         *
         *                    Returns <code>true</code>
         *                    if requested solution or product is available,
         *                    otherwise <code>false</code>.
         */
        public $isRequestedProduct;

    }

}
