<?php
// This file has been automatically generated.

namespace de\cas\gw\server\core\types {

    /**
     * @package de\cas\gw\server\core
     * @subpackage types
     *
     *        Short overview of a data object.
     */
    class ListItemOverview {

        /**
         * @var string
         *
         */
        public $guid;

        /**
         * @var string
         *
         */
        public $objectType;

        /**
         * @var string
         *
         */
        public $caption;

        /**
         * @var string
         *
         */
        public $description;

    }

}
