<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *
     *				\de\cas\open\server\api\types\ManagementResponseObject of the
     *				management operation that starts the process to reset a user's
     *				password. The operation returns a GGUID that must be used to
     *				actually change the password using MIResetPasswordRequest.
     *				Corresponding \de\cas\open\server\api\types\ManagementRequestObject: MIGetResetPasswordGUIDRequest
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     *	@see MIResetPasswordRequest
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see MIGetResetPasswordGUIDRequest
     */
    class MIGetResetPasswordGUIDResponse extends \de\cas\open\server\api\types\ManagementResponseObject {

        /**
         * @var string
         *
         *										Returns the GGUID that must be provided to reset
         *										the users password.
         */
        public $resetPasswordGUID;

        /**
         * @var string
         *
         *										Returns the user's email address.
         */
        public $emailAddress;

        /**
         * @var string
         *
         *										Returns the user's locale.
         */
        public $locale;

    }

}
