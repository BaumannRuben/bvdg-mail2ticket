<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *\de\cas\open\server\api\types\ManagementRequestObject of the management logic that saves a \de\cas\open\server\api\types\DatabaseServer.
     *			<br/>Corresponding \de\cas\open\server\api\types\ManagementResponseObject: MISaveDatabaseServerResponse
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see \de\cas\open\server\api\types\DatabaseServer
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     *	@see MISaveDatabaseServerResponse
     */
    class MISaveDatabaseServerRequest extends \de\cas\open\server\api\types\ManagementRequestObject {

        /**
         * @var \de\cas\open\server\api\types\DatabaseServer
         *Sets/Returns the database server to save.
         */
        public $databaseServer;

    }

}
