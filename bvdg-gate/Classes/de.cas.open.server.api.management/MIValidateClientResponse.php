<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *\de\cas\open\server\api\types\ManagementResponseObject of the ManagementOperation that validates a Client. Corresponding \de\cas\open\server\api\types\ManagementRequestObject: MIValidateClientRequest
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see MIValidateClientRequest
     */
    class MIValidateClientResponse extends \de\cas\open\server\api\types\ManagementResponseObject {

    }

}
