<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *\de\cas\open\server\api\types\ManagementResponseObject of the management operation that saves clients.
     *			<br/>Corresponding \de\cas\open\server\api\types\ManagementRequestObject: MISaveClientRequest.
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see MISaveClientRequest
     */
    class MISaveClientResponse extends \de\cas\open\server\api\types\ManagementResponseObject {

        /**
         * @var \de\cas\open\server\api\types\Client
         *Sets/Returns the saved client.
         */
        public $client;

    }

}
