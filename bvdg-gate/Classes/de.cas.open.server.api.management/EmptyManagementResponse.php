<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *\de\cas\open\server\api\types\ManagementResponseObject for operations that do not have a return value.
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     */
    class EmptyManagementResponse extends \de\cas\open\server\api\types\ManagementResponseObject {

    }

}
