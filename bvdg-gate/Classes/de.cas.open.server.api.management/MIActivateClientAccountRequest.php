<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *\de\cas\open\server\api\types\ManagementRequestObject of the management operation that
     *				activates a client to verify the email address and creates a schema.<br/>
     *				Corresponding \de\cas\open\server\api\types\ManagementResponseObject: MIActivateClientAccountResponse (See MIRegisterClientForActivationRequest)
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     *	@see MIActivateClientAccountResponse
     *	@see MIRegisterClientForActivationRequest
     */
    class MIActivateClientAccountRequest extends \de\cas\open\server\api\types\ManagementRequestObject {

        /**
         * @var string
         *Sets/Returns the prefix of the client to activate.
         */
        public $clientPrefix;

        /**
         * @var string
         *Sets/Returns the activationGUID, i.e. the GUID that was generated during the registration to verify the contact person's email.
         */
        public $activationGUID;

        /**
         * @var string
         *Sets/Returns the username of the first administrator account that will be created after the client was activated.
         */
        public $userName;

    }

}
