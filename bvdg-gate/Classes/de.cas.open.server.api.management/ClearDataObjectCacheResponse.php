<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *\de\cas\open\server\api\types\ManagementResponseObject for removing all items from the DataObject cache used in \de\cas\eim\dataaccess\jdbcimpl\DataObjectDaoJdbcImpl. \de\cas\open\server\api\types\ManagementRequestObject: ClearDataObjectCacheRequest
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     *	@see \de\cas\eim\dataaccess\jdbcimpl\DataObjectDaoJdbcImpl
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see ClearDataObjectCacheRequest
     */
    class ClearDataObjectCacheResponse extends \de\cas\open\server\api\types\ManagementResponseObject {

        /**
         * @var unknown
         *Sets/Returns the number of elements in the cache after the execution of the businessoperation.
         */
        public $cacheCount;

    }

}
