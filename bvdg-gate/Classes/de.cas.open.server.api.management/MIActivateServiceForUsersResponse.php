<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *\de\cas\open\server\api\types\ManagementResponseObject:<br/> activate external service for users.
     *				Corresponding \de\cas\open\server\api\types\ManagementRequestObject:
     *				MIActivateServiceForUsersRequest
     *				<p>Proxy operation for the {link de.cas.open.server.business.BusinessOperation}
     *				invoked by \de\cas\open\server\api\business\ActivateServiceForUsersResponse</p>
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see MIActivateServiceForUsersRequest
     *	@see \de\cas\open\server\api\business\ActivateServiceForUsersResponse
     */
    class MIActivateServiceForUsersResponse extends \de\cas\open\server\api\types\ManagementResponseObject {

        /**
         * @var array
         *
         *										Gets users id's included to service.
         */
        public $serviceUsersID;

    }

}
