<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *\de\cas\open\server\api\types\ManagementResponseObject:<br/> deactivate external service for users.
     *				Corresponding \de\cas\open\server\api\types\ManagementRequestObject: DeactivateServiceForUsersRequest
     *				<p>Proxy operation for the {link de.cas.open.server.business.BusinessOperation}
     *				invoked by \de\cas\open\server\api\business\DeactivateServiceForUsersResponse</p>
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see DeactivateServiceForUsersRequest
     *	@see \de\cas\open\server\api\business\DeactivateServiceForUsersResponse
     */
    class MIDeactivateServiceForUsersResponse extends \de\cas\open\server\api\types\ManagementResponseObject {

    }

}
