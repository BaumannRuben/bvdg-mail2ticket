<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *\de\cas\open\server\api\types\ManagementRequestObject of the management logic that adds new licences.
     *				No \de\cas\open\server\api\types\ManagementResponseObject.
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     */
    class AddLicenceRequest extends \de\cas\open\server\api\types\ManagementRequestObject {

        /**
         * @var array
         *Sets/Returns the licences.
         */
        public $licences;

    }

}
