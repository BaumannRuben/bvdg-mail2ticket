<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *\de\cas\open\server\api\types\ManagementResponseObject that retrieves a client by its prefix.
     *			<br/>Corresponding \de\cas\open\server\api\types\ManagementRequestObject: MIGetClientRequest
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see MIGetClientRequest
     */
    class MIGetClientResponse extends \de\cas\open\server\api\types\ManagementResponseObject {

        /**
         * @var \de\cas\open\server\api\types\Client
         *Sets/Returns the client.
         */
        public $client;

    }

}
