<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *\de\cas\open\server\api\types\ManagementRequestObject of the business operation that saves
     *				a user. This operation can be called by a global administrator.
     *				<br/>Corresponding \de\cas\open\server\api\types\ManagementResponseObject: MISaveUserResponse
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     *	@see MISaveUserResponse
     */
    class MISaveUserRequest extends \de\cas\open\server\api\types\ManagementRequestObject {

        /**
         * @var \de\cas\open\server\api\types\UserWithUserName
         *Sets/Gets the user to be saved.
         */
        public $userToSave;

        /**
         * @var string
         *Sets/Gets the password that should be set for the user.
         */
        public $password;

    }

}
