<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *\de\cas\open\server\api\types\ManagementRequestObject:<br/>
     *			GetBill returns the data to create a bill for using teamCRM.
     *				<p>Possible fault codes explicitly thrown by this operation are:
     *					<ul>
     *					<li>MANAGEMENT.BILLING_INVALID_CLIENTPREFIX - Client with the given prefix does not exist</li>
     *					</ul>
     *				</p>
     *				Corresponding \de\cas\open\server\api\types\ManagementResponseObject: GetBillDataResponse
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     *	@see GetBillDataResponse
     */
    class GetBillDataRequest extends \de\cas\open\server\api\types\ManagementRequestObject {

        /**
         * @var boolean
         *If property is true, the returned bill data is saved in the database. That property has to be set if a bill is created, but not for interim bills. The saved data is a fallback value if the external billing component has a critical error. The fallback data is not retrievable by the client at the moment.
         */
        public $saveBillDataInDB;

        /**
         * @var string
         *Returns the bill data of this client.
         */
        public $clientPrefix;

        /**
         * @var unknown
         *Lower data interval limit. In general, this is the first date of a month. Example. 1.3.2009 0 am. This interval limit data is included. [lowerIntervalDate, upperIntervalDate)
         */
        public $lowerIntervalDate;

        /**
         * @var unknown
         *Upper data interval limit. In general, this is the last day of a month. Example. 31.3.2009 0 am. This interval limit data is excluded. [lowerIntervalDate, upperIntervalDate)
         */
        public $upperIntervalDate;

    }

}
