<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *\de\cas\open\server\api\types\ManagementRequestObject of the management operation that creates a new demo client, i.e. a client that contains demo data. Corresponding \de\cas\open\server\api\types\ManagementResponseObject: EmptyManagementResponse>
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     *	@see EmptyManagementResponse
     */
    class MICreateDemoClientRequest extends \de\cas\open\server\api\types\ManagementRequestObject {

        /**
         * @var \de\cas\open\server\api\types\Client
         *Sets/Returns the demo client. You must set a contact person's name, christianname and email as well as the client's name and prefix.
         */
        public $demoClient;

    }

}
