<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *\de\cas\open\server\api\types\ManagementRequestObject of the ManagementOperation that validates a Client. Corresponding \de\cas\open\server\api\types\ManagementResponseObject: MIValidateClientResponse
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     *	@see MIValidateClientResponse
     */
    class MIValidateClientRequest extends \de\cas\open\server\api\types\ManagementRequestObject {

        /**
         * @var \de\cas\open\server\api\types\Client
         *Sets/Gets the Client object.
         */
        public $client;

    }

}
