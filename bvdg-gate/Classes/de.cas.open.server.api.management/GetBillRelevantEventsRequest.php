<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *\de\cas\open\server\api\types\ManagementRequestObject of the ManagementOperation that returns all events that are relevant for the bill in the given interval, e.g. the creation of a new user. Corresponding \de\cas\open\server\api\types\ManagementResponseObject: GetBillRelevantEventsResponse
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     *	@see GetBillRelevantEventsResponse
     */
    class GetBillRelevantEventsRequest extends \de\cas\open\server\api\types\ManagementRequestObject {

        /**
         * @var string
         *Returns the bill data of this client.
         */
        public $clientPrefix;

        /**
         * @var unknown
         *Lower data interval limit. In general, this is the first date of a month. Example. 1.3.2009 0 am. This interval limit data is included. [lowerIntervalDate, upperIntervalDate)
         */
        public $lowerIntervalDate;

        /**
         * @var unknown
         *Upper data interval limit. In general, this is the last day of a month. Example. 31.3.2009 0 am. This interval limit data is excluded. [lowerIntervalDate, upperIntervalDate)
         */
        public $upperIntervalDate;

    }

}
