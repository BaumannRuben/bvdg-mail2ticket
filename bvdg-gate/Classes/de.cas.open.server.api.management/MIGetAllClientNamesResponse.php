<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *\de\cas\open\server\api\types\ManagementResponseObject of the management operation that returns the names of all registered clients. <br />
     *				Corresponding \de\cas\open\server\api\types\ManagementRequestObject: MIGetAllClientNamesRequest
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see MIGetAllClientNamesRequest
     */
    class MIGetAllClientNamesResponse extends \de\cas\open\server\api\types\ManagementResponseObject {

        /**
         * @var array
         *Returns the names of all registered clients.
         */
        public $ClientNames;

    }

}
