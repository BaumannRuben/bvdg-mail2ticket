<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *
     *				\de\cas\open\server\api\types\ManagementResponseObject of the
     *				management operation that starts the process to reset a user's
     *				password. The operation returns a GGUID that must be used to
     *				actually change the password. Corresponding \de\cas\open\server\api\types\ManagementRequestObject: MIResetPasswordRequest
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see MIResetPasswordRequest
     */
    class MIResetPasswordResponse extends \de\cas\open\server\api\types\ManagementResponseObject {

        /**
         * @var string
         *
         *										Returns the user's email address.
         */
        public $emailAddress;

    }

}
