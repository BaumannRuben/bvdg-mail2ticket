<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *\de\cas\open\server\api\types\ManagementResponseObject of the management operation that
     *				activates a client to verify the email address and creates a schema.<br/>
     *				Corresponding \de\cas\open\server\api\types\ManagementRequestObject: MIActivateClientAccountRequest
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see MIActivateClientAccountRequest
     */
    class MIActivateClientAccountResponse extends \de\cas\open\server\api\types\ManagementResponseObject {

        /**
         * @var string
         *Sets/Returns the generated password of the first administrator account that was created during activation.
         */
        public $generatedPassword;

    }

}
