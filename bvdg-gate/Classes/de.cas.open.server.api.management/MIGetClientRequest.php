<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *\de\cas\open\server\api\types\ManagementRequestObject that retrieves a client by its prefix.
     *			<br/>Corresponding \de\cas\open\server\api\types\ManagementResponseObject: MIGetClientResponse
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     *	@see MIGetClientResponse
     */
    class MIGetClientRequest extends \de\cas\open\server\api\types\ManagementRequestObject {

        /**
         * @var string
         *Sets/Returns the client's prefix.
         */
        public $clientPrefix;

    }

}
