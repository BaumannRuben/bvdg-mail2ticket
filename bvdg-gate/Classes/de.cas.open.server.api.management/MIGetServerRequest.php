<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *\de\cas\open\server\api\types\ManagementRequestObject of the management logic that retrieves a \de\cas\open\server\api\types\DatabaseServer.
     *			The server's GGUID is preferred to its name.
     *			<br/>Corresponding \de\cas\open\server\api\types\ManagementResponseObject: MIGetServerResponse
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see \de\cas\open\server\api\types\DatabaseServer
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     *	@see MIGetServerResponse
     */
    class MIGetServerRequest extends \de\cas\open\server\api\types\ManagementRequestObject {

        /**
         * @var string
         *Sets/Returns the server's GGUID.
         */
        public $guid;

        /**
         * @var string
         *Sets/Returns the server's name.
         */
        public $serverName;

    }

}
