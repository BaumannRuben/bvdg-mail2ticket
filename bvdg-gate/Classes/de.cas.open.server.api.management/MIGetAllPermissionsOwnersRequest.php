<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *
     *				\de\cas\open\server\api\types\ManagementRequestObject of the \de\cas\open\server\management\ManagementOperation that retrieves a
     *				list of permissions
     *				owners (user, resource,
     *				group). Corresponding
     *				\de\cas\open\server\api\types\ManagementResponseObject: MIGetAllPermissionsOwnersResponse
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see \de\cas\open\server\management\ManagementOperation
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     *	@see MIGetAllPermissionsOwnersResponse
     */
    class MIGetAllPermissionsOwnersRequest extends \de\cas\open\server\api\types\ManagementRequestObject {

        /**
         * @var string
         *
         *										Returns the client.
         */
        public $client;

        /**
         * @var int
         *
         *										Restrict the result to certain types of permissionsOwners (which may be
         *										users, resources or groups).
         *										@see de.cas.eim.api.constants.PermissionsOwnerFilter for available filter constants.
         *										These can be combined (e.g. PermissionsOwnerFilter#USER | PermissionsOwnerFilter#GROUP to
         *										query for users and groups). Also define if deactivated users should be included or not.
         *										<br/>Valid values are:
         *										<ul>
         *											<li>USER: 1</li>
         *											<li>RESOURCE: 2</li>
         *											<li>GROUP: 4</li>
         *											<li>RETURN_DEACTIVATED: 8</li>
         *											<li>RETURN_HIDDEN: 16</li>
         *											<li>ALL: 7</li>
         *										</ul>
         */
        public $permissionsOwnerFilter;

    }

}
