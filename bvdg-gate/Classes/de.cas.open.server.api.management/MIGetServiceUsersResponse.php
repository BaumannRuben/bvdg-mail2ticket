<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *\de\cas\open\server\api\types\ManagementResponseObject:<br/> provide user ids included to external service.
     *				Corresponding \de\cas\open\server\api\types\ManagementRequestObject:
     *				MIGetServiceUsersRequest
     *				<p>Proxy operation for the {link de.cas.open.server.business.BusinessOperation}
     *				invoked by \de\cas\open\server\api\business\GetServiceUsersRequest</p>
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see MIGetServiceUsersRequest
     *	@see \de\cas\open\server\api\business\GetServiceUsersRequest
     */
    class MIGetServiceUsersResponse extends \de\cas\open\server\api\types\ManagementResponseObject {

        /**
         * @var array
         *
         *										Set user id's included to service.
         */
        public $serviceUsersID;

    }

}
