<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *\de\cas\open\server\api\types\ManagementResponseObject of the management logic that saves a \de\cas\open\server\api\types\DatabaseServer.
     *			<br/>Corresponding \de\cas\open\server\api\types\ManagementRequestObject: MISaveDatabaseServerRequest
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     *	@see \de\cas\open\server\api\types\DatabaseServer
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see MISaveDatabaseServerRequest
     */
    class MISaveDatabaseServerResponse extends \de\cas\open\server\api\types\ManagementResponseObject {

        /**
         * @var \de\cas\open\server\api\types\DatabaseServer
         *Sets/Returns the saved database server.
         */
        public $databaseServer;

    }

}
