<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *\de\cas\open\server\api\types\ManagementRequestObject: This operation serves for testing purposes only. It enables admins to check, whether the caches are correctly replicated. Corresponding \de\cas\open\server\api\types\ManagementResponseObject: ValidateCacheReplicationResponse
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     *	@see ValidateCacheReplicationResponse
     */
    class ValidateCacheReplicationRequest extends \de\cas\open\server\api\types\ManagementRequestObject {

        /**
         * @var array
         *Sets/Gets list of cache peers that are expected by the caller. If this property is set (list is non-empty), it will be used in preference over the property {@link #getNumberOfExpectedCachePeers()}.
         */
        public $expectedCachePeers;

        /**
         * @var int
         *Sets/Gets the number of expexted cache peers seen by the server to be tested. Will only be used, if {@link #getExpectedCachePeers()} is null or empty.
         */
        public $numberOfExpectedCachePeers;

        /**
         * @var string
         *Sets/Gets the cache that should be checked.
         */
        public $cacheName;

        /**
         * @var string
         *Sets/Gets the client for which the cache-entry should be checked.
         */
        public $clientName;

        /**
         * @var string
         *Sets/Gets the name of a cache-element which should be checked.
         */
        public $cacheKey;

        /**
         * @var boolean
         *Sets/Gets a flag that determines if the logic should check that the cache has been invalidated on every connected peer for this client. Flag #isCheckCacheUpdateViaCopy() and value of #getElementName() are ignored if #isCheckCacheInvalidation() is true.
         */
        public $checkCacheInvalidation;

        /**
         * @var boolean
         *Sets/Gets a flag that determines if the logic should check that the element identified by #getCacheKey() was replicated via copy to all connected peers.
         */
        public $checkCacheUpdateOrPutViaCopy;

        /**
         * @var boolean
         *Sets/Gets a flag that determines if the logic should check that the element identified by #getCacheKey() was updated on the local cache and was removed from all connected peers.
         */
        public $checkCacheUpdateViaRemoval;

        /**
         * @var boolean
         *Sets/Gets a flag that determines if the logic should check that the element identified by #getCacheKey() was removed from all connected peers.
         */
        public $checkCacheRemoval;

    }

}
