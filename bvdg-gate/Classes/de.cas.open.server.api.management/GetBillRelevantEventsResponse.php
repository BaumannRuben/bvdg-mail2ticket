<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *\de\cas\open\server\api\types\ManagementResponseObject of the ManagementOperation that returns all events that are relevant for the bill in the given interval, e.g. the creation of a new user. Corresponding \de\cas\open\server\api\types\ManagementRequestObject: GetBillRelevantEventsRequest
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see GetBillRelevantEventsRequest
     */
    class GetBillRelevantEventsResponse extends \de\cas\open\server\api\types\ManagementResponseObject {

        /**
         * @var array
         *Upper data interval limit. In general, this is the last day of a month. Example. 31.3.2009 0 am. This interval limit data is excluded. [lowerIntervalDate, upperIntervalDate)
         */
        public $events;

    }

}
