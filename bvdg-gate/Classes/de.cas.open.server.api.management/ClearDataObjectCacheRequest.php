<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *\de\cas\open\server\api\types\ManagementRequestObject for removing all items from the DataObject cache used in \de\cas\eim\dataaccess\jdbcimpl\DataObjectDaoJdbcImpl. Corresponding \de\cas\open\server\api\types\ManagementResponseObject: ClearDataObjectCacheResponse
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see \de\cas\eim\dataaccess\jdbcimpl\DataObjectDaoJdbcImpl
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     *	@see ClearDataObjectCacheResponse
     */
    class ClearDataObjectCacheRequest extends \de\cas\open\server\api\types\ManagementRequestObject {

    }

}
