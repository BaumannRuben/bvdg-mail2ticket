<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *
     *				\de\cas\open\server\api\types\ManagementRequestObject: The request-object to get all languages that are
     *				currently enabled by the system. Corresponding \de\cas\open\server\api\types\ManagementResponseObject: MIGetEnabledLanguagesResponse
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     *	@see MIGetEnabledLanguagesResponse
     */
    class MIGetEnabledLanguagesRequest extends \de\cas\open\server\api\types\ManagementRequestObject {

        /**
         * @var string
         *
         *										Optional argument, if executed as authenticated business
         *										operation. Specifies the target language of the
         *										enabled languages.
         */
        public $language;

        /**
         * @var string
         *
         *										Optional argument. Specifies the target country of the
         *										enabled languages.
         */
        public $country;

    }

}
