<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *\de\cas\open\server\api\types\ManagementRequestObject of the management operation that
     *				registers a new client that must be activated afterwards, i.e. the email
     *				address must be verified. To activate, call \de\cas\open\server\api\management\MIRegisterClientForActivationRequest.
     *				Corresponding \de\cas\open\server\api\types\ManagementResponseObject: MIRegisterClientForActivationResponse (See MIActivateClientAccountRequest)
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     *	@see MIRegisterClientForActivationResponse
     *	@see MIActivateClientAccountRequest
     */
    class MIRegisterClientForActivationRequest extends \de\cas\open\server\api\types\ManagementRequestObject {

        /**
         * @var \de\cas\open\server\api\types\Client
         *Sets/Returns the prefix (used for login later on) and name of the client.
         */
        public $client;

    }

}
