<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *
     *				\de\cas\open\server\api\types\ManagementRequestObject of the \de\cas\open\server\management\ManagementOperation that
     *				retrieves a user from the database. Corresponding \de\cas\open\server\api\types\ManagementResponseObject: MIGetUserResponse
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see \de\cas\open\server\management\ManagementOperation
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     *	@see MIGetUserResponse
     */
    class MIGetUserRequest extends \de\cas\open\server\api\types\ManagementRequestObject {

        /**
         * @var string
         *
         *										Returns the client of the user.
         */
        public $client;

        /**
         * @var int
         *
         *										Returns the user's ID.
         */
        public $userId;

    }

}
