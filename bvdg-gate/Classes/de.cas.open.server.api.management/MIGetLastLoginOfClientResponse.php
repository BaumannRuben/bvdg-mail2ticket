<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *\de\cas\open\server\api\types\ManagementResponseObject of the
     *				ManagementOperation that returns the last login date and time of the
     *				current client. Corresponding \de\cas\open\server\api\types\ManagementRequestObject: MIGetLastLoginOfClientRequest
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see MIGetLastLoginOfClientRequest
     */
    class MIGetLastLoginOfClientResponse extends \de\cas\open\server\api\types\ManagementResponseObject {

        /**
         * @var unknown
         *Returns the last login date/time of this
         *										client in a millisecond representation (as Long).
         */
        public $lastLoginInMillisString;

    }

}
