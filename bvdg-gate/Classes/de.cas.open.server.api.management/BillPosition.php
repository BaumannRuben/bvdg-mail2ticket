<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *The data of a bill position.
     */
    class BillPosition {

        /**
         * @var string
         *The type of a position, i.e. 'Number of active users'.
         */
        public $positionType;

        /**
         * @var int
         *The value of this position, i.e. '4' active users.
         */
        public $value;

    }

}
