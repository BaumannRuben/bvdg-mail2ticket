<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *
     *				\de\cas\open\server\api\types\ManagementResponseObject of the \de\cas\open\server\management\ManagementOperation that retrieves a
     *				list of permissions
     *				owners (user, resource,
     *				group). Corresponding
     *				\de\cas\open\server\api\types\ManagementRequestObject: MIGetAllPermissionsOwnersRequest
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     *	@see \de\cas\open\server\management\ManagementOperation
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see MIGetAllPermissionsOwnersRequest
     */
    class MIGetAllPermissionsOwnersResponse extends \de\cas\open\server\api\types\ManagementResponseObject {

        /**
         * @var array
         *
         *										List of information about permissions owners
         */
        public $permissionsOwners;

    }

}
