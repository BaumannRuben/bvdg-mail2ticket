<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *\de\cas\open\server\api\types\ManagementRequestObject of the management operation that returns the names of all registered clients. <br />
     *				Corresponding \de\cas\open\server\api\types\ManagementResponseObject: MIGetAllClientNamesResponse
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     *	@see MIGetAllClientNamesResponse
     */
    class MIGetAllClientNamesRequest extends \de\cas\open\server\api\types\ManagementRequestObject {

    }

}
