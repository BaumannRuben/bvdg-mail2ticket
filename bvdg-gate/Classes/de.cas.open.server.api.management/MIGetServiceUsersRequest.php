<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *\de\cas\open\server\api\types\ManagementRequestObject:<br/> provide user ids included to external service.
     *				Corresponding \de\cas\open\server\api\types\ManagementResponseObject:
     *				MIGetServiceUsersResponse
     *				<p>Proxy operation for the {link de.cas.open.server.business.BusinessOperation}
     *				invoked by \de\cas\open\server\api\business\GetServiceUsersRequest</p>
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     *	@see MIGetServiceUsersResponse
     *	@see \de\cas\open\server\api\business\GetServiceUsersRequest
     */
    class MIGetServiceUsersRequest extends \de\cas\open\server\api\types\ManagementRequestObject {

        /**
         * @var string
         *
         *										Sets the client to manage.
         */
        public $client;

        /**
         * @var string
         *
         *										Set service id, that should be provide.
         */
        public $serviceID;

    }

}
