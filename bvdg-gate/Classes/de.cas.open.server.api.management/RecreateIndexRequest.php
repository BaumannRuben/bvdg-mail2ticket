<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *\de\cas\open\server\api\types\ManagementRequestObject: TODO: Purpose of the business logic. Corresponding \de\cas\open\server\api\types\ManagementResponseObject: RecreateIndexResponse
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     *	@see RecreateIndexResponse
     */
    class RecreateIndexRequest extends \de\cas\open\server\api\types\ManagementRequestObject {

        /**
         * @var string
         *
         */
        public $client;

    }

}
