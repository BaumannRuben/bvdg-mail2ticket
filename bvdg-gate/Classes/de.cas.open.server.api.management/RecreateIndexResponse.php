<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *\de\cas\open\server\api\types\ManagementResponseObject: TODO: Purpose of the business logic. Corresponding \de\cas\open\server\api\types\ManagementRequestObject: RecreateIndexRequest
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see RecreateIndexRequest
     */
    class RecreateIndexResponse extends \de\cas\open\server\api\types\ManagementResponseObject {

    }

}
