<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *\de\cas\open\server\api\types\ManagementResponseObject of the business operation that saves
     *				a user. This operation can be called by a global administrator.
     *				<br/>Corresponding \de\cas\open\server\api\types\ManagementRequestObject: MISaveUserRequest
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see MISaveUserRequest
     */
    class MISaveUserResponse extends \de\cas\open\server\api\types\ManagementResponseObject {

        /**
         * @var \de\cas\open\server\api\types\UserWithUserName
         *Sets/Gets the saved user.
         */
        public $savedUser;

    }

}
