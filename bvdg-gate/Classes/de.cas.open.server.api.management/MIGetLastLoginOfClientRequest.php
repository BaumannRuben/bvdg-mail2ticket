<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *\de\cas\open\server\api\types\ManagementRequestObject of the
     *				ManagementOperation that returns the last login date and time of the
     *				current client. Corresponding \de\cas\open\server\api\types\ManagementResponseObject: MIGetLastLoginOfClientResponse
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     *	@see MIGetLastLoginOfClientResponse
     */
    class MIGetLastLoginOfClientRequest extends \de\cas\open\server\api\types\ManagementRequestObject {

        /**
         * @var string
         *Returns the client prefix.
         */
        public $clientPrefix;

    }

}
