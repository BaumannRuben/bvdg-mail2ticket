<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *\de\cas\open\server\api\types\ManagementRequestObject:<br/> deactivate external service for users.
     *				Corresponding \de\cas\open\server\api\types\ManagementResponseObject:
     *				MIDeactivateServiceForUsersResponse
     *				<p>Proxy operation for the {link de.cas.open.server.business.BusinessOperation}
     *				invoked by \de\cas\open\server\api\business\DeactivateServiceForUsersResponse</p>
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     *	@see MIDeactivateServiceForUsersResponse
     *	@see \de\cas\open\server\api\business\DeactivateServiceForUsersResponse
     */
    class MIDeactivateServiceForUsersRequest extends \de\cas\open\server\api\types\ManagementRequestObject {

        /**
         * @var string
         *
         *										Sets the client to manage.
         */
        public $client;

        /**
         * @var string
         *
         *										Sets the service id of the action to be
         *										retrieved.
         */
        public $serviceID;

        /**
         * @var array
         *
         *										Sets the users id for external service exception.
         */
        public $serviceUsersID;

    }

}
