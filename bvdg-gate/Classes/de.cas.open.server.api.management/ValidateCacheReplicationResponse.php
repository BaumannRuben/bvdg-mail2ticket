<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\management {

    /**
     * @package de\cas\open\server\api
     * @subpackage management
     *\de\cas\open\server\api\types\ManagementResponseObject: This operation serves for testing purposes only. It enables admins to check, whether the caches are correctly replicated. Corresponding \de\cas\open\server\api\types\ManagementRequestObject: ValidateCacheReplicationRequest
     *	@see \de\cas\open\server\api\types\ManagementResponseObject
     *	@see \de\cas\open\server\api\types\ManagementRequestObject
     *	@see ValidateCacheReplicationRequest
     */
    class ValidateCacheReplicationResponse extends \de\cas\open\server\api\types\ManagementResponseObject {

    }

}
