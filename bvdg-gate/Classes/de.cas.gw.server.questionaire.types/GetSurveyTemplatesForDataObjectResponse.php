<?php
// This file has been automatically generated.

namespace de\cas\gw\server\questionaire\types {

    /**
     * @package de\cas\gw\server\questionaire
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: Returns the available survey templates for the given data object.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: GetSurveyTemplatesForDataObjectRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetSurveyTemplatesForDataObjectRequest
     */
    class GetSurveyTemplatesForDataObjectResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\MassQueryResult
         *
         *                    The MassQueryResult with templates.
         */
        public $SurveyTemplates;

    }

}
