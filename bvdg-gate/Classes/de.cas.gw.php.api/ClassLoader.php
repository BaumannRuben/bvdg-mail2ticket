<?php

namespace de\cas\gw\php\api {

    /**
     * ClassLoader for the CAS web service API. Just include this file, and the classes should be available for use, provided that the base directory (parent to the namespace folders) is in the include path.
     *
     * @package de\cas\gw\php\api
     *
     * @author Martin.Franke
     */
    final class ClassLoader {

        const namespaceSeparator = '\\';
        const fileExtension = '.php';

        static private $instance;

        private $registered;

        /**
         * Returns the ClassLoader as a singleton.
         *
         * @return \de\cas\gw\php\api\ClassLoader
         */
        static public function getInstance() {
            if (!isset(self::$instance)) {
                self::$instance = new static;
            }
            return self::$instance;
        }

        // Private constructor; not for use from outside.
        private function __construct() {
            // do nothing
        }

        /**
         * Makes this ClassLoader available for autoload.
         *
         * You do NOT need to call this manually, because it is called automatically when you include this file!
         */
        public function register() {
            if (empty($registered)) {
                $registered = spl_autoload_register(array($this, 'autoload'), null, true);
            }
        }

        private function getRelativePathAndClassName(&$className) {
            $sepPos = strrpos($className, self::namespaceSeparator);
            if ($sepPos === FALSE)
                return '';
            $nsStart = (substr($className, 0, 1) == self::namespaceSeparator) ? 1 : 0;
            $namespace = substr($className, $nsStart, $sepPos - $nsStart);
            $className = substr($className, $sepPos + 1);
            if ($sepPos == 0)
                return '';
            return str_replace(self::namespaceSeparator, '.', $namespace) . DIRECTORY_SEPARATOR;
        }

        /**
         * Autoload method. Tries to load the CAS library class from the corresponding PHP file.
         * The class's namespace determines the file path. (We use a rather flat storage scheme for our files, no deep nesting of folders.)
         *
         * @param string $className
         * @return void
         */
        public function autoload($className) {
            $relativePath = self::getRelativePathAndClassName($className);
            $file = $relativePath . $className . self::fileExtension;
            $resolved = stream_resolve_include_path($file);
            if ($resolved !== FALSE)
                require_once $resolved;
        }
    }

    // ----- initialisation -----
    // Automatically register the ClassLoader for autoload.
    ClassLoader::getInstance()->register();
}
