<?php

namespace de\cas\open\server\api\eiminterface\interceptors {

    use \de\cas\open\server\api\eimws\IEIMServiceClientMessageInspector;
    use \de\cas\open\server\api\eiminterface\security\ISecurityTokenProvider;

    /**
     * Implementation of IEIMServiceClientMessageInspector that adds a SOAP security header to the SOAP input header list.
     *
     * @package de\cas\open\server\api
     * @subpackage eiminterface\interceptors
     */
    class WsSecurityHeaderInterceptor implements IEIMServiceClientMessageInspector {

        private $tokenProvider;

        /**
         * Creates a new instance of WsSecurityHeaderInterceptor.
         *
         * @param ISecurityTokenProvider $tokenProvider the delegate that is used to get the current security token
         */
        public function __construct(ISecurityTokenProvider $tokenProvider) {
            $this->tokenProvider = $tokenProvider;
        }

        /**
         * Adds the security header to the input header list of the EIMServicePortTypeClient.
         *
         * @see \de\cas\open\server\api\eimws\IEIMServiceClientMessageInspector::beforeSendRequest()
         */
        public function beforeSendRequest(&$function_name, array &$arguments, array &$options, array &$input_headers) {
            if ($function_name === 'executeUnauthenticated' || $function_name === 'getVersion')
                return;

            if (!is_null($this->tokenProvider)) {
                if (isset($options['soapaction'])) {
                    $soapAction = $options['soapaction'];
                } else {
                    $soapAction = NULL;
                }
                $securityToken = $this->tokenProvider->getSecurityToken($soapAction);
                if (!is_null($securityToken)) {
                    $securityHeader = $securityToken->getSecurityHeader();
                    if (!is_null($securityHeader)) {
                        array_push($input_headers, $securityHeader);
                    }
                }
            }
        }

        /**
         * @see \de\cas\open\server\api\eimws\IEIMServiceClientMessageInspector::afterReceiveReply()
         */
        public function afterReceiveReply(&$result, array &$output_headers) {
            // do nothing
        }
    }
}
