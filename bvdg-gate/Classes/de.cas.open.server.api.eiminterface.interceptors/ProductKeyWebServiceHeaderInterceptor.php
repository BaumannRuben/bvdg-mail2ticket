<?php

namespace de\cas\open\server\api\eiminterface\interceptors {

    use \de\cas\open\server\api\eimws\IEIMServiceClientMessageInspector;

    /**
     * Implementation of IEIMServiceClientMessageInspector that adds a product key header to the SOAP input header list.
     *
     * @package de\cas\open\server\api
     * @subpackage eiminterface\interceptors
     */
    class ProductKeyWebServiceHeaderInterceptor implements IEIMServiceClientMessageInspector {

        const productkeyNS = 'http://casopen.cas.de/ws/productkey';

        private $productKey;

        /**
         * Creates a new instance of ProductKeyWebServiceHeaderInterceptor.
         *
         * @param string $productKey the product key of the application
         */
        public function __construct($productKey) {
            $this->productKey = $productKey;
        }

        /**
         * Returns the product key SOAP header.
         *
         * @return \SoapHeader
         */
        protected function getProductKeyHeader() {
            if (is_null($this->productKey)) {
                return NULL;
            }

            $document = new \DOMDocument();

            $productKeyToken = $document->createElementNS(self::productkeyNS, 'productkey:ProductKey');
            $document->appendChild($productKeyToken);

            $keyAttrib = $document->createAttribute('key');
            $keyAttrib->value = $this->productKey;
            $productKeyToken->appendChild($keyAttrib);

//             $mustUnderstandAttrib = $document->createAttributeNS('http://schemas.xmlsoap.org/soap/envelope/', 'SOAP-ENV:mustUnderstand');
//             $mustUnderstandAttrib->value = 1;
//             $productKeyToken->appendChild($mustUnderstandAttrib);

            $productKeyXML = $document->saveXML($productKeyToken);

            $soapVarProductKey = new \SoapVar(
                    $productKeyXML,
                    XSD_ANYXML
                );

            $soapHeaderProductKey = new \SoapHeader(
                    self::productkeyNS,
                    'ProductKey',
                    $soapVarProductKey,
                    TRUE
                );

            return $soapHeaderProductKey;
        }

        /**
         * Adds the product key to the input header list of the EIMServicePortTypeClient.
         *
         * @see \de\cas\open\server\api\eimws\IEIMServiceClientMessageInspector::beforeSendRequest()
         */
        public function beforeSendRequest(&$function_name, array &$arguments, array &$options, array &$input_headers) {
            $productKeyHeader = $this->getProductKeyHeader();
            if (!is_null($productKeyHeader)) {
                array_push($input_headers, $productKeyHeader);
            }
        }

        /**
         * @see \de\cas\open\server\api\eimws\IEIMServiceClientMessageInspector::afterReceiveReply()
         */
        public function afterReceiveReply(&$result, array &$output_headers) {
            // do nothing
        }
    }
}
