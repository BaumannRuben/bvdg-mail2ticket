<?php
// This file has been automatically generated.

namespace de\cas\open\server\recurrences\types {

    /**
     * @package de\cas\open\server\recurrences
     * @subpackage types
     *
     *					\de\cas\open\server\api\types\RequestObject:<br/> This business operation deletes all events that
     *					belong to a given periodic event series identified by their common PeriodGUID.
     *					<br/>Corresponding \de\cas\open\server\api\types\ResponseObject: DeletePeriodicEventSeriesResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see DeletePeriodicEventSeriesResponse
     */
    class DeletePeriodicEventSeriesRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    Gets/Sets the object type for which the periodic
         *                    event
         *                    series should be deleted.
         */
        public $objectType;

        /**
         * @var string
         *
         *                    Gets/Sets the GGUID of the periodic event series
         *                    (PeriodGUID) that should be deleted.
         */
        public $periodGuid;

    }

}
