<?php
// This file has been automatically generated.

namespace de\cas\open\server\recurrences\types {

    /**
     * @package de\cas\open\server\recurrences
     * @subpackage types
     *\de\cas\open\server\api\types\ResponseObject:<br/> Returns the succeeded and failed links.
     *				Corresponding \de\cas\open\server\api\types\RequestObject: SaveLinksForPeriodicEventsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see SaveLinksForPeriodicEventsRequest
     */
    class SaveLinksForPeriodicEventsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\business\SavedAndFailedLinksContainer
         *
         *                    Returns the added and failed links.
         */
        public $savedAndFailedLinks;

    }

}
