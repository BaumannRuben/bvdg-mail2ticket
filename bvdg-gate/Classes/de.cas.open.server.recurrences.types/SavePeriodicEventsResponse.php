<?php
// This file has been automatically generated.

namespace de\cas\open\server\recurrences\types {

    /**
     * @package de\cas\open\server\recurrences
     * @subpackage types
     *
     *					\de\cas\open\server\api\types\ResponseObject: Contains the GGUID of the period that was saved.
     *					Corresponding \de\cas\open\server\api\types\RequestObject: SavePeriodicEventsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see SavePeriodicEventsRequest
     */
    class SavePeriodicEventsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var string
         *
         *											Sets/Gets the GGUID of the period that was saved. All event candidates
         *											will have this GGUID set as field PERIODGUID.
         */
        public $periodGuid;

        /**
         * @var boolean
         *
         *                    Sets/Gets the flag that determines if the base
         *                    event,
         *                    on which the calculation bases, is still part of the
         *                    resulting series.
         */
        public $baseEventPartOfSeries;

    }

}
