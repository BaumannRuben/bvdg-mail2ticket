<?php
// This file has been automatically generated.

namespace de\cas\open\server\recurrences\types {

    /**
     * @package de\cas\open\server\recurrences
     * @subpackage types
     *
     *        Describes an occurrence of a periodic event in GW.
     */
    class GWOccurrence {

        /**
         * @var unknown
         *
         */
        public $startDt;

        /**
         * @var unknown
         *
         */
        public $endDt;

        /**
         * @var unknown
         *
         */
        public $startDtOrig;

        /**
         * @var unknown
         *
         */
        public $endDtOrig;

        /**
         * @var boolean
         *
         */
        public $isExceptionEvent;

        /**
         * @var boolean
         *
         */
        public $isSingleModified;

    }

}
