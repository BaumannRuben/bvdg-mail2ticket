<?php
// This file has been automatically generated.

namespace de\cas\open\server\recurrences\types {

    /**
     * @package de\cas\open\server\recurrences
     * @subpackage types
     *\de\cas\open\server\api\types\ResponseObject:<br/> Handles link deletion for periodic events.
     *				Corresponding \de\cas\open\server\api\types\RequestObject: DeleteLinksForPeriodicEventsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see DeleteLinksForPeriodicEventsRequest
     */
    class DeleteLinksForPeriodicEventsResponse extends \de\cas\open\server\api\business\DeleteLinkResponse {

    }

}
