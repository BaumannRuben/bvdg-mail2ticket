<?php
// This file has been automatically generated.

namespace de\cas\open\server\recurrences\types {

    /**
     * @package de\cas\open\server\recurrences
     * @subpackage types
     *
     *        Defines an exception for an event in a recurrence.
     */
    class RecurrenceExceptionDate {

        /**
         * @var unknown
         *
         *                The original start date, which conforms to the recurrence rules.
         */
        public $oldStartDate;

        /**
         * @var boolean
         *
         *                If the referenced event should simply be skipped (not created).
         */
        public $skipEvent;

        /**
         * @var unknown
         *
         *                The new start date of the exception.
         */
        public $exceptionStartDate;

        /**
         * @var unknown
         *
         *                The new end date of the exception.
         */
        public $exceptionEndDate;

    }

}
