<?php
// This file has been automatically generated.

namespace de\cas\open\server\recurrences\types {

    /**
     * @package de\cas\open\server\recurrences
     * @subpackage types
     *\de\cas\open\server\api\types\RequestObject:<br/> Returns candidates that would result from a update of a recurrence.
     *			<br/>Corresponding \de\cas\open\server\api\types\ResponseObject: GetPeriodicEventUpdateCandidatesResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetPeriodicEventUpdateCandidatesResponse
     */
    class GetPeriodicEventUpdateCandidatesRequest extends \de\cas\open\server\recurrences\types\GetPeriodicEventCandidatesRequest {

        /**
         * @var string
         *
         *                    Sets/Gets the way a periodic event should be
         *                    updated
         *                    (just the event itself - SINGLE, the event itself and
         *                    all following events - FUTURE, all events - ALL)
         */
        public $periodicEventUpdateType;

        /**
         * @var boolean
         *
         *                    Sets/Gets the flag that determines if events,
         *                    that do
         *                    no longer conform to the periodic event series (e.g.
         *                    have been altered since in a single operation) should
         *                    be
         *                    updated, too.
         */
        public $updateNonPeriodConformEvents;

    }

}
