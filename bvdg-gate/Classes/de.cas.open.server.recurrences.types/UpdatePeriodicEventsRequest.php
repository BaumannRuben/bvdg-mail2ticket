<?php
// This file has been automatically generated.

namespace de\cas\open\server\recurrences\types {

    /**
     * @package de\cas\open\server\recurrences
     * @subpackage types
     *
     *					 \de\cas\open\server\api\types\RequestObject: <br/>This business operation is only available for an existing
     *					 periodic event series that should be updated. <br/> Saves the periodic-event update
     *					 candidates previously gotten through
     *					 \de\cas\open\server\appointments\types\GetPeriodicEventUpdateCandidatesRequest. Optionally,
     *					 \de\cas\open\server\appointments\types\GetPeriodicEventUpdateCandidatesRequest can be called with a
     *					 flag that saves the update candidates directly without returning them. In this case,
     *					 UpdatePeriodicEventsRequest is not required. <br/> A use case for this
     *					 business operation may be, to save update candidates (gotten through
     *					 \de\cas\open\server\appointments\types\GetPeriodicEventUpdateCandidatesRequest) that were shown
     *					 to the user who may have checked or altered single events.
     *					<br/>Corresponding \de\cas\open\server\api\types\ResponseObject: EmptyResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\appointments\types\GetPeriodicEventUpdateCandidatesRequest
     *	@see UpdatePeriodicEventsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see EmptyResponse
     */
    class UpdatePeriodicEventsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var \de\cas\open\server\api\types\DataObjectTransferable
         *
         *											Sets/Gets the event which served as the calculation basis for the {@link #getPeriodicEventsToBeCreated()} to be saved.
         *											<br/>
         *											See {@link de.cas.open.server.appointments.types.GetPeriodicEventUpdateCandidatesRequest#getReferencePeriodicEvent()}
         */
        public $referencePeriodicEvent;

        /**
         * @var array
         *
         *											Sets/Gets the events that should be treated as exceptions to the
         *											events that result from the rules defined in the #referencePeriodicEvent
         */
        public $exceptionDates;

        /**
         * @var string
         *
         *                    Sets/Gets the way a periodic event should be
         *                    updated
         *                    (just the event itself - SINGLE, the event itself and
         *                    all following events - FUTURE, all events - ALL)
         */
        public $periodicEventUpdateType;

    }

}
