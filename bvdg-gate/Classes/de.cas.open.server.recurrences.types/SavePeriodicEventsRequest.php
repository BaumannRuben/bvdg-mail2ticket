<?php
// This file has been automatically generated.

namespace de\cas\open\server\recurrences\types {

    /**
     * @package de\cas\open\server\recurrences
     * @subpackage types
     *
     *					\de\cas\open\server\api\types\RequestObject:
     *					<br/>
     *					This business operation should be used in conjunction with the
     *					\de\cas\open\server\appointments\types\GetPeriodicEventCandidatesRequest
     *					operation. <br/>
     * 					\de\cas\open\server\appointments\types\GetPeriodicEventCandidatesRequest returns a
     * 					\de\cas\open\server\api\types\MassQueryResult which contains candidates for the
     * 					periodic event set. SavePeriodicEventsRequest may now be used after conflicts have
     * 					been possibly solved to save the (modified?) candidates contained in the
     * 					\de\cas\open\server\api\types\MassQueryResult obtained from
     * 					\de\cas\open\server\appointments\types\GetPeriodicEventCandidatesRequest.
     * 					<br/>
     * 					Corresponding \de\cas\open\server\api\types\ResponseObject: SavePeriodicEventsResponse
     * 					@see GetPeriodicEventCandidatesRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\appointments\types\GetPeriodicEventCandidatesRequest
     *	@see \de\cas\open\server\api\types\MassQueryResult
     *	@see SavePeriodicEventsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see SavePeriodicEventsResponse
     */
    class SavePeriodicEventsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var \de\cas\open\server\api\types\DataObjectTransferable
         *
         *											Sets/Gets the event which served as the calculation basis for the {@link #getPeriodicEventsToBeCreated()} to be saved.
         *											<br/>
         *											See {@link de.cas.open.server.appointments.types.GetPeriodicEventCandidatesRequest#getReferencePeriodicEvent()}
         */
        public $referencePeriodicEvent;

        /**
         * @var array
         *
         *											Sets/Gets the events that should be treated as exceptions to the
         *											events that result from the rules defined in the #referencePeriodicEvent
         */
        public $exceptionDates;

    }

}
