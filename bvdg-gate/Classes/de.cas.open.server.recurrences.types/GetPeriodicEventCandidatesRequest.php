<?php
// This file has been automatically generated.

namespace de\cas\open\server\recurrences\types {

    /**
     * @package de\cas\open\server\recurrences
     * @subpackage types
     *
     *					\de\cas\open\server\api\types\RequestObject: Calculates (and optionally saves) the candidates which result from the given
     *					<code>referencePeriodicEvent</code>.
     *					<p>
     *						<b>All calculations will be done considering the TimeZone of the logged-in user!</b>
     *					</p>
     *					<p>
     *						<b>If the flag <code>saveCandidatesImmediatly</code> evaluates to
     *						<code>true</code>, the calculated periodic-event-candidates will be saved directly to the
     *						database and an empty GetPeriodicEventCandidatesResponse is returned to the
     *						callee.</b>
     *					</p>
     *					<p>
     *						Possible fault codes explicitly thrown by this operation are:
     *						<ul>
     *							<li>DATA_LAYER_END_DATE_EARLIER_THAN_START_DATE
     *								- If the events end date is before its start date.</li>
     *							<li>BUSINESS_PERIODIC_EVENT_PERIOD_END_DATE_EARLIER_THAN_PERIOD_START_DATE
     *								- If the events period-end date is before its period-start date.</li>
     *							<li>BUSINESS_PERIODIC_EVENT_PERIOD_SCHEME_NOT_VALID
     *								- If the events recurrence scheme is not valid.</li>
     *							<li>BUSINESS_PERIODIC_EVENT_DAY_OF_MONTH_EITHER_TOO_LARGE_OR_TOO_SMALL
     *								- If the field DAYOFMONTH is either too small or too large.</li>
     *							<li>BUSINESS_PERIODIC_EVENT_DAY_OF_MONTH_VALID_FOR_MONTHLY_SCHEME_ONLY
     *								- If the field {@link de.cas.eim.api.constants.APPOINTMENT#DAYOFMONTH} is set without the monthly recurrence scheme being set.</li>
     *							<li>BUSINESS_PERIODIC_EVENT_DAY_OFFSET_RULES_AND_DAY_OF_MONTH_RULE_CANNOT_BE_APPLIED_TOGETHER
     *								- If there are day-offset-rules and day-of-month rules applied together.</li>
     *							<li>BUSINESS_PERIODIC_EVENT_DAY_OFFSET_RULES_VALID_FOR_MONTHLY_SCHEME_ONLY
     *								- If there are day-offset-rules set but not the monthly recurrence scheme.</li>
     *							<li>BUSINESS_PERIODIC_EVENT_ALL_DAYS_OF_WEEK_EXCLUDED
     *								- If every day of the week is set as exclusion date.</li>
     *							<li>BUSINESS_PERIODIC_EVENT_SINGLE_DAY_AND_PSEUDO_DAY_FLAGS_MUST_BE_SET_DISTINCT
     *								- If flags for single days are set and -in the same instance- pseudo-day flags are set.</li>
     *							<li>BUSINESS_PERIODIC_EVENT_PSEUDO_DAY_FLAGS_MUST_BE_SET_DISTINCT
     *								- If more than one pseudo-day flag is set.</li>
     *							<li>BUSINESS_PERIODIC_EVENT_INTERNAL_ERROR_WHILE_DEFINING_PERIODIC_ICAL_EVENT
     *								- If an unknown error occurs while creating the event definition.</li>
     *							<li>BUSINESS_PERIODIC_EVENT_RECURRENCE_RULES_DO_NOT_RESULT_IN_ANY_EVENTS
     *								- If no events result from the given rules.</li>
     *							<li>BUSINESS_NO_VALID_RULES_SET_FOR_MONTHLY_SCHEME
     *								- If there could be no events calculated because the rules for the monthly scheme are missing.</li>
     *						</ul>
     *					</p>
     *					<p>Corresponding
     *					\de\cas\open\server\api\types\ResponseObject: GetPeriodicEventCandidatesResponse
     *					</p>
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetPeriodicEventCandidatesResponse
     *	@see \de\cas\open\server\api\types\ResponseObject
     */
    class GetPeriodicEventCandidatesRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var \de\cas\open\server\api\types\DataObjectTransferable
         *
         *											Sets/Gets the event from which the other periodic events
         *											will be calculated.
         */
        public $referencePeriodicEvent;

    }

}
