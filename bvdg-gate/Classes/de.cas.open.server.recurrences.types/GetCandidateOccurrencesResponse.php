<?php
// This file has been automatically generated.

namespace de\cas\open\server\recurrences\types {

    /**
     * @package de\cas\open\server\recurrences
     * @subpackage types
     *
     *          \de\cas\open\server\api\types\ResponseObject:<br/> Contains a list of candidate occurrences of the periodic event in GW.
     *          <br/>Corresponding \de\cas\open\server\api\types\RequestObject: GetCandidateOccurrencesRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetCandidateOccurrencesRequest
     */
    class GetCandidateOccurrencesResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *                    Contains a list of candidate occurrences of the periodic event in GW.
         */
        public $occurrences;

    }

}
