<?php
// This file has been automatically generated.

namespace de\cas\open\server\recurrences\types {

    /**
     * @package de\cas\open\server\recurrences
     * @subpackage types
     *
     *          \de\cas\open\server\api\types\RequestObject:<br/> This business operation returns all persisted events that
     *          belong to a given periodic event series.
     *          <br/>Corresponding \de\cas\open\server\api\types\ResponseObject: GetPersistedOccurrencesResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetPersistedOccurrencesResponse
     */
    class GetPersistedOccurrencesRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    The object type for which the periodic event series should be returned.
         */
        public $objectType;

        /**
         * @var \de\cas\open\server\api\types\DataObjectTransferable
         *
         *                    The data object that identifies the periodic event.
         */
        public $referencePeriodicEvent;

    }

}
