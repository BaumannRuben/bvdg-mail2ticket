<?php
// This file has been automatically generated.

namespace de\cas\open\server\recurrences\types {

    /**
     * @package de\cas\open\server\recurrences
     * @subpackage types
     *\de\cas\open\server\api\types\RequestObject:<br/> Saves \de\cas\open\server\api\types\LinkObjects for a defined set of events that belong
     *      			to a periodic event series. Accepts a list of links to be deleted for the periodic events series, too.
     *				<p>Possible fault codes explicitly thrown by this operation are:
     *					<ul>
     *	 					<li>
     *	 						{@link de.cas.eim.api.constants.ExceptionCode#BUSINESS_PERIODIC_EVENT_PERIOD_GUID_NOT_FOUND} -
     *	 						If the passed reference event has no period guid set.
     *	 					</li>
     *	 				</ul>
     *	 				see SaveLinkRequest for more exceptions.
     *				</p>
     *				Corresponding \de\cas\open\server\api\types\ResponseObject: SaveLinksForPeriodicEventsResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\LinkObject
     *	@see SaveLinkRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see SaveLinksForPeriodicEventsResponse
     */
    class SaveLinksForPeriodicEventsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var array
         *
         *                    Gets the \de\cas\open\server\api\types\LinkObjects that should be
         *                    applied to
         *                    the periodic event series.
         *	@see \de\cas\open\server\api\types\LinkObject
         */
        public $linksToBeSaved;

        /**
         * @var array
         *
         *                    Gets the GUIDs of links that should be deleted.
         */
        public $linksToBeDeleted;

        /**
         * @var string
         *
         *                    Gets/sets the update type which should be used.
         *                    It
         *                    defines the range of events that will be affected.
         */
        public $updateType;

        /**
         * @var \de\cas\open\server\api\types\DataObjectTransferable
         *
         *                    Gets/sets the reference event for which the whole
         *                    update should be performed
         */
        public $referencePeriodicEvent;

    }

}
