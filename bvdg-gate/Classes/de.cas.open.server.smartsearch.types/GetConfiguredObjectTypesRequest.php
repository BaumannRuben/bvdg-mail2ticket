<?php
// This file has been automatically generated.

namespace de\cas\open\server\smartsearch\types {

    /**
     * @package de\cas\open\server\smartsearch
     * @subpackage types
     *\de\cas\open\server\api\types\RequestObject:<br/> Gets the list of object types that are configured for indexing and searching.
     *			<br/>Corresponding \de\cas\open\server\api\types\ResponseObject: GetConfiguredObjectTypesResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetConfiguredObjectTypesResponse
     */
    class GetConfiguredObjectTypesRequest extends \de\cas\open\server\api\types\RequestObject {

    }

}
