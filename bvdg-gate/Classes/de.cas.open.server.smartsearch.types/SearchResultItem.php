<?php
// This file has been automatically generated.

namespace de\cas\open\server\smartsearch\types {

    /**
     * @package de\cas\open\server\smartsearch
     * @subpackage types
     *Class representing a search result item
     */
    class SearchResultItem {

        /**
         * @var string
         *Sets/Gets the GGUID
         */
        public $GGUID;

        /**
         * @var string
         *Type of the search result item
         */
        public $ObjectType;

        /**
         * @var string
         *Subtype of the search result item
         */
        public $ObjectSubType;

        /**
         * @var string
         *Caption of the search result item
         */
        public $Caption;

        /**
         * @var string
         *Detailed info of the search result item
         */
        public $DetailInfo;

    }

}
