<?php
// This file has been automatically generated.

namespace de\cas\open\server\smartsearch\types {

    /**
     * @package de\cas\open\server\smartsearch
     * @subpackage types
     *\de\cas\open\server\api\types\ResponseObject:<br/> Returns the search result.
     *			<br/>Corresponding \de\cas\open\server\api\types\RequestObject: SearchRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see SearchRequest
     */
    class SearchResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *Sets/Gets the DataObjects that match the query term.
         */
        public $Results;

        /**
         * @var unknown
         *The total number of hits.
         */
        public $NumHits;

        /**
         * @var unknown
         *The number of result pages.
         */
        public $NumPages;

    }

}
