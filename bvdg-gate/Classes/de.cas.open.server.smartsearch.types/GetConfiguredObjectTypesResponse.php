<?php
// This file has been automatically generated.

namespace de\cas\open\server\smartsearch\types {

    /**
     * @package de\cas\open\server\smartsearch
     * @subpackage types
     *\de\cas\open\server\api\types\ResponseObject:<br/> Returns the list of object types that are configured for indexing and searching.
     *			<br/>Corresponding \de\cas\open\server\api\types\RequestObject: GetConfiguredObjectTypesRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetConfiguredObjectTypesRequest
     */
    class GetConfiguredObjectTypesResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *List of configured object types.
         */
        public $DataObjectTypes;

    }

}
