<?php
// This file has been automatically generated.

namespace de\cas\open\server\sync\connectionbridge {

    /**
     * @package de\cas\open\server\sync
     * @subpackage connectionbridge
     *
     */
    class AddSyncProtocolEntryRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var unknown
         *
         */
        public $syncTimestamp;

        /**
         * @var string
         *
         */
        public $component;

        /**
         * @var int
         *
         */
        public $logLevel;

        /**
         * @var boolean
         *
         */
        public $isLocalTime;

        /**
         * @var int
         *
         */
        public $messageType;

        /**
         * @var string
         *
         */
        public $messageText;

        /**
         * @var string
         *
         */
        public $messagePayload;

        /**
         * @var string
         *
         */
        public $stackTrace;

        /**
         * @var \de\cas\open\server\sync\dao\transferables\TransferableSyncSessionContext
         *
         */
        public $syncSessionContext;

    }

}
