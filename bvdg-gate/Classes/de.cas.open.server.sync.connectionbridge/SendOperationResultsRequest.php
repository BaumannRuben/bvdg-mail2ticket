<?php
// This file has been automatically generated.

namespace de\cas\open\server\sync\connectionbridge {

    /**
     * @package de\cas\open\server\sync
     * @subpackage connectionbridge
     *
     */
    class SendOperationResultsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var array
         *
         */
        public $createRecordOperationResults;

        /**
         * @var array
         *
         */
        public $updateRecordOperationResults;

        /**
         * @var array
         *
         */
        public $deleteRecordOperationResults;

        /**
         * @var array
         *
         */
        public $createLinkOperationResults;

        /**
         * @var array
         *
         */
        public $deleteLinkOperationResults;

        /**
         * @var \de\cas\open\server\sync\dao\transferables\TransferableSyncSessionContext
         *
         */
        public $syncSessionContext;

    }

}
