<?php
// This file has been automatically generated.

namespace de\cas\open\server\sync\connectionbridge {

    /**
     * @package de\cas\open\server\sync
     * @subpackage connectionbridge
     *
     */
    class GetIdsFromViewRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var \de\cas\open\server\sync\dao\transferables\TransferableSyncViewDefinition
         *
         */
        public $syncViewDefinition;

        /**
         * @var int
         *
         */
        public $pageOffset;

        /**
         * @var int
         *
         */
        public $pageSize;

        /**
         * @var \de\cas\open\server\sync\dao\transferables\TransferableSyncSessionContext
         *
         */
        public $syncSessionContext;

    }

}
