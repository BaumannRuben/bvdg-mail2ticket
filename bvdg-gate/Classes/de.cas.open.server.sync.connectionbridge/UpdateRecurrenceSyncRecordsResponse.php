<?php
// This file has been automatically generated.

namespace de\cas\open\server\sync\connectionbridge {

    /**
     * @package de\cas\open\server\sync
     * @subpackage connectionbridge
     *
     */
    class UpdateRecurrenceSyncRecordsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\sync\dao\transferables\TransferableOperationResult
         *
         */
        public $successfulUpdate;

        /**
         * @var \de\cas\open\server\sync\dao\transferables\TransferableFailedOperationResult
         *
         */
        public $failedUpdate;

    }

}
