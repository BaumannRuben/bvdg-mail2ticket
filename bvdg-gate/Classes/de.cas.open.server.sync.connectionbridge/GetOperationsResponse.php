<?php
// This file has been automatically generated.

namespace de\cas\open\server\sync\connectionbridge {

    /**
     * @package de\cas\open\server\sync
     * @subpackage connectionbridge
     *
     */
    class GetOperationsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         */
        public $createRecordOperations;

        /**
         * @var array
         *
         */
        public $updateRecordOperations;

        /**
         * @var array
         *
         */
        public $deleteRecordOperations;

        /**
         * @var array
         *
         */
        public $createLinkOperations;

        /**
         * @var array
         *
         */
        public $deleteLinkOperations;

    }

}
