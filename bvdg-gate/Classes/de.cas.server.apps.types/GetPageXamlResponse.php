<?php
// This file has been automatically generated.

namespace de\cas\server\apps\types {

    /**
     * @package de\cas\server\apps\types
     *
     *
     *        \de\cas\open\server\api\types\ResponseObject: The request-object to get Xaml for the requested form.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: GetPageXamlRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetPageXamlRequest
     */
    class GetPageXamlResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var string
         *
         *                    The XAML of the page
         */
        public $pageContent;

    }

}
