<?php
// This file has been automatically generated.

namespace de\cas\server\apps\types {

    /**
     * @package de\cas\server\apps\types
     *
     *
     *        \de\cas\open\server\api\types\ResponseObject: Returns the UI contributions that should be added to the placeholder with the supplied id.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: GetFormContributionsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetFormContributionsRequest
     */
    class GetFormContributionsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         */
        public $contributions;

    }

}
