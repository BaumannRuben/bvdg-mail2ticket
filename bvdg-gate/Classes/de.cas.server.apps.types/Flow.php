<?php
// This file has been automatically generated.

namespace de\cas\server\apps\types {

    /**
     * @package de\cas\server\apps\types
     *
     *
     */
    class Flow {

        /**
         * @var string
         *
         */
        public $id;

        /**
         * @var array
         *
         */
        public $states;

        /**
         * @var array
         *
         */
        public $transitions;

    }

}
