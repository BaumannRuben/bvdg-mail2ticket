<?php
// This file has been automatically generated.

namespace de\cas\server\apps\types {

    /**
     * @package de\cas\server\apps\types
     *
     *
     *        \de\cas\open\server\api\types\RequestObject: Returns the DataObjectDescription as a XAML string (as a ResourceDictionary).
     *        If no ObjectType is specified, all the DataObjectDescriptionswill be returned.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: GetDataObjectDescriptionsInXAMLResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetDataObjectDescriptionsInXAMLResponse
     */
    class GetDataObjectDescriptionsInXAMLRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         */
        public $ObjectType;

    }

}
