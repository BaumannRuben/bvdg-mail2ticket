<?php
// This file has been automatically generated.

namespace de\cas\server\apps\types {

    /**
     * @package de\cas\server\apps\types
     *
     *
     */
    class AppDescription extends \de\cas\server\apps\types\ExecutableDescription {

        /**
         * @var string
         *
         */
        public $id;

        /**
         * @var string
         *
         */
        public $objectType;

        /**
         * @var boolean
         *
         */
        public $hasXap;

        /**
         * @var boolean
         *
         */
        public $isAppExtension;

    }

}
