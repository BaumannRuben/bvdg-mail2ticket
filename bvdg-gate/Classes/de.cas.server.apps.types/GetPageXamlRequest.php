<?php
// This file has been automatically generated.

namespace de\cas\server\apps\types {

    /**
     * @package de\cas\server\apps\types
     *
     *
     *        \de\cas\open\server\api\types\RequestObject: The request-object to get Xaml for the requested form.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: GetPageXamlResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetPageXamlResponse
     */
    class GetPageXamlRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    Optional argument, if there are several formtypes for dataobjecttype
         */
        public $pageId;

    }

}
