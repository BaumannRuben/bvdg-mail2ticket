<?php
// This file has been automatically generated.

namespace de\cas\server\apps\types {

    /**
     * @package de\cas\server\apps\types
     *
     *
     *        \de\cas\open\server\api\types\ResponseObject: The response-object to get the start app for the current user.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: GetStartAppRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetStartAppRequest
     */
    class GetStartAppResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\server\apps\types\AppDescription
         *
         *                    The AppDescription that describes the start App.
         */
        public $startApp;

        /**
         * @var \de\cas\server\apps\types\Flow
         *
         *                    The flow for the start App.
         */
        public $flow;

        /**
         * @var \de\cas\server\apps\types\FormMappings
         *
         *                    The form mappings for the start App.
         */
        public $formMappings;

    }

}
