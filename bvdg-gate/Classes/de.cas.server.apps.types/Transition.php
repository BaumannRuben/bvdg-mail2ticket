<?php
// This file has been automatically generated.

namespace de\cas\server\apps\types {

    /**
     * @package de\cas\server\apps\types
     *
     *
     */
    class Transition {

        /**
         * @var string
         *
         */
        public $on;

        /**
         * @var string
         *
         */
        public $from;

        /**
         * @var boolean
         *
         */
        public $fromStartState;

        /**
         * @var string
         *
         */
        public $to;

        /**
         * @var int
         *
         */
        public $pagesToKeep;

    }

}
