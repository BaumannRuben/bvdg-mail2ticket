<?php
// This file has been automatically generated.

namespace de\cas\server\apps\types {

    /**
     * @package de\cas\server\apps\types
     *
     *
     */
    class FormContribution {

        /**
         * @var string
         *
         */
        public $placeHolderId;

        /**
         * @var string
         *
         */
        public $objectType;

        /**
         * @var string
         *
         */
        public $headerText;

        /**
         * @var string
         *
         */
        public $when;

        /**
         * @var int
         *
         */
        public $ranking;

        /**
         * @var string
         *
         */
        public $userControl;

    }

}
