<?php
// This file has been automatically generated.

namespace de\cas\server\apps\types {

    /**
     * @package de\cas\server\apps\types
     *
     *
     *        \de\cas\open\server\api\types\RequestObject: The request-object to get a list of all the activated apps for the current user.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: GetAllActivatedAppsResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetAllActivatedAppsResponse
     */
    class GetAllActivatedAppsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         */
        public $solutionID;

    }

}
