<?php
// This file has been automatically generated.

namespace de\cas\server\apps\types {

    /**
     * @package de\cas\server\apps\types
     *
     *
     *        \de\cas\open\server\api\types\RequestObject: The request-object to get the start app for the current user.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: GetStartAppResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetStartAppResponse
     */
    class GetStartAppRequest extends \de\cas\open\server\api\types\RequestObject {

    }

}
