<?php
// This file has been automatically generated.

namespace de\cas\server\apps\types {

    /**
     * @package de\cas\server\apps\types
     *
     *
     */
    class AppLinkDescription extends \de\cas\server\apps\types\ExecutableDescription {

        /**
         * @var \de\cas\server\apps\types\AppDescription
         *
         */
        public $app;

        /**
         * @var array
         *
         */
        public $parameters;

    }

}
