<?php
// This file has been automatically generated.

namespace de\cas\server\apps\types {

    /**
     * @package de\cas\server\apps\types
     *
     *
     *        \de\cas\open\server\api\types\ResponseObject: Returns the DataObjectDescription as a XAML string (as a ResourceDictionary).
     *        If no ObjectType is specified, all the DataObjectDescriptionswill be returned.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: getDataObjectDescriptionsInXamlRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see getDataObjectDescriptionsInXamlRequest
     */
    class GetDataObjectDescriptionsInXAMLResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var string
         *
         */
        public $DataObjectDescriptionXAML;

    }

}
