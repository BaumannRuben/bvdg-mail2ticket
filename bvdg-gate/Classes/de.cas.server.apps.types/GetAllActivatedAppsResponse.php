<?php
// This file has been automatically generated.

namespace de\cas\server\apps\types {

    /**
     * @package de\cas\server\apps\types
     *
     *
     *        \de\cas\open\server\api\types\ResponseObject: The request-object to get a list of all the activated apps for the current user.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: GetAllActivatedAppsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetAllActivatedAppsRequest
     */
    class GetAllActivatedAppsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         */
        public $apps;

        /**
         * @var array
         *
         */
        public $appLinks;

    }

}
