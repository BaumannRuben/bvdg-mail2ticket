<?php
// This file has been automatically generated.

namespace de\cas\server\apps\types {

    /**
     * @package de\cas\server\apps\types
     *
     *
     *        \de\cas\open\server\api\types\ResponseObject: Returns the requested apps as a zip file.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: GetAppsAsZipRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetAppsAsZipRequest
     */
    class GetAppsAsZipResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var unknown
         *
         */
        public $zip;

    }

}
