<?php
// This file has been automatically generated.

namespace de\cas\server\apps\types {

    /**
     * @package de\cas\server\apps\types
     *
     *
     *        \de\cas\open\server\api\types\RequestObject: Returns the DataObjects from the Dossier of the DataObject,
     *        specified by the GGUID parameter, in a silverlight xaml compatible format, as a ResourceDictionary.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: GetDataObjectsInDossierOfInXAMLResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetDataObjectsInDossierOfInXAMLResponse
     */
    class GetDataObjectsInDossierOfInXAMLRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         */
        public $objectType;

        /**
         * @var string
         *
         */
        public $GGUID;

    }

}
