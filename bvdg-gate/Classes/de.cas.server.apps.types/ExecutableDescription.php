<?php
// This file has been automatically generated.

namespace de\cas\server\apps\types {

    /**
     * @package de\cas\server\apps\types
     *
     *
     */
    class ExecutableDescription {

        /**
         * @var string
         *
         */
        public $displayName;

        /**
         * @var string
         *
         */
        public $description;

        /**
         * @var unknown
         *
         */
        public $icon;

    }

}
