<?php
// This file has been automatically generated.

namespace de\cas\server\apps\types {

    /**
     * @package de\cas\server\apps\types
     *
     *
     *        \de\cas\open\server\api\types\ResponseObject: Returns a design time project as a zip file.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: CreateDesignTimeProjectAsZipRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see CreateDesignTimeProjectAsZipRequest
     */
    class CreateDesignTimeProjectAsZipResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var unknown
         *
         *                    The zipped design time project.
         */
        public $zip;

    }

}
