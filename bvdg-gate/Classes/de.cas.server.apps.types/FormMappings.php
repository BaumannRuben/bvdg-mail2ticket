<?php
// This file has been automatically generated.

namespace de\cas\server\apps\types {

    /**
     * @package de\cas\server\apps\types
     *
     *
     */
    class FormMappings {

        /**
         * @var string
         *
         */
        public $id;

        /**
         * @var array
         *
         */
        public $formMapping;

    }

}
