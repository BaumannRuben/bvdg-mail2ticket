<?php
// This file has been automatically generated.

namespace de\cas\server\apps\types {

    /**
     * @package de\cas\server\apps\types
     *
     *
     *        \de\cas\open\server\api\types\RequestObject: Returns the UI contributions that should be added to the placeholder with the supplied id.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: GetFormContributionsResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetFormContributionsResponse
     */
    class GetFormContributionsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         */
        public $placeHolderId;

    }

}
