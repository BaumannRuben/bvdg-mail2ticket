<?php
// This file has been automatically generated.

namespace de\cas\server\apps\types {

    /**
     * @package de\cas\server\apps\types
     *
     *
     */
    class ConfigurationAttribute {

        /**
         * @var string
         *
         */
        public $name;

        /**
         * @var string
         *
         */
        public $value;

    }

}
