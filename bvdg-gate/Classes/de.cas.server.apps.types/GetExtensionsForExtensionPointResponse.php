<?php
// This file has been automatically generated.

namespace de\cas\server\apps\types {

    /**
     * @package de\cas\server\apps\types
     *
     *
     *        \de\cas\open\server\api\types\ResponseObject: Returns all available contributions for the supplied extensionPoint-ID.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: GetExtensionsForExtensionPointRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetExtensionsForExtensionPointRequest
     */
    class GetExtensionsForExtensionPointResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *                    All available contributions for the supplied extensionPoint-ID.
         */
        public $extensions;

    }

}
