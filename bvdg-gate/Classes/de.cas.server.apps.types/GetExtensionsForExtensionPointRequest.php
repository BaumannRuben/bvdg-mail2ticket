<?php
// This file has been automatically generated.

namespace de\cas\server\apps\types {

    /**
     * @package de\cas\server\apps\types
     *
     *
     *        \de\cas\open\server\api\types\RequestObject: The request-object to get all available contributions for the supplied extensionPoint-ID.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: GetExtensionsForExtensionPointResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetExtensionsForExtensionPointResponse
     */
    class GetExtensionsForExtensionPointRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    the unique identifier of the extension point (e.g. org.eclipse.core.resources.builders)
         */
        public $extensionPointId;

    }

}
