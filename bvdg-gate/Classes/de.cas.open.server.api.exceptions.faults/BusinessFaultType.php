<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\exceptions\faults {

    /**
     * @package de\cas\open\server\api
     * @subpackage exceptions\faults
     *
     */
    class BusinessFaultType extends \de\cas\open\server\api\exceptions\faults\EIMFaultType {

        /**
         * @var string
         *
         */
        public $operantionName;

    }

}
