<?php
// This file has been automatically generated.

namespace de\cas\open\server\events\types {

    /**
     * @package de\cas\open\server\events
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: response object for the read accounting model request
     *        Corresponding \de\cas\open\server\api\types\RequestObject: AutoCalculateAccountingPricesRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see AutoCalculateAccountingPricesRequest
     */
    class AutoCalculateAccountingPricesResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var boolean
         *
         *                    returns true if the automatic selection of accounting positions was successful, false otherwise
         */
        public $autoSelectedSuccess;

        /**
         * @var \de\cas\open\server\events\types\AccountingPosition
         *
         *                    returns the accounting model
         */
        public $accounting;

    }

}
