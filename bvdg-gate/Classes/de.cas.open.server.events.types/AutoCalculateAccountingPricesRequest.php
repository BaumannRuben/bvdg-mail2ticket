<?php
// This file has been automatically generated.

namespace de\cas\open\server\events\types {

    /**
     * @package de\cas\open\server\events
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: reads the accounting model for an event
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: AutoCalculateAccountingPricesResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see AutoCalculateAccountingPricesResponse
     */
    class AutoCalculateAccountingPricesRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    Guid of the participant
         */
        public $participantGuid;

        /**
         * @var string
         *
         *                    GUID of the event
         */
        public $eventGuid;

        /**
         * @var unknown
         *
         *                    Date of the registration
         */
        public $registrationDate;

    }

}
