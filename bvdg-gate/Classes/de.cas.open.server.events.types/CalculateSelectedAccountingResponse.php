<?php
// This file has been automatically generated.

namespace de\cas\open\server\events\types {

    /**
     * @package de\cas\open\server\events
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: response object for the read accounting model request
     *        Corresponding \de\cas\open\server\api\types\RequestObject: CalculateSelectedAccountingRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see CalculateSelectedAccountingRequest
     */
    class CalculateSelectedAccountingResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\events\types\AccountingPosition
         *
         *                    returns the accounting model
         */
        public $accounting;

    }

}
