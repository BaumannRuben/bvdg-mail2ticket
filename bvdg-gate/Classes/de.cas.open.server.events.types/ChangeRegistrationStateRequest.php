<?php
// This file has been automatically generated.

namespace de\cas\open\server\events\types {

    /**
     * @package de\cas\open\server\events
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: changes the state of an existing registration
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: ChangeRegistrationStateResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see ChangeRegistrationStateResponse
     */
    class ChangeRegistrationStateRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    GUID of the registration
         */
        public $registrationGUID;

        /**
         * @var string
         *
         *                    New state for the registration
         */
        public $registrationstate;

    }

}
