<?php
// This file has been automatically generated.

namespace de\cas\open\server\events\types {

    /**
     * @package de\cas\open\server\events
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: response object for the add registration request.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: AddRegistrationWithAccountingRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see AddRegistrationWithAccountingRequest
     */
    class AddRegistrationWithAccountingResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var string
         *
         *                    The registration guid as result
         */
        public $result;

    }

}
