<?php
// This file has been automatically generated.

namespace de\cas\open\server\events\types {

    /**
     * @package de\cas\open\server\events
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\RequestObject: reads the accounting model for an event
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: CalculateSelectedAccountingResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see CalculateSelectedAccountingResponse
     */
    class CalculateSelectedAccountingRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    Guid of the participant
         */
        public $participantGuid;

        /**
         * @var string
         *
         *                    GUID of the event
         */
        public $eventGuid;

        /**
         * @var unknown
         *
         *                    Date of the registration
         */
        public $registrationDate;

        /**
         * @var array
         *
         *                    Guids of the accounting positions
         */
        public $accountingPosGuids;

    }

}
