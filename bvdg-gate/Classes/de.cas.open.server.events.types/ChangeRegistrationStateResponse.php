<?php
// This file has been automatically generated.

namespace de\cas\open\server\events\types {

    /**
     * @package de\cas\open\server\events
     * @subpackage types
     *
     *        \de\cas\open\server\api\types\ResponseObject: response object for the change registration state request
     *        Corresponding \de\cas\open\server\api\types\RequestObject: ChangeRegistrationStateRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see ChangeRegistrationStateRequest
     */
    class ChangeRegistrationStateResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var boolean
         *
         *                    returns true if the state is changed and false if not
         */
        public $result;

    }

}
