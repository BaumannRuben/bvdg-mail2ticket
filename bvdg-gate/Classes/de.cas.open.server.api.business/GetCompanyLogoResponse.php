<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\ResponseObject: Gets a company logo. Corresponding \de\cas\open\server\api\types\RequestObject: GetCompanyLogoRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetCompanyLogoRequest
     */
    class GetCompanyLogoResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var string
         *
         *										Sets/Returns the image extension, e.g. "JPG"
         */
        public $imageExtension;

        /**
         * @var unknown
         *Sets/Returns the image bytes
         */
        public $companyLogo;

    }

}
