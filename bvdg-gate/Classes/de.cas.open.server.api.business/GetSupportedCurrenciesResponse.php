<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\ResponseObject of the Operation that returns all supported currencies. Corresponding \de\cas\open\server\api\types\RequestObject: GetSupportedCurrenciesRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetSupportedCurrenciesRequest
     */
    class GetSupportedCurrenciesResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *Sets/Gets the currency list.
         */
        public $currencies;

    }

}
