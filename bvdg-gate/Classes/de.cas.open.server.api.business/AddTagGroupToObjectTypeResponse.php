<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Creates a Relation between a TagGroup and an ObjectType so that the group is
     *				available for it. Corresponding \de\cas\open\server\api\types\RequestObject: AddTagGroupToObjectTypeRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see AddTagGroupToObjectTypeRequest
     */
    class AddTagGroupToObjectTypeResponse extends \de\cas\open\server\api\types\ResponseObject {

    }

}
