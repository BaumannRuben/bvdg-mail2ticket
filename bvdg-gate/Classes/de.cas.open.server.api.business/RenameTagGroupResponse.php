<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Renames a tag group. Corresponding \de\cas\open\server\api\types\RequestObject: RenameTagGroupRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see RenameTagGroupRequest
     */
    class RenameTagGroupResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\InternationalTagGroupTransferable
         *
         *									Sets/Returns the renamed tag group.
         */
        public $renamedTagGroup;

    }

}
