<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\ResponseObject: TODO: Purpose of the business logic. Corresponding
     *				\de\cas\open\server\api\types\RequestObject: GetDefaultPropertyGroupsWithPrefixLikeRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetDefaultPropertyGroupsWithPrefixLikeRequest
     */
    class GetDefaultPropertyGroupsWithPrefixLikeResponse extends \de\cas\open\server\api\business\GetPropertyGroupsWithPrefixLikeResponse {

    }

}
