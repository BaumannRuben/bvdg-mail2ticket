<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\ResponseObject: Retrieves information about the attachments of an
     *				eMail. Corresponding \de\cas\open\server\api\types\RequestObject: GetEMailAttachmentInformationRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetEMailAttachmentInformationRequest
     */
    class GetEMailAttachmentInformationResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *										Filenames of the attachments. Can be null if no
         *										attachments are available.
         */
        public $Filenames;

    }

}
