<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\ResponseObject of the business logic that returns all \de\cas\open\server\api\types\PermissionsOwners that grant a certain
     *				foreigneditpermission for the current user. Corresponding \de\cas\open\server\api\types\RequestObject: GetGrantingPermissionsOwnersRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\PermissionsOwner
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetGrantingPermissionsOwnersRequest
     */
    class GetGrantingPermissionsOwnersResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *										Sets/Returns the objecttype for the granting
         *										permissionsowners (can be
         *										<code>null</code>).
         */
        public $permissionsOwners;

    }

}
