<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\ResponseObject of the
     *				\de\cas\open\server\business\BusinessOperation
     *				that checks whether a licence for a given module and user is available.
     * 				Corresponding \de\cas\open\server\api\types\RequestObject: IsLicenceAvailableRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\business\BusinessOperation
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see IsLicenceAvailableRequest
     */
    class IsLicenceAvailableResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var boolean
         *
         *                    Returns <code>true</code>
         *                    if a licence is available for the given user,
         *                    otherwise <code>false</code>.
         */
        public $available;

    }

}
