<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        Deletes the dependent \de\cas\open\server\api\types\DataObject
     *        that is identified by the given object type and GUID which meets the
     *        specified superior-dependency. Corresponding \de\cas\open\server\api\types\RequestObject: DeleteDependentObjectRequest
     *	@see \de\cas\open\server\api\types\DataObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see DeleteDependentObjectRequest
     */
    class DeleteDependentObjectResponse extends \de\cas\open\server\api\types\ResponseObject {

    }

}
