<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Request Object to retrieve a list of the favourite permissions
     *				owners (user, resource,	group) defined by the current or a certain user.
     *				Only administrators can retrieve the favourite permissions
     *				owners for other users.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject: GetFavouritePermissionsOwnersResponse
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetFavouritePermissionsOwnersResponse
     */
    class GetFavouritePermissionsOwnersRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var int
         *
         *										The user whose favourite permission owners shall be
         *										returned. If <code>null</code> the current
         *										user's list will be returned. Only administrators can
         *										retrieve the favourite permissions owners for other users.
         */
        public $userId;

        /**
         * @var int
         *
         *										Restrict the result to certain types of permissionsOwners (which may be
         *										users, resources or groups).
         *										@see de.cas.eim.api.constants.PermissionsOwnerFilter for available filter constants.
         *										These can be combined (e.g. PermissionsOwnerFilter#USER | PermissionsOwnerFilter#GROUP to
         *										query for users and groups). Also define if deactivated users should be included or not.
         *										<br/>Valid values are:
         *										<ul>
         *											<li>USER: 1</li>
         *											<li>RESOURCE: 2</li>
         *											<li>GROUP: 4</li>
         *											<li>RETURN_DEACTIVATED: 8</li>
         *											<li>RETURN_HIDDEN: 16</li>
         *											<li>ALL: 7</li>
         *										</ul>
         */
        public $permissionsOwnerFilter;

    }

}
