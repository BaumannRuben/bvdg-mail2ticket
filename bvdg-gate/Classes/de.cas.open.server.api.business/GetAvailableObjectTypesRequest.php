<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\RequestObject:<br/> Returns all object types that the current user has access to.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject:
     *				GetAvailableObjectTypesResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetAvailableObjectTypesResponse
     */
    class GetAvailableObjectTypesRequest extends \de\cas\open\server\api\types\RequestObject {

    }

}
