<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\RequestObject of the Operation that sets the new currency for the current client.
     *	@see \de\cas\open\server\api\types\RequestObject
     */
    class SetCurrencyForClientRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var \de\cas\open\server\api\types\Currency
         *Sets/Gets the  new currency for the current client
         */
        public $currency;

    }

}
