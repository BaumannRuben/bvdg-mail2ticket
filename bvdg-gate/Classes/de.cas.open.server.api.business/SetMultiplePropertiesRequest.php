<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject: This request object holds a list of EIMPropertyGroup
     *				objects which will be saved. Corresponding \de\cas\open\server\api\types\ResponseObject: SetMultiplePropertiesResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see SetMultiplePropertiesResponse
     */
    class SetMultiplePropertiesRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var array
         *
         *										A list of EIMPropertyGroups to be saved.
         */
        public $propertyList;

    }

}
