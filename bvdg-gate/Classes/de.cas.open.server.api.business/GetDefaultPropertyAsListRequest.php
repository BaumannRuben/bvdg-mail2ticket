<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Request object for the business operation that fetches a default property as a
     *				List of String-values by key and group.
     */
    class GetDefaultPropertyAsListRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         */
        public $propertyKey;

        /**
         * @var string
         *
         */
        public $propertyGroup;

    }

}
