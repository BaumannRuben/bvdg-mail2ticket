<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject: Accepts an address and returns its false duplicates.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject: GetFalsePositivesResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetFalsePositivesResponse
     */
    class GetFalsePositivesRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var array
         *
         *										Sets/Returns the guid of the addresses that will be checked.
         */
        public $guids;

    }

}
