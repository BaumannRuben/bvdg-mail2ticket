<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject of the business operation that executes other business
     *				operations asynchronously in a separate thread and returns immediately.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject: RunAsynchronouslyResponse. The
     *				status of the operation can be retrieved by executing the business operation
     *				GetAsynchronousOperationStatusRequest. If an operation is finished the
     *				status is destroyed.
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see RunAsynchronouslyResponse
     *	@see GetAsynchronousOperationStatusRequest
     */
    class RunAsynchronouslyRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var \de\cas\open\server\api\types\RequestObject
         *
         *										Sets/Returns the \de\cas\open\server\api\types\RequestObject of the business
         *										operation that shall be executed asynchronously.
         *	@see \de\cas\open\server\api\types\RequestObject
         */
        public $operationToExecute;

    }

}
