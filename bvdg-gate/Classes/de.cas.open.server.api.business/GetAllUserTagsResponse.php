<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Responseobject that wrapps a list of all usertags represented by \de\cas\open\server\api\types\LocalizedTag objects. Corresponding \de\cas\open\server\api\types\RequestObject: \de\cas\eim\api\business\GetAllUserTagsRequest
     *	@see \de\cas\open\server\api\types\LocalizedTag
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\eim\api\business\GetAllUserTagsRequest
     */
    class GetAllUserTagsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *									Returns List of \de\cas\open\server\api\types\Tag objects.
         *	@see \de\cas\open\server\api\types\Tag
         */
        public $tags;

    }

}
