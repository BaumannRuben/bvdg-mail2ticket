<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\RequestObject:<br/>
     *      			Changes the caller PIN of an user. Either the user itself can change his password or an admin can change the password. No \de\cas\open\server\api\types\ResponseObject.
     *				<p>Possible fault codes explicitly thrown by this operation are:
     *					<ul>
     *					<li>SECURITY_BAD_CREDENTIALS - bad old caller PIN or other forbidden requests</li>
     *					</ul>
     *				</p>
     *				Corresponding \de\cas\open\server\api\types\ResponseObject: ChangeCallerPinResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see ChangeCallerPinResponse
     */
    class ChangeCallerPinRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var int
         *
         *										Gets/Sets the userId of the user whose caller PIN
         *										should be changed.
         */
        public $userId;

        /**
         * @var int
         *
         *										Gets/Sets the old caller PIN of the user. Is not
         *										required for a caller who is logged in as admin.
         */
        public $oldCallerPin;

        /**
         * @var int
         *
         *										Gets/Sets the new caller PIN of the user. A value is
         *										required.
         */
        public $newCallerPin;

    }

}
