<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\RequestObject:<br/> Returns the length of the CalledId for the VoiceToCRM functionality.
     *				<p>Possible fault codes explicitly thrown by this operation are:
     *					<ul>
     *					<li>NO_EXCEPTION_CODE - no explanation</li>
     *					</ul>
     *				</p>
     *				Corresponding \de\cas\open\server\api\types\ResponseObject: GetCallerIdLengthResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetCallerIdLengthResponse
     */
    class GetCallerIdLengthRequest extends \de\cas\open\server\api\types\RequestObject {

    }

}
