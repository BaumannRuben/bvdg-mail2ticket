<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        \de\cas\open\server\api\types\RequestObject to retrieve all linked objects to a
     *        given GUID. If
     *        <code>attribute</code> is empty all regular links (without
     *				attribute) will be returned. If <code>attribute</code> is set all
     *				Links with this attribute will be returned.<br /> The result will be a
     *				list with <code>LinkObjects</code>. Corresponding \de\cas\open\server\api\types\ResponseObject: GetAllLinksByGGUIDResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetAllLinksByGGUIDResponse
     */
    class GetAllLinksByGGUIDRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										Gets/Sets the GGUID of the \de\cas\open\server\api\types\DataObject  whose linked object
         *										shall be returned.
         *	@see \de\cas\open\server\api\types\DataObject
         */
        public $GGUID;

        /**
         * @var string
         *
         *										Sets/Returns the type of the \de\cas\open\server\api\types\DataObject  whose linked objects
         *										shall be returned.
         *	@see \de\cas\open\server\api\types\DataObject
         */
        public $objectType;

    }

}
