<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\ResponseObject:<br/> Returns the length of the CalledId for the VoiceToCRM functionality.
     *				Corresponding \de\cas\open\server\api\types\RequestObject: GetCallerIdLengthRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetCallerIdLengthRequest
     */
    class GetCallerIdLengthResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var int
         *
         *										The maximum length of the caller id.
         */
        public $length;

    }

}
