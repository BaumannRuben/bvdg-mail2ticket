<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\RequestObject:<br/> Returns the requested (GGUID) action and corresponding messages
     *				NotificationServiceMessage objects in a transaction. Corresponding \de\cas\open\server\api\types\ResponseObject:
     *				GetActionAndMessagesResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see NotificationServiceMessage
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetActionAndMessagesResponse
     */
    class GetActionAndMessagesRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										Gets/Sets the GGUID of the action to be retrieved.
         */
        public $actionGGUID;

    }

}
