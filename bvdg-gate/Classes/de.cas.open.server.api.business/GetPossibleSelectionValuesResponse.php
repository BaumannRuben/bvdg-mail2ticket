<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\ResponseObject of the business operation
     *				that returns all possible \de\cas\open\server\datadefinition\types\SelectionValues
     *				of a \de\cas\open\server\api\types\SelectionValueListField including the
     *				displaynames in all available languages.
     *				Corresponding \de\cas\open\server\api\types\RequestObject: GetPossibleSelectionValuesRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\datadefinition\types\SelectionValue
     *	@see \de\cas\open\server\api\types\SelectionValueListField
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetPossibleSelectionValuesRequest
     */
    class GetPossibleSelectionValuesResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *										Returns all possible \de\cas\open\server\datadefinition\types\SelectionValues
         *										of a \de\cas\open\server\api\types\SelectionValueListField
         *										including the
         *										displaynames in all available languages.
         *	@see \de\cas\open\server\datadefinition\types\SelectionValue
         *	@see \de\cas\open\server\api\types\SelectionValueListField
         */
        public $selectionValues;

    }

}
