<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Response object for the business operation that removes all properties that
     *				belong to the given propertyGroup.
     */
    class RemovePropertiesByGroupResponse extends \de\cas\open\server\api\types\ResponseObject {

    }

}
