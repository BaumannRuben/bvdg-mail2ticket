<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject: Retrieves the image bytes for a given object.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject: GetImageForObjectResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetImageForObjectResponse
     */
    class GetImageForObjectRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										Sets/Returns the GGUID of the object the image shall
         *										be saved for.
         */
        public $ObjectGUID;

        /**
         * @var string
         *
         *										Sets/Returns the type of the object the image shall
         *										be saved for.
         */
        public $ObjectType;

    }

}
