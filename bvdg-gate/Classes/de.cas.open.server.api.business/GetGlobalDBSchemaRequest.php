<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject of the business operation that returns the global schema
     *				(for information and debugging). Corresponding \de\cas\open\server\api\types\ResponseObject: GetGlobalDBSchemaResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetGlobalDBSchemaResponse
     */
    class GetGlobalDBSchemaRequest extends \de\cas\open\server\api\types\RequestObject {

    }

}
