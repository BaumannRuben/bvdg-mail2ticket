<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Checks if the user has the necessary rights to link two dataobjects.
     *				Corresponding \de\cas\open\server\api\types\RequestObject: CheckIfLinkPermissionResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see CheckIfLinkPermissionResponse
     */
    class CheckIfLinkPermissionRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										Sets/Returns the guid of the first object in the link
         */
        public $guid1;

        /**
         * @var string
         *
         *										Sets/Returns the object type of the first object in the
         *										link
         */
        public $objectType1;

        /**
         * @var string
         *
         *										Sets/Returns the guid of the second object in the link
         */
        public $guid2;

        /**
         * @var string
         *
         *										Sets/Returns the object type of the second object in
         *										the link
         */
        public $objectType2;

    }

}
