<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\ResponseObject
     *				of the business operation that returns the ColumnAccessPermissions transitively, i.e. according to the
     *				passed <code>serviceID</code>, the permissionOwner
     *				and all his/her super groups.
     *				Corresponding \de\cas\open\server\api\types\RequestObject: GetColumnAccessPermissionsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see ColumnAccessPermission
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetColumnAccessPermissionsRequest
     */
    class GetColumnAccessPermissionsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *										Returns the permissions on the columns.
         */
        public $permissions;

    }

}
