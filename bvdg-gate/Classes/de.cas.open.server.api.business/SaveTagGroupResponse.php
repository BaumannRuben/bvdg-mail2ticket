<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Saves a new TagGroup. Corresponding \de\cas\open\server\api\types\ResponseObject: SaveTagGroupRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see SaveTagGroupRequest
     */
    class SaveTagGroupResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\InternationalTagGroupTransferable
         *
         *									Sets/Returns the tag group created.
         */
        public $TagGroup;

    }

}
