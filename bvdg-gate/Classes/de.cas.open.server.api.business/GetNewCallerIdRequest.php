<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\RequestObject:<br/>
     *			 Returns a new caller ID and assigns it to a user.
     *	 		If the argument proposedId is not used, this ID is returned.
     *	 		In the other cases a completely new caller ID is returned.<br/>
     *	 		If the user already has already a caller ID, the old one is returned.
     *	 		Administrator permissions required.
     *				<p>Possible fault codes explicitly thrown by this operation are:
     *					<ul>
     *					  <li>BusinessException  - BUSINESS_NO_FREE_CALLER_ID_FOUND  if no caller ID is left</li>
     *					  <li>BusinessException  - BUSINESS_PROPOSED_CALLER_ID_INVALID Proposed caller ID contained illegal chars or is too long or short.</li>
     *					  <li>DataLayerException - SECURITY_USER_DOES_NOT_EXIST if user with id 'userOid' does not exist</li>
     *					</ul>
     *				</p>
     *				Corresponding \de\cas\open\server\api\types\ResponseObject: GetNewCallerIdResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetNewCallerIdResponse
     */
    class GetNewCallerIdRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var int
         *
         *										assign a caller ID to this user
         */
        public $userOid;

        /**
         * @var string
         *
         *										A caller ID the user wishes, i.e. his business phone
         *										extension. Leave it empty to auto-assign a new caller
         *										ID.
         */
        public $proposedId;

    }

}
