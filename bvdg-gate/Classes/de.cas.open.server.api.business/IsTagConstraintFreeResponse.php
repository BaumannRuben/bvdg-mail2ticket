<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\ResponseObject: Checks if a tag is free of constraints. Corresponding
     *				\de\cas\open\server\api\types\RequestObject: IsTagConstraintFreeRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see IsTagConstraintFreeRequest
     */
    class IsTagConstraintFreeResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var boolean
         *
         *										Flag indicating if the tag is free (true) or not
         *										(false).
         */
        public $result;

    }

}
