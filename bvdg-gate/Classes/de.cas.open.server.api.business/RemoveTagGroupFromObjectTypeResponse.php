<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Removes the relation between a tag group and an object type. Corresponding
     *				\de\cas\open\server\api\types\RequestObject: RemoveTagGroupFromObjectTypeRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see RemoveTagGroupFromObjectTypeRequest
     */
    class RemoveTagGroupFromObjectTypeResponse extends \de\cas\open\server\api\types\ResponseObject {

    }

}
