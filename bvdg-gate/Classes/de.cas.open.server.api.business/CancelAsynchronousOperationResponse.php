<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\ResponseObject of the business operation that cancels an asynchronous
     *				business operations started by RunAsynchronouslyRequest. Corresponding
     *				\de\cas\open\server\api\types\RequestObject: CancelAsynchronousOperationRequest.
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see RunAsynchronouslyRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see CancelAsynchronousOperationRequest
     */
    class CancelAsynchronousOperationResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var boolean
         *
         *										Sets/Returns the flag that indicates if the operation
         *										could be cancelled. The flag is
         *										<code>false</code> if the operation could
         *										not be cancelled, typically because it has already
         *										completed normally; <code>true</code>.
         *										otherwise
         */
        public $cancelled;

    }

}
