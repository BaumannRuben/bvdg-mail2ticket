<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject: Sets a company logo. Corresponding \de\cas\open\server\api\types\ResponseObject: SetCompanyLogoResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see SetCompanyLogoResponse
     */
    class SetCompanyLogoRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var unknown
         *Sets/Returns the logo bytes
         */
        public $companyLogo;

        /**
         * @var string
         *
         *										Sets/Returns the image extension, e.g. "JPG"
         */
        public $imageExtension;

    }

}
