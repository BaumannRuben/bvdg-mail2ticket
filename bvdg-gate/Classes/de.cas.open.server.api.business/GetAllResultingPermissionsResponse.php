<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Fetches all permissions (direct- and foreign-edit-permissions) that result from
     *				participants on a \de\cas\open\server\api\types\DataObject. Corresponding \de\cas\open\server\api\types\RequestObject: GetAllResultingPermissionsRequest
     *	@see \de\cas\open\server\api\types\DataObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetAllResultingPermissionsRequest
     */
    class GetAllResultingPermissionsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         */
        public $permissionInformations;

    }

}
