<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject: Assignes a default tag to a tag group. If a default tag
     *				already exists, this value will be replaced with the new one. Corresponding
     *				\de\cas\open\server\api\types\ResponseObject: AssignDefaultTagToGroupResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see AssignDefaultTagToGroupResponse
     */
    class AssignDefaultTagToGroupRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										GGUID of the tag to be assigned as default tag.
         */
        public $tagGUID;

        /**
         * @var string
         *
         *										GGUID of the tag group the tag shall be assigned.
         */
        public $tagGroupGUID;

    }

}
