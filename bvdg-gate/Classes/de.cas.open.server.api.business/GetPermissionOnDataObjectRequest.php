<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        Returns the maximum permission that is granted to the authenticated user on a given dataobject.
     *        The returned permission is the maximum permission gained through:
     *        - direct permissions on the dataobject
     *        - permissions gained through group memberships
     *        - permissions granted by other users that have permissions on the dataobject (foreign edit permissions)
     *        Possible fault codes explicitly thrown by this operation are: None
     *        Exceptions that may occur when getting an object may be thrown when executing this operation, too.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject:
     *        GetPermissionOnDataObjectResponse
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetPermissionOnDataObjectResponse
     */
    class GetPermissionOnDataObjectRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var int
         *
         *                    The ID of a permission owner (user/resource/group) for which the permission should be determined.
         *                    This property is optional. If not set, the ID of the authenticated user will be used instead.
         */
        public $permissionOwnerId;

        /**
         * @var string
         *The GGUID of the dataobject to check the permissions for.
         */
        public $dataObjectGuid;

        /**
         * @var string
         *The type of the dataobject to check the permissions for.
         */
        public $objectType;

    }

}
