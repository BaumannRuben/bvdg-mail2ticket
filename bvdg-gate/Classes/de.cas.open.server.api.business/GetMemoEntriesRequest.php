<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Returns the values (complete, full length) of both fields of a journal entry.
     *				They are returned as typed values (String, Date, ...). Corresponding \de\cas\open\server\api\types\ResponseObject: GetMemoEntriesResponse
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetMemoEntriesResponse
     */
    class GetMemoEntriesRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var int
         *
         *										Id of the journal entry which has a 'MemoLogBook'
         *										entry.
         */
        public $journalEntryId;

    }

}
