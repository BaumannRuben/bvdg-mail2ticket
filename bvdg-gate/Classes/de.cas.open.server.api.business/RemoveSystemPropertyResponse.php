<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Response object for the business operation that removes a system-property by
     *				key and group.
     */
    class RemoveSystemPropertyResponse extends \de\cas\open\server\api\types\ResponseObject {

    }

}
