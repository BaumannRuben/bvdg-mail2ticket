<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\RequestObject:<br/>
     *			Returns the session info for the supplied hash code.
     *			<br/>Corresponding \de\cas\open\server\api\types\ResponseObject: SFGetSessionInfoByHashCodeResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see SFGetSessionInfoByHashCodeResponse
     */
    class SFGetSessionInfoByHashCodeRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										The hash code of the session.
         */
        public $sessionHash;

    }

}
