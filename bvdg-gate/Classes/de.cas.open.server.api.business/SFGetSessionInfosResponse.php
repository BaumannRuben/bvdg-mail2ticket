<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\ResponseObject:<br/>
     *			Returns all client sessions of an user, if they have been saved by SaveSessionInfoRequest. They are ordered by the insertTimestamp.
     *			<br/>Corresponding \de\cas\open\server\api\types\RequestObject: SFGetSessionInfosRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see SFGetSessionInfosRequest
     */
    class SFGetSessionInfosResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *										The list of currently active sessions of an user.
         */
        public $sessions;

    }

}
