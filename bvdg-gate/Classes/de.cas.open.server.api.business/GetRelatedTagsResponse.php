<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Responseobject for the businessoperation that returns all related tags to a
     *				given objecttype and list of GGUIDs of this type. Corresponding \de\cas\open\server\api\types\RequestObject: \de\cas\eim\api\business\GetRelatedTagsRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\eim\api\business\GetRelatedTagsRequest
     */
    class GetRelatedTagsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *										Returns a list of \de\cas\open\server\api\types\LocalizedTagGroups containing the
         *										related tags.
         *	@see \de\cas\open\server\api\types\LocalizedTagGroup
         */
        public $localizedTagGroups;

    }

}
