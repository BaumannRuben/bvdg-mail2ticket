<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\ResponseObject: Used to retrieve subscriptions of an user/group.
     *				Corresponding \de\cas\open\server\api\types\RequestObject: GetSubscribedActionsOfPermissionOwnerRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetSubscribedActionsOfPermissionOwnerRequest
     */
    class GetSubscribedActionsOfPermissionOwnerResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *										List of actions the user is subscribed to.
         */
        public $subscribedActions;

        /**
         * @var array
         *
         *										List of actions the user is unsubscribed to.
         */
        public $unsubscribedActions;

    }

}
