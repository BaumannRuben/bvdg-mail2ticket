<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        Creates a \de\cas\open\server\api\types\DataObject
     *        of the given object type that depends from a superior \de\cas\open\server\api\types\DataObject which is identified
     *        by the given superior object type and superior object guid.
     *        The \de\cas\open\server\api\types\DataObject is non-persisted
     *        until it is saved by means of the corresponding request (\de\cas\open\server\api\business\SaveDependentObjectRequest.
     *        Corresponding \de\cas\open\server\api\types\RequestObject:
     *        CreateDependentObjectRequest
     *	@see \de\cas\open\server\api\types\DataObject
     *	@see \de\cas\open\server\api\business\SaveDependentObjectRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see CreateDependentObjectRequest
     */
    class CreateDependentObjectResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\DataObjectTransferable
         *
         */
        public $dependentObject;

    }

}
