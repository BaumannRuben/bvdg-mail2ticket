<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Deletes a Tag. Corresponding \de\cas\open\server\api\types\ResponseObject: DeleteTagResponse
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see DeleteTagResponse
     */
    class DeleteTagRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *									Sets/Returns the GGUID of the tag to be deleted.
         */
        public $tagGUID;

    }

}
