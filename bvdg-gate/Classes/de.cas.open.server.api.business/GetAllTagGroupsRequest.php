<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject: Returns a list of tag groups, each with the available
     *				tags included for that group. Corresponding \de\cas\open\server\api\types\ResponseObject: GetAllTagGroupsResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetAllTagGroupsResponse
     */
    class GetAllTagGroupsRequest extends \de\cas\open\server\api\types\RequestObject {

    }

}
