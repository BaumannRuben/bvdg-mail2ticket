<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Request object for merging fields of an organization address to a contact
     *				address. Corresponding \de\cas\open\server\api\types\ResponseObject: MergeAddressObjectsResponse
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see MergeAddressObjectsResponse
     */
    class MergeAddressObjectsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var \de\cas\open\server\api\types\DataObjectTransferable
         *
         *										Sets/Returns the DataObject to be used.
         */
        public $contact;

        /**
         * @var string
         *
         *								Sets/Returns the guid of the organization to be used
         */
        public $organizationGuid;

        /**
         * @var boolean
         *
         *								Sets/Returns the flag, if alle relevant fields shall be copied
         *								(true) or just the organization name (false).
         */
        public $copyAllFields;

    }

}
