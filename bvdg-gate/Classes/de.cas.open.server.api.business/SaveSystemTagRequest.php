<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Saves a system tag. Corresponding \de\cas\open\server\api\types\ResponseObject: SaveSystemTagResponse
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see SaveSystemTagResponse
     */
    class SaveSystemTagRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var \de\cas\open\server\api\types\InternationalTagTransferable
         *
         *									Sets/Returns the system tag to be created.
         */
        public $SystemTag;

    }

}
