<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Response Object which contains a list of
     *				permissions owners.
     *        Corresponding
     *				\de\cas\open\server\api\types\RequestObject: GetAllForeignEditPermissionsOfUserRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetAllForeignEditPermissionsOfUserRequest
     */
    class GetAllPermissionsOwnersResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *										List of information about permissions owners
         */
        public $permissionsOwners;

    }

}
