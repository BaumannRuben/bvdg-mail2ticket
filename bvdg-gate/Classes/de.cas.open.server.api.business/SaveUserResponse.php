<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Response object for the business operation that
     *				saves a user. A user object can
     *				be modified by the corresponding user
     *				or the administrator of the client.
     */
    class SaveUserResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\UserWithUserName
         *Sets/Gets the saved user.
         */
        public $savedUser;

    }

}
