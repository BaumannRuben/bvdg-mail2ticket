<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\RequestObject of the business operation that returns the client's phone prefix or if none exists assigns a new (if assignNewPhonePrefix is true).
     *				If assignNewPhonePrefix is false and the client does not have a phone prefix, null is returned.
     *				<p>Possible fault codes explicitly thrown by this operation are:
     *					<ul>
     *						<li>BUSINESS_NO_FREE_PHONE_PREFIX_FOUND - the pool of automatically assignable phone prefixes is exhausted (contact your administrator)</li>
     *					</ul>
     *				</p>
     *				Corresponding \de\cas\open\server\api\types\ResponseObject: GetPhonePrefixForClientResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetPhonePrefixForClientResponse
     */
    class GetPhonePrefixForClientRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var boolean
         *
         *										If this property is true, a new phone prefix is
         *										assigned for the client if needed. If this property is
         *										false and the client does not have a phone prefix, null
         *										is returned.
         */
        public $assignNewPhonePrefix;

    }

}
