<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\RequestObject:<br/> Sets the smtp mailserver settings.
     *				No ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     */
    class SetMailserverSettingsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *Sets/Gets data object type name.
         */
        public $dataObjectName;

        /**
         * @var string
         *The SMTP host url
         */
        public $smtpHost;

        /**
         * @var string
         *The POP3 host url
         */
        public $pop3Host;

        /**
         * @var boolean
         *
         *										If a POP Login before SMTP shall be triggered
         */
        public $usePOPBeforeSMTP;

        /**
         * @var string
         *The SMTP user
         */
        public $smtpUser;

        /**
         * @var string
         *The SMTP password
         */
        public $smtpPassword;

        /**
         * @var int
         *The SMTP port
         */
        public $smtpPort;

        /**
         * @var int
         *The POP3 port
         */
        public $pop3Port;

        /**
         * @var string
         *The sender's email address
         */
        public $SenderAddress;

        /**
         * @var string
         *The sender's name
         */
        public $SenderName;

        /**
         * @var string
         *The sender's replyto-address
         */
        public $ReplyAddress;

        /**
         * @var boolean
         *
         *										If SSL is to be used for SMTP connection.
         */
        public $useSSL;

        /**
         * @var boolean
         *
         *										If SSL is to be used for POP3 connection.
         */
        public $usePOP3SSL;

        /**
         * @var boolean
         *
         *										If authentication is to be used.
         */
        public $useAuthentication;

        /**
         * @var boolean
         *
         *										If TLS is to be used for a SSL connection.
         */
        public $useTLS;

        /**
         * @var int
         *
         *										OID of the user the settings shall be set for. Can be null.
         *										If a value is set, the user that issues this operation must
         *										be an administrator.
         */
        public $userOID;

    }

}
