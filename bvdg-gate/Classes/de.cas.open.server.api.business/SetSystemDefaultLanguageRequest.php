<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject: Requests to change the default language for the current
     *				client. Corresponding \de\cas\open\server\api\types\ResponseObject: SetSystemDefaultLanguageResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see SetSystemDefaultLanguageResponse
     */
    class SetSystemDefaultLanguageRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										The default language to be set for the system.
         */
        public $defaultLanguage;

    }

}
