<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\ResponseObject: Returns a list of tag groups, each with the available
     *				tags included for that group. Corresponding \de\cas\open\server\api\types\RequestObject: GetAllTagGroupsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetAllTagGroupsRequest
     */
    class GetAllTagGroupsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *The requested tag groups.
         */
        public $tagGroups;

    }

}
