<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject: Gets the countries that are supported by the server.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject: GetSupportedCountriesResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetSupportedCountriesResponse
     */
    class GetSupportedCountriesRequest extends \de\cas\open\server\api\types\RequestObject {

    }

}
