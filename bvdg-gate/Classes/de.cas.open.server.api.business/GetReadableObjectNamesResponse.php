<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        Returns all object types that the current user has at least read access to.
     *        \de\cas\open\server\api\types\RequestObject: GetReadableObjectNamesRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetReadableObjectNamesRequest
     */
    class GetReadableObjectNamesResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *                    Gets the table and display names of the readable object types.
         */
        public $readableObjectTypes;

    }

}
