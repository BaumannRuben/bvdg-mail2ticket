<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject of the business operation
     *				that retrieves all taggroups and corresponding tags for an object type or
     *				all object types, that are used at least once. The result contains all
     * 				used taggroups, all used tags and the number of DataObjects that are
     * 				tagged with this tag. In order to just retrieve all available taggroups
     * 				use the \de\cas\open\server\api\types\DataObjectDescription.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject: GetGroupsAndTagsForObjectTypeResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\DataObjectDescription
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetGroupsAndTagsForObjectTypeResponse
     */
    class GetGroupsAndTagsForObjectTypeRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *									The object type the available groups and tags shall be
         *									retrieved for. If set to null, all available tag groups and
         *									tags contained therein for all object types will be
         *									returned.
         */
        public $objectType;

    }

}
