<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        Returns information about the logins of the current user to the server.
     *        \de\cas\open\server\api\types\ResponseObject: {@link
     *        GetCurrentUsersLoginInformationResponse }
     *	@see \de\cas\open\server\api\types\ResponseObject
     */
    class GetCurrentUsersLoginInformationRequest extends \de\cas\open\server\api\types\RequestObject {

    }

}
