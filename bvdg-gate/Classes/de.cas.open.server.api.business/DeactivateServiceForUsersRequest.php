<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\RequestObject:<br/> deactivate external service for users.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject:
     *				DeactivateServiceForUsersResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see DeactivateServiceForUsersResponse
     */
    class DeactivateServiceForUsersRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										Sets the service id of the action to be retrieved.
         */
        public $serviceID;

        /**
         * @var array
         *
         *											Sets the users id for external service exception.
         */
        public $serviceUsersID;

    }

}
