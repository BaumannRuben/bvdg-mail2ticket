<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\RequestObject:<br/> Masks the fields in objects to which the user
     *				has no access, except for the fields listed in the system properties as search.visible.objecttype.fields.on.noaccess.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject:
     *				MaskFieldsInDataObjectsWithNoAccessResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see MaskFieldsInDataObjectsWithNoAccessResponse
     */
    class MaskFieldsInDataObjectsWithNoAccessRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var \de\cas\open\server\api\types\MassQueryResult
         *
         *										Gets/Sets the massQueryResult to be masked.
         */
        public $massQueryResultToMask;

        /**
         * @var string
         *
         *										The object type that the massQueryResult holds.
         */
        public $objectType;

    }

}
