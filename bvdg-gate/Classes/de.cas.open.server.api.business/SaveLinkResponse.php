<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        @link de.cas.open.server.api.types.ResponseObject} for saving a link between two records. Corresponding \de\cas\open\server\api\types\RequestObject: SaveLinkRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see SaveLinkRequest
     */
    class SaveLinkResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\business\SavedAndFailedLinksContainer
         *
         *										Returns the added and failed links.
         */
        public $savedAndFailedLinks;

    }

}
