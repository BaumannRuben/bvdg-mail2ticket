<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Response object for merging two addresses. Corresponding \de\cas\open\server\api\types\RequestObject:
     *				MergeAddressObjectsRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see MergeAddressObjectsRequest
     */
    class MergeAddressObjectsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\DataObjectTransferable
         *
         *										Sets/Returns the contact data object to be returned.
         */
        public $contact;

    }

}
