<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\ResponseObject: Assignes a default tag to a tag group. If a default tag
     *				already exists, this value will be replaced with the new one. Corresponding
     *				\de\cas\open\server\api\types\RequestObject: AssignDefaultTagToGroupRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see AssignDefaultTagToGroupRequest
     */
    class AssignDefaultTagToGroupResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\InternationalTagGroupTransferable
         *
         *										The tag group with changed default tag value.
         */
        public $tagGroup;

    }

}
