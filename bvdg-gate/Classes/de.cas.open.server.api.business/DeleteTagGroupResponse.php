<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Deletes a tag group. Corresponding \de\cas\open\server\api\types\RequestObject: DeleteTagGroupRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see DeleteTagGroupRequest
     */
    class DeleteTagGroupResponse extends \de\cas\open\server\api\types\ResponseObject {

    }

}
