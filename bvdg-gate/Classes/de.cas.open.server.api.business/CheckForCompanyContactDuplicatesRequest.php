<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject for the business operation that checks for company
     *				duplicates of the given address. \de\cas\open\server\api\types\ResponseObject: CheckForCompanyContactDuplicatesResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see CheckForCompanyContactDuplicatesResponse
     */
    class CheckForCompanyContactDuplicatesRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var array
         *
         *										Sets/Returns the guids of the organisations that will be checked.
         */
        public $organisationsGuids;

    }

}
