<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\RequestObject:<br/> Tests the given mail server settings for validity.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject: TestMailserverSettingsResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see TestMailserverSettingsResponse
     */
    class TestMailserverSettingsRequest extends \de\cas\open\server\api\business\SetMailserverSettingsRequest {

    }

}
