<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\ResponseObject:<br/> Masks the fields in objects to which the user
     *				has no access, except for the fields listed in the system properties as search.visible.objecttype.fields.on.noaccess.
     *				Corresponding \de\cas\open\server\api\types\RequestObject: MaskFieldsInDataObjectsWithNoAccessRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see MaskFieldsInDataObjectsWithNoAccessRequest
     */
    class MaskFieldsInDataObjectsWithNoAccessResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\MassQueryResult
         *
         *										The MassQueryResult with masked fields.
         */
        public $maskedMassQueryResult;

    }

}
