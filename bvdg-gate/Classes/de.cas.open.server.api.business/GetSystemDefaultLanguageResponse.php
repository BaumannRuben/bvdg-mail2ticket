<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\ResponseObject: Contains the default language set for the current
     *				client. Corresponding \de\cas\open\server\api\types\RequestObject: GetSystemDefaultLanguageRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetSystemDefaultLanguageRequest
     */
    class GetSystemDefaultLanguageResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var string
         *
         *										The default language set for the current client.
         */
        public $defaultLanguage;

    }

}
