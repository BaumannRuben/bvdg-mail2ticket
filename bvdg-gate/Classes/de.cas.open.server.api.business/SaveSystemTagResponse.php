<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Returns the created system tag. Corresponding \de\cas\open\server\api\types\RequestObject: SaveSystemTagRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see SaveSystemTagRequest
     */
    class SaveSystemTagResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\InternationalTagTransferable
         *
         *									Sets/Returns the created system tag.
         */
        public $SystemTag;

    }

}
