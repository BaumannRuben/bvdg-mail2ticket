<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Checks if the user has the necessary rights to link two dataobjects.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject: CheckIfLinkPermissionRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see CheckIfLinkPermissionRequest
     */
    class CheckIfLinkPermissionResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var boolean
         *
         *										Flag if the linking of the two objects is possible for
         *										the user.
         */
        public $possible;

    }

}
