<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject: Returns a list of tags (user tags and user editable
     *				system tags) that are free of constraints. Corresponding \de\cas\open\server\api\types\ResponseObject: GetConstraintFreeTagsForUserResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetConstraintFreeTagsForUserResponse
     */
    class GetConstraintFreeTagsForUserRequest extends \de\cas\open\server\api\types\RequestObject {

    }

}
