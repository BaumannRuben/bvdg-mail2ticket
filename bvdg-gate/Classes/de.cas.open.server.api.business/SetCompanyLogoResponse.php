<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\ResponseObject: Sets a company logo. Corresponding \de\cas\open\server\api\types\RequestObject: SetCompanyLogoRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see SetCompanyLogoRequest
     */
    class SetCompanyLogoResponse extends \de\cas\open\server\api\types\ResponseObject {

    }

}
