<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\ResponseObject: Removes tags from multiple data objects. Corresponding
     *				\de\cas\open\server\api\types\RequestObject: RemoveTagsFromMultipleObjectsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see RemoveTagsFromMultipleObjectsRequest
     */
    class RemoveTagsFromMultipleObjectsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *										A list of data object GGUIDs where the removal of the
         *										tags was not successful, because the object could not
         *										be read/does not exist or the user has insufficient
         *										permissions to write to that object.
         */
        public $failureGUIDs;

    }

}
