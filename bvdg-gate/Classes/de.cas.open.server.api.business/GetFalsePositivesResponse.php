<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\ResponseObject: Returns the false duplicates. Corresponding \de\cas\open\server\api\types\RequestObject: GetFalsePositivesRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetFalsePositivesRequest
     */
    class GetFalsePositivesResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *										Returns the list of GGUIDs of false duplicates with the given guid in the first position in the list.
         */
        public $gguidsFalseDuplicates;

    }

}
