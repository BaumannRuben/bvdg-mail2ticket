<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject of the business operation that returns the status and/or
     *				result of an asynchronous business operations started by RunAsynchronouslyRequest. Corresponding \de\cas\open\server\api\types\ResponseObject: GetAsynchronousOperationStatusResponse. If an operation is finished the result
     *				can be retrieved just once, afterwards the status is destroyed.
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see RunAsynchronouslyRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetAsynchronousOperationStatusResponse
     */
    class GetAsynchronousOperationStatusRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										Sets/Returns the GGUID that identifies the asynchronous
         *										operation (as returned by RunAsynchronouslyResponse).
         *	@see RunAsynchronouslyResponse
         */
        public $asynchronousOperationGUID;

    }

}
