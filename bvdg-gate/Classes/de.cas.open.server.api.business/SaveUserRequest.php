<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Request object for the business operation that
     *				saves a user. A user object can
     *				be modified by the corresponding user
     *				or the administrator of the client.
     *				Throws a
     *				BUSINESS.LINKED_ADDRESS_NOT_READABLE if the linked address record is
     *				not readable.
     *				If a related address doesn't exist (corrupt database),
     *				than an address is automatically
     *				created and assigned to the user.
     */
    class SaveUserRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var \de\cas\open\server\api\types\UserWithUserName
         *
         *										Sets/Gets the user to be saved.
         */
        public $userToSave;

        /**
         * @var \de\cas\open\server\api\types\DataObjectTransferable
         *
         *										The address belongs to the user to save. Can be
         *										unsaved.
         */
        public $relatedAddress;

    }

}
