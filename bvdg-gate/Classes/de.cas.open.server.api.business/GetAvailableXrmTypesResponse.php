<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        Gets the available XRM types for the given data object type.
     *        Corresponding \de\cas\open\server\api\types\RequestObject:
     *        GetAvailableXrmTypesRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetAvailableXrmTypesRequest
     */
    class GetAvailableXrmTypesResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         */
        public $xrmTypes;

    }

}
