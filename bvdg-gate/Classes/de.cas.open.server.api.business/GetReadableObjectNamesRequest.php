<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        Returns all object types that the current user has at least read access to.
     *        \de\cas\open\server\api\types\ResponseObject: GetReadableObjectNamesResponse
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetReadableObjectNamesResponse
     */
    class GetReadableObjectNamesRequest extends \de\cas\open\server\api\types\RequestObject {

    }

}
