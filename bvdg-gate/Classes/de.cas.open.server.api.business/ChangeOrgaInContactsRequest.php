<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Copies the address fields of a primary organization to multiple contacts
     *				identified by their guid. Corresponding \de\cas\open\server\api\types\RequestObject: ChangeOrgaInContactsResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see ChangeOrgaInContactsResponse
     */
    class ChangeOrgaInContactsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var \de\cas\open\server\api\types\DataObjectTransferable
         *
         *										Sets/Returns the list of guids of the contacts to be
         *										changed
         */
        public $organization;

        /**
         * @var array
         *
         *										Sets/Returns the list of guids of the contacts to be
         *										changed
         */
        public $guidList;

        /**
         * @var boolean
         *
         *										Sets/Returns the boolean that indicates if the business
         *										address shall be copied.
         */
        public $copyBusinessAddress;

        /**
         * @var boolean
         *
         *										Sets/Returns the boolean that indicates if the visitor
         *										address shall be copied.
         */
        public $copyVisitorAddress;

    }

}
