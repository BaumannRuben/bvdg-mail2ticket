<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Deletes a Tag. Corresponding \de\cas\open\server\api\types\RequestObject: DeleteTagRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see DeleteTagRequest
     */
    class DeleteTagResponse extends \de\cas\open\server\api\types\ResponseObject {

    }

}
