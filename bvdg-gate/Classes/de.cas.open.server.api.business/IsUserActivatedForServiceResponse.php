<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\ResponseObject:<br/>
     *				Returns true if an user is registered for an external service, else false.
     *				Throws an IllegalArgumentException if serviceId or userId is emptry.
     *				Corresponding \de\cas\open\server\api\types\RequestObject: IsUserActivatedForServiceRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see IsUserActivatedForServiceRequest
     */
    class IsUserActivatedForServiceResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var boolean
         *
         *										True if user is registered for external service.
         */
        public $userActivated;

    }

}
