<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Request object to receive a detailed user object. The arguments are evaluated
     *				in the following order: id, userName, gguid.
     *				If all arguments are null, the currently authenticated user object is returned.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: GetUserResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetUserResponse
     */
    class GetUserRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var int
         *
         *										The id of the requested user. This property is
         *										preferred over the GGUID.
         */
        public $id;

        /**
         * @var string
         *
         *										The GGUID of the requested user. Please use id if
         *										possible, because 'get user by guid' is not cached.
         */
        public $gguid;

        /**
         * @var string
         *
         *										The username of the requested user.
         */
        public $username;

    }

}
