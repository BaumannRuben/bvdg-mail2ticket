<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\RequestObject:<br/> Returns the given address as a vcard.
     *				<p>Possible fault codes explicitly thrown by this operation are:
     *					<ul>
     *					<li>NO_EXCEPTION_CODE - no explanation</li>
     *					</ul>
     *				</p>
     *				Corresponding \de\cas\open\server\api\types\ResponseObject: ExportVCardAddressResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see ExportVCardAddressResponse
     */
    class ExportVCardAddressRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var \de\cas\open\server\api\types\DataObjectTransferable
         *
         *										The address to export to vcard.
         */
        public $address;

    }

}
