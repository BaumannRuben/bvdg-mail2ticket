<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        Loads the dependent \de\cas\open\server\api\types\DataObject
     *        for the given data object type and GUID that meets the specified
     *        superior-dependency. Corresponding \de\cas\open\server\api\types\ResponseObject: LoadDependentObjectResponse
     *	@see \de\cas\open\server\api\types\DataObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see LoadDependentObjectResponse
     */
    class LoadDependentObjectRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         */
        public $dependentObjectType;

        /**
         * @var string
         *
         */
        public $dependentObjectGuid;

        /**
         * @var string
         *
         */
        public $superiorObjectType;

        /**
         * @var string
         *
         */
        public $superiorObjectGuid;

        /**
         * @var array
         *
         */
        public $fields;

    }

}
