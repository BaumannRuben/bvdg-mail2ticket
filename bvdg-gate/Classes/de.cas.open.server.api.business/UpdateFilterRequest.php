<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *					\de\cas\open\server\api\types\RequestObject:<br/>
     *					Tells the server to either update a filter or skip the
     *      				current update in case the filters master filter is more current.
     *      				To check first if a filter is out of date see
     *      				CheckFilterUptodatenessRequest.
     *      				<p>
     *						<br/>Corresponding \de\cas\open\server\api\types\ResponseObject: UpdateFilterResponse
     *					</p>
     *					<p>
     *						Possible fault codes explicitly thrown by this operation are:
     *						<ul>
     *							<li>BUSINESS_FILTER_HAS_NO_TEMPLATE_FILTER_SET - In case the filter does not
     *								have a template filter guid set.</li>
     *							<li>BUSINESS_FILTER_TEMPLATE_FILTER_CANNOT_BE_ACCESSED - In case the template
     *								filter cannot be accessed (was deleted or permissions were changed)</li>
     *						</ul>
     *					</p>
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see CheckFilterUptodatenessRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see UpdateFilterResponse
     */
    class UpdateFilterRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										Sets/Gets the GGUID of the filter that should be
         *										updated.
         */
        public $filterGuid;

        /**
         * @var boolean
         *
         *										Sets/Gets the flag that determines if the current
         *										update should be skipped. If this flag evaluates to
         *										true, the filter will be set as up to date but will not
         *										be updated.
         */
        public $skipUpdate;

        /**
         * @var boolean
         *
         *										Sets/Gets the flag that determines if the relation
         *										between the filter and the referenced template filter
         *										should be deleted.
         */
        public $removeTemplateFilterRelation;

    }

}
