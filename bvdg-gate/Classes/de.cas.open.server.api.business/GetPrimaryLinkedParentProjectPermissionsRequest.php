<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        \de\cas\open\server\api\types\RequestObject of the
     *        \de\cas\open\server\business\BusinessOperation to retrieve the
     *        permissions of primary linked parent project.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject:
     *        GetPrimaryLinkedParentProjectPermissionsResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\business\BusinessOperation
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetPrimaryLinkedParentProjectPermissionsResponse
     */
    class GetPrimaryLinkedParentProjectPermissionsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    The DataObjectType of the current DataObject.
         */
        public $dataObjectType;

        /**
         * @var string
         *
         *                    The GGUID of the current DataObject.
         */
        public $dataObjectGuid;

        /**
         * @var string
         *
         *                    The GGUID of the parent project.
         */
        public $parentProjectGuid;

    }

}
