<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\ResponseObject:<br/> Contains the updated filter.
     *			<br/>Corresponding \de\cas\open\server\api\types\RequestObject: UpdateFilterRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see UpdateFilterRequest
     */
    class UpdateFilterResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\DataObjectTransferable
         *
         *										Sets/Gets the updated filter object.
         */
        public $updatedFilter;

    }

}
