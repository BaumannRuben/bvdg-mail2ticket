<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Request object for the business operation that sets default-properties for a
     *				propertyGroup.
     */
    class SetDefaultPropertiesByGroupRequest extends \de\cas\open\server\api\business\SetPropertiesByGroupRequest {

    }

}
