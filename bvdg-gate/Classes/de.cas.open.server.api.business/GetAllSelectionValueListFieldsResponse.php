<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\ResponseObject of the business operation
     *				that returns all existing \de\cas\open\server\api\types\SelectionValueListFieldDescriptions.
     *				Corresponding \de\cas\open\server\api\types\RequestObject: GetAllSelectionValueListFieldsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\SelectionValueListFieldDescription
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetAllSelectionValueListFieldsRequest
     */
    class GetAllSelectionValueListFieldsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         */
        public $selectionValueListFields;

        /**
         * @var string
         *
         *													Returns the dataObjectType the \de\cas\open\server\api\types\SelectionValueListFieldDescription
         *													belongs to.
         *	@see \de\cas\open\server\api\types\SelectionValueListFieldDescription
         */
        public $dataObjectType;

        /**
         * @var \de\cas\open\server\api\types\SelectionValueListFieldDescription
         *
         *													Returns the \de\cas\open\server\api\types\SelectionValueListFieldDescription.
         *	@see \de\cas\open\server\api\types\SelectionValueListFieldDescription
         */
        public $fieldDescription;

    }

}
