<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\ResponseObject The operation returns a boolean true if the unmarking
     *				was successful. Corresponding \de\cas\open\server\api\types\RequestObject: MarkAsPositiveRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see MarkAsPositiveRequest
     */
    class MarkAsPositiveResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var boolean
         *
         *										Shows whether the given gguids were unmarked or not. If
         *										one or both of the gguids where not found, then false
         *										is returned.
         */
        public $marked;

    }

}
