<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject for the business operation that identifies duplicates in
     *				the address database. Corresponding \de\cas\open\server\api\types\ResponseObject: FindDuplicatesResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see FindDuplicatesResponse
     */
    class FindDuplicatesRequest extends \de\cas\open\server\api\types\RequestObject {

    }

}
