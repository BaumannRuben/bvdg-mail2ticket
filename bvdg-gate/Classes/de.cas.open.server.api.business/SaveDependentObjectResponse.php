<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        Saves the given \de\cas\open\server\api\types\DataObject
     *        with the given object type as a dependency of a \de\cas\open\server\api\types\DataObject that is identified by
     *        the given superior object guid resp. the superior object type.
     *        Corresponding \de\cas\open\server\api\types\RequestObject:
     *        SaveDependentObjectRequest
     *	@see \de\cas\open\server\api\types\DataObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see SaveDependentObjectRequest
     */
    class SaveDependentObjectResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\DataObjectTransferable
         *
         */
        public $dependentObject;

    }

}
