<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\ResponseObject: No result. Corresponding \de\cas\open\server\api\types\RequestObject: SubscribePermissionsOwnerRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see SubscribePermissionsOwnerRequest
     */
    class UnsubscribePermissionsOwnerResponse extends \de\cas\open\server\api\types\ResponseObject {

    }

}
