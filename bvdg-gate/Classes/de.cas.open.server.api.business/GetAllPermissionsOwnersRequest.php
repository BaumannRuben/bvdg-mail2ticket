<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Request Object to retrieve a list of permissions
     *				owners (user, resource,
     *				group).
     *        Corresponding \de\cas\open\server\api\types\ResponseObject: GetAllForeignEditPermissionsOfUserResponse
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetAllForeignEditPermissionsOfUserResponse
     */
    class GetAllPermissionsOwnersRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var int
         *
         *										Restrict the result to certain types of permissionsOwners (which may be
         *										users, resources or groups).
         *										@see de.cas.eim.api.constants.PermissionsOwnerFilter for available filter constants.
         *										These can be combined (e.g. PermissionsOwnerFilter#USER | PermissionsOwnerFilter#GROUP to
         *										query for users and groups). Also define if deactivated users should be included or not.
         *										<br/>Valid values are:
         *										<ul>
         *											<li>USER: 1</li>
         *											<li>RESOURCE: 2</li>
         *											<li>GROUP: 4</li>
         *											<li>RETURN_DEACTIVATED: 8</li>
         *											<li>RETURN_HIDDEN: 16</li>
         *											<li>ALL: 7</li>
         *										</ul>
         */
        public $permissionsOwnerFilter;

    }

}
