<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\ResponseObject: Fetches property-groups and their related properties
     *				for the logged-in user. The filter criteria for the property groups is the
     *				given prefix. All property-groups found that start with this prefix are
     *				returned. Corresponding \de\cas\open\server\api\types\RequestObject: GetPropertyGroupsWithPrefixLikeRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetPropertyGroupsWithPrefixLikeRequest
     */
    class GetPropertyGroupsWithPrefixLikeResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *										A \java\util\List that contains all matching
         *										\de\cas\open\server\api\types\EIMPropertyGroupTransferables.
         *	@see \java\util\List
         *	@see \de\cas\open\server\api\types\EIMPropertyGroupTransferable
         */
        public $propertyGroups;

    }

}
