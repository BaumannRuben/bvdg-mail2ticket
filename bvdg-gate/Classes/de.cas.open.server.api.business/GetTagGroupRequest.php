<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject: Returns a tag group by its GGUID. Corresponding \de\cas\open\server\api\types\ResponseObject: GetTagGroupResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetTagGroupResponse
     */
    class GetTagGroupRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *GGUID of the tag group.
         */
        public $groupGUID;

    }

}
