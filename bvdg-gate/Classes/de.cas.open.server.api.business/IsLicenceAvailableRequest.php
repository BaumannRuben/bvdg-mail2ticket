<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\RequestObject of the
     *				\de\cas\open\server\business\BusinessOperation
     *				that checks whether a licence for a given module and user is available.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject:
     *				IsLicenceAvailableResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\business\BusinessOperation
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see IsLicenceAvailableResponse
     */
    class IsLicenceAvailableRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *                    Returns the module's identifier.
         */
        public $module;

        /**
         * @var int
         *
         *                    Returns the user's ID (OID).
         */
        public $userId;

    }

}
