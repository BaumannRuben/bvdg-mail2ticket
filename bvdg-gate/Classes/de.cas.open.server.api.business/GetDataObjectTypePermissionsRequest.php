<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject
     *				of the business operation that returns the DataObjectTypePermissions transitively, i.e. according to the
     *				passed <code>objectType</code>, the permissionOwner
     *				and all his/her super groups.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject: GetDataObjectTypePermissionsResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see DataObjectTypePermission
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetDataObjectTypePermissionsResponse
     */
    class GetDataObjectTypePermissionsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var int
         *
         *										Returns the ID of the accessor.
         */
        public $permissionOwnerId;

        /**
         * @var string
         *
         *										Returns the objecttype.
         */
        public $objectType;

    }

}
