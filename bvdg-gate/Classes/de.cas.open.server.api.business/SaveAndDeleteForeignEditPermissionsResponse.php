<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Empty Response Object. Corresponding \de\cas\open\server\api\types\RequestObject: SaveAndDeleteForeignEditPermissionsRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see SaveAndDeleteForeignEditPermissionsRequest
     */
    class SaveAndDeleteForeignEditPermissionsResponse extends \de\cas\open\server\api\types\ResponseObject {

    }

}
