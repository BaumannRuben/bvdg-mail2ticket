<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\ResponseObject:<br/> Contains currency informatiosn about the client of the currently logged in user.
     *				Corresponding \de\cas\open\server\api\types\RequestObject: GetCurrencyForCurrentUserRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetCurrencyForCurrentUserRequest
     */
    class GetCurrencyForCurrentUserResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\Currency
         *
         *										The currency that is set for the client. May be
         *										null.
         */
        public $currency;

    }

}
