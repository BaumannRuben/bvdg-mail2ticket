<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\ResponseObject:<br/> Saves the given action along with the given
     *				NotificationServiceMessage objects in a transaction.
     *				Corresponding \de\cas\open\server\api\types\RequestObject: SaveActionAndMessageRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see NotificationServiceMessage
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see SaveActionAndMessageRequest
     */
    class SaveActionAndMessagesResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\DataObjectTransferable
         *
         *										Gets/Sets the saved Action Dataobject.
         */
        public $action;

        /**
         * @var array
         *
         *										Gets the reference to the list of saved NotificationServiceMessages.
         *	@see NotificationServiceMessages
         */
        public $messages;

    }

}
