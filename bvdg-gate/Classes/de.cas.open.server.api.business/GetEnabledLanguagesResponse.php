<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\ResponseObject: Carries all languages currently enabled by the system.
     *				Corresponding \de\cas\open\server\api\types\RequestObject: GetEnabledLanguagesRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetEnabledLanguagesRequest
     */
    class GetEnabledLanguagesResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *										A list of available languages in lowercase two-letter
         *										format with their translated display values.
         */
        public $languages;

    }

}
