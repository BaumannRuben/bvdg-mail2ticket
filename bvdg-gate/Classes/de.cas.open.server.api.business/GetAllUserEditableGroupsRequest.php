<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Retrieves a list of TagGroups that are usereditable. Corresponding \de\cas\open\server\api\types\ResponseObject: GetAllUserEditableGroupsResponse
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetAllUserEditableGroupsResponse
     */
    class GetAllUserEditableGroupsRequest extends \de\cas\open\server\api\types\RequestObject {

    }

}
