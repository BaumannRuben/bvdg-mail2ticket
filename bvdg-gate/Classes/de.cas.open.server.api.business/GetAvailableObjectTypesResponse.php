<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\ResponseObject:<br/> Returns all object types that the current user has access to.
     *				Corresponding \de\cas\open\server\api\types\RequestObject:
     *				GetAvailableObjectTypesRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetAvailableObjectTypesRequest
     */
    class GetAvailableObjectTypesResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *                    Gets the table and display names of the available object types.
         */
        public $objectTypes;

    }

}
