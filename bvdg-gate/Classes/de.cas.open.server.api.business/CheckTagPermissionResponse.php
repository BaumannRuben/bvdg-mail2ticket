<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\ResponseObject: Checks a number of data objects for sufficient rights
     *				to add or remove tags. Corresponding \de\cas\open\server\api\types\RequestObject: CheckTagPermissionRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see CheckTagPermissionRequest
     */
    class CheckTagPermissionResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *										A list of data object GGUIDs where the user has
         *										insufficient permissions to add or remove tags.
         */
        public $failureGUIDs;

    }

}
