<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Response Object which contains a list of all
     *				foreign edit permissions granted
     *				by a permission owner. Corresponding
     *				\de\cas\open\server\api\types\RequestObject: GetAllForeignEditPermissionsOfUserRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetAllForeignEditPermissionsOfUserRequest
     */
    class GetAllForeignEditPermissionsOfUserResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *										List of all foreign edit permissions granted by a
         *										permission owner
         */
        public $permissions;

    }

}
