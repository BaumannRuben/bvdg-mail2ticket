<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\ResponseObject: TODO: Purpose of the business logic. Corresponding
     *				\de\cas\open\server\api\types\RequestObject: AddressSoundexCodesInitialiserRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see AddressSoundexCodesInitialiserRequest
     */
    class AddressSoundexCodesInitialiserResponse extends \de\cas\open\server\api\types\ResponseObject {

    }

}
