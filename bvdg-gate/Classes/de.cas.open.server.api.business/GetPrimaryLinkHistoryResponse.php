<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        \de\cas\open\server\api\types\ResponseObject of the
     *        \de\cas\open\server\business\BusinessOperation to retrieve the
     *        primary link history for the given DataObjectType.
     *        Corresponding \de\cas\open\server\api\types\RequestObject:
     *        GetPrimaryLinkHistoryRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\business\BusinessOperation
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetPrimaryLinkHistoryRequest
     */
    class GetPrimaryLinkHistoryResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *                    The primary link history for the given DataObjectType.
         */
        public $linkHistory;

    }

}
