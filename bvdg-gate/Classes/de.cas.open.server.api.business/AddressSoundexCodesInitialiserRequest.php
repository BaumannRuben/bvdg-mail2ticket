<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject: TODO: Purpose of the business logic. Corresponding
     *				\de\cas\open\server\api\types\ResponseObject: AddressSoundexCodesInitialiserResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see AddressSoundexCodesInitialiserResponse
     */
    class AddressSoundexCodesInitialiserRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var array
         *
         *										Accepts the list of fieldnames for which to generate
         *										the soundex codes
         */
        public $fieldNames;

    }

}
