<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *Session information of an user.
     */
    class SessionInfo {

        /**
         * @var string
         *
         *								The client prefix.
         */
        public $clientPrefix;

        /**
         * @var string
         *
         *								The login name of the user.
         */
        public $userName;

        /**
         * @var string
         *
         *								The session id.
         */
        public $sessionId;

        /**
         * @var string
         *
         *								Sets/Returns the IP address of the client. It is
         *								preferred to notify the client session with the same
         *								IP.
         */
        public $userIpAddres;

        /**
         * @var string
         *
         *								The address of the teamCRM server. It is preferred that
         *								this field contains the name (i.e. teamcrm1.cas.de),
         *								but it is also possible to store the IP address..
         */
        public $serverAddress;

        /**
         * @var unknown
         *
         *								Creation time of this session info entry.
         */
        public $insertTimestamp;

        /**
         * @var string
         *
         *								The server generated hashcode of the session id
         */
        public $sessionHash;

    }

}
