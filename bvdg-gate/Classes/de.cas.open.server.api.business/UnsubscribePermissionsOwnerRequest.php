<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject: Unsubscribes permission owners (i.e. users) for an action.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject: SubscribePermissionsOwnerResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see SubscribePermissionsOwnerResponse
     */
    class UnsubscribePermissionsOwnerRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										Unsubscribe the permission owners to this action..
         */
        public $actionGuid;

        /**
         * @var array
         *
         *										Permission owners to unsubscribe.
         */
        public $permissionsOwnerIds;

    }

}
