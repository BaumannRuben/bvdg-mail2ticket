<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject
     *				of the business operation that saves DataObjectTypePermissions.
     *				This operation can only called by administrators.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject: SaveDataObjectTypePermissionsResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see DataObjectTypePermission
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see SaveDataObjectTypePermissionsResponse
     */
    class SaveDataObjectTypePermissionsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var array
         *
         *										Returns the permissions on the objecttype.
         */
        public $permissions;

        /**
         * @var array
         *
         *										Returns the GGUIDs of the permissions to delete.
         */
        public $permissionsToDelete;

    }

}
