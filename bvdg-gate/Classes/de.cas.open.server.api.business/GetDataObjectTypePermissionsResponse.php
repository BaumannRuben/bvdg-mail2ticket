<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\ResponseObject
     *				of the business operation that returns the DataObjectTypePermissions transitively, i.e. according to the
     *				passed <code>serviceID</code>, the permissionOwner
     *				and all his/her super groups.
     *				Corresponding \de\cas\open\server\api\types\RequestObject: GetDataObjectTypePermissionsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see DataObjectTypePermission
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetDataObjectTypePermissionsRequest
     */
    class GetDataObjectTypePermissionsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *										Returns the permissions on the objecttype.
         */
        public $permissions;

    }

}
