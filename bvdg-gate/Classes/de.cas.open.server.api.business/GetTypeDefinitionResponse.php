<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        Gets the xml document that contains the description of additional fields
     *        and the list definitions for the specified datatype.
     *        Corresponding \de\cas\open\server\api\types\RequestObject:
     *        GetTypeDefinitionRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetTypeDefinitionRequest
     */
    class GetTypeDefinitionResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var unknown
         *
         */
        public $typeDefinitionXml;

    }

}
