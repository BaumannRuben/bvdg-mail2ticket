<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject: Requests a resource bundle. Corresponding \de\cas\open\server\api\types\ResponseObject: GetResourceBundleResponse
     *
     *				@see de.cas.eim.api.util.EIMResourceBundleUtil
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetResourceBundleResponse
     */
    class GetResourceBundleRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										Sets/Returns the resource bundles name.
         */
        public $bundleName;

        /**
         * @var string
         *
         *										Sets/Returns the resource bundles type (e.g. CLIENT or
         *										SERVER or ...). See \de\cas\open\server\api\types\ResourceBundleType.
         *	@see \de\cas\open\server\api\types\ResourceBundleType
         */
        public $bundleType;

        /**
         * @var \de\cas\open\server\api\types\EIMLocale
         *
         *										Sets/Returns the locale for which the resource bundle
         *										should be fetched.
         */
        public $locale;

        /**
         * @var boolean
         *
         *										Determines if the fallback mechanism should be used
         *										when fetching the resource bundle. If the fallback is
         *										used and the locale to get the resource bundle for is
         *										like 'de_DE', then the returned resource bundle will
         *										contain all resources found directly linked to 'de_DE'
         *										and all resources that are found linked to 'de' but not
         *										linked to 'de_DE'.
         */
        public $useFallback;

    }

}
