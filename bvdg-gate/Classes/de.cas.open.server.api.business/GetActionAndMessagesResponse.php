<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\ResponseObject:<br/> Returns the requested (GGUID) action and corresponding messages.
     *				Corresponding \de\cas\open\server\api\types\RequestObject: GetActionAndMessagesRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetActionAndMessagesRequest
     */
    class GetActionAndMessagesResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\DataObjectTransferable
         *
         *										Gets/Sets the Action Dataobject.
         */
        public $action;

        /**
         * @var array
         *
         *										Gets the reference to the list of NotificationServiceMessages.
         *	@see NotificationServiceMessages
         */
        public $messages;

    }

}
