<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        @link de.cas.open.server.api.types.ResponseObject} containing the requested list of links. Corresponding \de\cas\open\server\api\types\RequestObject: GetLinksRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetLinksRequest
     */
    class GetLinksResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *										Sets/Returns the list of LinkObjects
         */
        public $links;

    }

}
