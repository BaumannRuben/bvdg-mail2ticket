<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\ResponseObject of the business operation that returns the ForeignEditPermissions
     *				 granted to an accessor for a list of objecttypes. The return value contains:
     *				 <ul>
     *				 <li>Permissions directly granted from a user to the accessor.</li>
     *				 <li>Permissions directly granted from a group to the accessor. These permissions will be
     *				 copied for every member of the granting group.</li>
     *				 <li>Permissions directly granted from a user to a super group of the accessor.</li>
     *				 <li>Permissions directly granted from a group to a super group of the accessor. These
     *				 permissions will be copied for every member of the granting group.</li>
     *				 </ul>
     *				Corresponding \de\cas\open\server\api\types\RequestObject: GetResultingForeignEditPermissionsForAccessorRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see ForeignEditPermission
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetResultingForeignEditPermissionsForAccessorRequest
     */
    class GetResultingForeignEditPermissionsForAccessorResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *										Returns the granted permissions.
         */
        public $permissions;

    }

}
