<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Request object for the business operation that sets a property value by key and
     *				group.
     */
    class SetPropertyRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         */
        public $propertyKey;

        /**
         * @var string
         *
         */
        public $propertyValue;

        /**
         * @var string
         *
         */
        public $propertyGroup;

    }

}
