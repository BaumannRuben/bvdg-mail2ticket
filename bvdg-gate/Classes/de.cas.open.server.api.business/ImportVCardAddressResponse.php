<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\ResponseObject:<br/> TODO [Albina.Pace]: Purpose of the business logic.
     *			<br/>Corresponding \de\cas\open\server\api\types\RequestObject: ImportVCardAddressRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see ImportVCardAddressRequest
     */
    class ImportVCardAddressResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\DataObjectTransferable
         *The created, unsaved, address.
         */
        public $importedAddress;

        /**
         * @var unknown
         *
         *										Sets/Returns the content of the photo.
         */
        public $photo;

        /**
         * @var string
         *
         *										The extension of the photo file.
         */
        public $fileExtension;

    }

}
