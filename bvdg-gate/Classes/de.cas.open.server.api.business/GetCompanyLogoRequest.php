<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject: Gets a company logo. Corresponding \de\cas\open\server\api\types\ResponseObject: GetCompanyLogoResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetCompanyLogoResponse
     */
    class GetCompanyLogoRequest extends \de\cas\open\server\api\types\RequestObject {

    }

}
