<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\ResponseObject of the business operation that returns the ForeignEditPermissions granted by a permission owner or one of its
     *	 				super groups. The members of groups, a ForeignEditPermission is granted to, are
     *	 				added to.<br/>
     *				Corresponding \de\cas\open\server\api\types\RequestObject: GetResultingForeignEditPermissionsForGranterRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see ForeignEditPermission
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetResultingForeignEditPermissionsForGranterRequest
     */
    class GetResultingForeignEditPermissionsForGranterResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *										Returns the granted permissions.
         */
        public $permissions;

    }

}
