<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Request object for the business operation that sets system-properties for a
     *				propertyGroup.
     */
    class SetSystemPropertiesByGroupRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         */
        public $propertyGroup;

        /**
         * @var \de\cas\open\server\api\types\EIMPropertiesTransferable
         *
         */
        public $properties;

    }

}
