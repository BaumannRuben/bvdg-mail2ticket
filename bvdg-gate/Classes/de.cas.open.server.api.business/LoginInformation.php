<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        Contains statistics about the logins of a user from a certain client.
     *        \de\cas\open\server\api\types\RequestObject: GetReadableObjectNamesRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetReadableObjectNamesRequest
     */
    class LoginInformation extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var string
         *
         */
        public $UserGuid;

        /**
         * @var string
         *
         */
        public $ClientName;

        /**
         * @var unknown
         *
         */
        public $LastLoginTimestamp;

        /**
         * @var int
         *
         */
        public $LoginCount;

        /**
         * @var string
         *
         */
        public $ClientVersion;

    }

}
