<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\ResponseObject: Saves a tag group and the contained tags in a
     *				transaction. Corresponding \de\cas\open\server\api\types\RequestObject: SaveTagGroupAndTagsTransactionRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see SaveTagGroupAndTagsTransactionRequest
     */
    class SaveTagGroupAndTagsTransactionResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\InternationalTagGroupExtendedTransferable
         *
         *										Sets/Returns the saved tag group.
         */
        public $internationalTagGroupExtended;

    }

}
