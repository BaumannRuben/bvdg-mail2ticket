<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject: TODO: Purpose of the business logic. No \de\cas\open\server\api\types\ResponseObject.
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     */
    class ThrowExceptionRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										The FQCN of the exception to throw, e.g.
         *										"de.cas.eim.api.exceptions.IllegalArgumentException"
         */
        public $exeptionToThrow;

        /**
         * @var boolean
         *
         *										If this value is set to true, the transaction will be
         *										thrown within a transaction.
         */
        public $throwWithinTransaction;

    }

}
