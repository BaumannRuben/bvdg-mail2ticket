<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\RequestObject of the business operation
     *				that saves and deletes possible \de\cas\open\server\datadefinition\types\SelectionValues of a \de\cas\open\server\api\types\SelectionValueListField.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject:
     *				SavePossibleSelectionValuesResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\datadefinition\types\SelectionValue
     *	@see \de\cas\open\server\api\types\SelectionValueListField
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see SavePossibleSelectionValuesResponse
     */
    class SavePossibleSelectionValuesRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										Returns the dataobjecttype.
         */
        public $dataObjectType;

        /**
         * @var string
         *
         *										Returns the name of the \de\cas\open\server\api\types\SelectionValueListField.
         *	@see \de\cas\open\server\api\types\SelectionValueListField
         */
        public $fieldName;

        /**
         * @var array
         *
         *										Returns the \de\cas\open\server\datadefinition\types\SelectionValues
         *										to
         *										save. May be empty.
         *	@see \de\cas\open\server\datadefinition\types\SelectionValue
         */
        public $valuesToSave;

        /**
         * @var array
         *
         *										Returns the GGUIDs of the \de\cas\open\server\datadefinition\types\SelectionValues to
         *										delete. May be empty.
         *	@see \de\cas\open\server\datadefinition\types\SelectionValue
         */
        public $valuesToDelete;

    }

}
