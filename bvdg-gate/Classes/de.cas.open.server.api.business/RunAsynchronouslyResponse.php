<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\ResponseObject of the business operation that executes other business
     *				operations asynchronously in a separate thread and returns immediately.
     *				Corresponding \de\cas\open\server\api\types\RequestObject: RunAsynchronouslyRequest. The
     *				status of the operation can be retrieved by executing the business operation
     *				GetAsynchronousOperationStatusRequest. If an operation is finished the
     *				status is destroyed.
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see RunAsynchronouslyRequest
     *	@see GetAsynchronousOperationStatusRequest
     */
    class RunAsynchronouslyResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var string
         *
         *										Sets/Returns the GGUID that identifies the asynchronous
         *										operation. This GGUID can be used to retrieve the
         *										operations status, result or failures.
         */
        public $asynchronousOperationGUID;

    }

}
