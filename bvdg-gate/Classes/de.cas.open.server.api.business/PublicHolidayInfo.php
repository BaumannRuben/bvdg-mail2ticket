<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *Class representing a public holiday
     */
    class PublicHolidayInfo {

        /**
         * @var string
         *Sets/Gets the GGUID
         */
        public $GGUID;

        /**
         * @var unknown
         *The date of the public holiday
         */
        public $date;

        /**
         * @var string
         *The public holiday's name
         */
        public $name;

        /**
         * @var string
         *The country of the public holiday
         */
        public $country;

        /**
         * @var string
         *The state of the public holiday
         */
        public $state;

        /**
         * @var string
         *The region of the public holiday
         */
        public $region;

    }

}
