<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\ResponseObject of the business operation
     *				that retrieves all taggroups and corresponding tags for an object type or
     *				all object types, that are used at least once. The result contains all
     * 				used taggroups, all used tags and the number of DataObjects that are
     * 				tagged with this tag. In order to just retrieve all available taggroups
     * 				use the \de\cas\open\server\api\types\DataObjectDescription.
     *				Corresponding \de\cas\open\server\api\types\RequestObject: GetGroupsAndTagsForObjectTypeRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\DataObjectDescription
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetGroupsAndTagsForObjectTypeRequest
     */
    class GetGroupsAndTagsForObjectTypeResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *									List of \de\cas\open\server\api\types\TagGroup objects.
         *	@see \de\cas\open\server\api\types\TagGroup
         */
        public $tagGroups;

    }

}
