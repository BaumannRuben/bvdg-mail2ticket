<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\ResponseObject:<br/> See request object.
     *				Corresponding \de\cas\open\server\api\types\RequestObject: GetUsedCallerIdsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetUsedCallerIdsRequest
     */
    class GetUsedCallerIdsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\MassQueryResult
         *
         *										Used caller IDs with associated users.
         */
        public $usedCallerIdsResult;

    }

}
