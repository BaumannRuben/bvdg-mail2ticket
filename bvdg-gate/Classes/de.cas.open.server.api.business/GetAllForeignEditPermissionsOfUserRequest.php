<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Request Object to retrieve a list of
     *				foreigneditpermissions granted by the
     *				specified permission owner.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject:
     *				GetAllForeignEditPermissionsOfUserResponse
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetAllForeignEditPermissionsOfUserResponse
     */
    class GetAllForeignEditPermissionsOfUserRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var int
         *
         *										Sets/Returns the id of the permission
         *										owner(user,group).
         */
        public $permissionsOwnerId;

    }

}
