<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        Gets the \de\cas\open\server\api\types\DataObjects that
     *        depend from the \de\cas\open\server\api\types\DataObject
     *        which is identified by the given object type and GUID. Corresponding
     *        \de\cas\open\server\api\types\RequestObject: GetDependentObjectsRequest
     *	@see \de\cas\open\server\api\types\DataObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetDependentObjectsRequest
     */
    class GetDependentObjectsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         */
        public $dependentObjects;

    }

}
