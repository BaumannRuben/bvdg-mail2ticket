<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\RequestObject:<br/> provide user ids included to external service.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject:
     *				{@link "GetServiceUsersResponse"}
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     */
    class GetServiceUsersRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										Set service id, that should be provide.
         */
        public $serviceID;

    }

}
