<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\ResponseObject:<br/> See request object for info. Does not return anything.
     *				Corresponding \de\cas\open\server\api\types\RequestObject: DeletePhonePrefixForClientRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see DeletePhonePrefixForClientRequest
     */
    class DeletePhonePrefixForClientResponse extends \de\cas\open\server\api\business\EmptyResponse {

    }

}
