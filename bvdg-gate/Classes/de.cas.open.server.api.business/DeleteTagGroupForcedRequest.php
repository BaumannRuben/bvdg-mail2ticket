<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject: Deletes a tag group and all contained tags, their
     *				relations to data objects and all relations to data object types of the tag
     *				group. Corresponding \de\cas\open\server\api\types\ResponseObject: DeleteTagGroupForcedResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see DeleteTagGroupForcedResponse
     */
    class DeleteTagGroupForcedRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										GGUID of the tag group that shall be deleted.
         */
        public $tagGroupGUID;

    }

}
