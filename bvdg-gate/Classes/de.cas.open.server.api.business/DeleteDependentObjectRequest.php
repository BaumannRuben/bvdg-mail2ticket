<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        Deletes the dependent \de\cas\open\server\api\types\DataObject
     *        that is identified by the given object type and GUID which meets the
     *        specified superior-dependency. Corresponding \de\cas\open\server\api\types\ResponseObject: DeleteDependentObjectResponse
     *	@see \de\cas\open\server\api\types\DataObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see DeleteDependentObjectResponse
     */
    class DeleteDependentObjectRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         */
        public $dependentObjectType;

        /**
         * @var string
         *
         */
        public $dependentObjectGuid;

        /**
         * @var string
         *
         */
        public $superiorObjectType;

        /**
         * @var string
         *
         */
        public $superiorObjectGuid;

    }

}
