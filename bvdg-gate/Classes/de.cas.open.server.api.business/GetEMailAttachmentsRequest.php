<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject: Retrieves attachments for an eMail. Corresponding \de\cas\open\server\api\types\ResponseObject: GetEMailAttachmentsResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetEMailAttachmentsResponse
     */
    class GetEMailAttachmentsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										Sets/Returns the GGUID of the eMail object the
         *										attachments shall be retrieved for.
         */
        public $eMailGGUID;

        /**
         * @var boolean
         *
         *										Flag indicating if inline attachments shall be
         *										retrieved (flag set to 'true').
         */
        public $getInlineAttachments;

        /**
         * @var array
         *
         *										Gets the list in which the indeces of the attachments
         *										to be retrieved can be set.
         */
        public $attachmentIndeces;

    }

}
