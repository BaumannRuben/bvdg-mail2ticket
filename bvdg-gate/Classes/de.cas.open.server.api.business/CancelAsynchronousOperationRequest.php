<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject of the business operation that cancels an asynchronous
     *				business operations started by RunAsynchronouslyRequest. Corresponding
     *				\de\cas\open\server\api\types\ResponseObject: CancelAsynchronousOperationResponse.
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see RunAsynchronouslyRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see CancelAsynchronousOperationResponse
     */
    class CancelAsynchronousOperationRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										Sets/Returns the GGUID that identifies the asynchronous
         *										operation (as returned by RunAsynchronouslyResponse).
         *	@see RunAsynchronouslyResponse
         */
        public $asynchronousOperationGUID;

    }

}
