<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject: The request-object to query the list
     *				of supported charset encodings of the server's Java runtime.
     *	@see \de\cas\open\server\api\types\RequestObject
     */
    class GetSupportedCharsetsRequest extends \de\cas\open\server\api\types\RequestObject {

    }

}
