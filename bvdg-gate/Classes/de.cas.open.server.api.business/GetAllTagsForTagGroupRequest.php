<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject: Returns a list of tags
     *				for the given taggroup id. Corresponding \de\cas\open\server\api\types\RequestObject: GetAllTagsForTagGroupResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetAllTagsForTagGroupResponse
     */
    class GetAllTagsForTagGroupRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         */
        public $tagGroupGGUID;

    }

}
