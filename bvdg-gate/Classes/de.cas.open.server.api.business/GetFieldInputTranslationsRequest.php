<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        Gets the available translations in one language for a field input item.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject:
     *        GetFieldInputTranslationsResponse
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetFieldInputTranslationsResponse
     */
    class GetFieldInputTranslationsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         */
        public $objectType;

        /**
         * @var string
         *
         */
        public $fieldName;

        /**
         * @var string
         *
         */
        public $srcLanguageIsoCode;

        /**
         * @var string
         *
         */
        public $dstLanguageIsoCode;

        /**
         * @var array
         *
         */
        public $srcFieldValue;

    }

}
