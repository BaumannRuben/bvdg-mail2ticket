<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\RequestObject:<br/> Saves the given action along with the given
     *				NotificationServiceMessage objects in a transaction. Corresponding \de\cas\open\server\api\types\ResponseObject:
     *				SaveActionAndMessageResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see NotificationServiceMessage
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see SaveActionAndMessageResponse
     */
    class SaveActionAndMessagesRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var \de\cas\open\server\api\types\DataObjectTransferable
         *
         *										Gets/Sets the Action Dataobjects to be saved.
         */
        public $action;

        /**
         * @var array
         *
         *										Gets the reference to the list of NotificationServiceMessages to be saved.
         */
        public $messages;

    }

}
