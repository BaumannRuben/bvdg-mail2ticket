<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Request object for the business operation that fetches a default-property by
     *				key and group.
     */
    class GetDefaultPropertyRequest extends \de\cas\open\server\api\business\GetPropertyRequest {

    }

}
