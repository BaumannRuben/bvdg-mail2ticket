<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\ResponseObject for the business operation that identifies potentially
     *				missing primary organisation links in the address database.
     *				Corresponding \de\cas\open\server\api\types\RequestObject: FindPotentialPrOrgsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see FindPotentialPrOrgsRequest
     */
    class FindPotentialPrOrgsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *										Returns the list of sets containing at least one contact
         *										and one organisation which is a potential primary organisation.
         *										The organisation gguid is the first item in the list.
         */
        public $companyContactSets;

    }

}
