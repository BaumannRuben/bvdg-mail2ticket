<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Saves a new TagGroup. Corresponding \de\cas\open\server\api\types\RequestObject: SaveTagGroupResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see SaveTagGroupResponse
     */
    class SaveTagGroupRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var \de\cas\open\server\api\types\InternationalTagGroupTransferable
         *
         *									Sets/Returns the tag group to be created.
         */
        public $TagGroup;

    }

}
