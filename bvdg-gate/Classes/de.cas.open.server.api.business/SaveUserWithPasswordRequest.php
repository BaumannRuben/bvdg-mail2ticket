<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Request object of the business operation that
     *				saves a user and his/her
     *				password.
     */
    class SaveUserWithPasswordRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var \de\cas\open\server\api\types\UserWithUserName
         *
         *										Sets/Gets the user to be saved.
         */
        public $userToSave;

        /**
         * @var \de\cas\open\server\api\types\DataObjectTransferable
         *
         *										The address belongs to the user to save. Can be
         *										unsaved.
         */
        public $relatedAddress;

        /**
         * @var string
         *
         *										Sets/Gets the password that should be set for the
         *										user.
         */
        public $password;

    }

}
