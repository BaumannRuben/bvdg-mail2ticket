<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\ResponseObject:<br/> The server saves the client session information
     *      		like IP address and session ID. That can be used to find the teamCRM server which stores one or N client sessions.
     *			<br/>Corresponding \de\cas\open\server\api\types\RequestObject: SFSaveSessionInfoRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see SFSaveSessionInfoRequest
     */
    class SFSaveSessionInfoResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\business\SessionInfo
         *Gets/Sets the saved session info.
         */
        public $sessionInfo;

    }

}
