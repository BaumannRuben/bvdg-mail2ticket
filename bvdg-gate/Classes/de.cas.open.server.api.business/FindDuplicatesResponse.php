<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\ResponseObject for the business operation that identifies duplicates in
     *				the address database. Corresponding \de\cas\open\server\api\types\RequestObject: FindDuplicatesRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see FindDuplicatesRequest
     */
    class FindDuplicatesResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *										Returns the list of duplicate sets of company contacts.
         */
        public $companyDuplicateSets;

        /**
         * @var array
         *
         *										Returns the list of duplicate sets of single contacts.
         */
        public $singleContactDuplicateSets;

    }

}
