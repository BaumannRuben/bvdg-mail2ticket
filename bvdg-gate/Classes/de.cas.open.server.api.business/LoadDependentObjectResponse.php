<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        Loads the dependent \de\cas\open\server\api\types\DataObject
     *        for the given data object type and GUID that meets the specified
     *        superior-dependency. Corresponding \de\cas\open\server\api\types\RequestObject: LoadDependentObjectRequest
     *	@see \de\cas\open\server\api\types\DataObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see LoadDependentObjectRequest
     */
    class LoadDependentObjectResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\DataObjectTransferable
         *
         */
        public $dependentObject;

    }

}
