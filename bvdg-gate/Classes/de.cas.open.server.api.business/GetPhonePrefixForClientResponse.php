<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\ResponseObject of the business operation that returns the client's phone prefix or if none exists assigns a new.
     *				Corresponding \de\cas\open\server\api\types\RequestObject: GetPhonePrefixForClientRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetPhonePrefixForClientRequest
     */
    class GetPhonePrefixForClientResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var string
         *
         *										Sets/Returns the phone prefix assigned to the client.
         *										If the property createNewPhonePrefix is false and the
         *										client does not have a phone prefix, null is returned.
         */
        public $phonePrefix;

    }

}
