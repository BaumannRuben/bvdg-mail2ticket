<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject: Checks if a tag is free of constraints. Corresponding
     *				\de\cas\open\server\api\types\ResponseObject: IsTagConstraintFreeResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see IsTagConstraintFreeResponse
     */
    class IsTagConstraintFreeRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *GGUID of tag to be checked.
         */
        public $tagGUID;

    }

}
