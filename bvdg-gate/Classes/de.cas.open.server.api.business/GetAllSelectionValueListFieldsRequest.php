<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\RequestObject of the business operation
     *				that returns all existing \de\cas\open\server\api\types\SelectionValueListFieldDescriptions.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject:
     *				GetAllSelectionValueListFieldsResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\SelectionValueListFieldDescription
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetAllSelectionValueListFieldsResponse
     */
    class GetAllSelectionValueListFieldsRequest extends \de\cas\open\server\api\types\RequestObject {

    }

}
