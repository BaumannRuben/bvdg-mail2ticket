<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *					A bean containing changes related to a link.
     */
    class LinkChange {

        /**
         * @var string
         *
         *								The GUID of the link that was created/deleted.
         */
        public $linkGuid;

        /**
         * @var string
         *
         *								The type of the change (one of {@link JournalChangeAction#CREATE} or {@link JournalChangeAction#PURGE}.
         */
        public $changeType;

        /**
         * @var string
         *
         *								The GUID of linked record one.
         */
        public $recordGuid1;

        /**
         * @var string
         *
         *								The GUID of linked record two.
         */
        public $recordGuid2;

        /**
         * @var string
         *
         *								The object type of linked record one.
         */
        public $objectType1;

        /**
         * @var string
         *
         *								The object type of linked record two.
         */
        public $objectType2;

        /**
         * @var int
         *
         *                The journal id of this entry
         */
        public $journalId;

    }

}
