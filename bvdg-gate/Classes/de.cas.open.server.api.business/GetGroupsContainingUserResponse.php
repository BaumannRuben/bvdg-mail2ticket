<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Response Object to receive all groups containing the current user.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: GetGroupsContainingUserRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetGroupsContainingUserRequest
     */
    class GetGroupsContainingUserResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *The requested Groups.
         */
        public $grouplist;

    }

}
