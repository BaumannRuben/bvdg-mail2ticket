<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject: Tries to find a system
     *				tag in a tag group by a given value. A set of languages can be passed
     *				optionally
     *				to restrict the search for these languages. If more than one match is
     *				found, the first match is returned. If no match could be found, null
     *				will be returned. Corresponding \de\cas\open\server\api\types\ResponseObject: FindSystemTagByValueResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see FindSystemTagByValueResponse
     */
    class FindSystemTagByValueRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *GGUID of the tag group in which the tag should be searched.
         */
        public $tagGroupGuid;

        /**
         * @var string
         *The value of the system tag that should be searched for.
         */
        public $value;

        /**
         * @var array
         *Optional: A set of language strings (de, en, ...) for which the search should be restricted.
         */
        public $languages;

    }

}
