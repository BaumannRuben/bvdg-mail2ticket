<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\RequestObject:<br/> Merges the two given objects.
     *				The fields must have already been merged, since this requires user interaction. The permissions, user tags
     *				and links are copied into the merged object.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject:
     *				MergeObjectsResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see MergeObjectsResponse
     */
    class MergeObjectsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var \de\cas\open\server\api\types\DataObjectTransferable
         *
         *										Gets/Sets the data object to contain the merged result.
         */
        public $objectToMerge;

        /**
         * @var \de\cas\open\server\api\types\DataObjectTransferable
         *
         *										The data object to copy the links, tags and permissions from, into the objectToMerge, which will be
         *										finally deleted.
         */
        public $objectToDelete;

    }

}
