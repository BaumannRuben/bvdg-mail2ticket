<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject: Subscribes permission owners (i.e. users) for an action.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject: SubscribePermissionsOwnerResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see SubscribePermissionsOwnerResponse
     */
    class SubscribePermissionsOwnerRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										Subscribe the permission owners to this action..
         */
        public $actionGuid;

        /**
         * @var array
         *
         *										Subscribe the permission owners to this action..
         */
        public $permissionsOwnerIds;

    }

}
