<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Request object for the business operation that fetches a system-property by
     *				key.
     */
    class GetSystemPropertyRequest extends \de\cas\open\server\api\business\GetPropertyRequest {

    }

}
