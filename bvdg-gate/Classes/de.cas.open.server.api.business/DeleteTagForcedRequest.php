<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject: Deletes a tag and all its relations to data objects,
     *				i.e. removed the tag from all existing data objects and after that deletes the
     *				tag from the database. Corresponding \de\cas\open\server\api\types\ResponseObject: DeleteTagForcedResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see DeleteTagForcedResponse
     */
    class DeleteTagForcedRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										GGUID of the tag that shall be deleted.
         */
        public $tagGUID;

    }

}
