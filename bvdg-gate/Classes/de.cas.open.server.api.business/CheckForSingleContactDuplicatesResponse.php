<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\ResponseObject: for the business operation that checks if single
     *				contact duplicates exist for the given address. Corresponding \de\cas\open\server\api\types\RequestObject: CheckForSingleContactDuplicatesRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see CheckForSingleContactDuplicatesRequest
     */
    class CheckForSingleContactDuplicatesResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *										Returns the list of GGUIDs of possible single contact
         *										duplicates.
         */
        public $gguidsSingleContactDuplicates;

    }

}
