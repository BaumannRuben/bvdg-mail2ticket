<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Request object for saving and deleting links between records. Corresponding \de\cas\open\server\api\types\ResponseObject: SaveAndDeleteLinksResponse
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see SaveAndDeleteLinksResponse
     */
    class SaveAndDeleteLinksRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var array
         *
         *										Sets/Returns the links to establish.
         */
        public $linksToSave;

        /**
         * @var array
         *
         *										Sets/Returns the gguids of the links to be deleted.
         */
        public $linkGUIDsToDelete;

    }

}
