<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Request object for the business operation that gets default-properties that
     *				belong to a propertyGroup.
     */
    class GetDefaultPropertiesByGroupRequest extends \de\cas\open\server\api\business\GetPropertiesByGroupRequest {

    }

}
