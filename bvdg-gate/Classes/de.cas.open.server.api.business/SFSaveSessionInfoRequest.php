<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\RequestObject:<br/> The server saves the client session information
     *      like IP address and session ID. That can be used to find the teamCRM server which stores one or N client sessions.
     *			<br/>Corresponding \de\cas\open\server\api\types\ResponseObject: SFSaveSessionInfoResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see SFSaveSessionInfoResponse
     */
    class SFSaveSessionInfoRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *Gets/Sets the client prefix.
         */
        public $sessionId;

        /**
         * @var string
         *
         *										Sets/Returns the IP address of the client. It is
         *										preferred to notify the client session with the same
         *										IP.
         */
        public $userIpAddress;

        /**
         * @var string
         *
         *										The address of the teamCRM server. It is preferred that
         *										this field contains the name (i.e. teamcrm1.cas.de),
         *										but it is also possible to store the IP address..
         */
        public $serverAddress;

    }

}
