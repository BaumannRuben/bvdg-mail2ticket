<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject: Returns a list of tags
     *				for the given taggroup id. Corresponding \de\cas\open\server\api\types\RequestObject: GetAllTagsForTagGroupRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetAllTagsForTagGroupRequest
     */
    class GetAllTagsForTagGroupResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *The requested tags.
         */
        public $tags;

    }

}
