<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Request object for the business operation that removes default-properties that
     *				belong to a propertyGroup.
     */
    class RemoveDefaultPropertiesByGroupRequest extends \de\cas\open\server\api\business\RemovePropertiesByGroupRequest {

    }

}
