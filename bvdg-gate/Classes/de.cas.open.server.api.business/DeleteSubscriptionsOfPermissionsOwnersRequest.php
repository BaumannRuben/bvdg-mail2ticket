<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject: Deletes (un)subscriptions for an action.
     *				This means the resulting subscription is derived from group subscriptions.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject: DeleteSubscriptionsOfPermissionsOwnersResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see DeleteSubscriptionsOfPermissionsOwnersResponse
     */
    class DeleteSubscriptionsOfPermissionsOwnersRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										Delete (un)subscriptions for this action..
         */
        public $actionGuid;

        /**
         * @var array
         *
         *										Delete (un)subscriptions this list of users.
         */
        public $permissionsOwnerIds;

    }

}
