<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Request Object to save a user's favourite permissions
     *				owners (user, resource, group).
     *				Only administrators can save favourite permissions
     *				owners for other users. Leave the list empty to remove
     *				all saved favourite permission owners from it.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject: SaveFavouritePermissionsOwnersResponse
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see SaveFavouritePermissionsOwnersResponse
     */
    class SaveFavouritePermissionsOwnersRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var int
         *
         *										The user whose favourite permission owners shall be
         *										saved. Set the parameter to <code>null</code>
         *										if you want to save the list for the current
         *										user. Only administrators can
         *										save the favourite permissions owners for other users.
         */
        public $userId;

        /**
         * @var array
         *
         *										List of favourite permissions owner IDs to save.
         *										Leave it empty to remove all favourite permission
         *										owners from the list.
         */
        public $permissionsOwners;

    }

}
