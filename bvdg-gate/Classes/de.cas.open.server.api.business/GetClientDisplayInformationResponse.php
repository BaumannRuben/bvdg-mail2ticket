<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\ResponseObject:<br/> Contains display information about the client of the currently logged in user.
     *				Corresponding \de\cas\open\server\api\types\RequestObject: GetClientDisplayInformationRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetClientDisplayInformationRequest
     */
    class GetClientDisplayInformationResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var string
         *
         *										The company name that is set for the client. May be
         *										null.
         */
        public $clientCompany;

    }

}
