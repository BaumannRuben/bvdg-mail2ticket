<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        @link de.cas.open.server.api.types.RequestObject} for deleting links between records. Corresponding \de\cas\open\server\api\types\ResponseObject: DeleteLinkResponse
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see DeleteLinkResponse
     */
    class DeleteLinkRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var array
         *
         *										Sets/Returns the GGUIDs of the links to be deleted.
         */
        public $linksToBeDeleted;

    }

}
