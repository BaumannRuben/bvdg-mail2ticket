<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        @link de.cas.open.server.api.types.ResponseObject} containing all links to the GGUID and attribute passed in the
     *        request. Corresponding \de\cas\open\server\api\types\RequestObject: GetAllLinksByGGUIDRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetAllLinksByGGUIDRequest
     */
    class GetAllLinksByGGUIDResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *										Gets the list of \de\cas\open\server\api\types\LinkObject.
         *	@see \de\cas\open\server\api\types\LinkObject
         */
        public $links;

    }

}
