<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\ResponseObject: Contains no specific information. If any error ocurrs,
     *				a Exception is thrown.
     *	@see \de\cas\open\server\api\types\ResponseObject
     */
    class CheckServerStateResponse extends \de\cas\open\server\api\types\ResponseObject {

    }

}
