<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Response object for saving and deleting links between records. Corresponding \de\cas\open\server\api\types\RequestObject: SaveAndDeleteLinksRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see SaveAndDeleteLinksRequest
     */
    class SaveAndDeleteLinksResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\business\SavedAndFailedLinksContainer
         *
         *										Returns the added links and the exceptions for the
         *										links that could not be deleted or could not be saved.
         */
        public $savedAndFailedLinks;

    }

}
