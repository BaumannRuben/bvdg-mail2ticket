<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\ResponseObject:<br/>Please see request object.
     *				Corresponding \de\cas\open\server\api\types\RequestObject: GetFreeCallerIdsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetFreeCallerIdsRequest
     */
    class GetFreeCallerIdsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *Free caller IDs.
         */
        public $callerIds;

    }

}
