<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Request object for the business operation that removes a default-property by
     *				key and group.
     */
    class RemoveDefaultPropertyRequest extends \de\cas\open\server\api\business\RemovePropertyRequest {

    }

}
