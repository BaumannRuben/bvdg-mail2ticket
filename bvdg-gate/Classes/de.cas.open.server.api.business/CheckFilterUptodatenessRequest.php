<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *	      		\de\cas\open\server\api\types\RequestObject:<br/>
     *	      		Checks if a filter-object is up-to-date.
     *				<p>
     *					Corresponding \de\cas\open\server\api\types\ResponseObject: CheckFilterUptodatenessResponse
     *				</p>
     *				<p>
     *					Possible fault codes explicitly thrown by this operation are:
     *					<ul>
     *						<li>BUSINESS_FILTER_HAS_NO_TEMPLATE_FILTER_SET - In case the filter does not
     *							have a template filter guid set.</li>
     *						<li>BUSINESS_FILTER_TEMPLATE_FILTER_CANNOT_BE_ACCESSED - In case the template
     *							filter cannot be accessed (was deleted or permissions were changed)</li>
     *					</ul>
     *				</p>
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see CheckFilterUptodatenessResponse
     */
    class CheckFilterUptodatenessRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										Sets/Gets the GGUID of the filter that should be
         *										checked.
         */
        public $filterGuid;

    }

}
