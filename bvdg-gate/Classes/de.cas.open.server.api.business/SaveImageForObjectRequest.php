<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject: Saves an image for an object. An existing image will be
     *				overwritten. Corresponding \de\cas\open\server\api\types\ResponseObject: SaveImageForObjectResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see SaveImageForObjectResponse
     */
    class SaveImageForObjectRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										Sets/Returns the GGUID of the object the image shall
         *										be saved for.
         */
        public $ObjectGUID;

        /**
         * @var string
         *
         *										Sets/Returns the type of the object the image shall
         *										be saved for.
         */
        public $ObjectType;

        /**
         * @var unknown
         *
         *										Sets/Returns the bytes of the image to be saved.
         */
        public $ImageBytes;

        /**
         * @var string
         *
         *										Sets/Returns the file extension for the image, e.g.
         *										'jpg' or 'png'.
         */
        public $ImageFileExtension;

    }

}
