<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject: Adds tags to multiple objects identified by a list of
     *				GGUIDs. Corresponding \de\cas\open\server\api\types\ResponseObject: TagMultipleObjectsResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see TagMultipleObjectsResponse
     */
    class TagMultipleObjectsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var array
         *
         *										List of Tags (LocalizedTag) that shall be added.
         */
        public $tags;

        /**
         * @var string
         *
         *										Object type of dataobjects to be tagged.
         */
        public $objectType;

        /**
         * @var array
         *
         *										List of GGUIDs of data objects the given tag shall be
         *										added to.
         */
        public $objectGUIDs;

        /**
         * @var unknown
         *
         *										Current DataObjectDescriptionVersion of the client.
         */
        public $dataObjectDescriptionVersion;

    }

}
