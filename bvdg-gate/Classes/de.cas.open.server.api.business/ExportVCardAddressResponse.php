<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\ResponseObject:<br/> Returns a string containing a vcard.
     *				Corresponding \de\cas\open\server\api\types\RequestObject: ExportVCardAddressRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see ExportVCardAddressRequest
     */
    class ExportVCardAddressResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var string
         *The address in vcard format.
         */
        public $vcard;

    }

}
