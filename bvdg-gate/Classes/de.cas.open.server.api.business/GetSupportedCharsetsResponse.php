<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject: The list
     *				of supported charset encodings of the server's Java runtime.
     *	@see \de\cas\open\server\api\types\RequestObject
     */
    class GetSupportedCharsetsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *										The Java canonical names of the supported charset encodings.
         */
        public $SupportedCharsets;

    }

}
