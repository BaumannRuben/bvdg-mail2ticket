<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject: Saves a tag group and the contained tags in a
     *				transaction. Corresponding \de\cas\open\server\api\types\ResponseObject: SaveTagGroupAndTagsTransactionResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see SaveTagGroupAndTagsTransactionResponse
     */
    class SaveTagGroupAndTagsTransactionRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var \de\cas\open\server\api\types\InternationalTagGroupExtendedTransferable
         *
         *										Sets/Returns the tag group to be saved.
         */
        public $internationalTagGroupExtended;

        /**
         * @var \de\cas\open\server\api\types\InternationalTagTransferable
         *
         *										Sets/Returns the default tag of the group
         */
        public $defaultTag;

    }

}
