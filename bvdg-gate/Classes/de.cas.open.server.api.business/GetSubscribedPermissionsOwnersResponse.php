<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\ResponseObject: No result. Corresponding \de\cas\open\server\api\types\RequestObject: GetSubscribedPermissionsOwnersRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetSubscribedPermissionsOwnersRequest
     */
    class GetSubscribedPermissionsOwnersResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *										List of subscribed permission owners IDs.
         */
        public $permissionsOwnerIds;

    }

}
