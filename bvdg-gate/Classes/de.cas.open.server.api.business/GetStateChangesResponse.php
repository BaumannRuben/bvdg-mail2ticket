<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *					See request definition for documentation.
     *					Corresponding \de\cas\open\server\api\types\RequestObject: GetStateChangesRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetStateChangesRequest
     */
    class GetStateChangesResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var int
         *
         *										The journalId of the last entry which was processed.
         *										Should be used for future calls to get only the newest changes.
         */
        public $journalId;

        /**
         * @var unknown
         *
         *										The timestamp of the last entry which was processed.
         *										Can be used for future calls to get only the newest changes. It is advisable to use {@link #journalId} for
         *										accuracy reasons.
         */
        public $timestamp;

        /**
         * @var boolean
         *
         *										In case there were more changes available than fitting in the response, this property will evaluate to true.
         *										In case there are no more changes available, this property will evaluate to false.
         */
        public $moreChangesAvailable;

        /**
         * @var array
         *
         *										Contains the changes on records that were found. May be null or empty.
         */
        public $recordChanges;

        /**
         * @var array
         *
         *										Contains the changes on links that were found. May be null or empty.
         */
        public $linkChanges;

    }

}
