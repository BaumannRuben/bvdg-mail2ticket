<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Request object for the business operation that removes properties contained in
     *				a TransferableEIMProperties-object that belong to a propertyGroup.
     */
    class RemovePropertiesRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         */
        public $propertyGroup;

        /**
         * @var \de\cas\open\server\api\types\EIMPropertiesTransferable
         *
         */
        public $properties;

    }

}
