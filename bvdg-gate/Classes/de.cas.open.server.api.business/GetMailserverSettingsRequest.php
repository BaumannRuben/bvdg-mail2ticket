<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\RequestObject:<br/> Gets the SMTP mailserver settings.
     *				<p>Possible fault codes explicitly thrown by this operation are:
     *					<ul>
     *					<li>NO_EXCEPTION_CODE - no explanation</li>
     *					</ul>
     *				</p>
     *				Corresponding \de\cas\open\server\api\types\ResponseObject: GetMailserverSettingsResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetMailserverSettingsResponse
     */
    class GetMailserverSettingsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *Sets/Gets data object type name.
         */
        public $dataObjectName;

        /**
         * @var int
         *
         *										OID of the user the settings shall be retrieved for. Can be null.
         *										If a value is set, the user that issues this operation must
         *										be an administrator.
         */
        public $userOID;

    }

}
