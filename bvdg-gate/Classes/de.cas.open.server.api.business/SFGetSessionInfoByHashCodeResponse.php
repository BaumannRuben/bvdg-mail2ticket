<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\ResponseObject:<br/>
     *			Returns the session info for the supplied hash code.
     *			<br/>Corresponding \de\cas\open\server\api\types\RequestObject: SFGetSessionInfoByHashCodeRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see SFGetSessionInfoByHashCodeRequest
     */
    class SFGetSessionInfoByHashCodeResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\business\SessionInfo
         *
         *										The session information for the supplied hash code
         */
        public $sessionInfo;

    }

}
