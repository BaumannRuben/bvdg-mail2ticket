<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\ResponseObject: The response contains a boolean describing if the
     *				marking was successful or not. Corresponding \de\cas\open\server\api\types\RequestObject: MarkAsFalsePositiveRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see MarkAsFalsePositiveRequest
     */
    class MarkAsFalsePositiveResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var boolean
         *
         *										Shows whether the given gguids were marked or not. If
         *										one or both of the gguids where not found, then false
         *										is returned.
         */
        public $marked;

    }

}
