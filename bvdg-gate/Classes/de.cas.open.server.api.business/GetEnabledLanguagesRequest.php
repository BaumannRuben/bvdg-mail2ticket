<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject: The request-object to get all languages that are
     *				currently enabled by the system. Corresponding \de\cas\open\server\api\types\ResponseObject: GetEnabledLanguagesResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetEnabledLanguagesResponse
     */
    class GetEnabledLanguagesRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										Optional argument, if executed as authenticated business
         *										operation. Specifies the target language of the
         *										enabled languages.
         */
        public $language;

        /**
         * @var string
         *
         *										Optional argument. Specifies the target country of the
         *										enabled languages.
         */
        public $country;

    }

}
