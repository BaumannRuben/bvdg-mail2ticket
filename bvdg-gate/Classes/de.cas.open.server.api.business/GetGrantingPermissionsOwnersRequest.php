<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject of the business logic that returns all \de\cas\open\server\api\types\PermissionsOwners that grant a certain
     *				foreigneditpermission for the given user. Corresponding \de\cas\open\server\api\types\ResponseObject: GetGrantingPermissionsOwnersResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\PermissionsOwner
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetGrantingPermissionsOwnersResponse
     */
    class GetGrantingPermissionsOwnersRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var int
         *
         *											The permissionOwnerId of the user to whome the permissions are
         *											granted. If <code>null</code> the permissions for the current
         *											user are returned.
         */
        public $permissionOwnerId;

        /**
         * @var int
         *
         *										Sets/Returns the minimum permission the \de\cas\open\server\api\types\PermissionsOwners grant to the
         *										current user.
         *	@see \de\cas\open\server\api\types\PermissionsOwner
         */
        public $requestedPermission;

        /**
         * @var string
         *
         *										Sets/Returns the objecttype for the granted permissions
         *										(can be <code>null</code>).
         */
        public $objectType;

        /**
         * @var int
         *
         *											Restrict the result to certain types of permissionsOwners (which may be
         *											users, resources or groups).
         *											@see de.cas.eim.api.constants.PermissionsOwnerFilter for available filter constants.
         *											These can be combined (e.g. PermissionsOwnerFilter#USER | PermissionsOwnerFilter#GROUP to
         *											query for users and groups). Also define if deactivated users should be included or not.
         *											<br/>Valid values are:
         *											<ul>
         *												<li>USER: 1</li>
         *												<li>RESOURCE: 2</li>
         *												<li>GROUP: 4</li>
         *												<li>RETURN_DEACTIVATED: 8</li>
         *												<li>ALL: 7</li>
         *											</ul>
         */
        public $permissionsOwnerFilter;

    }

}
