<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\ResponseObject:<br/> No Response.
     *				Corresponding \de\cas\open\server\api\types\RequestObject: ChangeCallerPinRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see ChangeCallerPinRequest
     */
    class ChangeCallerPinResponse extends \de\cas\open\server\api\business\EmptyResponse {

    }

}
