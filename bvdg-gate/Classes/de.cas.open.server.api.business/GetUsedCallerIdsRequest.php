<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\RequestObject:<br/>
     *			Returns all used caller IDs.
     *			It contains the columns 'display name', 'caller ID' and 'OID' of user.
     *			Administrator permissions required.
     *			Corresponding \de\cas\open\server\api\types\ResponseObject: GetUsedCallerIdsResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetUsedCallerIdsResponse
     */
    class GetUsedCallerIdsRequest extends \de\cas\open\server\api\types\RequestObject {

    }

}
