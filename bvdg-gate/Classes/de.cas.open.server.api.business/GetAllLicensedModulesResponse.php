<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        Gets all modules that are licensed to the logged in user.
     *        Corresponding \de\cas\open\server\api\types\RequestObject:
     *        GetAllLicensedModulesRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetAllLicensedModulesRequest
     */
    class GetAllLicensedModulesResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *                    Contains the modules that are licensed to the logged user.
         */
        public $licensedModules;

    }

}
