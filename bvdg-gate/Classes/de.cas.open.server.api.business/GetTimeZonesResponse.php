<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Result object which contains all supported TimeZoneContinentInfos (which
     *				contain in sum all supported TimeZones) of the EIM server. Corresponding \de\cas\open\server\api\types\RequestObject: GetTimeZonesRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetTimeZonesRequest
     */
    class GetTimeZonesResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *										List of supported TimeZoneContinentInfos (which contain
         *										in sum all supported TimeZones).
         */
        public $timeZoneContinentInfos;

    }

}
