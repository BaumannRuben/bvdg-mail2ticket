<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject: Fetches property-groups and their related properties for
     *				the logged-in user. the filter criteria for the property groups is the given
     *				prefix. All property-groups found that start with this prefix are returned.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject: GetPropertyGroupsWithPrefixLikeResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetPropertyGroupsWithPrefixLikeResponse
     */
    class GetPropertyGroupsWithPrefixLikeRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										The prefix for which matching property-groups will be
         *										returned.
         */
        public $propertyGroupNamePrefix;

    }

}
