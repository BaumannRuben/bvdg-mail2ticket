<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Creates a Relation between a TagGroup and an ObjectType so that the group is
     *				available for it. Corresponding \de\cas\open\server\api\types\ResponseObject: AddTagGroupToObjectTypeResponse
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see AddTagGroupToObjectTypeResponse
     */
    class AddTagGroupToObjectTypeRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *GGUID of the group.
         */
        public $groupGUID;

        /**
         * @var string
         *
         *									ObjectType the group will be added for.
         */
        public $objectType;

    }

}
