<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Object that represents the fields in the NotificationServiceMessages table, i.e. holds the notification message
     *				itself in a specific language for a specific action.
     */
    class NotificationServiceMessage {

        /**
         * @var string
         *
         *								Sets/Gets the GGUID of this object.
         */
        public $GGUID;

        /**
         * @var string
         *
         *								Sets/Returns the insert user.
         */
        public $InsertUser;

        /**
         * @var string
         *
         *								Sets/Returns the update user.
         */
        public $UpdateUser;

        /**
         * @var unknown
         *
         *								Sets/Returns the insert timestamp.
         */
        public $InsertTimestamp;

        /**
         * @var unknown
         *
         *								Sets/Returns the update timestamp.
         */
        public $UpdateTimestamp;

        /**
         * @var string
         *
         *								Sets/Gets the GGUID of the corresponding action.
         */
        public $ActionGUID;

        /**
         * @var string
         *
         *								Sets/Gets the language of this message.
         */
        public $Language;

        /**
         * @var string
         *
         *								Sets/Gets the message content.
         */
        public $Text;

        /**
         * @var string
         *
         *								Sets/Gets the subject of the message.
         */
        public $Subject;

    }

}
