<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Response Object to rreceive a detailed user object.
     *        Corresponding \de\cas\open\server\api\types\RequestObject: GetUserRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetUserRequest
     */
    class GetUserResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\User
         *The requested user details.
         */
        public $user;

    }

}
