<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        @link de.cas.open.server.api.types.RequestObject}: Retrieves a list of links from the database. Corresponding \de\cas\open\server\api\types\ResponseObject: GetLinksResponse
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetLinksResponse
     */
    class GetLinksRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var \de\cas\open\server\api\types\LinkObject
         *
         *										Gets/Sets the LinkObject that sets the filter
         *										criteria.
         */
        public $LinkObject;

    }

}
