<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\ResponseObject: Deletes a tag and all its relations to data objects,
     *				i.e. removed the tag from all existing data objects and after that deletes the
     *				tag from the database. Corresponding \de\cas\open\server\api\types\RequestObject: DeleteTagForcedRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see DeleteTagForcedRequest
     */
    class DeleteTagForcedResponse extends \de\cas\open\server\api\types\ResponseObject {

    }

}
