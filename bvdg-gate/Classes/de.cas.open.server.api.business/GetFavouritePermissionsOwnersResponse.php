<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Response Object which contains a list of the favourite permissions
     *				owners (user, resource,	group) defined by the current or a certain user.
     *				Corresponding \de\cas\open\server\api\types\RequestObject: GetFavouritePermissionsOwnersRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetFavouritePermissionsOwnersRequest
     */
    class GetFavouritePermissionsOwnersResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *										List of favourite permissions owners.
         */
        public $permissionsOwners;

    }

}
