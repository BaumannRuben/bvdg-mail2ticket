<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\ResponseObject:<br/> Tests the given mail server settings for validity.
     *				Corresponding \de\cas\open\server\api\types\RequestObject: TestMailserverSettingsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see TestMailserverSettingsRequest
     */
    class TestMailserverSettingsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var boolean
         *
         *										Sets/Gets the flag if the test was successful
         */
        public $success;

        /**
         * @var string
         *Sets/Gets the exception code
         */
        public $exception;

        /**
         * @var string
         *
         *										Sets/Gets the exception message
         */
        public $exceptionMessage;

    }

}
