<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Request object of the business operation
     *				that runs
     *				a query. This operation can be used to
     *				run queries asychronously.
     */
    class QueryRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *Sets/Returns the query.
         */
        public $query;

        /**
         * @var unknown
         *
         *										Sets/Returns the first record that
         *										shall be returned, that is the offset
         *										(<strong>zero-based</strong>)
         *										of the first record in the result of the database
         *	            						query. If it is <code>null</code>,
         *	            						the method assumes 0.
         */
        public $firstRecord;

        /**
         * @var unknown
         *
         *										Sets/Returns the number of records that shall be
         *										contained in the result (if the query
         *	            						returns more rows than this).
         */
        public $recordCount;

        /**
         * @var boolean
         *
         *										If true a SELECT statement is cloned and appended
         *										with a UNION so that it also
         *	            						returns private records of other users.
         */
        public $includePrivateForeignRecords;

    }

}
