<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Response object of the business operation
     *				that runs a query. This operation can be used to
     * 				run queries asychronously.
     */
    class QueryResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\MassQueryResult
         *Sets/Returns the result.
         */
        public $savedUser;

    }

}
