<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Request object of the business operation that
     *				saves a group.
     */
    class SaveGroupRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var \de\cas\open\server\api\types\Group
         *
         *										Sets/Gets the group that should be updated /
         *										saved.
         */
        public $group;

    }

}
