<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject: Retrieves the object types a group is available for.
     *				Corresponding \de\cas\open\server\api\types\ResponseObject: GetObjectTypesForTagGroupResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetObjectTypesForTagGroupResponse
     */
    class GetObjectTypesForTagGroupRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										Sets/Returns the tag group GGUID
         */
        public $groupGUID;

    }

}
