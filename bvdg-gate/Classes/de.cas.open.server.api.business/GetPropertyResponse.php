<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Response object for the business operation that fetches a property by key and
     *				group.
     */
    class GetPropertyResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var string
         *
         */
        public $propertyValue;

    }

}
