<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *					\de\cas\open\server\api\types\ResponseObject:<br/> Tells the callee if the filter that was checked is up-to-date
     *					(see {@link CheckFilterUptodatenessResponse#isFilterUpToDate()}).
     *					<br/>Corresponding \de\cas\open\server\api\types\RequestObject: CheckFilterUptodatenessRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see CheckFilterUptodatenessRequest
     */
    class CheckFilterUptodatenessResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var boolean
         *
         *										Sets/Gets the flag indicating if the filter is
         *										up-to-date.
         */
        public $filterUpToDate;

    }

}
