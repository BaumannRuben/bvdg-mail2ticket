<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\ResponseObject: Returns nothing. Corresponding \de\cas\open\server\api\types\RequestObject:
     *				SaveUserPreferencesRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see SaveUserPreferencesRequest
     */
    class SaveUserPreferencesResponse extends \de\cas\open\server\api\types\ResponseObject {

    }

}
