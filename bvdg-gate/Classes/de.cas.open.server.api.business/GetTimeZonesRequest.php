<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				Requests a list of TimeZoneContinentInfo-objects which carry information about
     *				the supported TimeZones per continent. Corresponding \de\cas\open\server\api\types\ResponseObject:
     *				GetTimeZonesResponse
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetTimeZonesResponse
     */
    class GetTimeZonesRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var array
         *
         *										List of continent-names for which
         *										TimeZoneContinentInfos will be returned. If no
         *										continent-names are passed in, all available
         *										TimeZoneContinentInfos will be returned.
         */
        public $continentNames;

    }

}
