<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        Gets the xml document that contains the description of additional fields
     *        and the list definitions for the specified datatype.
     *        Corresponding \de\cas\open\server\api\types\ResponseObject:
     *        GetTypeDefinitionResponse
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetTypeDefinitionResponse
     */
    class GetTypeDefinitionRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         */
        public $dataObjectType;

    }

}
