<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				An e-mail attachment that can be used for SendEmailRequests.
     */
    class Attachment {

        /**
         * @var string
         *
         *								The file name which is equal to the attachment name.
         */
        public $filename;

        /**
         * @var unknown
         *The attachment content.
         */
        public $content;

        /**
         * @var string
         *The content ID (cid).
         */
        public $contentID;

        /**
         * @var string
         *The disposition of the attachment.
         */
        public $disposition;

        /**
         * @var string
         *The content type of the attachment.
         */
        public $contentType;

        /**
         * @var string
         *The encoding of the attachment.
         */
        public $encoding;

        /**
         * @var int
         *The size of the attachment.
         */
        public $size;

    }

}
