<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *		    @link de.cas.open.server.api.types.ResponseObject} for deleting links.
     *		    Corresponding \de\cas\open\server\api\types\RequestObject: DeleteLinkRequest
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see DeleteLinkRequest
     */
    class DeleteLinkResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var array
         *
         *										Returns the failures that occurred while the links
         *										where deleted.
         */
        public $failures;

    }

}
