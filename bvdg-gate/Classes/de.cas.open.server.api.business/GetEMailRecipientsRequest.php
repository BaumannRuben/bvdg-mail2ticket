<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\RequestObject: Returns the E-Mail addresses and names from the TO and
     *				CC fields of an archived E-Mail. Corresponding \de\cas\open\server\api\types\ResponseObject: GetEMailRecipientsResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see GetEMailRecipientsResponse
     */
    class GetEMailRecipientsRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var string
         *
         *										Sets/Returns the GGUID of the EMAILSTORE object whose
         *										recipients shall be returned.
         */
        public $emailGGUID;

    }

}
