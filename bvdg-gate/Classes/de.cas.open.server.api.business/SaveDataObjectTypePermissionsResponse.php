<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *				\de\cas\open\server\api\types\ResponseObject
     *				of the business operation that saves DataObjectTypePermissions.
     *				Corresponding \de\cas\open\server\api\types\RequestObject: SaveDataObjectTypePermissionsRequest
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see DataObjectTypePermission
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see SaveDataObjectTypePermissionsRequest
     */
    class SaveDataObjectTypePermissionsResponse extends \de\cas\open\server\api\business\EmptyResponse {

    }

}
