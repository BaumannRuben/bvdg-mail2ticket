<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *
     *        Gets the last used \de\cas\open\server\api\types\DataObjects
     *        for the given object type and scope in an ordered form (most recent used one at first
     *        position). Corresponding \de\cas\open\server\api\types\RequestObject: GetLastUsedDataObjectsRequest
     *	@see \de\cas\open\server\api\types\DataObject
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see GetLastUsedDataObjectsRequest
     */
    class GetLastUsedDataObjectsResponse extends \de\cas\open\server\api\types\ResponseObject {

        /**
         * @var \de\cas\open\server\api\types\MassQueryResult
         *
         *                    The last used \de\cas\open\server\api\types\DataObjects in an ordered
         *                    form (most recent used one at first position)
         *	@see \de\cas\open\server\api\types\DataObject
         */
        public $dataObjects;

    }

}
