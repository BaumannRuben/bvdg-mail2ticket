<?php
// This file has been automatically generated.

namespace de\cas\open\server\api\business {

    /**
     * @package de\cas\open\server\api
     * @subpackage business
     *\de\cas\open\server\api\types\RequestObject:<br/> Imports a vcard address by parsing the given byte array.
     *			<br/>Corresponding \de\cas\open\server\api\types\ResponseObject: ImportVCardAddressResponse
     *	@see \de\cas\open\server\api\types\RequestObject
     *	@see \de\cas\open\server\api\types\ResponseObject
     *	@see ImportVCardAddressResponse
     */
    class ImportVCardAddressRequest extends \de\cas\open\server\api\types\RequestObject {

        /**
         * @var unknown
         *
         *										Sets/Returns the content of the vcard.
         */
        public $vcardContent;

        /**
         * @var string
         *The charset to use.
         */
        public $charset;

    }

}
