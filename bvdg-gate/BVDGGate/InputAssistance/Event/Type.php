<?php

class HochwarthIT_BVDGGate_InputAssistance_Event_Type{
    const EVENT_TYPE_FACHKONGRESS = "Fachkongress";
    const EVENT_TYPE_AUSBILDUNG = "Ausbildung";

    static function getAllOptions(){
        return array(
            array("value" => 1, "label" => self::EVENT_TYPE_FACHKONGRESS),
            array("value" => 2, "label" => self::EVENT_TYPE_AUSBILDUNG),
        );
    }

}